﻿using Moq;
using MvcContrib.TestHelper;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using Widgets.Controllers;
using Widgets.Helpers;

namespace Widgets.Tests.Controllers
{
    [TestFixture]
    public class ApplicationControllerTests
    {
        private ApplicationController _controller;
        private Mock<ApplicationControllerHelper> _mockHelper;

        [SetUp]
        public void SetUp()
        {
            _mockHelper = new Mock<ApplicationControllerHelper>();
            _mockHelper.Setup(x => x.ReadListingWidgetJavascript()).Returns("");
            _mockHelper.Setup(x => x.GetLastModified()).Returns("123456");

            _controller = new ApplicationController(_mockHelper.Object);
            var builder = new TestControllerBuilder();
            builder.InitializeController(_controller);
        }
       
        [Test]
        public void RenderListings_Get_Should_Render_Listing_Widget_Javascript()
        {
            // act
            var result = _controller.RenderJavascript();

            // assert
            Assert.AreEqual("application/x-javascript", result.ContentType);
            _mockHelper.Verify(x=>x.ReadListingWidgetJavascript(), Times.Once());
        }

        [Test]
        public void RenderListings_Get_Should_Contain_URL_Reference_To_Widget_CSS()
        {
            // arrange
            const string expectedUrl = "/Content/widget.css";

            // act
            var result = _controller.RenderJavascript();

            // assert
            Assert.That(result.Content, new SubstringConstraint(expectedUrl));
        }        
        
        [Test]
        public void RenderSearch_Get_Should_Contain_URL_Reference_To_Listing_Index()
        {
            // arrange
            const string expectedUrl = "/widget/displaylistings/?callback=?&accountkey=";

            // act
            var result = _controller.RenderJavascript();

            // assert
            Assert.That(result.Content, new SubstringConstraint(expectedUrl));
        }         
        
        [Test]
        public void RenderSearch_Get_Should_Contain_URL_Reference_To_Listing_Search()
        {
            // arrange
            const string expectedUrl = "/widget/search/?callback=?&accountkey=";

            // act
            var result = _controller.RenderJavascript();

            // assert
            Assert.That(result.Content, new SubstringConstraint(expectedUrl));
        }

        [Test]
        public void RenderSearch_Get_Should_Contain_URL_Reference_To_Listing_Map_Search()
        {
            // arrange
            const string expectedUrl = "/widget/mapsearch/?callback=?&accountkey=";

            // act
            var result = _controller.RenderJavascript();

            // assert
            Assert.That(result.Content, new SubstringConstraint(expectedUrl));
        }
    }
}
