﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Moq;
using MvcContrib.TestHelper;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    [TestFixture]
    public class ContactsControllerTests : FixtureBase
    {
        private Mock<IAuthProvider> _auth;
        private ContactsController _controller;
        private Guid _expectedAccountID;
        private Account _expectedAccount;

        public ContactsControllerTests()
        {
            Mapper.CreateMap<Contact, ContactsViewModel>();
            Mapper.CreateMap<ContactsViewModel, Contact>();
            Mapper.CreateMap<Agent, AgentInfo>();
            Mapper.CreateMap<Account, AccountViewModel>();
        }

        [SetUp]
        public void SetUp()
        {
            // setup account
            _expectedAccount = TestHelpers.CreateAccount();
            Session.Save(_expectedAccount);
            Session.Flush();

            _expectedAccountID = _expectedAccount.Id;

            // create mocks  
            _auth = new Mock<IAuthProvider>();
            _auth.Setup(x => x.GetAccountKey()).Returns(_expectedAccountID);

            // create controller
            _controller = new ContactsController(_auth.Object, Session);

            // initialize
            var builder = new TestControllerBuilder();
            builder.InitializeController(_controller);
        }

        [Test]
        public void Index_Get_Should_Render_Index_View()
        {
            // arrange
            foreach(var contact in ExpectedContacts())
            {
                Session.Save(contact);
            }
            Session.Flush();

            // act
            var result = _controller.Index(string.Empty);

            // assert
            Assert.AreEqual("Index", result.ViewName);
            Assert.IsInstanceOf<ContactsIndexViewModel>(result.ViewData.Model);
        }

        [Test]
        public void Index_Get_Should_Render_Index_View_With_Success_Message()
        {
            // arrange
            foreach (var contact in ExpectedContacts())
            {
                Session.Save(contact);
            }
            Session.Flush();

            // act
            var result = _controller.Index("This is a success message");

            // assert
            Assert.AreEqual("Index", result.ViewName);
            Assert.IsInstanceOf<ContactsIndexViewModel>(result.ViewData.Model);
            Assert.AreEqual("This is a success message", result.ViewBag.Message);
        }

        [Test]
        public void Index_Post_Should_Update_The_PositionNumber_Of_The_Contacts()
        {
            // arrange
            foreach (var contact in ExpectedContacts())
            {
                Session.Save(contact);
            }
            Session.Flush();

            var expectedContacts = new List<ContactsOrder>
                                       {
                                           new ContactsOrder
                                               {
                                                   id = 2,
                                                   order = 0
                                               },
                                           new ContactsOrder
                                               {
                                                   id = 1,
                                                   order = 1
                                               }
                                       };

            // act
            var result = _controller.Index(expectedContacts);
            var resultData = (IDictionary<string, object>) new System.Web.Routing.RouteValueDictionary(result.Data);

            // assert
            Assert.AreEqual("Contacts order updated", resultData["message"]);
        }


        private IEnumerable<Contact> ExpectedContacts()
        {
            var expectedAgent = TestHelpers.CreateAgent(_expectedAccount);
            Session.Save(expectedAgent);
            Session.Flush();

            var expectedContacts = new List<Contact>
                                       {
                                           new Contact
                                               {
                                                   Account = _expectedAccount,
                                                   Agent = expectedAgent,
                                                   PositionNumber = 1
                                               },
                                           new Contact
                                               {
                                                   Account = _expectedAccount,
                                                   Agent = expectedAgent,
                                                   PositionNumber = 2
                                               }
                                       };
            return expectedContacts;
        }
    }
}
