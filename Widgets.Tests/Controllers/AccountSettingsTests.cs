﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Moq;
using MvcContrib.TestHelper;
using NHibernate.Linq;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    [TestFixture]
    public class AccountSettingsTests : FixtureBase
    {
        private Mock<IAuthProvider> _auth;
        private AccountSettingsController _controller;

        [SetUp]
        public void SetUp()
        {
            _auth = new Mock<IAuthProvider>();

            _controller = new AccountSettingsController(_auth.Object, Session );
        }

        public AccountSettingsTests()
        {
            Mapper.CreateMap<AccountUrlViewModel, AccountUrl>();
            Mapper.CreateMap<AccountUrl, AccountUrlViewModel>();
        }

        [Test]
        public void AuthUrl_Get_Should_Render_Authorized_Urls_View()
        {
            // arrange
            var account = TestHelpers.CreateAccount();
            Session.Save(account);

            _auth.Setup(x => x.GetAccountKey()).Returns(account.Id);

            // expected urls
            Session.Save(new AccountUrl
                             {
                                 Account = account,
                                 Url = "somewebsite.com"
                             });
            Session.Save(new AccountUrl
                             {
                                 Account = account,
                                 Url = "testing.com"
                             });
            Session.Flush();

            // act
            var result = _controller.ManageUrls();

            // assert
            Assert.AreEqual("ManageUrls", result.ViewName);
        }

        [Test]
        public void AddUrl_Get_Should_Render_Add_Url_View()
        {
            // arrange

            // act
            var result = _controller.AddUrl();

            // assert
            Assert.AreEqual("AddUrl", result.ViewName);
        }

        [Test]
        public void AddUrl_Post_Should_Save_Account_Url()
        {

            // arrange
            var expectedAccountUrl = new AccountUrlViewModel
            {
                Url = "somesite.com"
            };

            var account = TestHelpers.CreateAccount();
            Session.Save(account);

            _auth.Setup(x => x.GetAccountKey()).Returns(account.Id);

            var builder = new TestControllerBuilder();
            builder.InitializeController(_controller);

            // act
            var result = _controller.SaveUrl(expectedAccountUrl);

            // assert
            var actual = Session.Query<AccountUrl>().SingleOrDefault(x => x.Url == expectedAccountUrl.Url);
            Assert.IsNotNull(actual);
        }

        [Test]
        public void Edit_Get_Should_Render_Edit_Url_View()
        {
            // arrange
            var expectedAccountUrl = new AccountUrl
            {
                Id = 1,
                Url = "Somesite.com"
            };

            Session.Save(expectedAccountUrl);
            Session.Flush();

            // act
            var result = _controller.EditUrl(1);
            var resultData = (AccountUrlViewModel)result.ViewData.Model;

            // assert
            Assert.AreEqual("EditUrl", result.ViewName);
            Assert.AreEqual(1, resultData.Id);
            Assert.AreEqual("Somesite.com", resultData.Url);
        }

        [Test]
        public void Edit_Post_Should_Save_Modifed_Url()
        {
            // arrange
            var account = TestHelpers.CreateAccount();
            Session.Save(account);

            var expectedAccountUrl = new AccountUrl
            {
                Url = "somesite.com",
                Account = account
            };
            Session.Save(expectedAccountUrl);

            var modifiedAccountUrl = new AccountUrlViewModel()
            {
                Id = expectedAccountUrl.Id,
                Url = "modifiedsite.com"
            };

            var builder = new TestControllerBuilder();
            builder.InitializeController(_controller);

            var postedForm = new FormCollection
                                 {
                                     {"Id", expectedAccountUrl.Id.ToString()},
                                     {"Url", modifiedAccountUrl.Url}
                                 };

            // act
            _controller.ValueProvider = postedForm.ToValueProvider();
            var result = _controller.SaveUrl(modifiedAccountUrl);

            // assert
            var actual = Session.Get<AccountUrl>(expectedAccountUrl.Id);
            Assert.IsNotNull(actual);
            Assert.AreEqual(modifiedAccountUrl.Url, actual.Url);
        }
    }
}
