﻿using System;
using System.Web.Mvc;
using AutoMapper;
using Moq;
using MvcContrib.TestHelper;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    [TestFixture]
    public class DisclaimersControllerTests : FixtureBase
    {
        private DisclaimersController _controller;
        private Mock<IAuthProvider> _auth;
        private Account _expectedAccount;
        private Guid _expectedAccountID;

        public DisclaimersControllerTests()
        {
            Mapper.CreateMap<Disclaimer, DisclaimerViewModel>();
            Mapper.CreateMap<DisclaimerViewModel, Disclaimer>();
        }

        [SetUp]
        public void SetUp()
        {
            // setup account
            _expectedAccount = TestHelpers.CreateAccount();
            Session.Save(_expectedAccount);
            Session.Flush();

            _expectedAccountID = _expectedAccount.Id;

            // create mocks  
            _auth = new Mock<IAuthProvider>();
            _auth.Setup(x => x.GetAccountKey()).Returns(_expectedAccountID);

            _controller = new DisclaimersController(_auth.Object, Session);
        }

        [Test]
        public void ManageDisclaimers_Get_Should_Render_ManageDisclaimers_View()
        {
            // arrange

            // act
            var result = _controller.ManageDisclaimers();

            // assert
            Assert.AreEqual("ManageDisclaimers", result.ViewName);
        }

        [Test]
        public void Create_Get_Should_Render_Create_View()
        {
            // arrange

            // act
            var result = _controller.Create();

            // assert
            Assert.AreEqual("Create", result.ViewName);
        }

        [Test]
        public void Create_Post_Should_Save_Disclaimer()
        {
            // arrange
            var model = GetDisclaimerViewModel();

            // act
            var result = _controller.Create(model) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("ManageDisclaimers", result.RouteValues["action"]);
        }

        [Test]
        public void Create_Post_Should_Render_Create_View_If_ModelState_Not_Valid()
        {            
            
            // arrange
            var model = GetDisclaimerViewModel();
            model.BoardCode = null;

            _controller.ModelState.AddModelError("BoardCode", "Required");

            // act
            var result = _controller.Create(model) as ViewResult;

            // assert
            Assert.AreEqual("Create", result.ViewName);
        }

        [Test]
        public void Edit_Get_Should_Render_Edit_View()
        {
            // arrange
            var expectedDisclaimer = GetExpectedDisclaimer();
            Session.Save(expectedDisclaimer);
            Session.Flush();

            // act
            var result = _controller.Edit(expectedDisclaimer.ID) as ViewResult;

            // assert
            Assert.AreEqual("Edit", result.ViewName);
        }

        [Test]
        public void Edit_Post_Should_Save_Changes_To_Disclaimer()
        {
            // arrange
            var expectedDisclaimer = GetExpectedDisclaimer();
            Session.Save(expectedDisclaimer);
            Session.Flush();

            var model = GetDisclaimerViewModel();
            model.ID = expectedDisclaimer.ID;
            model.BoardCode = "Modified";

            var form = new FormCollection
                           {
                               {"BoardCode", model.BoardCode},
                               {"MLSListing", model.MLSListing},
                               {"MLSListingDetails", model.MLSListingDetails},
                               {"ExclusiveListing", model.ExclusiveListing },
                               {"ExclusiveListingDetails", model.ExclusiveListingDetails}
                           };

            var builder = new TestControllerBuilder();
            builder.InitializeController(_controller);
            _controller.ValueProvider = form.ToValueProvider();
  
            // act
            var result = _controller.Edit(model) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("ManageDisclaimers", result.RouteValues["action"]);
        }

        [Test]
        public void Delete_Get_Should_Delete_Disclaimer()
        {
            // arrange
            var expectedDisclaimer = GetExpectedDisclaimer();
            Session.Save(expectedDisclaimer);
            Session.Flush();

            // act
            var result =  _controller.Delete(expectedDisclaimer.ID) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("ManageDisclaimers", result.RouteValues["action"]);
        }

        private static DisclaimerViewModel GetDisclaimerViewModel()
        {
            var model = new DisclaimerViewModel
                            {
                                BoardCode = "Saskatoon",
                                MLSListing = "MLS Listing Disclaimer",
                                MLSListingDetails = "MLS Listing Details Disclaimer",
                                ExclusiveListing = "Exclusive Listing Disclaimer",
                                ExclusiveListingDetails = "Exclusive Listing Details Disclaimer"
                            };
            return model;
        }

        private static Disclaimer GetExpectedDisclaimer()
        {
            var expectedDisclaimer = new Disclaimer
            {
                BoardCode = "Saskatoon",
                MLSListing = "MLS Listing Disclaimer",
                MLSListingDetails = "MLS Listing Details Disclaimer",
                ExclusiveListing = "Exclusive Listing Disclaimer",
                ExclusiveListingDetails = "Exclusive Listing Details Disclaimer"
            };

            return expectedDisclaimer;
        }
    }
}
