﻿using System;
using System.Web.Mvc;
using AutoMapper;
using Moq;
using MvcContrib.TestHelper;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    public class BulletinBoardControllerTests : FixtureBase
    {
        private Mock<IAuthProvider> _auth;
        private BulletinBoardController _controller;

        private Account _expectedAccount;

        public BulletinBoardControllerTests()
        {
            Mapper.CreateMap<BulletinBoardViewModel, BulletinBoard>();
            Mapper.CreateMap<BulletinBoard, BulletinBoardViewModel>();
        }

        [SetUp]
        public void SetUp()
        {
            // setup account
            _expectedAccount = TestHelpers.CreateAccount();
            Session.Save(_expectedAccount);
            Session.Flush();

            // create mocks
            _auth = new Mock<IAuthProvider>();
            _auth.Setup(x => x.GetAccountKey()).Returns(_expectedAccount.Id);

            // create controller
            _controller = new BulletinBoardController(_auth.Object, Session);
        }

        [Test]
        public void DisplayBulletin_Get_should_render_display_view()
        {
            // arrange
            Session.Save(
                new BulletinBoard
                    {
                        Account = _expectedAccount,
                        Agent = TestHelpers.CreateAgent(_expectedAccount),
                        Description = "Some Message",
                        IsNews = false,
                        StartDate = DateTime.Now,
                        Title = "title"
                    }
                );


            // act
            var result = _controller.DisplayBulletin() as ViewResult;

            // assert
            Assert.AreEqual("DisplayBulletin", result.ViewName);
            Assert.IsInstanceOf<BulletinBoardIndexViewModel>(result.ViewData.Model);
        }

        [Test]
        public void Index_Get_should_render_index_view()
        {
            // arrange
            Session.Save(
                new BulletinBoard
                    {
                        Account = _expectedAccount,
                        Agent = TestHelpers.CreateAgent(_expectedAccount),
                        Description = "Some Message",
                        IsNews = false,
                        StartDate = DateTime.Now,
                        Title = "title"
                    }
                );

            // act
            var result = _controller.Index() as ViewResult;

            // assert
            Assert.AreEqual("Index", result.ViewName);
            Assert.IsInstanceOf<BulletinBoardIndexViewModel>(result.ViewData.Model);
        }

        [Test]
        public void Create_Get_should_render_create_view()
        {
            // arrange

            // act
            var result = _controller.Create();

            // assert
            Assert.AreEqual("Create", result.ViewName);
            Assert.IsInstanceOf<BulletinBoardViewModel>(result.ViewData.Model);
            Assert.IsFalse(((BulletinBoardViewModel)result.ViewData.Model).IsNews);
        }

        [Test]
        public void Create_Post_Should_Create_New_Bulletin_Board()
        {
            // arrange
            var expectedBulletinBoard = new BulletinBoardViewModel
                                            {
                                                Title = "some title",
                                                Description = "some description",
                                                StartDate = DateTime.Now
                                            };

            var expectedAgent = TestHelpers.CreateAgent(_expectedAccount);
            Session.Save(expectedAgent);

            var mock = new Mock<ControllerContext>();
            mock.SetupGet(p => p.HttpContext.User.Identity.Name).Returns(expectedAgent.UserName);
            _controller.ControllerContext = mock.Object;

            // act
            var result = _controller.Create(expectedBulletinBoard) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
        }

        [Test]
        public void Edit_Get_should_render_edit_view()
        {
            // arrange
            var expectedBulletin = ExpectedBulletin();
            Session.Save(expectedBulletin);

            // act
            var result = _controller.Edit(1);

            // assert
            Assert.AreEqual("Edit", result.ViewName);
            Assert.IsInstanceOf<BulletinBoardViewModel>(result.ViewData.Model);
        }

        [Test]
        public void Edit_Post_Should_Save_Changes_to_bulletin()
        {
            // arrange
            var expectedBulletin = ExpectedBulletin();
            Session.Save(expectedBulletin);
            Session.Flush();

            var editedBulletin = new BulletinBoardViewModel
                                      {
                                          Id = expectedBulletin.Id,
                                          Title = "edited title",
                                          Description = "edited description",
                                          StartDate = expectedBulletin.StartDate
                                      };

            var form = new FormCollection
                           {
                               {"Id", expectedBulletin.Id.ToString()},
                               {"Title", editedBulletin.Title},
                               {"Description", editedBulletin.Description},
                               {"StartDate", editedBulletin.StartDate.ToString()}
                           };

            // init controller
            var builder = new TestControllerBuilder();
            builder.InitializeController(_controller);

            // act
            _controller.ValueProvider = form.ToValueProvider();
            var result = _controller.Edit(editedBulletin) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
        }

        [Test]
        public void Delete_Should_Delete_The_Specified_Bulletin_board_message()
        {
            // arrange
            var expectedBulletin = ExpectedBulletin();
            Session.Save(expectedBulletin);

            // act
            var result = _controller.Delete(expectedBulletin.Id) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("BulletinBoard", result.RouteValues["controller"]);
        }

        [Test]
        public void DisplaySingleBulletin_should_render_partial_view_for_the_specified_bulletin_board_message()
        {
            // arrange
            var expectedBulletin = ExpectedBulletin();
            Session.Save(expectedBulletin);

            // act
            var result = _controller.DisplaySingleBulletin(expectedBulletin.Id);

            // assert
            Assert.AreEqual("DisplaySingleBulletin", result.ViewName);
        }

        private BulletinBoard ExpectedBulletin()
        {
            var expectedAgent = TestHelpers.CreateAgent(_expectedAccount);
            Session.Save(expectedAgent);
            Session.Flush();

            return new BulletinBoard
                       {
                           Title = "some title",
                           Description = "some description",
                           StartDate = DateTime.Now,
                           Account = _expectedAccount,
                           Agent = expectedAgent,
                           IsNews = false
                       };
        }
    }
}
