﻿using System.Web.Mvc;
using AutoMapper;
using Moq;
using MvcContrib.TestHelper;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    [TestFixture]
    public class SiteFeaturesControllerTests : FixtureBase
    {
        private Mock<IAuthProvider> _auth;
        private SiteFeaturesController _controller;
        private Account _expectedAccount;

        public SiteFeaturesControllerTests()
        {
            Mapper.CreateMap<AccountFeatures, AccountFeaturesViewModel>();
            Mapper.CreateMap<AccountFeaturesViewModel, AccountFeatures>();
        }

        [SetUp]
        public void SetUp()
        {
            _expectedAccount = TestHelpers.CreateAccount();
            Session.Save(_expectedAccount);
            Session.Flush();

            // setup mocks
            _auth = new Mock<IAuthProvider>();
            _auth.Setup(x => x.GetAccountKey()).Returns(_expectedAccount.Id);

            _controller = new SiteFeaturesController(_auth.Object, Session);
        }

        [Test]
        public void ManageFeatures_Get_Should_Render_View()
        {
            // arrange
            Session.Save(
                new AccountFeatures
                    {
                        FeaturedListing = false,
                        Listing = true,
                        Map = true,
                        Search = true,
                        Account = _expectedAccount
                    }
                );

            // act
            var result = _controller.ManageFeatures();

            // assert
            Assert.AreEqual("ManageFeatures", result.ViewName);
            Assert.IsInstanceOf<AccountFeaturesViewModel>(result.ViewData.Model);
        }

        [Test]
        public void ManageFeatures_Post_Should_Create_Features()
        {
            // arrange
            var expectedViewModel = new AccountFeaturesViewModel()
            {
                Id = 0,
                FeaturedListing = false,
                Listing = true,
                Map = true,
                Search = true
            };

            // act
            var result = _controller.ManageFeatures(expectedViewModel) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Home", result.RouteValues["controller"]);
        }


        [Test]
        public void ManageFeatures_Post_Should_Edit_Features()
        {
            // arrange
            var expectedViewModel = new AccountFeaturesViewModel
            {
                Id = 1,
                FeaturedListing = false,
                Listing = true,
                Map = true,
                Search = true
            };

            Session.Save(
                new AccountFeatures()
                    {
                        FeaturedListing = false,
                        Listing = true,
                        Map = true,
                        Search = true
                    }
                );

            var builder = new TestControllerBuilder();
            builder.InitializeController(_controller);

            var form = new FormCollection()
                           {
                               {"Id", expectedViewModel.Id.ToString()},
                               {"FeaturedListing", expectedViewModel.FeaturedListing.ToString()},
                               {"Listing", expectedViewModel.Listing.ToString()},
                               {"Map", expectedViewModel.Map.ToString()},
                               {"Search", expectedViewModel.Search.ToString()}
                           };

            // act
            _controller.ValueProvider = form.ToValueProvider();
            var result = _controller.ManageFeatures(expectedViewModel) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Home", result.RouteValues["controller"]);
        }
    }
}
