﻿using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Moq;
using NHibernate.Linq;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    [TestFixture]
    public class FileManagementControllerTests : FixtureBase
    {
        private Mock<IAuthProvider> _auth;
        private DocumentsController _controller;
        private Account _account;

        public FileManagementControllerTests()
        {
            Mapper.CreateMap<AccountFile, AccountFileViewModel>();
            Mapper.CreateMap<AccountFileViewModel, AccountFile>();
        }

        [SetUp]
        public void SetUp()
        {
            _auth = new Mock<IAuthProvider>();
            _controller = new DocumentsController(_auth.Object, Session);

            // create account for tests
            _account = TestHelpers.CreateAccount();
            Session.Save(_account);
            Session.Flush();

            _auth.Setup(x => x.GetAccountKey()).Returns(_account.Id);
        }

        [Test]
        public void Index_Get_Should_List_Files_For_Account()
        {
            // arrange
            /*var accountFile = new AccountFile
                                   {
                                       Account = _account,
                                       FileName = "SomeFile",
                                       FileContent = new byte[1],
                                       MimeType = "na"
                                   };
            Session.Save(accountFile);
            Session.Flush();
            
            // act
            var result = _controller.Index("");

            // assert
            Assert.AreEqual("Index", result.ViewName);
            Assert.IsInstanceOf<AccountFilesIndexViewModel>(result.ViewData.Model);
            Assert.AreEqual(1, ((AccountFilesIndexViewModel) result.ViewData.Model).AccountFiles.Count());*/
        }

        [Test]
        public void Create_Get_Should_Render_Create_View()
        {
            // arrange
            
            // act
            var result = _controller.Create();

            // assert
            Assert.AreEqual("Create", result.ViewName);
        }

        [Test]
        public void Create_Post_Should_Upload_New_File()
        {
            // arrange
            
            // setup mock for posted file
            var mockFile = new Mock<HttpPostedFileBase>();
            mockFile.Setup(x => x.ContentLength).Returns(1);
            mockFile.Setup(x => x.FileName).Returns("Some File");
            mockFile.Setup(x => x.ContentType).Returns("application/pdf");
            mockFile.Setup(x => x.InputStream).Returns(new MemoryStream(new byte[1]));

            // act
            var result = _controller.Create(mockFile.Object) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);

            var files = Session.Query<AccountFile>();
            Assert.AreEqual(1, files.Count());
        }
        
        [Test]
        public void Details_Get_Should_Return_File()
        {
            // arrange
            /*var file = new AccountFile
                           {
                               Account = _account,
                               FileContent = new byte[1],
                               FileName = "test",
                               MimeType = "text"
                           };

            Session.Save(file);
            Session.Flush();

            // act
            var result = _controller.Details(file.Id) as FileContentResult;

            // assert
            Assert.AreEqual(file.FileContent, result.FileContents);*/
        }

        [Test]
        public void Delete_Get_Should_Delete_File_And_Redirect_To_Index()
        {
            // arrange
            /*var file = new AccountFile
                           {
                Account = _account,
                FileContent = new byte[1],
                FileName = "test",
                MimeType = "text"
            };

            Session.Save(file);
            Session.Flush();

            // act
            var result = _controller.Delete(file.Id) as RedirectToRouteResult;

            // assert
            var files = Session.Query<AccountFile>();

            Assert.AreEqual(0, files.Count());
            Assert.AreEqual("Index", result.RouteValues["action"]);*/
        }
    }
}
