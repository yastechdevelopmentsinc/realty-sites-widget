﻿using System;
using System.Web;
using System.Web.Mvc;
using Moq;
using MvcContrib.TestHelper;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    [TestFixture]
    public class AuthControllerTests : FixtureBase
    {
        private AuthController _controller;
        private Mock<IAuthProvider> _auth;
        private Mock<IEmailProcessor> _email;
        private readonly TestControllerBuilder _builder = new TestControllerBuilder();

        [SetUp]
        public void SetUp()
        {
            _auth = new Mock<IAuthProvider>();
            _email = new Mock<IEmailProcessor>();

            _controller = new AuthController(Session, _auth.Object, _email.Object);
            _builder.InitializeController(_controller);
        }

        [Test]
        public void SignIn_Get_Should_Render_Sign_In_View()
        {
            // arrange

            // act
            var result = _controller.SignIn();

            // assert
            Assert.AreEqual("SignIn", result.ViewName);
        }

        [Test]
        public void SignIn_Post_Should_Authenticate_User_With_Valid_Credentials()
        {
            // arrange
            var expectedAccount = TestHelpers.CreateAccount();
            Session.Save(expectedAccount);

            var expectedAgent = TestHelpers.CreateAgent(expectedAccount);
            Session.Save(expectedAgent);
            Session.Flush();

            var expectedModel = new SignInViewModel { UserName =expectedAgent.UserName, Password = expectedAgent.Password };

            _auth.Setup(x => x.GetAccountKey()).Returns(expectedAccount.Id);

            // act
            var result = _controller.SignIn(expectedModel, null) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Home", result.RouteValues["controller"]);
            _auth.Verify(provider => provider.SetAuthCookie(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Guid>(), It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void SignIn_Post_Should_Return_Sign_View_With_Invalid_Credentials()
        {
            // arrange
            var expectedAccount = TestHelpers.CreateAccount();
            Session.Save(expectedAccount);

            var expectedAgent = TestHelpers.CreateAgent(expectedAccount);
            Session.Save(expectedAgent);
            Session.Flush();

            var expectedModel = new SignInViewModel { UserName = expectedAgent.UserName, Password = "badpassword" };

            _auth.Setup(x => x.GetAccountKey()).Returns(expectedAccount.Id);

            // act
            var result = _controller.SignIn(expectedModel, null) as ViewResult;

            // assert
            Assert.AreEqual("SignIn", result.ViewName);
            _auth.Verify(provider => provider.SetAuthCookie(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Guid>(), It.IsAny<string>()), Times.Never());
            Assert.False(result.ViewData.ModelState.IsValid);
        }

        [Test]
        public void SignIn_Post_Should_Redirect_To_Return_Url_After_Authentication()
        {
            // arrange
            const string expectedRedirectUrl = "/Account";
            var expectedAccount = TestHelpers.CreateAccount();
            Session.Save(expectedAccount);

            var expectedAgent = TestHelpers.CreateAgent(expectedAccount);
            Session.Save(expectedAgent);
            Session.Flush();

            var expectedModel = new SignInViewModel { UserName = expectedAgent.UserName, Password = expectedAgent.Password };

            _auth.Setup(x => x.GetAccountKey()).Returns(expectedAccount.Id);
            
            // act
            var result = _controller.SignIn(expectedModel, expectedRedirectUrl) as RedirectResult;

            // assert
            Assert.AreEqual(expectedRedirectUrl, result.Url);
            _auth.Verify(provider => provider.SetAuthCookie(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Guid>(), It.IsAny<string>()), Times.Once());
        }
    }
}
