﻿using System.Web.Script.Serialization;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    [TestFixture]
    public class WidgetControllerTests : FixtureBase
    {
        //private Mock<IRepository<Location>> _repository;
        //private static readonly string MvcAppPath = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + "\\..\\..\\..\\Widgets");
        //private readonly AppHost _appHost = new AppHost(MvcAppPath);

        [Test]
        public void Populate_Locations_Should_Return_Distinct_List_Of_Locations_Based_Upon_City()
        {
            // arrange
            const string city = "Saskatoon";

            foreach(var loc in TestHelpers.GenerateFakeLocations())
            {
                Session.Save(loc);
            }
            Session.Flush();

            var controller = new WidgetController(null, Session);

            // act
            var result = controller.PopulateLocations(city,null,System.Guid.NewGuid());
            var serializer = new JavaScriptSerializer();
            var output = serializer.Serialize(result.Data);

            // assert
            Assert.AreEqual(@"{""filteredLocations"":[{""AreaName"":""All"",""Listings"":0},{""AreaName"":""Arbor Creek"",""Listings"":0},{""AreaName"":""Briarwood"",""Listings"":0}]}", output);
        }

        //[Test, Category("Integration")]
        //public void DisplayListings_Get_Should_Return_Listings_As_JsonpResult()
        //{
        //    _appHost.SimulateBrowsingSession(browsingSession =>
        //    {
        //        // Request the root URL
        //        RequestResult result = browsingSession.ProcessRequest("listing/displaylistings?callback=expected&accountkey=C2B7BE3F-8EA1-4E5D-B504-AF1D1FB16138");
        //        Assert.AreEqual("application/json", result.Response.ContentType);
        //    });
        //}

        //[Test, Category("Integration")]
        //public void Search_Get_Should_Return_SearchForm_As_JsonpResult()
        //{
        //    _appHost.SimulateBrowsingSession(browsingSession =>
        //    {
        //        // Request the root URL
        //        RequestResult result = browsingSession.ProcessRequest("listing/search?callback=expected&accountkey=C2B7BE3F-8EA1-4E5D-B504-AF1D1FB16138");
        //        Assert.AreEqual("application/json", result.Response.ContentType);
        //    });
        //}

        //[Test, Category("Integration")]
        //public void SearchListings_Get_Should_Return_Return_Filtered_Listings_As_JsonpResult_When_Widget_Type_Is_listingsearch()
        //{
        //    _appHost.SimulateBrowsingSession(browsingSession =>
        //    {
        //        // Request the root URL
        //        RequestResult result = browsingSession.ProcessRequest("/listing/SearchListings/?callback=jQuery1710398601396009326_1329519559737&WidgetType=_listingsearch&PriceLow=-1&PriceHigh=-1&Bathrooms=0&Bedrooms=0&City=-1&AreaName=-1&Residential=true&Residential=false&ForLease=false&Commercial=false&Acreage=false&FarmsAndLand=false&Condominium=false&_=1329519561951");
        //        Assert.AreEqual("application/json", result.Response.ContentType);
        //    });
        //}

        //[Test, Category("Integration")]
        //public void SearchListings_Get_Should_Return_RefreshMap_As_JsonpResult_When_Widget_Type_Is_listingmapsearch()
        //{
        //    _appHost.SimulateBrowsingSession(browsingSession =>
        //    {
        //        // Request the root URL
        //        RequestResult result = browsingSession.ProcessRequest("/listing/SearchListings/?callback=jQuery1710398601396009326_1329519559737&WidgetType=_listingmapsearch&PriceLow=-1&PriceHigh=-1&Bathrooms=0&Bedrooms=0&City=-1&AreaName=-1&Residential=true&Residential=false&ForLease=false&Commercial=false&Acreage=false&FarmsAndLand=false&Condominium=false&_=1329519561951");
        //        Assert.AreEqual("application/json", result.Response.ContentType);
        //    });
        //}

        //[Test, Category("Integration")]
        //public void DetailedListing_Return_Detailed_View_Of_Listing_As_JsonpResult()
        //{
        //    _appHost.SimulateBrowsingSession(browsingSession =>
        //    {
        //        // Request the root URL
        //        RequestResult result = browsingSession.ProcessRequest("listing/detailedlisting?property=residential&mlsnumber=12345");
        //        Assert.AreEqual("application/json", result.Response.ContentType);
        //    });
        //}
    }
}
