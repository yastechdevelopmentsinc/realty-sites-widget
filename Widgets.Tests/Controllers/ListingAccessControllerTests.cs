﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AutoMapper;
using Moq;
using MvcContrib.TestHelper;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    [TestFixture]
    public class ListingAccessControllerTests : FixtureBase
    {
        private Mock<IAuthProvider> _auth;
        private ListingAccessController _controller;

        public ListingAccessControllerTests()
        {
            Mapper.CreateMap<ListingAccessViewModel, AccountListingAccess>();
            Mapper.CreateMap<AccountListingAccess, ListingAccessViewModel>();
        }

        [SetUp]
        public void SetUp()
        {
            _auth = new Mock<IAuthProvider>();
            _controller = new ListingAccessController(_auth.Object, Session);
        }

        [Test]
        public void ManageListingAccess_Get_Should_Render_Manage_View()
        {
            // arrange
            var account = ExpectedAccount();
            _auth.Setup(x => x.GetAccountKey()).Returns(account.Id);

            // act
            var result = _controller.ManageListingAccess();

            // assert
            Assert.IsInstanceOf<ManageListingAccessViewModel>(result.Model);
            Assert.AreEqual("ManageListingAccess", result.ViewName);

        }

        [Test]
        public void ManageListingAccess_Post_Should_Create_New_Access()
        {
            // arrange
            var account = ExpectedAccount();
            _auth.Setup(x => x.GetAccountKey()).Returns(account.Id);

            var expectedModel = new ListingAccessViewModel
                                    {
                                        Commercial = ListingDisplayAccess.Brokerage,
                                        FarmsAndLand = ListingDisplayAccess.Brokerage,
                                        ForLease = ListingDisplayAccess.Brokerage,
                                        MultiFam = ListingDisplayAccess.None,
                                        Residential = ListingDisplayAccess.IDX
                                    };

            // act
            var result = _controller.ManageListingAccess(expectedModel) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Home", result.RouteValues["controller"]);
        }

        [Test]
        public void ManageListingAccess_Post_Should_Update_Existing_Information()
        {
            // arrange
            var actualAccount = new AccountListingAccess
                                    {
                                        Commercial = ListingDisplayAccess.Brokerage,
                                        FarmsAndLand = ListingDisplayAccess.Brokerage,
                                        ForLease = ListingDisplayAccess.Brokerage,
                                        MultiFam = ListingDisplayAccess.None,
                                        Residential = ListingDisplayAccess.IDX
                                    };

            Session.Save(actualAccount);
            Session.Flush();

            var modifedAccount = Mapper.Map<AccountListingAccess, ListingAccessViewModel>(actualAccount);
            modifedAccount.Residential = ListingDisplayAccess.Brokerage;
            
            var form = new FormCollection()
                           {
                               {"Id", modifedAccount.Id.ToString()},
                               {"Commercial", modifedAccount.FarmsAndLand.ToString()},
                               {"FarmsAndLand", modifedAccount.ForLease.ToString()},
                               {"ForLease", modifedAccount.ForLease.ToString()},
                               {"MultiFam", modifedAccount.MultiFam.ToString()},
                               {"Residential", modifedAccount.Residential.ToString()}
                           };

            // initialize contorller
            _controller.ValueProvider = form.ToValueProvider();
            var builder = new TestControllerBuilder();
            builder.InitializeController(_controller);

            // act
            var result = _controller.ManageListingAccess(modifedAccount) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Home", result.RouteValues["controller"]);
        }

        private Account ExpectedAccount()
        {
            var account = TestHelpers.CreateAccount();
            Session.Save(account);
            Session.Flush();

            return account;
        }
    }
}
