﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Moq;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    [TestFixture]
    public class ReferralContollerTests : FixtureBase
    {
        private Mock<IAuthProvider> _auth;
        private ReferralController _controller;

        public ReferralContollerTests()
        {
            Mapper.CreateMap<ReferralInfo, Referral>();
            Mapper.CreateMap<Referral, ReferralInfo>();

            Mapper.CreateMap<Lead, LeadInfo>();
            Mapper.CreateMap<LeadInfo, Lead>();

            Mapper.CreateMap<Agent, AgentInfo>();
            Mapper.CreateMap<Account, AccountViewModel>();
        }

        [SetUp]
        public void SetUp()
        {
            _auth = new Mock<IAuthProvider>();
            _controller = new ReferralController(_auth.Object, Session);
        }

        [Test]
        public void Index_Get_Should_Render_Referall_Index_View()
        {
            // arrange
            var account = ExpectedAccount();
            _auth.Setup(x => x.GetAccountKey()).Returns(account.Id);

            var agent = TestHelpers.CreateAgent(account);
            Session.Save(agent);
            Session.Flush();

            Session.Save(new Referral
                             {
                                 Account = account,
                                 Agent = agent,
                                 LastReferralDate = null,
                                 PositionNumber = 1
                             });
            Session.Flush();

            // act
            var result = _controller.Index();

            // assert
            Assert.IsInstanceOf<ReferralIndexViewModel>(result.Model);
            Assert.AreEqual(1, ((ReferralIndexViewModel)result.Model).Referrals.Count());
            Assert.AreEqual("Index", result.ViewName);
        }

        [Test]
        public void History_Get_Should_Render_View()
        {
            // arrange
            var account = ExpectedAccount();
            
            var agent = TestHelpers.CreateAgent(account);
            Session.Save(agent);
            Session.Flush();

            Session.Save(new Lead
                             {
                                 Agent = agent,
                                 Created = DateTime.Now,
                                 Note = "some note"
                             });            
            Session.Save(new Lead
                             {
                                 Agent = agent,
                                 Created = DateTime.Now,
                                 Note = "another lead note"
                             });
            Session.Flush();

            // act
            var result = _controller.History(agent.AgentID);

            // assert
            Assert.AreEqual("History", result.ViewName);
            Assert.AreEqual(2, ((IEnumerable<LeadInfo>)result.Model).Count());
        }

        [Test]
        public void AddLead_Get_Should_Render_View()
        {
            // arrange

            // act
            var result = _controller.AddLead(1);

            // assert
            Assert.AreEqual("AddLead", result.ViewName);
        }

        [Test]
        public void AddLead_Post_Should_Save_New_Lead_And_Move_Agent_Referral_Position_To_The_End()
        {
            // arrange
            var account = ExpectedAccount();

            var agent = TestHelpers.CreateAgent(account);
            Session.Save(agent);
            Session.Flush();

            Session.Save(new Referral
                             {
                                 Account = account,
                                 Agent = agent,
                                 LastReferralDate = null,
                                 PositionNumber = 0
                             });
            Session.Flush();

            var lead = new LeadInfo
                           {
                               Note = "some note"
                           };

            // act
            var result = _controller.AddLead(lead, agent.AgentID) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Referral", result.RouteValues["controller"]);

            var actualReferral = Session.Get<Referral>(1);
            Assert.AreEqual(1, actualReferral.PositionNumber);
        }

        private Account ExpectedAccount()
        {
            var account = TestHelpers.CreateAccount();
            Session.Save(account);
            Session.Flush();

            return account;
        }
    }
}
