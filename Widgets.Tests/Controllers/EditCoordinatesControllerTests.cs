﻿using Moq;
using MvcContrib.TestHelper;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    public class EditCoordinatesControllerTests : FixtureBase
    {
        private Mock<IAuthProvider> _auth;
        private Mock<IListingService>  _listingService;

        [SetUp]
        public void SetUp()
        {
            _listingService = new Mock<IListingService>();
            _auth = new Mock<IAuthProvider>();
        }

        [Test]
        public void Index_Get_Should_Render_Index_View()
        {
            // arrange
            var controller = new EditCoordinatesController(_auth.Object, Session, _listingService.Object);
            
            // act
            var result = controller.Index();

            // assert
            Assert.AreEqual("Index", result.ViewName);
        }

        [Test]
        public void Index_Post_Should_Save_Latitude_And_Longitude_For_Given_MLSNumber_And_Return_Success_message()
        {
            // arrange
            var expectedListing = TestHelpers.GetSingleResidential();
            _listingService.Setup(x => x.GetResidentialListing(It.IsAny<int>())).Returns(expectedListing);
            
            var replacement = new ReplacementCoord
                                      {
                                          Latitude = 56.99,
                                          Longitude = -106.343,
                                          PropertyRef = 1224
                                      };

            Session.Save(replacement);
            Session.Flush();
            
            var builder = new TestControllerBuilder();
            var controller = new EditCoordinatesController(_auth.Object, Session, _listingService.Object);
            builder.InitializeController(controller);

            var expectedModel = new EditCoordinatesViewModel()
                                    {
                                        PropertyType = PropertyTypeConstants.Residential,
                                        Address = "some address",
                                        City = "some city",
                                        Latitude = 54.32612,
                                        Longitude = -105.29155,
                                        MlsNumber = 11211
                                    };

            // act
            var result = controller.Index(expectedModel);
            var resultData = result.Data;

            // assert
            Assert.AreEqual("{ html = <p>Coordinates updated succesfully</p> }", resultData.ToString());
            
        }
    }
}
