﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Moq;
using MvcContrib.TestHelper;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    public class NewsControllerTests : FixtureBase
    {
        private Mock<IAuthProvider> _auth;
        private NewsController _controller;
        private Account _expectedAccount;

        public NewsControllerTests()
        {
            Mapper.CreateMap<NewsItemViewModel, BulletinBoard>();
            Mapper.CreateMap<BulletinBoard, NewsItemViewModel>();
        }

        [SetUp]
        public void SetUp()
        {
            // setup account
            _expectedAccount = TestHelpers.CreateAccount();
            Session.Save(_expectedAccount);
            Session.Flush();

            // create mocks
            _auth = new Mock<IAuthProvider>();
            _auth.Setup(x => x.GetAccountKey()).Returns(_expectedAccount.Id);

            // create controller
            _controller = new NewsController(_auth.Object, Session);
        }

        [Test]
        public void Index_Get_should_render_index_view()
        {
            // arrange
            Session.Save(ExpectedBulletin());

            // act
            var result = _controller.Index();

            // assert
            Assert.AreEqual("Index", result.ViewName);
            Assert.IsInstanceOf<NewsIndexViewModel>(result.ViewData.Model);
        }

        [Test]
        public void Create_Get_should_render_create_view()
        {
            // arrange

            // act
            var result = _controller.Create();

            // assert
            Assert.AreEqual("Create", result.ViewName);
            Assert.IsInstanceOf<NewsItemViewModel>(result.ViewData.Model);
            Assert.IsTrue(((NewsItemViewModel)result.ViewData.Model).IsNews);
        }

        [Test]
        public void Create_Post_Should_Create_News_Item()
        {
            // arrange
            var expectedBulletinBoard = new NewsItemViewModel
                                            {
                                                Title = "some title",
                                                Description = "some description",
                                                StartDate = DateTime.Now
                                            };

            var expectedAgent = TestHelpers.CreateAgent(_expectedAccount);
            Session.Save(expectedAgent);

            var mock = new Mock<ControllerContext>();
            mock.SetupGet(p => p.HttpContext.User.Identity.Name).Returns(expectedAgent.UserName);
            _controller.ControllerContext = mock.Object;

            // act
            var result = _controller.Create(expectedBulletinBoard) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
        }

        [Test]
        public void Edit_Get_should_render_edit_view()
        {
            // arrange
            var expectedBulletin = ExpectedBulletin();
            Session.Save(expectedBulletin);

            // act
            var result = _controller.Edit(expectedBulletin.Id);

            // assert
            Assert.AreEqual("Edit", result.ViewName);
            Assert.IsInstanceOf<NewsItemViewModel>(result.ViewData.Model);
        }

        [Test]
        public void Edit_Post_Should_Save_Changes_to_news_item()
        {
            // arrange
            var expectedBulletin = ExpectedBulletin();
            Session.Save(expectedBulletin);
            Session.Flush();

            var editedBulletin = new NewsItemViewModel
                                     {
                                         Id = expectedBulletin.Id,
                                         Title = "edited title",
                                         Description = "edited description",
                                         StartDate = expectedBulletin.StartDate
                                     };

            var form = new FormCollection
                           {
                               {"Id", expectedBulletin.Id.ToString()},
                               {"Title", editedBulletin.Title},
                               {"Description", editedBulletin.Description},
                               {"StartDate", editedBulletin.StartDate.ToString()}
                           };

            // init controller
            var builder = new TestControllerBuilder();
            builder.InitializeController(_controller);

            // act
            _controller.ValueProvider = form.ToValueProvider();
            var result = _controller.Edit(editedBulletin) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
        }

        [Test]
        public void Delete_Should_Delete_The_Specified_News_Item()
        {
            // arrange
            var expectedBulletin = ExpectedBulletin();
            Session.Save(expectedBulletin);

            // act
            var result = _controller.Delete(expectedBulletin.Id) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("News", result.RouteValues["controller"]);
        }

        private BulletinBoard ExpectedBulletin()
        {
            var expectedAgent = TestHelpers.CreateAgent(_expectedAccount);
            Session.Save(expectedAgent);
            Session.Flush();

            return new BulletinBoard
            {
                Title = "some title",
                Description = "some description",
                StartDate = DateTime.Now,
                Account = _expectedAccount,
                Agent = expectedAgent,
                IsNews = false
            };
        }
    }
}
