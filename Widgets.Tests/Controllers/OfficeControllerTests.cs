﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AutoMapper;
using Moq;
using MvcContrib.TestHelper;
using NHibernate;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    class OfficeControllerTests : FixtureBase
    {
        private Mock<IAuthProvider> _auth;
        private OfficeController _controller;

        [SetUp]
        public void SetUp()
        {
            Mapper.CreateMap<AccountOffice, AccountOfficeInfo>();
            Mapper.CreateMap<AccountOfficeInfo, AccountOffice>();
            Mapper.CreateMap<Account, AccountViewModel>();

            _auth = new Mock<IAuthProvider>();
            _controller = new OfficeController(_auth.Object, Session);
        }

        [Test]
        public void ManageOffices_Get_Should_Return_List_Of_Agents_And_Render_Manage_Offices_View()
        {
            // arrange
            var expectedAccount = ExpectedAccount(Session);
            _auth.Setup(x => x.GetAccountKey()).Returns(expectedAccount.Id);

            foreach (var agent in TestHelpers.GenerateFakeOffices())
            {
                agent.Account = expectedAccount;
                Session.Save(agent);
            }

            Session.Flush();

            // act
            var result = _controller.ManageOffices();
            var resultData = (ManageOfficesViewModel)result.ViewData.Model;

            // assert
            Assert.AreEqual("ManageOffices", result.ViewName);
            Assert.AreEqual(2, resultData.Offices.Count());
        }

        [Test]
        public void Create_Get_Should_Render_Create_View()
        {
            // arrange

            // act
            var result = _controller.Create();

            // assert
            Assert.AreEqual("Create", result.ViewName);
        }

        [Test]
        public void Create_Post_Should_Save_Office()
        {
            // arrange
            var expectedAccount = ExpectedAccount(Session);
            Session.Flush();

            _auth.Setup(x => x.GetAccountKey()).Returns(expectedAccount.Id);

            // arrange
            var office = new AccountOfficeInfo
                {
                    Name = "Realty Executives Office 2",
                    Address = "123 Fake 2",
                    City = "Saskatoon",
                    Province = "Saskatchewan",
                    Phone = "(306) 373-7520",
                    Fax = "(306) 955-6235",
                    Website = "http://rexsaskatoon.com/office2",

                };

            // act 
            var result = _controller.Create(office) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("ManageOffices", result.RouteValues["action"]);
            Assert.AreEqual("Office", result.RouteValues["controller"]);
        }

        [Test]
        public void Edit_Get_Should_Render_Edit_View()
        {
            // arrange
            var account = ExpectedAccount(Session);
            var expectedOffice = TestHelpers.CreateOffice(account);
            Session.Save(expectedOffice);
            Session.Flush();

            _auth.Setup(x => x.GetAccountKey()).Returns(account.Id);

            // act
            var result = _controller.Edit(expectedOffice.Id);
            var resultData = (AccountOfficeInfo)result.ViewData.Model;

            // assert
            Assert.AreEqual("Edit", result.ViewName);
            Assert.AreEqual(expectedOffice.Name, resultData.Name);
            Assert.AreEqual(expectedOffice.City, resultData.City);
            Assert.AreEqual(expectedOffice.Address, resultData.Address);
        }

        [Test]
        public void Edit_Post_Should_Save_Changes_To_Office()
        {
            // arrange
            var account = ExpectedAccount(Session);
            var expectedOffice = TestHelpers.CreateOffice(account);
            Session.Save(expectedOffice);
            Session.Flush();

            _auth.Setup(x => x.GetAccountKey()).Returns(account.Id);

            var modifiedOffice = new AccountOfficeInfo
            {
                Id = expectedOffice.Id,
                Name = "Modified Office Name",
                Address = expectedOffice.Address,
                Province = expectedOffice.Province,
                PostalCode = expectedOffice.PostalCode
            };

            var modifiedOfficeForm = new FormCollection
                        {
                            {"Id", modifiedOffice.Id.ToString()},
                            {"Name", modifiedOffice.Name},
                            {"Address", modifiedOffice.Address},
                            {"Province", modifiedOffice.Province},
                            {"PostalCode", modifiedOffice.PostalCode},
                        };

            _controller.ValueProvider = modifiedOfficeForm.ToValueProvider();

            // initialize controller
            var builder = new TestControllerBuilder();
            builder.InitializeController(_controller);

            // act
            var result = _controller.Edit(modifiedOffice) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("ManageOffices", result.RouteValues["action"]);
            Assert.AreEqual("Office", result.RouteValues["controller"]);
        }

        [Test]
        public void Delete_Office()
        {
            // arrange
            var account = ExpectedAccount(Session);
            var expectedOffice = TestHelpers.CreateOffice(account);
            Session.Save(expectedOffice);
            Session.Flush();

            _auth.Setup(x => x.GetAccountKey()).Returns(account.Id);

            // act
            var result = _controller.Delete(expectedOffice.Id) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("ManageOffices", result.RouteValues["action"]);
            Assert.AreEqual("Office", result.RouteValues["controller"]);
        }        

        private static Account ExpectedAccount(ISession session)
        {
            var account = TestHelpers.CreateAccount();
            session.Save(account);

            return account;
        }
    }
}
