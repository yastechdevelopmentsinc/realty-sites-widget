﻿using System;
using System.Linq;
using System.Web.Mvc;
using Moq;
using MvcContrib.TestHelper;
using NHibernate.Linq;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    [TestFixture]
    public class SettingsControllerTests : FixtureBase
    {
        private SettingsController _controller;
        private Mock<IAuthProvider> _auth;
        private Account _expectedAccount;
        private Guid _expectedAccountID;

        [SetUp]
        public void SetUp()
        {
            // setup account
            _expectedAccount = TestHelpers.CreateAccount();
            Session.Save(_expectedAccount);
            Session.Flush();

            _expectedAccountID = _expectedAccount.Id;

            // create mocks  
            _auth = new Mock<IAuthProvider>();
            _auth.Setup(x => x.GetAccountKey()).Returns(_expectedAccountID);

            _controller = new SettingsController(_auth.Object, Session);
        }

        [Test]
        public void ManageSettings_Get_Should_Render_ManageSettings_View()
        {
            // arrange
            
            // act
            var result = _controller.ManageSettings();

            // assert
            Assert.AreEqual("ManageSettings", result.ViewName);
        }

        [Test]
        public void ManageSettings_Post_Should_Save_Mortgage_Calculator_Defaults()
        {
            // arrange
            var expectedSettings = new ManageSettingsViewModel
                                       {
                                           Amortization = 25,
                                           Term = 5,
                                           InterestRate = 3.98,
                                           Principal = 200000,
                                           DownPayment = 20
                                       };

            var form = new FormCollection
                           {
                               {"Amortization", expectedSettings.Amortization.ToString()},
                               {"Term", expectedSettings.Term.ToString()},
                               {"InterestRate", expectedSettings.InterestRate.ToString()},
                               {"Principal", expectedSettings.Principal.ToString()},
                               {"DownPayment", expectedSettings.DownPayment.ToString()},
                           };

            var builder = new TestControllerBuilder();
            builder.InitializeController(_controller);
            _controller.ValueProvider = form;

            // act
            var result = _controller.ManageSettings(expectedSettings) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Home", result.RouteValues["controller"]);
            Assert.AreEqual("Index", result.RouteValues["action"]);
        }
    }
}
