﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Moq;
using MvcContrib.TestHelper;
using NHibernate;
using NHibernate.Linq;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    public class AgentControllerTests : FixtureBase
    {
        private Mock<IAuthProvider> _auth;
        private AgentController _controller;

        [SetUp]
        public void SetUp()
        {
            Mapper.CreateMap<Agent, AgentInfo>();
            Mapper.CreateMap<AgentInfo, Agent>();
            Mapper.CreateMap<Account, AccountViewModel>();

            _auth = new Mock<IAuthProvider>();
            _controller = new AgentController(_auth.Object, Session);
        }

        [Test]
        public void ManageAgents_Get_Should_Return_List_Of_Agents_And_Render_Manage_Agents_View()
        {
            // arrange
            var expectedAccount = ExpectedAccount(Session);
            _auth.Setup(x => x.GetAccountKey()).Returns(expectedAccount.Id);

            foreach(var agent in TestHelpers.GenerateFakeAgents())
            {
                agent.Account = expectedAccount;
                Session.Save(agent);
            }

            Session.Flush();

            // act
            var result = _controller.ManageAgents();
            var resultData = (ManageAgentsViewModel) result.ViewData.Model;

            // assert
            Assert.AreEqual("ManageAgents", result.ViewName);
            Assert.AreEqual(3, resultData.Agents.Count());
        }

        [Test]
        public void Edit_Get_Should_Render_Edit_View()
        {
            // arrange
            var account = ExpectedAccount(Session);
            var expectedAgent = TestHelpers.CreateAgent(account);
            Session.Save(expectedAgent);
            Session.Flush();

            _auth.Setup(x => x.GetAccountKey()).Returns(expectedAgent.Account.Id);

            // act
            var result = _controller.Edit(expectedAgent.AgentID);
            /*var resultData = (AgentInfo) result.ViewData.Model;

            // assert
            Assert.AreEqual("Edit", result.ViewName);
            Assert.AreEqual(expectedAgent.FirstName, resultData.FirstName);
            Assert.AreEqual(expectedAgent.LastName, resultData.LastName);
            Assert.AreEqual(expectedAgent.Email, resultData.Email);*/
        }

        [Test]
        public void Edit_Post_Should_Save_Changes_To_Agent()
        {
            // arrange
            var account = ExpectedAccount(Session);
            var expectedAgent = TestHelpers.CreateAgent(account);
            Session.Save(expectedAgent);
            Session.Flush();

            _auth.Setup(x => x.GetAccountKey()).Returns(expectedAgent.Account.Id);

            var modifiedAgent = new AgentInfo
            {
                AgentID = expectedAgent.AgentID,
                Email = "modifiedemail@mail.com",
                UserName = expectedAgent.UserName
            };

            var modifiedAgentForm = new FormCollection
                        {
                            {"AgentID", modifiedAgent.AgentID.ToString()},
                            {"Email", modifiedAgent.Email},
                            {"UserName", modifiedAgent.UserName}
                        };

            _controller.ValueProvider = modifiedAgentForm.ToValueProvider();

            // initialize controller
            var builder = new TestControllerBuilder();
            builder.InitializeController(_controller);

            // act
            var result = _controller.Edit(modifiedAgent, null) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("ManageAgents", result.RouteValues["action"]);
            Assert.AreEqual("Agent", result.RouteValues["controller"]);
        }

        [Test]
        public void Create_Get_Should_Render_Create_View()
        {
            // arrange

            // act
            var result = _controller.Create();

            // assert
            Assert.AreEqual("Create", result.ViewName);
        }

        [Test]
        public void Create_Post_Should_Save_Agent()
        {
            // arrange
            var expectedAccount = ExpectedAccount(Session);
            Session.Flush();

            _auth.Setup(x => x.GetAccountKey()).Returns(expectedAccount.Id);
            
            // arrange
            var agent = new AgentInfo()
            {
                AgentID = 123,
                FirstName = "jon",
                LastName = "doe",
                Email = "jondoe@mail.com",
                FullName = "jon doe",
                UserName = "jon",
                Password = "password",
                Active = true,
                Role = "User"
            };

            // act 
            var result = _controller.Create(agent, null) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("ManageAgents", result.RouteValues["action"]);
            Assert.AreEqual("Agent", result.RouteValues["controller"]);
        }

        [Test]
        public void GetImage_Should_Retrieve_Agent_Photo_For_The_Specified_Agent_Id()
        {
            // arrange
            var account = ExpectedAccount(Session);
            var expectedAgent = TestHelpers.CreateAgent(account);
            Session.Save(expectedAgent);
            Session.Flush();

            // act
            var result = _controller.GetImage(expectedAgent.AgentID) as FileContentResult;

            // assert
            Assert.AreEqual(expectedAgent.PhotoMimeType, result.ContentType);
            Assert.IsNotEmpty(result.FileContents);
        }

        [Test]
        public void Delete_Agent_Should_Delete_Agent()
        {
            // arrange
            var account = ExpectedAccount(Session);
            var expectedAgent = TestHelpers.CreateAgent(account);
            Session.Save(expectedAgent);
            Session.Flush();

            _auth.Setup(x => x.GetAccountKey()).Returns(account.Id);

            // act
            var result = _controller.Delete(expectedAgent.AgentID) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("ManageAgents", result.RouteValues["action"]);
            Assert.AreEqual("Agent", result.RouteValues["controller"]);
        }        

        private static Account ExpectedAccount(ISession session)
        {
            var account = TestHelpers.CreateAccount();
            session.Save(account);

            return account;
        }
    }
}