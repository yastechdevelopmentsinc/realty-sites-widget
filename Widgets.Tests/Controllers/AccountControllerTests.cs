﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Moq;
using MvcContrib.TestHelper;
using NHibernate.Linq;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    [TestFixture]
    public class AccountControllerTests : FixtureBase
    {
        private Mock<IAuthProvider> _auth;
        
        private AccountController _controller;

        public AccountControllerTests()
        {
            Mapper.CreateMap<AccountViewModel, Account>();
            Mapper.CreateMap<Account, AccountViewModel>();
        }

        [SetUp]
        public void SetUp()
        {
            _auth = new Mock<IAuthProvider>();

            _controller = new AccountController(Session, _auth.Object);
        }

        [Test]
        public void ManageAccounts_Get_Should_Return_List_Of_Accounts_And_Render_Manage_Accounts_View()
        {
            // arrange
            foreach(var account in TestHelpers.FakeAccounts())
            {
                Session.Save(account);
                Session.Flush();
            }

            // act
            var result = _controller.ManageAccounts(null);
            var resultData = (ManageAccountsViewModel) result.ViewData.Model;

            // assert
            Assert.AreEqual("ManageAccounts", result.ViewName);
            Assert.AreEqual(2, resultData.Accounts.Count());
        }

        [Test]
        public void Create_Get_Should_Render_Create_Account_View()
        {
            // arrange

            // act 
            var result = _controller.Create();
            var resultData = (AccountViewModel) result.ViewData.Model;

            // assert
            Assert.AreEqual("Create", result.ViewName);
            Assert.IsNull(resultData.AccountType);
        }        
        
        [Test]
        public void Create_Post_Should_Save_Account()
        {
            // arrange
            var account = new AccountViewModel
                              {
                                  AccountType = AccountType.Agent,
                                  Name = "Fake account one",
                                  BrokerCode = "Realty",
                                  AgentCode = "Agent",
                                  BoardCode = "Saskatoon",
                                  Email = "realty@mail.com"
                              };

            // act 
            var result = _controller.Create(account, null, null, null);

            // assert
            var expectedAccount = Session.Query<Account>().SingleOrDefault(x => x.Email == account.Email);
            Assert.AreEqual(account.Name, expectedAccount.Name);
        }

        [Test]
        public void Edit_Get_Should_Render_Edit_View()
        {
            // arrange
            var expectedAccount = TestHelpers.CreateAccount();

            Session.Save(expectedAccount);
            Session.Flush();

            // act
            var result = _controller.Edit(expectedAccount.Id);
            var resultData = result.ViewData.Model;

            // assert
            Assert.IsInstanceOf<AccountViewModel>(resultData);
            Assert.AreEqual(expectedAccount.Id, ((AccountViewModel)resultData).Id);
            Assert.AreEqual("Edit", result.ViewName);

        }

        [Test]
        public void Edit_Post_Should_Save_Changes_To_Account()
        {
            // arrange
            var expectedAccount = TestHelpers.CreateAccount();
            Session.Save(expectedAccount);
            Session.Flush();

            var modifiedAccount = new AccountViewModel
                                      {
                                          Id = expectedAccount.Id,
                                          Email = "modifiedemai@mail.com"
                                      };

            // initialize controller
            var builder = new TestControllerBuilder();
            builder.InitializeController(_controller);

            var postedForm = new FormCollection
                           {
                               {"Id", expectedAccount.Id.ToString()},
                               {"Email", modifiedAccount.Email}
                           };
            _controller.ValueProvider = postedForm.ToValueProvider();

            // act
            var result = _controller.Edit(modifiedAccount, null, null, null) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Home", result.RouteValues["controller"]);
        }
    }
}
