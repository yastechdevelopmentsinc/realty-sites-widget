﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Moq;
using MvcContrib.TestHelper;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    [TestFixture]
    class LinksControllerTests : FixtureBase
    {
        private Mock<IAuthProvider> _auth;
        private LinksController _controller;

        [SetUp]
        public void SetUp()
        {
            _auth = new Mock<IAuthProvider>();
            _controller = new LinksController(_auth.Object, Session);

            Mapper.CreateMap<AccountLink, AccountLinkViewModel>();
            Mapper.CreateMap<AccountLinkViewModel, AccountLink>();
        }

        [Test]
        public void ManageLinks_Get_Should_Return_List_Of_Links_And_Render_Manage_Links_View()
        {
            // arrange
            var link1 = new AccountLink
                {
                    Name = "fake link 1",
                    Website = "http://fakesite1.com"
                };
            Session.Save(link1);
            var link2 = new AccountLink
            {
                Name = "fake link 2",
                Website = "http://fakesite2.com"
            };
            Session.Save(link2);
            Session.Flush();

            // act
            var result = _controller.ManageLinks();
            var resultData = (ManageLinksViewModel)result.ViewData.Model;

            // assert
            Assert.AreEqual("ManageLinks", result.ViewName);
            Assert.AreEqual(2, resultData.Links.Count());
        }

        [Test]
        public void Create_Get_Should_Render_Create_Link_View()
        {
            // arrange

            // act 
            var result = _controller.Create();
            var resultData = (AccountLinkViewModel)result.ViewData.Model;

            // assert
            Assert.AreEqual("Create", result.ViewName);
        }

        [Test]
        public void Create_Post_Should_Save_Link()
        {
            // arrange
            var account = TestHelpers.CreateAccount();
            Session.Save(account);
            Session.Flush();

            var expectedLink = new AccountLinkViewModel
                {
                    Name = "Fake Link one",
                    Website = "http://www.fakesite.com"
                };

            // act 
            var result = _controller.Create(expectedLink) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("ManageLinks", result.RouteValues["action"]);
            Assert.AreEqual("Links", result.RouteValues["controller"]);
        }

        [Test]
        public void Edit_Get_Should_Render_Edit_View()
        {
            // arrange
            var expectedLink = new AccountLink
            {
                Name = "fake link 1",
                Website = "http://fakesite1.com"
            };

            Session.Save(expectedLink);
            Session.Flush();

            // act
            var result = _controller.Edit(expectedLink.Id);
            var resultData = result.ViewData.Model;

            // assert
            Assert.IsInstanceOf<AccountLinkViewModel>(resultData);
            Assert.AreEqual(expectedLink.Id, ((AccountLinkViewModel)resultData).Id);
            Assert.AreEqual("Edit", result.ViewName);
        }

        [Test]
        public void Edit_Post_Should_Save_Changes_To_Link()
        {
            // arrange
            var expectedAccount = TestHelpers.CreateAccount();
            Session.Save(expectedAccount);
            Session.Flush();

            var expectedLink = new AccountLink
            {
                Account = expectedAccount,
                Name = "fake link 1",
                Website = "http://fakesite1.com"
            };

            Session.Save(expectedLink);
            Session.Flush();
            
            var modifiedLink= new AccountLinkViewModel
            {
                Id = expectedLink.Id,
                Name = "updated"
            };

            // initialize controller
            var builder = new TestControllerBuilder();
            builder.InitializeController(_controller);

            var postedForm = new FormCollection
                           {
                               {"Id", expectedLink.Id.ToString()},
                               {"Name", modifiedLink.Name}
                           };
            _controller.ValueProvider = postedForm.ToValueProvider();

            // act
            var result = _controller.Edit(modifiedLink) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("ManageLinks", result.RouteValues["action"]);
            Assert.AreEqual("Links", result.RouteValues["controller"]);
        }

        [Test]
        public void Delete_Get_Should_Delete_Specified_Link()
        {
            // setup
            var expectedAccount = TestHelpers.CreateAccount();
            Session.Save(expectedAccount);
            Session.Flush();

            var expectedLink = new AccountLink
            {
                Account = expectedAccount,
                Name = "fake link 1",
                Website = "http://fakesite1.com"
            };

            Session.Save(expectedLink);
            Session.Flush();

            // act
            var result = _controller.Delete(expectedLink.Id) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("ManageLinks", result.RouteValues["action"]);
            Assert.AreEqual("Links", result.RouteValues["controller"]);

        }
    }
}
