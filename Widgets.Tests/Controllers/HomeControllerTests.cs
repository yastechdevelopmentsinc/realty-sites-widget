﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Moq;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    [TestFixture]
    public class HomeControllerTests : FixtureBase
    {
        private Mock<IAuthProvider> _auth;
        private HomeController _controller;

        [SetUp]
        public void SetUp()
        {
            _auth = new Mock<IAuthProvider>();
            _controller = new HomeController(_auth.Object, Session);
        }


        [Test]
        public void Index_Get_Should_Render_Index_View()
        {
            // arrange

            // act
            var result = _controller.Index();

            // assert
            Assert.AreEqual("Index", result.ViewName);
        }

        [Test]
        public void Current_Account_Get_Should_Render_Partial_View()
        {
            // arrange
            var account = TestHelpers.CreateAccount();
            Session.Save(account);
            Session.Flush();

            _auth.Setup(x => x.GetAccountKey()).Returns(account.Id);

            // act
            var result = _controller.CurrentAccount() as PartialViewResult;

            // assert
            Assert.IsInstanceOf<CurrentAccountViewModel>(result.ViewData.Model);
            Assert.AreEqual("CurrentAccount",result.ViewName);
        }

        [Test]
        public void Current_Account_Get_Should_Render_Empty_Result_When_No_Account_Selected()
        {
            // act
            var result = _controller.CurrentAccount() as EmptyResult;
      
            // assert
            Assert.AreSame(typeof(EmptyResult), result.GetType());
        }
    }
}
