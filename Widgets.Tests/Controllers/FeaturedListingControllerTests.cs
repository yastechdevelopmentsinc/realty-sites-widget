﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using Moq;
using NUnit.Framework;
using Widgets.Controllers;
using Widgets.Domain;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models;
using Widgets.Models.ViewModels;
using Widgets.Tests.Domain;

namespace Widgets.Tests.Controllers
{
    public class FeaturedListingControllerTests : FixtureBase
    {
        private Mock<IListingService> _listingService;
        private Mock<IAuthProvider> _auth;
        private FormsAuthenticationTicket _expectedticket;
        private Guid _expectedAccountID;
        private Account _expectedAccount;

        public FeaturedListingControllerTests()
        {
            Mapper.CreateMap<Residential, Listing>();
        }

        [SetUp]
        public void SetUp()
        {
            _listingService = new Mock<IListingService>();
            _auth = new Mock<IAuthProvider>();

            _listingService.Setup(repository => repository.GetResidentialListing(It.IsAny<int>())).Returns(TestHelpers.GetSingleResidential());

            // setup account and auth ticket
            _expectedAccount = TestHelpers.CreateAccount();
            Session.Save(_expectedAccount);
            Session.Flush();

            _expectedAccountID = _expectedAccount.Id;
            _auth.Setup(x => x.GetAccountKey()).Returns(_expectedAccountID);
        }

        [Test]
        public void Index_Get_Should_Render_Index_View_And_Display_List_Of_Featured_Listings()
        {
            // arrange
            Session.Save(
                new FeaturedListing
                    {
                        Account = _expectedAccount,
                        Mlsnumber = 445566
                    }
                );

            var controller = new FeaturedListingController(_auth.Object, Session, _listingService.Object);

            // act
            var result = controller.Index();
            var resultData = (List<Listing>) result.ViewData.Model;

            // assert
            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual(1, resultData.Count());
        }

        [Test]
        public void Create_Post_Should_Save_FeaturedListing()
        {
            // arrange

            var controller = new FeaturedListingController(_auth.Object, Session, _listingService.Object);

            // act
            var result = controller.Create(1234) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("FeaturedListing", result.RouteValues["controller"]);
        }

        [Test]
        public void Create_get_should_render_create_view()
        {
            // arrange
            var controller = new FeaturedListingController(_auth.Object, Session, _listingService.Object);

            // act
            var result = controller.Create();

            // assert
            Assert.AreEqual("Create", result.ViewName);
        }

        [Test]
        public void Delete_should_delete_featured_listing()
        {
            // arrange
            Session.Save(
                new FeaturedListing
                    {
                        Account = _expectedAccount,
                        Mlsnumber = 445566
                    }
                );

            var controller = new FeaturedListingController(_auth.Object, Session, _listingService.Object);

            // act
            var result = controller.Delete(445566) as RedirectToRouteResult;

            // assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("FeaturedListing", result.RouteValues["controller"]);
        }
    }
}
