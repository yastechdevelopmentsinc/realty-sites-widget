﻿using System;
using System.Collections;
using FluentNHibernate.Testing;
using NUnit.Framework;
using Widgets.Domain.Entities;

namespace Widgets.Tests.Domain
{
    [TestFixture]
    public class MappingTests : FixtureBase
    {
        [Test]
        public void Can_map_residential_entity()
        {
            
            new PersistenceSpecification<Residential>(Session)
                .CheckProperty(x => x.ResiID, 1)
                .CheckProperty(x => x.MlsNumber, 133)
                .CheckProperty(x => x.PropertyType, "Bi-Level")
                .CheckProperty(x => x.Address, "234 some street")
                .CheckProperty(x => x.City, "Some CIty")
                .CheckProperty(x => x.ListPrice, 249000m)
                .CheckProperty(x => x.YearBuilt, 120)
                .CheckProperty(x => x.SqFootage, "1300")
                .CheckProperty(x => x.SubAreaName, "suburbia")
                .CheckProperty(x => x.InternetComm, "description goes here")
                .CheckProperty(x => x.BrokerName, "brokers broker")
                .CheckProperty(x => x.AgentBoardCode, "board code")
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_for_lease_entity()
        {
            new PersistenceSpecification<ForLease>(Session)
                .CheckProperty(x => x.ForLeaseID, 1)
                .CheckProperty(x => x.MlsNumber, 133)
                .CheckProperty(x => x.PropertyType, "Bi-Level")
                .CheckProperty(x => x.Address, "234 some street")
                .CheckProperty(x => x.City, "Some CIty")
                .CheckProperty(x => x.ListPrice, 249000m)
                .CheckProperty(x => x.YearBuilt, 120)
                .CheckProperty(x => x.SubAreaName, "suburbia")
                .CheckProperty(x => x.InternetComm, "description goes here")
                .CheckProperty(x => x.BrokerName, "brokers broker")
                .CheckProperty(x => x.AgentBoardCode, "board code")
                .VerifyTheMappings();
        }


        [Test]
        public void Can_map_commercial_entity()
        {
            new PersistenceSpecification<Commercial>(Session)
                .CheckProperty(x => x.CommID, 1)
                .CheckProperty(x => x.MlsNumber, 133)
                .CheckProperty(x => x.PropertyType, "Bi-Level")
                .CheckProperty(x => x.Address, "234 some street")
                .CheckProperty(x => x.City, "Some CIty")
                .CheckProperty(x => x.ListPrice, 249000m)
                .CheckProperty(x => x.YearBuilt, 120)
                .CheckProperty(x => x.SubAreaName, "suburbia")
                .CheckProperty(x => x.InternetComm, "description goes here")
                .CheckProperty(x => x.BrokerName, "brokers broker")
                .CheckProperty(x => x.AgentBoardCode, "board code")
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_farms_and_land_entity()
        {
            new PersistenceSpecification<FarmsAndLand>(Session)
                .CheckProperty(x => x.FarmsAndLandID, 1)
                .CheckProperty(x => x.MlsNumber, 133)
                .CheckProperty(x => x.PropertyType, "Bi-Level")
                .CheckProperty(x => x.Address, "234 some street")
                .CheckProperty(x => x.City, "Some CIty")
                .CheckProperty(x => x.ListPrice, 249000m)
                .CheckProperty(x => x.YearBuilt, 120)
                .CheckProperty(x => x.SubAreaName, "suburbia")
                .CheckProperty(x => x.InternetComm, "description goes here")
                .CheckProperty(x => x.BrokerName, "brokers broker")
                .CheckProperty(x => x.AgentBoardCode, "board code")
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_multi_family_entity()
        {
            new PersistenceSpecification<MultiFam>(Session)
                .CheckProperty(x => x.MultFamID, 1)
                .CheckProperty(x => x.MlsNumber, 133)
                .CheckProperty(x => x.PropertyType, "Bi-Level")
                .CheckProperty(x => x.Address, "234 some street")
                .CheckProperty(x => x.City, "Some CIty")
                .CheckProperty(x => x.ListPrice, 249000m)
                .CheckProperty(x => x.YearBuilt, 120)
                .CheckProperty(x => x.SubAreaName, "suburbia")
                .CheckProperty(x => x.InternetComm, "description goes here")
                .CheckProperty(x => x.BrokerName, "brokers broker")
                .CheckProperty(x => x.AgentBoardCode, "board code")
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_agent_entity()
        {
            new PersistenceSpecification<Agent>(Session, new CustomEqualityComparer())
                .CheckProperty(x => x.AgentID, 1)
                .CheckProperty(x => x.AgentIndCode, "sandyb")
                .CheckProperty(x => x.FirstName, "sandy")
                .CheckProperty(x => x.LastName, "bay")
                .CheckProperty(x => x.FullName, "Sandy Bay")
                .CheckProperty(x => x.Email, "sandy@Mail.com")
                .CheckProperty(x => x.UserName, "username")
                .CheckProperty(x => x.Password, "password")
                .CheckProperty(x => x.Active, true)
                .CheckProperty(x => x.Staff, true)
                .CheckProperty(x => x.Role, "Super")
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_account_entity()
        {
            new PersistenceSpecification<Account>(Session, new CustomEqualityComparer())
                .CheckProperty(x => x.AccountType, AccountType.Agent)
                .CheckProperty(x => x.Name, "SomeAccoutName")
                .CheckProperty(x => x.AgentCode, "agentCodeHere")
                .CheckProperty(x => x.BrokerCode, "brokerCodeHere")
                .CheckProperty(x => x.BoardCode, "boardCodeHere")
                .CheckProperty(x=>x.Email, "someemail@mail.com")
                .CheckProperty(x => x.AccountType, AccountType.Agent)
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_area_entity()
        {
            new PersistenceSpecification<Area>(Session)
                .CheckProperty(x => x.AreaID, 1)
                .CheckProperty(x => x.AreaCode, "S1")
                .CheckProperty(x => x.AreaDescription, "Saskatoon")
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_location_entity()
        {           
            new PersistenceSpecification<Location>(Session)
                .CheckProperty(x => x.AreaID, 1)
                .CheckProperty(x => x.CityName, "Saskatoon")
                .CheckProperty(x => x.AreaName, "Briarwood")
                .CheckProperty(x => x.AreaCode, "S1")
                .CheckProperty(x => x.DistrictCode, "BW")
                .CheckProperty(x => x.SubAreaName, "BW-Briarwood")
                .CheckProperty(x => x.Latitude, 52.10929)
                .CheckProperty(x => x.Longitude, -106.56504)
                .CheckProperty(x => x.Active, true)
                .CheckProperty(x => x.Listings, 0)
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_openhouse_entity()
        {
            new PersistenceSpecification<OpenHouse>(Session)
                .CheckProperty(x => x.OpHoID, 1)
                .CheckProperty(x => x.Description, "some description")
                .CheckProperty(x => x.MlsNumber, 12345)
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_featuredlisting_entity()
        {
            var account = TestHelpers.CreateAccount(Guid.NewGuid());
            Session.Save(account);
            Session.Flush();

            new PersistenceSpecification<FeaturedListing>(Session, new CustomEqualityComparer())
                .CheckProperty(x => x.Id, 1)
                .CheckProperty(x=>x.Mlsnumber, 1234)
                .CheckReference(x=>x.Account, account)
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_accounturl_entity()
        {
            var account = TestHelpers.CreateAccount(Guid.NewGuid());
            Session.Save(account);
            Session.Flush();

            new PersistenceSpecification<AccountUrl>(Session)
                .CheckProperty(x=>x.Id, 1)
                .CheckProperty(x=>x.Url, "somewebsite.com")
                .CheckReference(x=>x.Account, account);
        }

        [Test]
        public void Can_map_account_listing_access_entity()
        {
            var account = TestHelpers.CreateAccount(Guid.NewGuid());
            Session.Save(account);
            Session.Flush();

            new PersistenceSpecification<AccountListingAccess>(Session, new CustomEqualityComparer())
                .CheckProperty(x => x.Id, 1)
                .CheckProperty(x => x.Residential, ListingDisplayAccess.IDX)
                .CheckProperty(x => x.Commercial, ListingDisplayAccess.IDX)
                .CheckProperty(x => x.FarmsAndLand, ListingDisplayAccess.IDX)
                .CheckProperty(x => x.ForLease, ListingDisplayAccess.IDX)
                .CheckProperty(x => x.MultiFam, ListingDisplayAccess.IDX)
                .CheckReference(x => x.Account, account)
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_account_office_entity()
        {
            var account = TestHelpers.CreateAccount(Guid.NewGuid());
            Session.Save(account);
            Session.Flush();

            new PersistenceSpecification<AccountOffice>(Session, new CustomEqualityComparer())
                .CheckProperty(x => x.Id, 1)
                .CheckProperty(x=> x.Name, "Some Brokerage North Office")
                .CheckProperty(x=> x.Address, "123 Fake Street")
                .CheckProperty(x=> x.City, "Saskatoon")
                .CheckProperty(x=> x.Province, "Saskatchewan")
                .CheckProperty(x=> x.PostalCode, "S0H150")
                .CheckReference(x => x.Account, account)
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_bulletin_board_entity()
        {
            var account = TestHelpers.CreateAccount(Guid.NewGuid());
            Session.Save(account);

            var agent = TestHelpers.CreateAgent(account);
            Session.Save(agent);
            Session.Flush();

            var date = new DateTime();

            new PersistenceSpecification<BulletinBoard>(Session, new CustomEqualityComparer())
                .CheckProperty(x => x.Id, 1)
                .CheckProperty(x => x.Title, "some title")
                .CheckProperty(x => x.Description, "description here")
                .CheckProperty(x => x.StartDate, date)
                .CheckReference(x => x.Account, account)
                .CheckReference(x => x.Agent, agent)
                .VerifyTheMappings();

        }

        [Test]
        public void Can_map_bulletin_board_photo()
        {
            var account = TestHelpers.CreateAccount(Guid.NewGuid());
            Session.Save(account);

            var agent = TestHelpers.CreateAgent(account);
            Session.Save(agent);
            Session.Flush();

            var bulletin = new BulletinBoard
                               {
                                   Account = account,
                                   Agent = agent,
                                   Description = "some description",
                                   IsNews = true,
                                   Title = "the title"
                               };

            new PersistenceSpecification<BulletinBoardPhoto>(Session, new CustomEqualityComparer())
                .CheckProperty(x=>x.Id, 1)
                .CheckProperty(x=>x.PhotoCaption, "caption")
                .CheckProperty(x=>x.PhotoMimeType, "image/jpeg")
                .CheckReference(x=>x.BulletinBoard, bulletin)
                .VerifyTheMappings();

        }

        [Test]
        public void Can_map_referral_entity()
        {
            var account = TestHelpers.CreateAccount(Guid.NewGuid());
            Session.Save(account);
            Session.Flush();

            var agent = TestHelpers.CreateAgent(account);
            Session.Save(agent);
            Session.Flush();

            var date = DateTime.Now;
            date = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Kind);

            new PersistenceSpecification<Referral>(Session, new CustomEqualityComparer())
                .CheckProperty(x => x.Id, 1)
                .CheckProperty(x => x.PositionNumber, 4)
                .CheckProperty(x => x.LastReferralDate, date)
                .CheckReference(x => x.Agent, agent)
                .CheckReference(x => x.Account, account)
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_lead_entity()
        {
            var account = TestHelpers.CreateAccount(Guid.NewGuid());
            Session.Save(account);

            var agent = TestHelpers.CreateAgent(account);
            Session.Save(agent);
            Session.Flush();

            var date = DateTime.Now;
            date = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Kind);

            new PersistenceSpecification<Lead>(Session, new CustomEqualityComparer())
                .CheckProperty(x => x.LeadId, 1)
                .CheckProperty(x => x.Created, date)
                .CheckProperty(x => x.Note, "lead note here")
                .CheckReference(x => x.Agent, agent)
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_contacts_entity()
        {
            var account = TestHelpers.CreateAccount(Guid.NewGuid());
            Session.Save(account);

            var agent = TestHelpers.CreateAgent(account);
            Session.Save(agent);
            Session.Flush();

            new PersistenceSpecification<Contact>(Session, new CustomEqualityComparer())
                .CheckProperty(x => x.Id, 1)
                .CheckProperty(x=>x.PositionNumber, 1)
                .CheckProperty(x=>x.AgentTitle, "some title")
                .CheckReference(x=>x.Account, account)
                .CheckReference(x=>x.Agent, agent)
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_replacementcoord_entity()
        {
            new PersistenceSpecification<ReplacementCoord>(Session)
                .CheckProperty(x => x.ReplacementID, 1)
                .CheckProperty(x => x.PropertyRef, 12234)
                .CheckProperty(x => x.Latitude, 52.12656)
                .CheckProperty(x => x.Longitude, -106.66248)
                .VerifyTheMappings();
        }

        [Test]
        public void Can_map_disclaimer_entity()
        {
            new PersistenceSpecification<Disclaimer>(Session)
                .CheckProperty(x => x.ID, 1)
                .CheckProperty(x => x.BoardCode, "Saskatoon")
                .CheckProperty(x=>x.MLSListing, "some disclaimer")
                .CheckProperty(x=>x.MLSListingDetails, "some disclaimer")
                .CheckProperty(x=>x.ExclusiveListing, "some disclaimer")
                .CheckProperty(x=>x.ExclusiveListingDetails, "some disclaimer");
        }

        [Test]
        public void Can_map_accountfile_entity()
        {
            var account = TestHelpers.CreateAccount(Guid.NewGuid());
            Session.Save(account);
            Session.Flush();

            /*new PersistenceSpecification<AccountFile>(Session)
                .CheckProperty(x => x.FileContent, new byte[1])
                .CheckProperty(x => x.FileName, "Some file name")
                .CheckProperty(x => x.MimeType, "text/plain")
                .CheckReference(x => x.Account, account);*/
        }

        [Test]
        public void Can_map_calculatordefaults_entity()
        {
            new PersistenceSpecification<CalculatorDefaults>(Session)
                .CheckProperty(x => x.Amortization, 25)
                .CheckProperty(x => x.Term, 5)
                .CheckProperty(x => x.InterestRate, 3.98)
                .CheckProperty(x => x.Principal, 100000)
                .CheckProperty(x => x.DownPayment, 20);
        }

    }

    public class CustomEqualityComparer : IEqualityComparer
    {
        public new bool Equals(object x, object y)
        {
            if (x==null || y==null)
            {
                return false;
            }
            if (x is Agent && y is Agent)
            {
                return ((Agent) x).AgentID == ((Agent) y).AgentID;
            }
            if (x is Account && y is Account)
            {
                return ((Account) x).Id == ((Account) y).Id;
            }
            if (x is BulletinBoardPhoto && y is BulletinBoardPhoto)
            {
                return ((BulletinBoardPhoto)x).Id == ((BulletinBoardPhoto)y).Id;
            }
            if (x is BulletinBoard && y is BulletinBoard)
            {
                return ((BulletinBoard)x).Id == ((BulletinBoard)y).Id;
            }            
            if (x is Area && y is Area)
            {
                return ((Area)x).AreaID == ((Area)y).AreaID;
            }
            return x.Equals(y);
        }

        public int GetHashCode(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
