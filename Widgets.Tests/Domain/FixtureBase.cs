﻿using FluentNHibernate.Cfg;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;
using Widgets.Domain.Entities;

namespace Widgets.Tests.Domain
{
    public class FixtureBase
    {
        public ISession Session { get; private set; }
        private static ISessionFactory _sessionFactory { get; set; }
        private static Configuration _configuration { get; set; }

        [SetUp]
        public void SetUp()
        {
            Session = SessionFactory.OpenSession();
            BuildSchema(Session);
        }

        private static ISessionFactory SessionFactory
        {
            get
            {
               if (_sessionFactory == null)
               {
                    var cfg = Fluently.Configure()
                        .Database(FluentNHibernate.Cfg.Db.SQLiteConfiguration.Standard.ShowSql().InMemory())
                        .Mappings(configuration => configuration.FluentMappings.AddFromAssemblyOf<Residential>())
                        .ExposeConfiguration(c => _configuration = c);

                    _sessionFactory = cfg.BuildSessionFactory();
               }

                return _sessionFactory;
            }
        }

        private static void BuildSchema(ISession session)
        {
            var export = new SchemaExport(_configuration);
            export.Execute(true, true, false, session.Connection, null);
        }

        [TearDown]
        public void TearDownContext()
        {
            Session.Close();
            Session.Dispose();
        }


    }
}
