using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Security;
using Widgets.Domain.Entities;
using Widgets.Models;

namespace Widgets.Tests
{
    public class TestHelpers
    {
        public static List<Residential> FakeResidentailListings()
        {
            var fakeResidentialListings = new List<Residential>
                                              {
                                                  new Residential
                                                      {
                                                          ResiID = 123,
                                                          AgentIndCode = "agent1",
                                                          AgentBrokerCode = "broker2",
                                                          AgentBoardCode = "board1",
                                                          AgentBoardProv = "Saskatoon",
                                                          Address = "123 Fake Street",
                                                          BrokerName = "Fake Broker",
                                                          City = "Springfield",
                                                          InternetComm = "Description",
                                                          ListPrice = 129000,
                                                          MlsNumber = 445566,
                                                          SqFootage = "1200",
                                                          PropertyType = PropertyTypeConstants.Residential,
                                                          TypeDwelling = TypeDwelling.Condo,
                                                          SubAreaName = "Suberbia",
                                                          YearBuilt = 1965,
                                                          Bathrooms = 1,
                                                          NumbBeds = 2,
                                                          CoAgentIndCode = ""
                                                      },
                                                  new Residential
                                                      {
                                                          ResiID = 1235,
                                                          AgentIndCode = "agent2",
                                                          AgentBrokerCode = "broker1",
                                                          AgentBoardCode = "board1",
                                                          AgentBoardProv = "Saskatoon",
                                                          Address = "111 Street",
                                                          BrokerName = "111 Broker",
                                                          City = "Fieldspring",
                                                          InternetComm = "111 A longer Description",
                                                          ListPrice = 239000,
                                                          MlsNumber = 235566,
                                                          SqFootage = "700",
                                                          PropertyType = PropertyTypeConstants.Residential,
                                                          SubAreaName = "Acreage",
                                                          YearBuilt = 1937,
                                                          Bathrooms = 3,
                                                          NumbBeds = 3,
                                                          CoAgentIndCode = ""
                                                      },
                                                    new Residential
                                                       {
                                                           ResiID = 1237,
                                                           AgentIndCode = "agent2",
                                                           AgentBrokerCode = "broker1",
                                                           AgentBoardCode = "board1",
                                                           AgentBoardProv = "Saskatoon",
                                                           Address = "some Street",
                                                           BrokerName = "some Real Broker",
                                                           City = "Springfield",
                                                           InternetComm = "333 A longer Description",
                                                           ListPrice = 126000,
                                                           MlsNumber = 129000,
                                                           SqFootage = "2680",
                                                           PropertyType = PropertyTypeConstants.FarmsAndLand,
                                                           TypeDwelling = TypeDwelling.Acreage,
                                                           SubAreaName = "Suberbia",
                                                           YearBuilt = 2005,
                                                           Bathrooms = 4,
                                                           NumbBeds = 5,
                                                           CoAgentIndCode = ""
                                                       },                                                  
                                                       new Residential
                                                      {
                                                          ResiID = 1299,
                                                          AgentIndCode = "agent6",
                                                          AgentBrokerCode = "broker1",
                                                          AgentBoardCode = "board1",
                                                          AgentBoardProv = "Saskatoon",
                                                          Address = "Nowhere Street",
                                                          BrokerName = "111 Broker",
                                                          City = "Fieldspring",
                                                          InternetComm = "Nothing listing",
                                                          ListPrice = 709000,
                                                          MlsNumber = 98732,
                                                          SqFootage = "2000",
                                                          PropertyType = PropertyTypeConstants.Residential,
                                                          SubAreaName = "",
                                                          YearBuilt = 2012,
                                                          Bathrooms = 4 ,
                                                          NumbBeds = 5,
                                                          CoAgentIndCode = "agent2"
                                                      },
                                                     
                                              };

            return fakeResidentialListings;
        }
        
        public static List<Commercial> FakeCommercialListings()
        {
            var fakeCommercialListings = new List<Commercial>
                                             {
                                                 new Commercial
                                                     {
                                                         CommID = 1234,
                                                         AgentIndCode = "agent1",
                                                         Address = "1243 Real Street",
                                                         BrokerName = "Real Broker",
                                                         City = "Fieldspring",
                                                         InternetComm = "A longer Description",
                                                         ListPrice = 139000,
                                                         MlsNumber = 335566,
                                                         PropertyType = PropertyTypeConstants.Commercial,
                                                         SubAreaName = "Another New Area",
                                                         YearBuilt = 1973
                                                     }
                                             };

            return fakeCommercialListings;
        }           
        
        public static List<ForLease> FakeForLeaseListings()
        {
            var fameForLeaseListings = new List<ForLease>
                                           {
                                                  new ForLease
                                                      {
                                                          ForLeaseID = 1237,
                                                          AgentIndCode = "agent2",
                                                          Address = "333 Street",
                                                          BrokerName = "333 Real Broker",
                                                          City = "Springfield",
                                                          InternetComm = "333 A longer Description",
                                                          ListPrice = 454600,
                                                          MlsNumber = 367836,
                                                          PropertyType = PropertyTypeConstants.ForLease,
                                                          SubAreaName = "Suberbia",
                                                          YearBuilt = 2005
                                                      },
                                             };

            return fameForLeaseListings;
        }        
        
        public static List<FarmsAndLand> FakeFarmsandLandListings()
        {
            var fakeFarmsAndLandListings = new List<FarmsAndLand>
                                               {
                                                   new FarmsAndLand
                                                       {
                                                           FarmsAndLandID = 1236,
                                                           AgentIndCode = "agent2",
                                                           Address = "222 Real Street",
                                                           BrokerName = "222 Broker",
                                                           City = "Fieldspring",
                                                           InternetComm = "222 A longer Description",
                                                           ListPrice = 439000,
                                                           MlsNumber = 23111,
                                                           SqFootage = "1723",
                                                           PropertyType = PropertyTypeConstants.FarmsAndLand,
                                                           SubAreaName = "Petoria",
                                                           YearBuilt = 2011,
                                                           Bathrooms = 3,
                                                           NumbBeds = 5
                                                       },
                                               };

            return fakeFarmsAndLandListings;
        }

        public static List<Location> GenerateFakeLocations()
        {
            var locations = new List<Location>
                                {
                                    new Location
                                        {
                                            Active = true,
                                            AreaName = "Briarwood",
                                            //AreaCode = "B3",
                                            AreaID = 450,
                                            CityName = "Saskatoon",
                                            DistrictCode = "S1",
                                            Latitude = 52.12656,
                                            Longitude = -106.66248,
                                            SubAreaName = "BW-Briarwood"
                                        },
                                    new Location
                                        {
                                            Active = true,
                                            AreaName = "Martensville",
                                           // AreaCode = "S6",
                                            AreaID = 55,
                                            CityName = "Martensville",
                                            DistrictCode = "MA",
                                            Latitude = 52.28949,
                                            Longitude = -106.66845,
                                            SubAreaName = "MA-Martensville"
                                        } ,
                            new Location
                                {
                                            Active = true,
                                            AreaName = "Arbor Creek",
                                            //AreaCode = "S1",
                                            AreaID = 444,
                                            CityName = "Saskatoon",
                                            DistrictCode = "AR",
                                            Latitude = 52.13222,
                                            Longitude = -106.56806,
                                            SubAreaName = "AR-Arbor Creek"
                                        } ,
                                        new Location
                                            {
                                            Active = false,
                                            AreaName = "Aberdeen",
                                            //AreaCode = "S7",
                                            AreaID = 535,
                                            CityName = "Aberdeen",
                                            DistrictCode = "AB",
                                            Latitude = 52.32612,
                                            Longitude = -106.29155,
                                            SubAreaName = "AB-Aberdeen"
                                        }
                                };

            return locations;
        }

        public static List<Account> FakeAccounts()
        {
            return new List<Account>
                       {
                           new Account
                               {
                                   Id = new Guid(),
                                   AccountType = AccountType.Agent,
                                   Name = "Fake account one",
                                   BrokerCode = "Realty",
                                   AgentCode = "Agent",
                                   BoardCode = "Saskatoon",
                                   Email = "realty@mail.com"
                               },
                           new Account
                               {
                                   Id = new Guid(),
                                   AccountType = AccountType.Broker,
                                   Name = "Fake account two",
                                   BrokerCode = "Remax",
                                   AgentCode = string.Empty,
                                   BoardCode = "Regina",
                                   Email = "remax@mail.com"
                               }
                       };
        } 

        public static List<Agent> GenerateFakeAgents()
        {
            return new List<Agent>
                       {
                           new Agent
                               {
                                   Active = true,
                                   AgentID = 1211,
                                   AgentIndCode = "almte",
                                   FirstName = "Terry" ,
                                   LastName = "Alm" ,
                                   Company = "Realty Executives",
                                   Phone = "(306) 373-7520" ,
                                   Cell = "(306) 241-0432" ,
                                   Fax = "(306) 955-6235" ,
                                   Email = "terryalm@terryalm.com" ,
                                   FullName = "Terry   Alm" ,
                                   Website = "http://terryalm.com/?Bb=WE&Cc=1690618",
                                   AgentTitle = "REALTOR�",
                                   UserName = "terry",
                                   Password = "password",
                                   Role = "User"
                               }  ,
                               new Agent
                               {
                                   Active = true,
                                   AgentID = 63,
                                   AgentIndCode = "almje",
                                   FirstName = "Jenafor" ,
                                   LastName = "Alm" ,
                                   Company = "Realty Executives",
                                   Phone = "(306) 373-7520" ,
                                   Cell = "(306) 241-0432" ,
                                   Fax = "(306) 955-6235" ,
                                   Email = "j.alm@realtyexecutives.com" ,
                                   FullName = "Jenafor Almm" ,
                                   Website = "http://rexsaskatoon.virtualdatadev.com/default.aspx",
                                   AgentTitle = "REALTOR�",
                                   UserName = "jenafor",
                                   Password = "password",
                                   Role = "User"
                                   
                               }    ,
                               new Agent
                               {
                                   Active = true,
                                   AgentID = 82,
                                   AgentIndCode = "chaije",
                                   FirstName = "Jeff" ,
                                   LastName = "AlChaim" ,
                                   Company = "Realty Executives",
                                   Phone = "(306) 373-7520" ,
                                   Cell = "(306) 241-0432" ,
                                   Fax = "(306) 955-6235" ,
                                   Email = "jjeffchai@realtyexecutives.com" ,
                                   FullName = "Jeff Chai" ,
                                   Website = "http://jeffchai.com",
                                   AgentTitle = "REALTOR�",
                                   UserName = "jeff",
                                   Password = "password",
                                   Role = "User"
                               }
                       };
        }

        public static List<AccountOffice> GenerateFakeOffices()
        {
            return new List<AccountOffice>
                       {
                           new AccountOffice
                               {
                                   Name = "Realty Executives Office 1",
                                   Address = "123 Fake 1",
                                   City = "Saskatoon",
                                   Province = "Saskatchewan",
                                   Phone = "(306) 373-7520" ,
                                   Fax = "(306) 955-6235" ,
                                   Website = "http://rexsaskatoon.com/office1",
                               },
                               new AccountOffice
                               {
                                   Name = "Realty Executives Office 2",
                                   Address = "123 Fake 2",
                                   City = "Saskatoon",
                                   Province = "Saskatchewan",
                                   Phone = "(306) 373-7520" ,
                                   Fax = "(306) 955-6235" ,
                                   Website = "http://rexsaskatoon.com/office2",
                                   
                               }  
                       };
        }

        public static Residential GetSingleResidential()
        {
            return new Residential
                       {
                           ResiID = 123,
                           AgentIndCode = "agent1",
                           Address = "123 Fake Street",
                           BrokerName = "Fake Broker",
                           City = "Springfield",
                           InternetComm = "Description",
                           ListPrice = 129000,
                           MlsNumber = 445566,
                           SqFootage = "1200",
                           PropertyType = PropertyTypeConstants.Residential,
                           TypeDwelling = TypeDwelling.Condo,
                           SubAreaName = "Suberbia",
                           YearBuilt = 1965,
                           Bathrooms = 1,
                           NumbBeds = 2,
                           Latitude = 52.32612,
                           Longitude = -106.29155,
                       };
        }

        public static FormsAuthenticationTicket GetFakeTicket(Guid id)
        {
            return new FormsAuthenticationTicket
                (
                2,
                "username",
                DateTime.Now,
                DateTime.Now.AddHours(2),
                true,
                id.ToString(),
                FormsAuthentication.FormsCookiePath
                );
        }


        public static Account CreateAccount(Guid id)
        {
            var account = new Account
                              {
                                  Id = id,
                                  Name = "some Account",
                                  AgentCode = "agentcode",
                                  BrokerCode = "brokercode",
                                  BoardCode = "boardcode",
                                  AccountType = AccountType.Agent,
                                  Email = "account@mail.com",
                              };
            return account;
        }

        public static Account CreateAccount()
        {
            var account = new Account
            {
                Name = "some Account",
                AgentCode = "agentcode",
                BrokerCode = "brokercode",
                BoardCode = "boardcode",
                AccountType = AccountType.Agent,
                Email = "account@mail.com",
            };
            return account;
        }

        public static Agent CreateAgent(Account account)
        {
            var agent = new Agent
                            {
                                Account = account,
                                Active = true,
                                AgentIndCode = "test",
                                FirstName = "jon",
                                LastName = "doe",
                                FullName = "jon doe",
                                Email = "jon@mail.com",
                                UserName = "user",
                                Password = "pass",
                                PhotoMimeType = "image/jpeg",
                                Photo = Encoding.ASCII.GetBytes("0xFFD8FFE12DC345786966000049492A000800000011000F01020009000000DA000000100102000F000000E40000001201030001000000010071731A01050001000000F40000001B01050001000000FC000000280103000100000002007E7F31010200260000000401000032010200140000002A010000130203000100000002007E8098820200050000003E01000069870400010000004401000001A403000100000000007F7F02A403000100000000007F7F03A403000100000000008A8906A403000100000000008A8A0AA403000100000000008E8D0CA40300010000000000908F3804000046554A4946494C4D00FF46696E655069782033383030202000FF480000000100000048000000010000004469676974616C2043616D6572612046696E655069782033383030202020566572312E303000323030333A30383A32392031343A31383A3037002020202000001D009A82050001000000A60200009D82050001000000AE020000228803000100000002000404278803000100000064000A070090070004000000303232300390020014000000B60200000490020014000000CA0200000191070004000000010203000291050001000000DE02000001920A0001000000E60200000292050001000000EE02000003920A0001000000F602000004920A0001000000FE0200000592050001000000060300000792030001000000050019190892030001000000000019190992030001000000100019190A920500010000000E0300007C920700120100001603000000A00700040000003031303001A00300010000000100010502A00400010000000006000003A0040001000000000800000EA2050001000000280400000FA20500010000003004000010A20300010000000300135117A20300010000000200234200A30700010000000372820901A30700010000000128292A000000000A000000D00700003403000064000000323030333A30383A32392031343A31383A303700323030333A30383A32392031343A31383A303700200000000A000000F8020000640000006202000064000000820300006400000000000000640000002C01000064000000580200006400000046554A4946494C4D0C000000140000000700040000003031333000100200080000000201000001100300010000000300000002100300010000000000000003100300010000000000000010100300010000000200000011100A00010000000A0100002010030001000000000000002110030001000000000000002210030001000000000000002310030002000000FC03FD023010030001000000000000003110030001000000000000003210030001000000010000000011030001000000000000000111030001000000000000000012030001000000000000000013030001000000000000000113030001000000000000000213030001000000000000000000000046494E4520202000000000000A0000002C0F0000010000002C0F000001000000080003010300010000000600E37012010300010000000100E3701A010500010000009E0400001B01050001000000A6040000280103000100000002009A3B0102040001000000AE04000002020400010000000D2900001302030001000000020031190000000048000000010000004800000001000000FFD8FFDB00430004030202030303030304060404040406050506060A0D0F0A0709090C12100E0E0D0E0F0F0E1216111014181816151C140E101A1B1A191D1F1D14181F1A1A1A19FFDB0043010404040605060C06060C190E0E111919191919191919191919191919191919191919191919191919191919191919191919191919191919191919191919191919FFDB0043020404040605060C06060C190E0E111919191919191919191919191919191919191919191919191919191919191919191919191919191919191919191919191919FFC000110800A0007803011200021101031102FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00FA9A5B6B95E14E0E46315D2AF81B560724038F7AF6BDBC7B9F3AA8BEC6040D7C9B5B041C735D62783AF4800B2AE7A9F4A5EDE3DCD161A4F5489BC3F3EACE31200220BD0F7A9A0B0BAD39CBBA348B8C0D86B9EBDAA2F775675514E93F7AE6D552875A8DDB6488C871DEB8E54671DD1DCB1107D4B86445FBC703347CAC3A641ACCB6DBD623648848B8EF46020E3A67F2A69D9DD18D6846AC796AA2AC9692AE703233DAADEF218024107A568AABEA79D5729A32768369FDE67082790908338EB5A266894905B906ABDBBE8888E4F4E2FF00793FD3FCCCC9EC668C28DC09638007535A9B9091D338E29C710D6E8A9E4D4DB4A12FD7FC8C97D1EE963691C8055738EF5AC429182320D38E2E49EA8A9E4B4B95F2377FEBC8E68C6719CF7AE81EC6D19193CB0031E481CD752C747B1C2F26AA93774D9CE346A393CD6E9D234F78D8226096EBDC62B458D82767732594D571E6567F3FE91CFB0639AD1BDD29622A220CCB8C9CF6FF003F4AE8A58984F6672D7C1D4A3F1AB192401D4743FCEA66888CFEB5D31A8737215B61CE6A630E7AF7AB5510B90EB4B100F1DAB98BDF1B4562E934819E333246AA809277103A0EB8EBC64E3B57CF7D4EA5B98FAF96611BDB63619F52694F2BE5E7B9ED496DAFE837F0BDC5ADCC72A2799BB61E9B3AF1D78C8FCC7A8CCF372E8E36264A6D39277F99755D1976B364E2982343D2B2B22E356A5ACE37449E547C160091D0D33CB146BDCA556DF63FAFB89B02982439C354B4CE985583D10E247D291A40B8F4349153925BBB0C9EF2CAD63596EA54851A58E20CE4005E460AA39EECC401EA4E2B10EB2FA9F89D74FB6765B5D121F3EF5D4B00D733A911C44A9C1DA84BBAB2F1BA26156A0EDCCF632F6B4EF64D5CD86B6DAD2485892C4E2ABEA1AB4D04F65696E0192E7CC91DB72FEEA28C659F6960C46485F94360B024629C64DD919D6A5049CFFE09526D567B60C0120AF19FA1A6496F2DFA79B6C7259DD76B707E4620FF002AF429AA4D7BF63814A49DA0CAD75E389E21188E3CB06C3FB8AA775E12D679262DC84F3B4826AE34B0EDD924FE6394EBADDB44717C4FFB2B31B884BAB1F53F2D615E68D7F1160548FA8AD9E1E8CB78992AF5A1B48EAFFE168F87A4BB8504DB2DFC96691886CEEEC0000F4FCABCE2E2C2FD5D80420E79E29470345C5AD4A78CAD74EE7AC1F1EF8544863FB5AB60819507FC2BC6E4D3EF93030464F6A51CB293FB4C6F31ACBA23DA4C3A5EA2924D66E19864FCBDC9F5FAD795685AD78834DB9436F26D39C1DDD083ED4E783A94D7EEA7F7983AB4AABFDF42DE87A4AE9B77B82F96431E80D5F83C45A7C8B12C92AA48D186392319FCEB9278AA8AFEE9A53CB29CADEFFE1FF04F9D3C7DF16ADA3F10F8734316FF00B844D4A6D6E1B989F6AC0A7CB8D925457203B63122640247E1F327FC2CCD5E4F11A6AB1787B4FD1E2360033DEA0F2809C0092A5B8DFBDA22464450106303CC8D94B97F3FEB7526F9F9BDD5D6CFFC823156575AB3DC2D3F6A4D3344F0D695A6CC8EC2F6CEEE5F3E3B76684AC923A95413343953C82158AA9C01F7735F2BAEBF6317D89FC869DEC6FEDE79A2955C452B2ED2555D1E373BB3F3E1D08E70CA7AF24B32AD51283B7DDF8EE6B1A5CB2BDFFAFEBFAEA7DE5A67ED22C34CF040D52268B50D76055923B679196CEDACD819659613E5CABE644D2367326362363390BF19CBF153C5973A5D84770904D68D6B0D909D60446678531B04A1159BCB56018172769048C85A53CC2114B7BFF57358A9DDE9FD743F45BC49F1BFE1DF86AFE3D1EF2FA3935396558C5AABA8642FB42972C542AB1650093C93E8188F81743F13ACA64D6E6BF9AE2FECADD1ED6D66050DC0F363DA81D59D893972AD8523CB5C3E76AD6B85C542ABBC959155DCF96F1D3F13F472E355115ED8D8AC7BDEE37B93B946C8D07CCD8272704AA9C7F787BE3E76D0FF006C3BEB5F0BF8C2FF005BD26DEC74DF0F4A2C2C2CACD97CF458B8066DF338546CAA285497A332891436CED828B574F4EE6539CFE2EDD3BFCFFE07EA7BBF8C3E227843C0DE1ED4BC4BE24BB8ED2CACADE597F78E8AD332216F2E3DECA0BB00768CF26BE1DF8E3FB40DCFC61B3F0E7853418CD938D333776B6D2FDD134A2410C8C8C5594246858148CAB61B8C14ADA142C9CE5B7E64C6A7B5B46D6EFE56DCF6BF837F1B75ED6CCBE25D4EE61B28B50BE87ED96CD36E79A6BB95942A44C819DD595D22F2E41B5176334E62555F14F0778B6E3C0FE1FF0018BE9DA74C9AAE9BA659DA5F5C34130293CCB3798F246EE5566563B1588DD944500F9CFB6E389A5529B725B2FB974F99CD89A12A7594693FF27F89F5EB7C4DF0778AEF351F0F5A3C96FAB69773047731B14F3A15370A8E14AB49F7860363214301214CE07C35A1FC59D7BC31E39B3D7AF2D425F594C24B8D36379E1605976DC38D83729605F811854DC768E30BCF431B4A32D2E97F5F930AB86AAED7776BFAD3E47E8278622BF8EC6EA2BCB8135D2DECAF344319B73280E11B92431521DB2EFF00331C36DDB8F9AAC3F6AFF1021D6A7B0974FD1F4D6B5D42E2660B08BD9EF22B78BE7659E7B68B123B995766F73B36940335D1392AAEEA48C15197489F59ADCCAA705B200C91DEBC43E0E7ED04FF001835EF115E2C56BA65969F676262B68DD1E563335C031CADE5E18150AFFBB6C2E00DCD99338B842DB9D34E589BDD37A7767B64D63A75D865923C961CE7A66A558A1FDD23480C8C9BB8C7CC063240F4ACD54E5DA477C6159FC7047332FC3AB832656452A49E076AE9A692DADC2C93CC230D22C6BB980DCCC7000CF727815AFD6A63FABABFC2EDEA8F3AF17E972787748BBD4258F08919513AE084620EDC8246727014752C401D6B3BE2447E248F59D1B46F0D4F2AC1E24BC89EE9E27755B3243279D88D497490B2ABE4945217701E62B0B8E2A56DC8AB874A565FA1AFE07F0D2EA3A2D86A3E21296F7925A896EACC91BADE4DCC5F2D91F260AED0515801F31249C709AF78CF46B4F87FE1DBBD32E6DEDB5E934EB1BC3A65BCB0F94CA62965903B3C49B7CE8DDD9DB01B382030C871579DBE3B12A118BB38737F56FEBFE09EEB068BA22C4BF67853CB6505597B8F635E096BFB61F8734EF095BEB92DC437F0B2B436C365D40F2C88C80C7FBC8E54631A1DD232CCC37600DC0872A6A4B5553F121E2A0AF19533E20B86D4AEB51D2ECFCA863B252B6D3132FEF3CB7CEE3B5F05430C7CC4103B90385C0B8BED3B5765FDEBBC4AD105541FBB5E0901A4203606071CE707046EC8F8E84392EED6FEBBEC75A69EE422C23B29B4DD235965B1B7866B4479633B9BC99412F22E0918EF8F94E491FC2D8CED435AB27B5B40A05CBC6BE423E0F1C1F95B8C70391B403C8273CD7753F7A6E4D5FF0026268EF3C51E2AF03DBDA5CD87832C9AE34E5D59AFEC2F3546537B700AED2922A145DAC31FF2CF7023E57019B3C5D95C5CC905B4B3C02E6DAD2EA3DB14D9F2C198E7E620A9DCE136F04640041F945293E595ACADD925F9B2630EECA3AF7C5AD56049746F0F4ED6166C3F7D24617CD98B0191E66D0E14E06406009E71C80303C5FE1FB4D3AEADAE6C9A792D2FA149639648F6A3B81890230621823E471FE19F4B0D184617823AE3056347C2F75E26D7A587C1B65770449ACDCC3066E5C22799BB2859F8E87206EC8193D3A8C7D0D5ED239B51687CCDAE91231076C6E7E6049C7700E0719C1F4AE8751DEC8E8A7453F7A4F43DC3C307C21E11D6AEEEEDAE3FE12AB2B42F1C7FDA11F949323288D5888E762368C152B3803AF38AF3FD374EBAD2F45B0BA96433C77516EB88F23E5573F2E3A9FD3F3C1AC6A622728FB3BD9AD7FAF9055C252927ECD6AFFCFF00CCF76F88FF001F757F8817FE16D5F4B9ADD2DB4BD26E34C8EDF7CA67B74503F792C9C1673B8004C8C3824C614B07F326F1087D36D5432222DBBA036D16D2A0ED5632BEECEF73CB360E380091F22F9F5EAD5826A6CF3DC3974917A3FED845B59A075996D6D0416A77EE931B94903600D972DF2EFDE072379238A09E2C7D4669D2D808A68AE00B708E50AF9FB814C2F940F99B8A1C13C31C16E08CB9B4F7B6FF0031E9BD8D1B6B8F12EA506ADA4EB704F26B33C70DAC504ACDE7C92293E5B36633B86328BBA446C95C3050C1B9BB7D79355D42DECFEDB0DA40B135BE66591B291A0DA088D4BE0E02A641C0FBC42800744A528BBA56B0B4D4EF353F1378A043A8F8AB5479A74D42EE5BDB8B8741FEBF0C63495DA22C8C773B908E72AC300115C2EB9AABCDA7A5AADD491DBC2B014742A3CF8D818FEE0233B4118F98700E49E2A5CE73D14BFE0F7FEB41A49C8F6AF01FED3FF107C2569E1CD2B49B89EE4D82B4E3CC0CC2EA72AA1E33C72AA83E5DC491CE186703C4AD1EFA28E5BFBA9D525967F2D4BE5BF7A085F98631B5436E1B81E723391533A9561649F9F97FC309C52D4F77F1D7ED4FF103C4096F1DC5E9B4B78A08C4F2C52BED7746678D981C1E0751BC8520300096DDE4B02E9925D4D23DCF9AD1DB070F327EEC9DFF003A92E738D9920A24AC46303A159862AA356BE8125795DF53D7BC43F1C56F74EF0D68371133EA1A4DD5E4960130FE4CB7378B202772CA5919557CB60C492EDB9D82957F0AD4AE9AE60BA7B657961866DB12AA13E5053F200460F1DF861D3924015ACB11564ECDDBFAEE4AA6AD689EA53FED01ADC9A1C967229DF069F058D9B4531F9516131BED425BE663856DBB014F959496CD7905C6AFA7AC17E16E19ADA1B9F9638C48038248E19B042919C12BBC1C020F243588AFB37E41ECD743B9D17E22D8B6AB0EB1AD0B882D2E669A7596D2355958E193116D78D061B706C1518E3078C71D636C979E58BD81E65B685618D633B583F52B9DA4051F311C1391824F068856509F34B57FD7609415ACF531B4FD42C6EB51BAFB44EFF69B89FECBBD49124C1D583BE4B60B1EA416E739E315911E9FA8476443BB5B5C0BA468A2287E753C330C771C630A73CF2303315692934AFF00D7F572F91B26BBD4358B988DA2C8C2CECE3D90AAB168D43306C649EBF419E30471C5DD3742D4EE6FB51115A91224AB20122ABF9484E49236E1B839E00E01E315A38C69DA52B6A36AE8B7E13D1E7F17788F46D06C92383EDB76CF33BE3F75128DF2927723108ABB800771E80EEC674FE1AEA6346F895E1817762B2319CDABC6923AB31B98845BD981CF1BB7638079523048AE8C3528CA71EDEBA7904972A6CF79BFF04F82D22B0B29B4A82ED34EB6315AFDA7F7C515C066E64DD9E4719CE0640382736B539C170D82A07033E83A57BCF10A0AC91CB1A2E5AB6CF2AD73F66FF036A41DF4A67D22727E5F2C968C9CE79463E9D36B2FE3D2BB7B8B89CA1F298260EE24F4183DFF00C8AC67885BF2DFD0DA34A4BA9E317FE07F1BF87AFACB48D4AEA0BDD3AEE3B92B244AA24F2E2DB8DC4AEE1FC2301DB8246704E7A2F891A8DCE93A9D8C92C464924D1B6C72CACC21F3165CCA10018276ED27E6E98FA1F3F155E36BC69EAFD0ECC2D384677AB2B7DE7316968E1AE6CE4B8658DD762C9805A323A1C1E3F5524719154EDB52D62EC9648EDA74115D379B09C92215273E596DC0363E52CA33CE01208AE195793D271D3D4F4EB4B0B283BCBF07FF000079BEB1B510C52C934B7B9F26E2DDA2511F528C158B907E40304221CB11FC396E6126B89BCB2C1A69EE15C9C9F98920E09C8EBDFA9CE68F67CC9A3C4945743734DB18506A41E36937C88F6ECCCC5A0006F7647425738C16CA363239CF5D55D5DADF46B78AF8C96925BDDC28F08F955E3721D4BB2ED3953C90ECE4E140DBE5E0635A5249732BFE1FD7DE24EEAC54D43C5D75A6DB5C35A2AFD83516BAB648860AC3B4E488C06E76AB70D8C7CE76905035615A584AB332DDC5FE9171199610CA39690641DAE47041272437B0CF23684125A8ED66685A84FB0496E96A25596ED6F5E4C8F31C22F2BBC6E21554B642AA9C9CB7DC144963A82A4BABCBA8096DAF2C965B91E63EE24B96F29DDD41DE4A16E3209039A893BBBDC6A2D96A6F105B29D364C2C5FBE54BA0B9FB4140DF798C85B2C41DA3010617257924E135C4D0A59EA16ACF2942D8696205018CE4F566DDC60B6571962318FBCFD8251B5C3959AC9A8DD2DA1B75BA0D636F762172579DB202403F419C7CC0F518C0E312EB5078B4F914303F69B86C12A7CC60AA3BE300124F0BCE461BA0A9F62DBF30B6A5D8E316773712DBDC249319123440A4B32927D41000C007E607E6C0CE0E2B5F9F32737C0408B3C8CDE5DB7015890DB546777C9BB00F3F538CD6904ED6B8B93B9A73F8A3C44D23DB477093F98C8EBB883F71001F337CC06DC707D3A71C65C33E9FF6EB810C661B29199E3B591DB2CB92546FE06573DF00E33D78A8953B2D3FAFEBD44E2BA9EBFA8FC28D3A4963963B89636B73E544AE536A80E40C16DA3860383C93DFB54F0EBD793DAC9A55D2AEA1023C50AF9AC5644571FC0D90768C63E60CABD70339AF0A3ED1B5CB52EBD35FEBE67B12C2A57B59FF005E655D1F4AD235313EA2F6F241F6F90B4B198F6FCE41E002147CA41E031E064024AAD6D5CEB3A7DE58AC9656E74FB092F51A38646F3044AABD19C040BBB3FDC19F4042E66AD4973725B4F5BDB4F22654BD9BF79106973C9A3EBF69ABDB6991CD71A6EA56F73179E8248DE647CA12BBA33B4756DB8F94951C920747A05A3EB9269D0D94915BB6A17C2359AE724E66E0499553F22B74C02401F74E79CA956ACA71F65F169F3D7B7FC3FCC99A4F496C74BAC5A0B6BABB836EDF2E575EB9C73D88383FE78A975FB2D120BD9ED74912B58DB8FB3DAFDA197788D3805B680BB881CEDC0FC80AFD1A14DCA29B56D3EED2FBFF00C31E2AA9CAF7D9FF005A18914782589C0CE47B81DEA42A598BB739E493DE885171FEBFAFEBB153A8A464788FC29FDBD6B650CF6C27861B992539CED425768248F4CFB1F422AEEB36F3ADAC1710B4692AC5398770258396400820E47D71E98E40CFCE7124A74FD9FB3767AFE875E09F35DFF5D4F3FB9F87D616497F7B0E965A080A9791E2F923254ED504747720F1B7A0279EABD3CB736D7EE2054633796D2DC33E58052428C2FCD929F7473D0F52706BE72189ABCBCADF53BFD9293773875F04DB4114BA82FEEEDE3686158E53F348DB0FCA54A82141C1FBE327904D770DA59B685A71219E290BC8B122911CAA7EEC6FD0152CA4F049EBC9C552C454B5D3D3FAFB86A9F73828F45371716D65F670E561592D56401D033A87EBC0FE20718E0F041209AEF05B6B1A41B4D36D6EA344B6B9796D22620847994BB3090285C9C28625812140E7181BCEB34EE9D9BEA44A8BD2ECE1AD7E15C915D43E605B48E35822915DB72AB478DC7E5241CF6F988EFBBA3577CD6AF1C5751C6F2492456AE6D588E02B3EF3D33C633D09E1BAE3A64B17566ADCDBADD02A0AE99C8DD7C2BB5920B8433060254329724BBB61815CF3CF5C1E7FDEE78E9D2CF39B5F35DDCA4D70C779F94272A41E0F382A41E067F16955F10B5E6B02A09743937F831E1C6812D1B7091994078810EABB15482338CFAE0E09FCABB25BA9EDE059A380C118B70CC580277484AE37606E270719EC370C75A858DAEEEE32D7FA6374A3DB4389D5FE0A47AB5BC6D15E08272923C4361CBBBB86DCEDB8E4ED240C7181DCEE63DB4FA96A264692ED3CC882873F37320625B24B742DD7200FA735A2C762213D1DC1528BDD1E571FC22D7ADEE3C821DAD556E1048DB55C1DBB7E6209383C0232460E064024FAC3BC72457339B896D1161184055BCC957B2AF07249C72C303B6719DEA665896B6F98961E11D64AE8F1EB3F841E26BE6FB2DE5B48856558E28D406918156638C65805C0E3DCF4C357AC4B35C6A173E54B1466C1667DA51543386C9C8CED38C8E010B8E7E94E38EC4D47A2B193A50B68AE666889A9EADA2C375A8FCDA848099E452B89A312320765CE7240661F283B7241039A87C2B7775358C96D724C9676F36F31E1701DD5BA862010C060F392003C85DA7A731C0A83F694F67D3B7A0E8CB99D8D49135158AD7549A28EF3CCB844DCD962CCAF83C1C9FBC7182BB46785FBC4A5DEB9A25CA1817CE5091F90B2C83E6F94E4F2A3AE31C8CE7F1C5790A8D9EDA1ADDED737BE192EA9AA789FFE12462B0D9E8FA68D4A67B8793799B2446C8D1FCCB8665DBB9D149EE4ED06AF866FECF48B5D582C2D33DFC02CDA685C2AA2ACBBCAB260641201C638231918607E8324C1D3F6FCF2B34BA3EFD3EED4E4C749AA768F5FC0E9F52595A565DBB4AF403B571C658A5B98EE1617864556C31CE5777A11C76E7DEBEC2556EEC91E546165AB3A6543C86182DC0F7AC5D363BCBA64325C480798586D76C119E3F4A4EBC61AA5FD7DE354DB76B97B55BBBBB1BCD3E789DE2923859A36008DAD86E43763E9D3F1EDCF36B5797515B5A6A0AC9750595B9908704B3EC070E39E39E4107192091D2BE2B3AA8B115EEAEB974D7D6E7B385A7ECE1DEE69B2DC595A49ACCB048904ED1DB2B004A85C2AB6481CF033C0C60E0E4FCC73E66D56D84E93DAFD9DE2413AEF5E1001F311903A803A90324E00CD797F536F7674265E4BFD0AE37B392D006CF992753B09500F23863C9EA41CE79033990BDEDA693690DB5A2B5A3945490280D0C910932189DCF840D924920AE091952149E0E497BB704EECDFBA749EF76B47E4C232888C99F2E304950B938209E7A7B9E49AE4F48D6FC437AB1A5AC405D2CF1EC0C80A3A82594118652320374071D339C553C3568BBA45FB4EACEB9A79D2E56D6EADD95D49205C01B9B3C81D3A11CFCAB9CE3D6A28BE1CEB5731ADC5D42E6D2E6095ADEE15490A58023701B5B711F281860793C6012A182AAF560A6AFA1552FB4DB9B70EA0429E49F35E58D59C4A9BB68CE41039E70077386C62AFE99F09753D56C669F52BFB6B389658A2B7D8A492DB8025F6E18220049241248C0520B32ECB0D3D2E0E5D91CE964B891EE0CCAD12CCC56453F3153B70081B80CF5FBA7009272315E8EFF0F1D20B7B849185DAC691BAAF113AF3F2E30779552FBC15039C6E283274FAAECBA0297BBB1E663C4730653245B2DAE0BBA80A76B089B760121BD7191FA00C6BD2A1D3AEECE2BBB3BE202402577963380A5D47232A4A90323DB3D01C1A4B091490AEEE71970BA9DDB4AD22468B24AF010C48DACA7EE91F7BE99FF1AED67F0E409F3218E29A55334F143C87DA0F44231807279DBD07403074FABC34D762937D0C38FC2B73771C6F6D124B2CD1955B746C190A11D002F8EB920818C71EDD309240B1C775706F4593ED8C48305772E090000704FA000E739CF147B28757A12E2CF17D03E1F78A6F50B5B32DBD8C9B6392E5D72A849C642F24ECC83DB3DB3D2BD9347F02F8623D32D2DAF239AF20FB26D9144C046AE59770C11B4190E30C7247CB900671D93A92946CF533A94E2A5781E3971F0FFC5374262F2656DAE198AA90640BBCFF0011C800673C139C81CF15D95A681E23D36F60D12E0ACBB45B896EA30A55559D1CB21FBE5739076AF233ED5928DFA0F4BEE61F85748F19BDE5D9977DEC09A697B891033213113F302724E3D48EF9E0727D5E1D52E2CBC3B2585C5BC7717EF692A6F894AC28F2C5E512808525483C7DE2382D90327AF0D3F672BC7A98D58F34753CB85807B444317D98C9991D171F7DB96FBB91D7927D79CD36F0DFC705DCEF3473BC28ED90B8CEDCF1C311ED9FD2BDC945463AB3CE526DEC6BE98504B1A124F38E9D3354746120B88269253B58A03FDD5CE338E28538F460E2EDAA3BCD07C0D6FAB595DEADA3DBC51DFD94F235C322904A30CAB1C360E0EE249518EB93F280DF0AEADA95B437EF6B7496D131855CEE8FCC66F98854476507233BBAE38F515E5E60B96D50EDC3758B3B4D2FC33A2E9763E4EABA9C7A835E59B3F90B16CDAB9CB7CE490CD8036E131819C1ED466F10E908D79712233496FB258D22DC414182DCF5E78FEE8E7A8AF31D651DCEE54AE47A8786346BC7B2BA33BCF0DA58CF1CEF25C30777620FCCE8C3E76DA01DD920700E0F39C2F5EE44EB6912CEDB608C459604866180A7685214E4301CE4F1ED0EBAFE915ECD7566AFF66D8D9B456D05925BC6F7225552C8494C71C12A0B60F380BCF4273583AB7877C5DA835B69E165B733CF8B25489CC92F2C58951D98E002091923EA25D69F442718C773A0FF00848B4CB722488B4A1992691DC0701C67246460E3A0CE41DC467B564699F0CED9E495D5CDC279820DF13E3CB0EC4233641C11C630B8DC47CDC60E729D67BA2AF17A587CBE28D32FA6BB6854DCC97E63DD90A4F0A5481C721403DB9CF38C935AD378034686E5ED6E17FB2AE239920631ED66C000E55986430C8C72D8EA587228E4A8F761CCBB1CE3F8B3553099921DD2E12142C8A630367CA0630A0B0C8E83F0AEB352D3ACECAC638678DED4C96447EF6219510EE657EA79DCBF31C018DC3A939B5092DD829F53845D13525D3E364B8F2042E80468B911B3B6DCF1B597A64649039E7B1EC27F11E9496D248B2462386064457440FB541C338CFDCEBD083DC018357ECE36DC9BBDCC05D2B548668EF6E2DE4B44091F925CE37F5C92B9538604801940EE41E49F43D274BF0FDA69BA77893C577CB67697331BB5D36CDB75CDD6E3B49552CA522C86059DC01B71DC1AD6386D2E95BF025CD77B9C758DB5E8B6B81A8EA11D909A62EF320DCA1F249192ABC127182B90475E0D6F6BDE2FD4BC4DA9D95B5BDB5B697A44765E5C56E10C93C2C18B167B9701C824918071CE31C6594A118D95EE11727E4725E17F1D7843C5A6C2696FE5BA922B2B3BB2AC184615610305D5705C3105B25F0E4E4601C721E15F8E3A66A49A76917563B67BB2B15B3C65953CD40172A1700718E0001401C7ABA919415946DFD7A19C1C5F53D3935B6BFF0036FB5195AF84B23348EE48C9661BF23CC908CF19FDE67031B893BAB85BAF13F88EC6FE5105B2C93AA0DD1CAC41DC465B04E0851F5181D38E6B99D497546E9237B5AB1D223B7BFD5ADC2DDDFB42D1C97480F2250036E75DA4B2670324E7BEEE40C0D7F5E8F46F07DFCBA95B490DD1BD58EDA3C7C9246C589F9B767703D3E56F940CE0E09EAC23BD54DEDFD58CEAC5B8BE457671E1A5C794ED975665C1F4078C7B62B1BC3DE2CBCD7F5C874C86D4C2272FBE6791995028CE48D9D09E075C9E2BDA75E97577389E16B456AADF33A3B18D4490DB0E320A9F63C561EA3E27B0D32EF56D2648A54B984CD6EEDB71F390403C90DF98CE3B513C4D3B5D31D3C155935A3D7AD99DF787B4F8A649A4B8730247179892EE2AAB203D0901B3C1F43D72718C8DAB38B4ABBF0DF8584B2F94B2D959E5BCBE5B60038214101769C8C9DCC01CF385F27138975A2A363BA9D0F6727AF91B10F80AE3ECB2DCBCE254B78BCC89E36C1CA82D8FC48054964CF040208224D27578B4958E5B495E2586DC0966DC4B79A49F989CA390BF2E08E738050815CF1A71EC39487782F4D9679AE752B8D2DADAC2E2E84917988DBF722307478D1953690CCB90CF8751B5B279A579F1122D3135A8ED6D8C023B885206595CC6FE5BB797963C64283B495E5429E1464DB696A09396ECD981F417D4A2D000F32548FCA992D046C5DF0A11236263CB29278C025BA81839C6B8F146A56D31D4668A6B9D4DEED8AF9E0ABC52B82EAD2B6E077704F0E4919C64701732B8CD5BFF001BDBCD72C6C508BD10CA914430BE5F1939791C0049E7271EC72335CD5E78AE26BE6B9B7B3FB48B469229E28A79304AC8DBD999F7F04E40F2C60E3F88354F35B729AF33A2B4F126A3A9C324B7AE228C4ED6D2C854338DA80E01327DE390598231C6319CEDAC14D5354737CD6B6ADBE60B717C524729E5860A3CC20230C9540D9C1000EB9028525D026D742E2D8DB2ADDDFCEC6776852DE47219444A5D154314CEDDD26D0A46D393B70738397E206F115DF87356B09648AD2EA196DEEB4B9ADF005A4B6732CD956629B7210027ED51053B892B54A3AABE845EE528FE34F83FC39AD1D2ED208757D4F4B801BBB1862322916F361D195231E612483B720FCA43320F997CFBC3DF02FC41A1EB7A96A5A8411E936C02CBA43CEA8D2DB5CDB347246CCC0AED2833960060F02345C81A5A106BA992E69F91EAFA1DAF8C23B2BED47C4C925D6A5A9DF3626981924998AA8C973B954F50111E5008E2462771A57B2789EDAE2FAE18A5BD9DAAB88A498160AE7A95671E5E48078F98807EEF5230AB51BD64CDD599A377A8C063B78AD8C96CBB4EEF2C94058E31C052BF7473C12C7E6CD73D73ABEA925BAB3CB19B681DDBC964FDD28976E321700162371C64B1C13D0565ED13EA164BA1E55F0E13491ADCFAACD79159369D6A2544B86C34CCD85014A8C83B4924E4700E39C11E81E02F80CDA4599D76F6FDACAEAF34E9655F39010A5D3745B4348B92EB8604636AE7EF1E0F76266DAF74E0A50B6ACBD716B71732C57AA187DA6DE75D91F57317FD74F972CC4E3049C00719C0AC5D4757D46CF5916DA75C5C5C4892AAA05C32306042E4F030AB9FBC4FCB9F97AEEF3DC2FA33AA4DA4737E3D4D42CBEC1A7DD33B32AC9329618254B6D191818E54918C8C1E3D68F8A3730DCEB22EA033491BDBF0D32156243B06E0FBE7A63F1EA7AA93B276563B30F0FE636BE0DE91A8096F353B59843732DC476B015DA5C05219B3938504ED196C0233CF04576BE09F085ADAF8334D805BCD7334F69F6B4FDD95DA5955D8C84BAA81C9F2D8860429CF42A071E677B9189ADCCF956C796FC4F4B21E2DBE9ED030866D8482BB42853B5401D8600C0E31563E33F8B3478F5AD2EDEE956DA45B52C27DC4C52A9C7DC6627EE9C9C64F51824741537BEE7560EBAB38367ABE8BAE4F178434586E62D96E2CEDA676553E64B1C6873E59C4A32BC938539DD909900D799FC28F8C7A2EB3E20D23C3D2C1A7B4A62592D2636AAA4F94A55937301991D7273C67A658B102E542718F3496879D524B9DA89DB427C43A4DFBC570B216B284AB330251FE71F26E0DCAF7C8727233DC13DBD95CF84EE2D2DEF99E5B832AA398219231C92D9666540020F999739C8F941C039C546D764B4E47383C3D6FAA482EB10DA886DE46F272E1B799004F95412190BF450D92395E056378DBC45169DA8DB5FBDB5BB4061920B688B92D185890095C808FB9892CBF30048E84641A53B14A9B9EDAD8D7D13C3F0DEDEEA1730D90BABA5731B4916F291C58576914A160D80A08CAB1C16E87681E7779F16FC462F440C229591926920D81BE64552D9EAC09DA0BE194F5E94954D5F6365869D936CF643E119EDA46B2D2E382E6FEE6391E5B6888655DA0636CDC22EC27F85B8392090C49F0DB7F115DDDEA56975ABCA6E2297CB331998B31890700E32791F2804FD7039AAB396C3F66A11BDCF66D32C344FB39B79A53B2EAEE5953F7A5D2257FDE0044B91B63E898C70482C57695875AFDB8FC03A1E9FA568B0E8575ACDA5A4264916F9D6328EF924AE05C7DE6C91CA91C63A554E84D24CC959F6FBC2EA4D03FB3634323492DBB4E5E7BA6CC4C8872A006E4F96C7F87F84F51C1AE475FF89DE06D4FC3361AEE836736837926A4F692D9ADCF9912E1572EC321D77E542E1511B0C0E4824F1D5A9C964D1A28DAEFA1AF7DE27B3819EDE588412B0F2BC90DF70E4852A11CAE477C2B0C3118E32BC4DE6A32ACF2C5308EE658EF638DD616678DBCB66CED6570A4600195EBC1079DCDCD0C446A454A0EEBC8725CAED24D1D04FE27D31EC3CBB5DD2C6D7502BAE720C518386C10C7711B81E800C100E4E3954D5E15BB992F46E37D36660C41F34027009DCC08273C8519E3767F86A35E2FFAE9B194DA66E1D6ADED259AE4C2CAD748B70992C1991F0432F6C9F6EA3B9AA33C7AB5AA2DFE976CB67225DC91C534CF98E505402A41DF8642181CE4E7B63A2E79FD942954E4DCFFD9FFDB00430004010101010101010102030202020203020203030506070503040406090807070607070807090B09080A0C0C0B0A0E0A07080D0D0D0D0E100F0A0C0F0D0D0D0DFFDB004301040202030203060303060D0707080D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0DFFDB004302040202030203060303060D0707080D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0DFFC00011080800060003011200021101031102FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00FA980FBA71BCA9DCD4284DABC2A82769C7F157D03691F32DF2EC1FB9DD8524B1393BBB1A57257BA0456E58F5A12495D13CCE4F61AB3FFB79CBE148EC69C1586CE0904EEE783CD3E74D58232B3D45DBBB71C7071B7EA3D6906DE379076B1230714AC53924F40E386CEEDA703D73492B893EEAEC3F74E0F1F8D0DD84E6EE2EF56CB37C800DA9C526E39554000CE36AF4E29D9DB51A7F310A63E6639DDF8F02925F90FDC2371C9A2293DC26B9761FE6739C8C6307E9F4A6711FDE3F2E32B8FEB4A4EDB0A316F563B6E0938042FCCA3BE698CCF0AEE0090CC00C9E80D38393D01C7A923CA9FF002D4603100B9ED9FA544DBB2F1459073BF03934D46DADC96EC4C1F3888B1DA170A476A8B79F94B8C8620B91C62850BEC36EE4A25CF57CE1B03D48A6236E6DD1B9620F231D3150D58718C9A2512CA8FF00BA70368C063CE73D466A212B4AC16352A55B7107A534ADA82934580E836145019861B26A02483F74C8546EC9F7A4E29EE529345959B3BBE60C40C1C0E715045262364452589CA1278CD1CB604F4D4B50DC8CF2401CB1C1AACD3F67255C80A73EB472296E109B5A44B4976AB8F9B67CDB9BDC7E355FCD9827014AAB0079E4D0E2982AAD68CB3F68C65A425803BB27B541F6BE486CA127A018C8A126F414A696C585B81FDE677EADC7406A1FB4C8E7084EE071C7A53506C4E49B2C7DA42E79DCC00381502C8D8E55895E5B3D47E54AD61CA6E5A16BED1C329E09E4E7A8CD56F38EDE9952B924F6A2D7139F2AB16D672871828CC3073E955C4DFDE073C0E392684FA305371D8B31CA9C74015B0BC74F5A83CF3C311903E52474E7D68B5C6E5F796FCFCF5E070062ABA4DC2F04307C824F6A16852ACE3B96A298FCDC1214E7EB50C5293DF70CE00F4A4EC82355C996A297FD63F0CA460F3DEA1859770E3602DB58FA6294A5CA5C24FA96BED4B95C6E1900A923D2AB8B86F9B0DF2E4E5B3C9AA52525A9519A45AFB4348E79C13C83EB504723FCB27500E3142EC839AEEE5B82E015E8402C49C542927A12323383DA8E4686E5CBB976DEEBEE8DD82A7AFD6ABA339DBC640E01F6A4E29EE0A5734AD2F3EE0238079ED55ED67658D720671924F1C56556829EC51ADA55D67F76AA003C9E7A556B1B98F76F438C71C74AE4C4613B0E151C1E869A5CE3A8E738AAEB742555604938C83D3A573CA8337A7985486899685C21E3383540DFCC97630F94660BF5F5A5F5495AE690CDE6DD9A347F1CD36D9B31FD79159CA3CACECC3D7F6F1B8EA291A05140051400514005140051400514005140051400514005140051400514005140051400514005140051400514005140051400514005140051400514005140051400514005140051400514005140051400514005140051400119E08C823068A2F614E0AA2B48AB776C627E9804E47BD58B8844CB8C648E45694ABF2EE79799647CFEFD32991EDC9E07B53A58995C8C13CE2B6854523C8AD859D2766861FA63BD2B2955E9D4D36D3D4CAA41C756368AB46536D3B20FC3A51F87039A049D84EBDB91C8A0F1F8F5A099BBE8C3AFB0147E1DF26804F4D43D7B92334679E983D28172C5681DBAE39A0F5EB9A01B5B20FC33CE28FD28134E227E3F4A5FC3AF140B70FC79EF494007E19E31473E9918A003F0A2800FEBC51F86066800A3F0EFC5001D7AF20F5A4FC33EF400BFCBB51F8D00C3F0EDCD1F8639E2800FE406051F8628010FD39EA283D7DCF028017F0E01C7E349F31E7A00318F7A00339E41CF3CD1D0F5C6460D000DFEF15E33C5232654ED380071ED425706EC07F878DB9E0E3B527FBB8500F34D42FB86FB0BCF4C75E324D35941E338DC7239EE29F2D84DBE82F23EF10320703DA8DBCE704151C9A1BD2E0D5C0F5C018C8C9F6A3EF6FCB9218641EF496A0B45640C1464E59893C01E94830338072C31C537104D20CF4F4149CEDE46003B45351B0D3BEC19FF0067209C0A33CF5182303EB4EC0D5C3EFF002BCED1F30F4347299E3248C11DB145EE0D5C4CFF00CB4C820027E98A3F1DA09C6451B8AD65A0B9DDF7CE173B8E05232B74CE0E33CD1CA90D79812473F787047D283F7B76D032A0120FA50804DC0E47041E28FDE315E01C93B8FD29F2807CC1FE55195C2820FAD1C77F9727248F534EC019EBB08073F3679E693AB155CB823200EB8A1AB89268107D324E09148449D5B2AC0E0FD05009207C71F2B20C904FAD0F9F9768C853C92681B7611F79DF862400021FA51824C7FDC3D7EB40A4B9B40E58F5C0C633415FF59CFCB9F9493DE812F710981FDDDCB9C9C9EE28FDD6ED9B4963961CD08A4EE04B1EDC1381EF47CBB4865DC41DCA69B5664A7CDB0609EF83824FA0C5071EB86EA3DF14277D8528B8EA19EF8C100018EF4831BB39E40C1F6A1C4A8BBA177F7CE38C53727E76908000DC0F4E7D314D46DBA173B171F2678762B819E3348377CB924E4E78F4A39182A97172723001E79C72291DBE6EE0A8E7D08A6A3CDA0A4EEF503FED0CA938A471FDC24E48C7B554628ABBD83018FDEC85C8C8EC28F9F18DB80491C7418A1E9A0945A06231B70588381EE691F9E07009E71C51057612D16A2326DCF1824608279CD023CF2AFB7E6049EBC0AB6D226D7D000E3925481C63B9A5914EE94E461B914277D4695B463739E33F7707DE971EFC0E4E0D36D14D241B369F9892A067F0F7A0A8F42700600ED493B929F2EA2797F7411B806CD196E48253230DEF4D3636D35A88E3E7DC176AE70B8EF49B0AFF00ABC2AE72D8AB49F52538B1557FBC768279C76A4CFAF41D07BD055A2B413076B67E619C9FF6A94A9D92055C3633CF1427704B4B0D2ABB6291D7681C8C76A519C727A824FD4535A92E5CAB713E4C9939CB2E0E281820BB9CED5C9C7414EC0D296A21F981E32D8E00A0190C679570064374C0A0971560D863E19B1819EB43EEDCBB9C1620673D39F7A13B8E316B641BBEF1DC3230003D31ED4BFDFC0048500F140377DF4118975924080EE393EC6978242E36B13C11E9E942053E883E61E59DAAAB9E07AFD68E0B6036EC1C138E68D86DDC4F95E4C29C1E99EC28DD8DDB0E532030C739A3606D37A8A14AF72CC0E303DE91BE5F9F691825460FAD17B89452052EABBB0598B6D031DA8EDD4B0EA47A9A06DF2871FBC25B738C32AD0A032ECDB8C72B9EE686EC095C36EED9CB2824939FE4283D0E7818EBDA860ADD40E77ECDB82C393EDF4A1B603F23124F507D6849B0945475427E841C6076CD1CC8796CB03D4F6A6959EA2B37B07122F6201E7F0A6823CBE9C96C120F6A6D6BA0464DE8C5FE2E4F00E00A1B1CE32C07CA33DE9ABBD10DD83E56DFB8E48C3362936E0E71B4951F2FF850958972D74109FE20A546402683E66DC120306C9C770698DA907980B2AE482396F52293E63DB76D18F719A056930FBDB973B89381ED8A23E8DF29461FCA8128F306F1FBCD884155DCD9EE68557F5DA08C103AD16B0D45BD06F2DC98D806184C75269C07CDD770072D9EA2A9C93D8141ADC4F93B9CED3B5C0EB4BF349C05038E9D4FE34A2EC138DB61BE5C9C648DAC32BFFD734EF299C8C121538229F3B12831A0700329DC473E94FF00247F77007CC7DE8F68C7C961B1A91B78F98120E3B8A7FCFD89C11DBAD273627148622B7DE5380A48C74CD3B6AE3760BB05CED63439B634A4D68274EDC11838EA69464B6E082318C800F7A425AB07EE0609C6067BD031BB603827924F7A0AE56D88DF74FCD91D0E7B1A37F3F2292064364F4C5161B5741B4EF5DC3231C63D697FBBE58241E5FE94DC84A2968370DFEF3679C77A5C7C9C310A8496CD24EC12826274F9B1D4E0E3D6976FCB9DA5C118523BD529F7138586E46130B8F989C0A7633F2E0237DEFA8A14A2814589C67A0C838269319E98201E40F4AA524C49DB7039C9E4938C0FC28C622F33EE92E4107D28495EC813D740007FCB318E7271EB48491F36460004D371B073B13665BA9CB0DA00ED4EC67D88193F4A149AD0715CDAB0C62366C80B18F9BD4D35BE4F9946EDDDC534E4126A3A21709C75DA0E700E3347A9CEE19CE29CA56172B7B0DE85BE50D91C633D29437CDD3040C8A2ED09AB09B626C05248C6EE3B668023C6FE7682727D4D34545DD6A2F3F77A83C1A0F98593CA505836093D81A1BB04A2A5B07FB5D0A9047BD0D1F386C874048E69295C3D9D906E3FC7CB1048FA522FF10072A467DC1A62BDD5837AAF3DC9DBC75A178CF1924E466805268362E0FCB807961DCD26DFE3230D8C330EF40DA52D850A9FBEDC0648000F61411F7B272368CB1A023169682046F995382E0004D29E7F764ED5C726807AEE86F7D99CE38240EF49F37FAB403686EA3B9A7CA24FA30C20E33B8A92C7F1A07CAB938259B19F7A1BB028DF51576FF09CA81EBD7DA90F0FB785523E50077FAD16B835CA039E0212C4E78EF49974CED390460FE34EC24800DE09D9B7E62283C2FCA4E40F9714008FF2F39019464EDED4A5DBD0039C63AF0680B0824FF969F7491920F614BF7CF5C64019340584382C871801BA9EC28C8E76B0600907EA28017620298E3248FC28C2E41CFCC7900F6A0371AEAAABB3702BBB7107D29707EF6324F00509D82E378DBB36EE00E0FD2964122F7DAA3D2AE32BE80D58E7D17F796A119386C30EB8347CAFD30493806BCBBDCF764BA8BE66EDEAC5701B24FA1A4F9CE1642700E17E942889C9A40A4649DA6521700B5081BFBBF2962C41E9F9D538A424EE1F3B330126C246703B01EB49B7AF968725B2C0F7A13BAB5C7B0BF3F5CE031C1FA0A36B74C6DDCB91EFF5A2D6DC12132DF3B2282C4E486E845232731EC00B678F63ED4AF61AB837DFE40254066E7919A4977A8246D05DBE63D49F6A5CCD8E514B51279230D2754013E5EE1A9BDB7F039CAE3BD349C4A53F77411B608BE6396DDB8FE3498FDD920B236E254F5A716E2CCE4D31384F2F248DC7048EC2977EED87633A3360F6393551AB6DD038DF660B2F18500468A58E392D9A6F97B53EEE4676851C600A2D193BA05A6E3FED384F2B71076E4B8EDED51B4DBB3BC120280AA07F5A1413E809B44BBDF0ADB77231C2E78C9A8D5F1B376D2082AA00E452E4BA072B3BB1EBB82F94A412A771DCDD8FBD346E31B47B8020E47719A5CB6D5873C9920933214C14463C367A537796023CEF04602F439F5A12B824D2D592F9F90FB0805082E0F5C53371FB8E36956047AB50A56D013689049DF1BBE5CE47F214DCF1F74FC878FC69C526269BD8937963BF7952530431EE2A3223EBF788C163E86852EC35E64A199C8F97F76002CC0E0FE151E7CEEA4920601CF5C74C0A4984FDE7A684A8764BE6A962C176039FE751C1719DEA397039C76069BB2411D59309D738638270013D3F2A8FCC7F223E41249014F502A52B8DA5DC97CEFAB856DC703A62A2171D32D800E48A715CDB0E6D5AE4F14BFBAFBDBC93923BE2A25918F60373727DA9356145A5ADCB22718CE7EF36001EA2A11230E8C46D38E3AD2E5B04A5CC585B9CCC1F0158000E38CD4427FDE7CC72AA003EF9A1AB095BA9683C9BFE738054329F5A8229395192300B0C9EC7EB4AC6909A4598E603EEFCA01C13EF5124BE9C8033F853489751A7A1721930376495638CAFF4A82DA76DFD7098C114A4B9752E9D4E62EDB4EBD71839C8A82DEE38E410B8CF34F99B4529A4CBD0C8AC1768C00496CF7355E0BD11F19E090060F7A13B14AA29E8CBF049EE403C007B55687503C6D61BC1C73D334E4EEAE87CF77A1A36773FF2CFA03C81EB8AA497C53EF30C75E2B39D253293B9AB65A87518C71819ACEB5D6C7CBC862A3000E338ACAA6139B60BD8D7B631CB2AB4AA1307827A8AA963AE47F26E3C938FA565570B34B404ECEE6C5A951F75B72F406AA586BB1F73952703DAB92AE1671D5A3A70B8DF62F5342A386F51D41072A4641AC5D3947A1DD0C6D19F524A45915FA1A56B171A919ECC5A282828A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A0028A00431A3751934B426D6C454C3D3A9BA2AEA0817A7001CD4B7768245CEEE4738F5ADA954499E466F964A5770454FC7B64D2BA907F1E2B68BEA8F1E74391F9894BDFE51D3914DCC970B8947FF00AE84EEC52A69067DB9CF147F314C86AC27FBC39ED4A7F22781412D72EAC4FD3228FD79E6806DBD760FAF4C71475EDDF9A04937B07F21D283D7AE40E9409AB07F91483EB8E314007A738C9C1A3FC78A0039F4C606452F5F7C50095C4FC323FAD1FA91C8A004F4EC734BC6DE3820F6A02C1FCE93F324000D001B80E492B9E07B500F7F4383400647F176A427EE1C6413D3DA80628FA6290F4E9E61038A01DC077E71DC01E9473C71B80180474142D415FA8644BB4024127907DA8F9B6AF206D6C0CF4345EC26AE1F2FFBC01E3EB41F7241032BF4A698C4E3A74E39146FF46DCCBCB67D28B3424D483F0CBE79E38C51BB78FBDC30CB71D684D2DC6958396FBE5402000290AEEF2F600368C05F6142B3003F27CE4839250FE141DE79E080727EA6A92EE26DAD84F973EA08C9C51CFA0C83F363A0A147B8C5EA7AE46383E9473DCE5739A695B7253BEE86EFC0EA1403CFB9A57F97271907839E94587CA1FDEEC400573DE909E719DC4720E3B534AE30CE1473824E003D3149D07CA3393D47AD26AC02BE33F200B81823B8CFA5059B1CF24F040A69360ACF7136F55C6080093EB473FC2C41CF3F4A1207E4229246ECE38C63AE69475E38C719038CD3B008CC5B23A6D387C7A51D9F1CEE279F7A0038F9491C1182734719FEE85E4E4D0F4134D898EBF310AC010450594F6C8C614D0B51A560F994F5C827031DE930173B802C460E0F4CD02E50CEE27A050BF89A4E7D3236E73EF40C5DFD067040E31DA8DC7AF503A51604AC0E178EC718C9EA4537F8578DA06437B9A695D893B8BBD3D401D3AD26C031F28566192169A8A7B0C4EE76E1430C83EF47F13E082EEB824F4C0A6A36DC8968C566F942E38239247534DF9533C1EB81E9472A634DA42FF739E48C1A4F331DC6738E69C636D86BDEDC5E8BD00DBC526F39E8028E411EF425706ACB40DCA5704E1CF233D050E7870C3E66184069B5AD8515CBB876F2C9E40E31483E74F9902B1207D3145AC392B813DBA060052FA05C200721BD4D38A4F70E5D44EDF2F241214374CD1F7BB9240E33D3F2A6DF71B17F79F7318C900EDE83149BFB336D1D0907B524F51349EE2799CF5C0036A9C77A18FCBC72A0E067B8AAB5C1FBBB07DFC84E0819A376E3F38DA319040E28B584D736AC1B0C42A2E41FBE45267186CE23CE19BAD54617D46A49E81BCAF0B9CE3007A52337FC04EE273ED42A64B490BD871900ED1CF22938F99F2071826874CA8ED66274DBB8B311CE3FFAF46FF3027390C30074CD55AC29BEC277DE090D8C9A4F2F9CBE718C6DA690AF6073BD7EF70C42D1FF0001E4E49C7614ED6125CCF502BD323008238A5EDB98E369C934AF62A6925663406F9B032B8007B529E994E4B11B8D17B92E0A3F311946DFBB90392297FE99A8191CE4F7A771C55B411413D17E523802825C1FBB80093D68B84A16776C4C7538283001E7814BCEE071B481BB04502949444CFEEDF711F28C95F5A52D8FF5841E79C50282E6D80E38192819411EB9348EA38DC0E4E08F71422A49C740C67E476DAB8A5F9B3BB255D86140F4F6A13B12D584DD85D9BB2377EB49FBC39F981627078AAB5B561CAE4BDD0DCC33CE5872734677F1F788E09A2294B7135288BCFF001018201005220E3EFE09E08ED4A4927A15156D45CEF4F255B084EE23B1A0E3006EC672071D8538C53D8539B427539639233B691D80EE1001D2A92B0A6EC2FCC3E6DB80C3239A43BBB9E0818A493BEA0A3741C6D75C90C4633DB9A32DE59F2C00DD3F0A6C1BE5D1012ECCAACC1A351CAF634364FDF055D47008A12049B5A8D2BCFCAB904ED00F414E4C9E0FCC470370E99A0518A90D55C7DDEDC107FA539470DC101796C7AD0DD86A1CDB8981FC6725B83EF4EC7D09EA2939245256D06EDF519C0C0C9A7C43CCC6CDAA5BD7D6873B0D53BEC37E4CA70368CB0FA8A71EFE6647381F51F4A4AA21356D0671183852C5CE39A77C9BBE40707033DB8A7CC986C371E51F9579231C7A9A76E1F2AB6548392578A2E38A486AFC9CE4962C48A5F90EEF98B67343696E249BD83692F9C8E0628F2FE6F90ED2A78E7804535AAD04F462317C6490A47538E3146F04FF007C8383C74C50D5C6F5D168076FA1C7503FBD47BE7182493ED405ACB513233B7FBBCFD28E3811E02920BFB7D2813577B8A17B64900633DE93DF90546E27D41A0A51EE0BE68FE20A0F049149BB8E1B2181269A8B626EE1B973C0CB03C11DE8FF00608059B0063A8A138A14AEC067692AA42838627BD2B13FBBDFF30DC49C7B51CDCC118F289F3E7CD04918C67B52727395E464902AB913138DFE217FD5F5652719C7D693E830A3819EE2850B0F45B0B864F946DDCC0127B734DC03CED0AC460E2851EE1292B0BD032E016C7183C0A6E17F809040EB9AA516F62622E64C1E42F380D8E2931F746370073D69A8DC391B178CE7396039C521C0F504105B1DE9B8A481E9A0B9F6EBC8A46F619DC727D85251D02526D6A2F7CF503A6293B8D8707A9F7A7CADAB02D7602864EF8EC285078F9543139E7A7BD38AE506EFB877E1415EB82690751C00A18A9514081FEF739218D1962791919C281D8D0D5C697331571BBE463863CE7D6823EFA6005600FB6684AC3E66B4131FEC852492DEE0D0CA22F9BA9EA71EB426989C6DB89BB1D8E3A1FA52E5957767701C803A9A04347CAAEC818B11939ED4E3B655DC01183C73CD09DCA49A5A074F971B994F3EF9A395CBE4600C13E8684ACEE1195D6AC4C746DC08DC3EB49CE3E5C00471C77A1BB894ACEE057961F749E47BE68756F4DE001961EB4D209A5BA107D720727D8D1F32F2EB820641F5A6D5C4DD832ADB8839CE467D28FBDDF047240EE28B80019EDBA93E6C9F9B0AC318C74A012B898E3A6483855FF0A56C7F002C1066806AC2127F338A76E3B53E6F9436E03B6680684E0F7CAF420F73463F8FA7181405AE24626CB125768E178E46697DB3C9E33E94092E5173FDEF9C8E78EF4636EC64FA8E7AD0348427CCCE0100F079E7F3A5652C73280A7007CA7FA51B0D42E35B8FBC7000C0CF7FAD365F31837C992784E7BD382BB1CDA4AC60E220198B2820A97CF523DA9487FE16DB8185005798A573DD953D75130AF9F9301CED0CA697AB6C62C09F998B0FBA68BB6C52A6A234B71B140520E0F7E052166E7030C0961E8714EF625A4B615663F2F01B2C58B7D69037DCF98A92723A7143B0256D18E4E5B3B7A0C9E7AE3D29ACD8CEE39DA06693D4AE64B4DC4B8654FDDB230209246339CFBD279D24F8CE7E51B49EC00E9424FA8DBA6F7070B1F92046C40249C1E79A6EE42ACD9FE3C97A76B04AAA4AD1109937F00B1230A053783972AC88DC939EB8FE54D3B19B6DB0F9A1E4848950EE6C1CF5A31E5C7B4A02739009E71F8D394A3D0B8CADBA0380DBBEFB270064E769E78A180619C0F309C0563D07D6973DD582CE3A8C7F93387CE06E751D7068907076B94678F0FB4640AA8CEDA1127642F3C100846181BBA0C535635DADB4B12170149EE3F4A2F1BD86BCC071B9A5C2A9248CF4A4DF21F216524C920DBF28CE28724B4051E61E9BC657770785DA3348ACA8DE5DB963804316EF8A24AFE61B0A24FDE166603202A63D4537CD4381E59438DB8E307DE85249037625327CFD7254F2BF5A6FCC7C9C9428BF776F078F5AA4935A049396E380CCA0AB17555C6DE8777BD0C7E563BB716181E8A694269E8C49728EDDC2FC8CD1B640FAFBD376314F9D8B64ED048E8694E3663E5BAB8EE7CD861913697040FF00649A4DD19F915B953923BE6945A4351438BFF0676ED5CE31D4D190ECDF223161B40F7F4A2D615D6C39771F2B6960C5C9049EE7DA9BE60C02CE40418208E01F6A39B9B71B492D07A98FEF310AA5B0C7DC7734C0F958A3C874002023AE3E94D27125B5D494CA1C0C80157E52CBD5853367FB032A485C76A56E6653D576244D9E6672402BF2E7B5307CDFBCDC01C6147B8A525660E16572442D9E84A819183DCD362738CE371539638ED4DEA0EF6B9344F2163C64AA96DA31807EA69859B72A96C647E9425CC117626F323FBEA3E6000602A30DB198F214A80DCF268926814ACEE5885D77B8604170300F6A893CA73C332B28DC87A8A96AE0E5CCF42D4132270785009273EB50C6CB9E84B05E4E6868A532CC37A366D0C4E0962DC76A803AF60B8070D492B151936B72D477BDF3B49E32DDFF002AAF1DCE4F94CE1896DC71E83B0A12E83E7B2D4B326A4623F315395E4E79C1ED8A861DB2739C8619E7B5370E5D592AA397C24F15CC83E50A54119518E2A38A3391F3821412F9EFF4A4ECB565C5F32D4B96BA8B7DCE410793DEA28439D9C0619E869A57571DE4B634B4ED4DD38DE39E462AB5B2C8B8C00571951E84D2508CDEA545BEA68C5ABB3638E00C0E785155ECD1DF2A46081B973513C2D37D0A7E46BE8BAC39F91F919CA9F4AA7683B39392318F4FA57362705096C542B4E9ECCD98AEB7F7C822A0D3DC98F3D7033F857154A1C8EC7450CC2A2DD968383EE6A3493F0E2A1C2C74D3C7A96E4B9A62BFE19E28E4BEC6B0C4466C7D1FAE054B56344EE145001450014500145001450014500145001450014500145001450014500145001450014500145001450014500145001450014500145001450014500145001450014500145001450014500145001450014500F62BDCCAD9EB8038A5BC5D87D41E95AD3499E4668EAC5BBBB10B7D391D2865E9CF39E95AC1A47975E3293107E4739347F935572234DB76618A5C7E03B52F688A5452DC161DDEFCD4B678DC17B75A89D4713A309964312EC4324457B7D6ADCF6A938E983DAA6389B3D4D717C3338C79A1A94BF9D3A785E23C8E7248AD615398F2F11869507CB24338FC734B8F6AB7AEC631F413F8FAD1DFF0A15C528F330EDEBCD1B7DF07A7E140B91A0FC7AF149B7FC68069B17BFAF18268F9F1B33C13922813560E07DD383F7A91471D38230450349BD809EF8C00318A3E5C95C1047CDEDCD0269ADC4EDC00013FAD1D1BAE091D0500B50C67191839E4E7A51F285D98C64E5B1D05001D7BF04E07B9A083F738000E28013FD5738C823068DEBCF180A30D42F305271D800520FC9B431C8A46246361F970081ED4357D8136F71DCEDE4F19E291BB2E327191F8D38C6E0C6FC98DC0E588E68E3EE38C1E467BD3516B606AE2FEF36E1405C7238F5A6FCD8C29C003209A6D5D825615587F0B608FD28FC323AFD68926C04FFC7B039F7A3EF76E33FCA9A5A035713F7822E80124951EB47CBD719201EBD40A16828C7945DA130AB8031C1F73499F9B0C4649E3E945862F3CFCD900607F8D21E7B1C024534AE081B8CBBFCDF2E4E68C7CB907B601A12B8376079370DE4F0D85031D0526655EE5F9C91424D02570F3147C8A46D5390076A07490632400C0E3AE7B50D20D837204CE70CDC8F4CD0C1BCB6F94056038CF5C516B30061263AF246D19E947F10DA70A41241AA6AC392484CF312E4127059BB0A3602CB26E2540C103A52108781CB7424E7B668C6D1F786DDD9391DA8010375118208033E8D4EDB8DDB43151D734008CD96F970AA060D26159DB2B9C8E307140012790C5482704E28D833D08057031D38A003B6EDC01C8183DE8D9D58138180C0FF4A1009FC3B76E558E07E1463A0F2CB6E3823D3DE9BE504AC046178392782293CA627939C138CFA5095F56005B85390EEC7181D4519C741819039A76E5D505C46545C6E18620E01A3D4B0DC1467239DB5516DAD4528DD89928A8719C9E41EC283C22E58B163919EA68B27B8C4DB16738CB1E467B529DDE80E7F3A71D7A85AC1FEB18E32180CF038148DFC437152460EDA6A09835717EF700F1D4FA8C5211D38258AE3E9428F9825603FEE8054601A362B6DC962CA7229A8A8EE26E403076392C4AE7183DE97E61E8EBB727EA3B5272BEC128DC4FB801C60124D0ABC3719DCD939EC0D3724C2298D7F9002C1412D838A7794F8F9902B29E003EB4D3486358A29E4E718271EF4AA31D482AA7241A6D2077420247257711C714ABFED8DB9249C7414D4EC4DA5B8DEEEFB7E6242E01A52879E172471E84D3534C6A3D589FC5B48E59495A3E52ABE60CA29DAC28521B760FD71C7D287DD8EB851C0FA50A4D8B5F51308DB78DA0720F714103F7793C8E0B0EF4C1262753D0EE5396CF7FC2894127E772013938EE0509D89E5B3D440A7F8893B8E57F1A573BBEE06E9827BD3BB636FD9A07FDDFF0741B49CF6A5E83E65DC48E452B8D3E64264B49F74121413CF4143673D0B331C003D29AD50B55B89CFF00100501C9FAD1D4600008E334D2B89A5BA0F971D7904F143F555DB907EF50D5C1CB9B713F1C91D7DA83B48639242280B9EF45C5CAAF7418FBA72029386CF614BF37EEF0C39E4E3B5171B5A6A3723BB86018281EB4A9FED7CCCEFF00313D6804ADA8839F9738C1C0FAD2F978254903A819A77B6C118F33DC4DB8EE376703DE9483F3003241E73E94735F7070E513AFFBC7AE294FDD56C955276B669FB4B6C3E4D2E27F09EDB4E7EB4154565E5B91950454A77128DF4621F9BB631D3DC539D3EEF1938CD529240E17F31BECD82413818A76D509BBA10739FF001A3DA2071695D8DC7A8DA40E94EE100CB01B867EB42A80AD31B8F941ED9C9C0A747F372AA4AA8CE7B7349CD8D53484FE1DDB4B0CE38EC69D1F21471927E63E94AF60493D90DDBCE54609E093DA9CDBFE63C3648523D05352B872A435810BBFE5C86C0C1ED4BB31FC1B5891823A1149BB86CF5D03CA2A5148C2B139CFAD1DFE60090720E286DB051520EA5415EF81F4A490E47DDDA40C9C7714EF7DC1BE4D855F35BEE2A8058823D291F1D7249032477E292096AAE273BBEF606028CFA8A5EE140E48C9C76AB4EFA48871521A1766707209DC734A7A30C905860903918A2F75A14A36DC68E303EE293F30F5268939F9958070768C8ED4D3EE2945BD50BF26C5C8DEAA73C8E734855FCCCEECBE0313D78A124F5061F746D51804E063B66827E6EA49033922806DB106EC023272D8607BD29FE339D8768DBF4A0566FA89FBBF97682AD9C91ED47FDB43C0CE71DE8B8D248377DFF9B726001ED49C6DCEDC15F9BE94D5BA84A5242AE32C02EECFCA3E9487A650820282D8EA334349EC24DADC3FB9BCF0A770EC68FDD7FCB53B4919E68B2D869B96E83E453B9B730237003BD07CB77CE0E1542AAF6A13696A36AC260FCDF21572DC107B0F6A524B71B8292DB8BE393549B4896"),
                                Role = UserRole.Super.ToString()
                            };
            return agent;
        }

        public static AccountOffice CreateOffice(Account account)
        {
            var office = new AccountOffice
                {
                    Account = account,
                    Name = "Realty Executives Office 1",
                    Address = "123 Fake 1",
                    City = "Saskatoon",
                    Province = "Saskatchewan",
                    Phone = "(306) 373-7520",
                    Fax = "(306) 955-6235",
                    Website = "http://rexsaskatoon.com/office1",
                };

            return office;
        }
    }
}