﻿using System.Web.Mvc;
using System.Web.Routing;
using Moq;
using NUnit.Framework;
using Widgets.HtmlHelpers;
using Widgets.Models;

namespace Widgets.Tests
{
    [TestFixture]
    public class HtmlHelperTests
    {

        [Test]
        public void PagingHelper_Should_Generate_Page_Links()
        {
            // arrange
            HtmlHelper helper = null;

            var model = new PagingInfo()
                            {
                                CurrentPage = 1,
                                ItemsPerPage = 10,
                                TotalItems = 30
                            };

            // act
            var result = helper.PageLinks(model);

            // assert
            Assert.AreEqual(result.ToString(),
                           @"<span>Page 1 of 3</span><a class=""first"" data-url=""1"" href=""#""><<</a><a class=""previous""><</a><a class=""selected"" data-url=""1"" href=""#"">1</a><a data-url=""2"" href=""#"">2</a><a data-url=""3"" href=""#"">3</a><a class=""next"" data-url=""2"" href=""#"">></a><a class=""last"" data-url=""3"" href=""#"">>></a>");
        }

        [Test]
        public void ActionLinkHelper_Should_Add_Selected_Class_To_Link_when_controller_name_matches()
        {
            // arrange
            HtmlHelper helper = null;

            var routes = new RouteCollection();
            var mockview = new Mock<ViewContext>();
            var mockcontainer = new Mock<IViewDataContainer>();

            var routeData = new RouteData();
            routeData.Values.Add("controller", "fakeController");
            routeData.Values.Add("action", "fakeAction");

            mockview.Setup(x => x.RouteData).Returns(routeData);

            helper = new HtmlHelper(mockview.Object, mockcontainer.Object, routes);

            // act
            var result = helper.SelectedActionLink("Test", "fakeAction", "fakeController");

            // assert
            Assert.AreEqual(result.ToString(), @"<a Class=""selected"" href="""">Test</a>");
        }
    }
}


