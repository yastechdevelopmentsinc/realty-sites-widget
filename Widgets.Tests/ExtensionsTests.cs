﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NUnit.Framework;
using Widgets.Domain.Entities;
using Widgets.Extensions;
using Widgets.Models;

namespace Widgets.Tests
{
    [TestFixture]
    public class ExtensionsTests
    {
        private IEnumerable<Location> _locations;

        [SetUp]
        public void Setup()
        {
            _locations = TestHelpers.GenerateFakeLocations();
        }

        [Test]
        public void ToSelectList_Should_Convert_Generic_Collection_To_A_List_Of_Select_List_Items()
        {
            // arrange

            // act
            var result = _locations.ToSelectList(location => location.AreaID.ToString(), location => location.CityName, "Select");

            // assert
            Assert.AreEqual(result[0].Text, "Select");
            Assert.AreEqual(_locations.Count() + 1, result.Count());
            Assert.AreEqual(typeof(List<SelectListItem>), result.GetType());
        }        
        
        [Test]
        public void ToSelectList_Should_Not_Insert_An_Empty_Default_Option()
        {
            // arrange

            // act
            var result = _locations.ToSelectList(location => location.AreaID.ToString(), location => location.CityName, "");

            // assert
            Assert.AreNotEqual(result[0].Text, "");
            Assert.AreEqual(_locations.Count(), result.Count());
            Assert.AreEqual(typeof(List<SelectListItem>), result.GetType());
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetRandomElement_From_List_Should_Throw_Argument_Null_Exception()
        {
            // arrange
            var nulllist = new List<int>();
            nulllist = null;

            // act
            var result = nulllist.GetRandomElements(1);

            // assert

        }

        [Test] 
        public void GetRandomElement_From_List_Should_Return_Random_Element()
        { 
            // arrange
            var emptylist = new List<int> { 1 ,2, 3, 4 };

            // act
            var result = emptylist.GetRandomElements(4);

            // assert
            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void IQueryable_PriceLow_Should_Filter_Results_GRE_Price_Specified()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            const decimal priceLow = 127000m;

            // act
            var result = list.PriceLow(priceLow);

            // assert
            Assert.AreEqual(3, result.Count());
        }

        [Test]
        public void IQueryable_PriceLow_Should_Return_Query_If_No_Price_Specifed()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            const decimal priceLow = 0m;

            // act
            var result = list.PriceLow(priceLow);

            // assert
            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void IQueryable_PriceHigh_Should_Filter_Results_LRE_Price_Specified()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            const decimal priceHigh = 130000m;

            // act
            var result = list.PriceHigh(priceHigh);

            // assert
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void IQueryable_PriceHigh_Should_Return_Query_If_No_Price_Specifed()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            const decimal priceHigh = 0m;

            // act
            var result = list.PriceHigh(priceHigh);

            // assert
            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void IQueryable_PriceHigh_Should_Return_Query_If_Price_Specified_LTOEZERO()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            const decimal priceHigh = -1m;

            // act
            var result = list.PriceHigh(priceHigh);

            // assert
            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void IQueryable_Bedrooms_Should_Filter_Results_GRE_Bedrooms_Specified()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            const int numBedrooms = 3;

            // act
            var result = list.Bedrooms(numBedrooms);

            // assert
            Assert.AreEqual(3, result.Count());
        }

        [Test]
        public void IQueryable_Bedrooms_Should_Return_Query_If_No_Bedrooms_Specifed()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            const int numBedrooms = 0;

            // act
            var result = list.Bedrooms(numBedrooms);

            // assert
            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void IQueryable_Bathrooms_Should_Filter_Results_GRE_Bathrooms_Specified()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            const int numBathrooms = 3;

            // act
            var result = list.Bathrooms(numBathrooms);

            // assert
            Assert.AreEqual(3, result.Count());
        }

        [Test]
        public void IQueryable_Bathrooms__Should_Return_Query_If_No_Bathrooms_Specifed()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            const int numBathrooms = 0;

            // act
            var result = list.Bathrooms(numBathrooms);

            // assert
            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void IQueryable_City_Should_Filter_Results_On_City_Specified()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            const string city = "Springfield";

            // act
            var result = list.City(city);

            // assert
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void IQueryable_City_Should_Return_Query_If_No_City_Specifed()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            var city = string.Empty;

            // act
            var result = list.City(city);

            // assert
            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void IQueryable_City_Should_Return_Query_If_All_Specifed()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            var city = "All";

            // act
            var result = list.City(city);

            // assert
            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void IQueryable_Area_Should_Filter_Results_On_Area_Specified()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            const string area = "Suberbia";

            // act
            var result = list.Area(area);

            // assert
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void IQueryable_Area_Should_Return_Query_If_No_Area_Specifed()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            var area = string.Empty;

            // act
            var result = list.Area(area);

            // assert
            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void IQueryable_Area_Should_Return_Query_If_All_Specifed()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            var area = "All";

            // act
            var result = list.Area(area);

            // assert
            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void IQueryable_Address_Should_Filter_Results_On_Address_Specified()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            const string address = "111 Street";

            // act
            var result = list.Address(address);

            // assert
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void IQueryable_Adress_Should_Return_Query_If_No_Address_Specifed()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            var address = string.Empty;

            // act
            var result = list.Address(address);

            // assert
            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void IQueryable_Listing_Access_Should_Filter_Results_On_Agent_Listing_Access()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            var account = TestHelpers.CreateAccount(new Guid());
            account.AgentCode = "agent1";
            account.Access = new AccountListingAccess
                                 {
                                     Account = account,
                                     Residential = ListingDisplayAccess.Agent
                                 };
        
            // act
            var result = list.ListingAccess(account, PropertyTypeConstants.Residential);

            // assert
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void IQueryable_Listing_Access_Should_Filter_Results_On_Brokerage_Listing_Access()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            var account = TestHelpers.CreateAccount(new Guid());
            account.BrokerCode = "broker1";
            account.Access = new AccountListingAccess
            {
                Account = account,
                Residential = ListingDisplayAccess.Brokerage
            };

            // act
            var result = list.ListingAccess(account, PropertyTypeConstants.Residential);

            // assert
            Assert.AreEqual(3, result.Count());
        }

        [Test]
        public void IQueryable_Listing_Access_Should_Filter_Results_On_IDX_Listing_Access()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            var account = TestHelpers.CreateAccount(new Guid());
            account.BoardCode = "board1";
            account.Access = new AccountListingAccess
            {
                Account = account,
                Residential = ListingDisplayAccess.IDX
            };

            // act
            var result = list.ListingAccess(account, PropertyTypeConstants.Residential);

            // assert
            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void IQueryable_Listing_Access_Should_Return_Query_When_No_Listing_Access()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            var account = TestHelpers.CreateAccount(new Guid());
            account.Access = new AccountListingAccess
            {
                Account = account,
                Residential = ListingDisplayAccess.None
            };

            // act
            var result = list.ListingAccess(account, PropertyTypeConstants.Residential);

            // assert
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void IQueryable_Agent_Should_Return_Query_If_No_Agent_Specified()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            var agentId = string.Empty;

            // act
            var result = list.Agent(agentId, string.Empty);

            // assert
            Assert.AreEqual(4, result.Count());
        }        
        
        [Test]
        public void IQueryable_Agent_Should_Return_Listings_Filtered_By_AgentId()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            var agentId = "agent1";

            // act
            var result = list.Agent(agentId, string.Empty);

            // assert
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void IQueryable_Agent_Should_Return_Listings_Filtered_By_CoAgentId()
        {
            // arrange
            var list = TestHelpers.FakeResidentailListings().AsQueryable();
            var agentId = "agent2";

            // act
            var result = list.Agent(agentId, PropertyTypeConstants.Residential);

            // assert
            Assert.AreEqual(3, result.Count());
        }
    }
}
