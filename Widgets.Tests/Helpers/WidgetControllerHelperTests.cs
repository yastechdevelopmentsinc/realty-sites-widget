﻿using System;
using System.Linq;
using AutoMapper;
using Moq;
using NUnit.Framework;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Helpers;
using Widgets.Models;
using Widgets.Models.ViewModels;

namespace Widgets.Tests
{
    [TestFixture]
    public class WidgetControllerHelperTests
    {
        private Mock<IListingService> _repository;

        [SetUp]
        public void SetUp()
        {
            Mapper.CreateMap<Residential, Listing>();
            Mapper.CreateMap<ForLease, Listing>();
            Mapper.CreateMap<Commercial, Listing>();
            Mapper.CreateMap<FarmsAndLand, Listing>();
            Mapper.CreateMap<MultiFam, Listing>();
            Mapper.CreateMap<Exclusive, Listing>();

            _repository = new Mock<IListingService>();
            _repository.Setup(repository => repository.GetAllResidential()).Returns(TestHelpers.FakeResidentailListings().AsQueryable);
            _repository.Setup(repository => repository.GetAllCommercial()).Returns(TestHelpers.FakeCommercialListings().AsQueryable);
            _repository.Setup(repository => repository.GetAllFarmsAndLand()).Returns(TestHelpers.FakeFarmsandLandListings().AsQueryable);
            _repository.Setup(repository => repository.GetAllForLease()).Returns(TestHelpers.FakeForLeaseListings().AsQueryable);
        }
    
        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Return_Paginated_List_Of_Results()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 3, searchCriteria);

            // act
           // var result = helper.GetListingWidgetData();
            //var resultData = result.PagingInfo;

            // assert 
            //Assert.AreEqual(1, resultData.CurrentPage);
            //Assert.AreEqual(3, resultData.ItemsPerPage);
            //Assert.AreEqual(7, resultData.TotalItems);
            //Assert.AreEqual(3, resultData.TotalPages);
        }

        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Return_Listings_Filtered_By_Price()
        {
            // arrange
            var searchCriteria = new SearchCriteria
                                     {
                                         PriceLow = 125000m,
                                         PriceHigh = 130000m,
                                         Account = TestHelpers.CreateAccount()
                                     };
            
            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(2, resultData.Count());
            //Assert.GreaterOrEqual(resultData.FirstOrDefault().ListPrice, searchCriteria.PriceLow);
            //Assert.LessOrEqual(resultData.FirstOrDefault().ListPrice, searchCriteria.PriceHigh);
        }

        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Return_Listings_Filtered_By_Number_Of_Bathrooms()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                Bathrooms = 3,
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(6, resultData.Count());
        }

        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Return_Listings_Filtered_By_Number_Of_Bedrooms()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                Bedrooms = 4,
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
           // Assert.AreEqual(5, resultData.Count());
        }

        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Return_Listings_Filtered_By_City()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                City = "Springfield",
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(3, resultData.Count());
        }

        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Return_Listings_Filtered_By_Location()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                AreaName = "Suberbia",
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(3, resultData.Count());
        }

        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Return_All_Listings_If_Location_Filter_Is_All()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                AreaName = "All",
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(7, resultData.Count());
        }

        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Return_All_Listings_If_Location_Filter_Is_All_And_City_Filter_Is_All()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                City = "",
                AreaName = "All",
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(7, resultData.Count());
        }   

        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Filter_Listings_By_Residential_Property_Type()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                Residential = true,
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act 
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(3, resultData.Count());
        }

        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Filter_Listings_By_Commercial_Property_Type()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                Commercial = true,
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act 
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(1, resultData.Count());
        }

        [Test]
        public void ListingControllerHelper_Should_Filter_Listings_By_ForLease_Property_Type()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                ForLease = true,
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act 
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(1, resultData.Count());
        }        
        
        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Filter_Listings_By_FarmsAndLand_Property_Type()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                FarmsAndLand = true,
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act 
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(2, resultData.Count());
        }        
        
        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Filter_Listings_By_Acreage_Property_Type()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                Acreage = true,
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act 
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(1, resultData.Count());
        }        
        
        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Filter_Listings_By_Condo_Property_Type()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                Condominium = true,
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act 
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(1, resultData.Count());
        }

        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Filter_Listings_By_Access()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                Account = new Account
                {
                    AgentCode = "agent2",
                    Access = new AccountListingAccess
                                 {
                                     Residential = ListingDisplayAccess.Agent,
                                     Commercial = ListingDisplayAccess.Agent,
                                     FarmsAndLand = ListingDisplayAccess.Agent,
                                     ForLease = ListingDisplayAccess.Agent
                                 }
                    
                }
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(5, resultData.Count());
        }

        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Filter_Listings_By_Access_Including_Co_Agent_Listings()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                Account = new Account
                {
                    AgentCode = "agent2",
                    Access = new AccountListingAccess
                    {
                        Residential = ListingDisplayAccess.Agent,
                        Commercial = ListingDisplayAccess.Agent,
                        FarmsAndLand = ListingDisplayAccess.Agent,
                        ForLease = ListingDisplayAccess.Agent
                    }

                }
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(5, resultData.Count());
        }

        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Only_Query_Residential_Listings()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                WidgetOptions = WidgetOptions.ResidentialListings,
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act 
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(4, resultData.Count());
        }

        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Only_Query_Commercial_Listings()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                WidgetOptions = WidgetOptions.CommercialListings,
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act 
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(1, resultData.Count());
        }        
        
        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Only_Query_ForLease_Listings()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                WidgetOptions = WidgetOptions.ForLeaseListings,
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act 
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(1, resultData.Count());
        }        
        
        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Only_Query_MultiFam_Listings()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                WidgetOptions = WidgetOptions.MultiFamListings,
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act 
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(0, resultData.Count());
        }        
        
        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Only_Query_Exclusive_Listings()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                WidgetOptions = WidgetOptions.ExclusiveListings,
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act 
           // var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(0, resultData.Count());
        }

        [Test]
        public void ListingControllerHelper_GetListingWidgetData_Should_Only_Query_FarmsAndLand_Listings()
        {
            // arrange
            var searchCriteria = new SearchCriteria
            {
                WidgetOptions = WidgetOptions.FarmsAndLandListings,
                Account = TestHelpers.CreateAccount()
            };

            var helper = new WidgetControllerHelper(_repository.Object, 1, 10, searchCriteria);

            // act 
            //var result = helper.GetListingWidgetData();
            //var resultData = result.Listings;

            // assert
            //Assert.AreEqual(1, resultData.Count());
        }

    }


}
