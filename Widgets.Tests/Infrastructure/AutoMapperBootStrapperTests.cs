﻿using AutoMapper;
using NUnit.Framework;
using Widgets.Infrastructure;

namespace Widgets.Tests.Infrastructure
{
    [TestFixture]
    public class AutoMapperBootStrapperTests
    {
        [Test]
        public void Automapper_mappings_are_valid()
        {
            AutoMapperBootStrapper.Bootstrap();
            Mapper.AssertConfigurationIsValid();
        }
    }
}
