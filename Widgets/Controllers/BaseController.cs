﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Infrastructure;
using Widgets.Infrastructure.Abstract;

namespace Widgets.Controllers
{
    public class BaseController : Controller
    {
        private readonly IAuthProvider _auth;
        private readonly ISession _session;

        public BaseController(IAuthProvider auth, ISession session)
        {
            _auth = auth;
            _session = session;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                // Create a custom Principal Instance and assign to Current User 
                var accountKey = ((VdcIPrinicipal)filterContext.HttpContext.User).ApiKey;
                var accountType = ((VdcIPrinicipal)filterContext.HttpContext.User).AccountType;

                dynamic viewBag = filterContext.Controller.ViewBag;
                if (accountType != null)
                {
                    viewBag.AccountType = accountType;                    
                }
                viewBag.AccountFeatures = _session.Query<AccountFeatures>()
                    .Cacheable()
                    .CacheRegion("LongTerm")
                    .SingleOrDefault(x => x.Account.Id == accountKey);
            }
        }

        /// <summary>
        /// Override the authorize aspect of the admin related controllers so that the logged in identity & related roles can be checked
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User != null)
            {
                if (filterContext.HttpContext.User.Identity.IsAuthenticated)
                {
                    if (filterContext.HttpContext.User.Identity is FormsIdentity)
                    {
                        // Get Forms Identity From Current User
                        var id = (FormsIdentity) filterContext.HttpContext.User.Identity;

                        // Create a custom Principal Instance and assign to Current User (with caching)
                        var principal = (VdcIPrinicipal) filterContext.HttpContext.Cache.Get(id.Name);
                        var ticket = _auth.GetAuthTicket();
                        if (principal == null)
                        {
                            // Create and populate your Principal object with the needed data and Roles.
                            principal = new VdcIPrinicipal(new VdcIdentity(ticket.Name, ticket.UserData));
                            filterContext.HttpContext.Cache.Add(
                                id.Name,
                                principal,
                                null,
                                System.Web.Caching.Cache.NoAbsoluteExpiration,
                                new TimeSpan(0, 30, 0),
                                System.Web.Caching.CacheItemPriority.Default,
                                null);

                            HttpContext.User = principal;
                        }
                        
                        filterContext.HttpContext.User = principal;

                        // if the user doesn't have an account key ("administrators") redirect to the account page
                        if (principal.ApiKey == Guid.Empty && 
                            (string)filterContext.RouteData.Values["action"] != "ManageAccounts" && 
                            (string)filterContext.RouteData.Values["action"] != "Assume" &&
                            ((string)filterContext.RouteData.Values["action"] != "Create" && (string)filterContext.RouteData.Values["controller"] != "Account"))
                        {
                            filterContext.Result = new RedirectToRouteResult(
                                new RouteValueDictionary
                                        {
                                                { "controller", "Account" },
                                                { "action", "ManageAccounts" }
                                        } );
                        }
                    }
                }
            }

            base.OnAuthorization(filterContext);
        }
    }
}
