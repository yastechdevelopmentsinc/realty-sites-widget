﻿using System.Collections.Generic;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using System.Linq;

namespace Widgets.Controllers
{
    public class BulletinBoardController : BaseController
    {
        private readonly IAuthProvider _auth;
        private readonly ISession _session;

        public BulletinBoardController(IAuthProvider auth, ISession session) : base(auth, session)
        {
            _auth = auth;
            _session = session;
        }

        /// <summary>
        /// Render the index view which displays list of bulleting board & news item messages
        /// </summary>
        [HttpGet]
        [Authorize]
        public ActionResult Index()
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var bulletins = AutoMapper.Mapper.Map<IEnumerable<BulletinBoard>, IEnumerable<BulletinBoardViewModel>>(
                _session.Query<BulletinBoard>().Where(x => x.Account == account))
                .OrderByDescending(x => x.Id)
                .OrderByDescending(x => x.StartDate);

            var model = new BulletinBoardIndexViewModel
                            {
                                Bulletins = bulletins
                            };


            return View("Index", model);
        }

        /// <summary>
        /// Render the create bulletin board view
        /// </summary>
        [HttpGet]
        [Authorize]
        public ViewResult Create()
        {
            var model = new BulletinBoardViewModel
                            {
                                IsNews = false
                            };
            return View("Create", model);
        }

        /// <summary>
        /// Create a new bulletin board message
        /// </summary>
        /// <param name="bulletin">Posted bulletin</param>
        [HttpPost, ValidateInput(false)]
        [Authorize]
        public ActionResult Create(BulletinBoardViewModel bulletin)
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var agent = _session.Query<Agent>().SingleOrDefault(x => x.UserName == User.Identity.Name);

            var model = AutoMapper.Mapper.Map<BulletinBoardViewModel, BulletinBoard>(bulletin);
            model.Account = account;
            model.Agent = agent;

            _session.Save(model);
            _session.Flush();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Render the edit view to edit an existing bulletin
        /// </summary>
        /// <param name="id">The id of the bulletin message to edit</param>
        [HttpGet]
        [Authorize]
        public ViewResult Edit(int id)
        {
            var model = AutoMapper.Mapper.Map<BulletinBoard, BulletinBoardViewModel>(_session.Get<BulletinBoard>(id));
            return View("Edit", model);
        }

        /// <summary>
        /// Save changes to a bulletin board message made on the edit form
        /// </summary>
        /// <param name="bulletin">posted bulletin</param>
        [HttpPost, ValidateInput(false)]
        [Authorize]
        public ActionResult Edit(BulletinBoardViewModel bulletin)
        {
            if (ModelState.IsValid)
            {
                var model = _session.Get<BulletinBoard>(bulletin.Id);
                TryUpdateModel(model);

                _session.Save(model);
                _session.Flush();

                return RedirectToAction("Index", "BulletinBoard");
            }

            return View("Edit", bulletin);
        }

        /// <summary>
        /// Delete the specified bulletin board message
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public ActionResult Delete(int id)
        {
            var model = _session.Get<BulletinBoard>(id);
            if(model.Account.Id == _auth.GetAccountKey())
            {
                _session.Delete(model);
                _session.Flush();
            }

            return RedirectToAction("Index", "BulletinBoard");
        }

        /// <summary>
        /// Display the bulletin board messages in an easily digestible format
        /// </summary>
        [HttpGet]
        [Authorize]
        public ActionResult DisplayBulletin()
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var bulletins = AutoMapper.Mapper.Map<IEnumerable<BulletinBoard>, IEnumerable<BulletinBoardViewModel>>(_session.Query<BulletinBoard>().Where(x => x.Account == account)
                )
                .OrderByDescending(x => x.Id)
                .OrderByDescending(x => x.StartDate);

            var model = new BulletinBoardIndexViewModel
            {
                Bulletins = bulletins
            };


            return View("DisplayBulletin", model);
        }

        /// <summary>
        /// Is opened from the display bulleting view as a light box window
        /// </summary>
        /// <param name="id">The bulletin board message to display</param>
        [HttpGet]
        public PartialViewResult DisplaySingleBulletin(int id)
        {
            var bulletin = AutoMapper.Mapper.Map<BulletinBoard, BulletinBoardViewModel>(_session.Get<BulletinBoard>(id));
            return PartialView("DisplaySingleBulletin", bulletin);
        }
    }


}