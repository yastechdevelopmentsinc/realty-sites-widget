﻿using System.Text;
using System.Web.Mvc;
using Widgets.Helpers;
using Widgets.Models;
using Widgets.Properties;

namespace Widgets.Controllers
{
    /// <summary>
    /// Controller that is responsbile for rendering widget specific javascript
    /// </summary>
    public class ApplicationController : Controller
    {
        private readonly ApplicationControllerHelper _applicationControllerHelper;
        public ApplicationController(ApplicationControllerHelper helper)
        {
            _applicationControllerHelper = helper;
        }

        [HttpGet]
        [ActionName("js")]
        public ContentResult RenderJavascript()
        {
            var js = new StringBuilder();

            js.AppendLine("var _vdcopts = _vdcopts || {};");
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}';", "_defaultHostName", Settings.Default.DefaultHostName));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}?{3}';", "_css", Settings.Default.DefaultHostName, "/Content/widget.css", _applicationControllerHelper.GetLastModified()));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}';", WidgetType.ListingWidget, Settings.Default.DefaultHostName, "/widget/displaylistings/?callback=?&accountkey="));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}';", WidgetType.SearchWidget, Settings.Default.DefaultHostName, "/widget/search/?callback=?&accountkey="));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}';", WidgetType.MapSearchWidget, Settings.Default.DefaultHostName, "/widget/mapsearch/?callback=?&accountkey="));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}';", WidgetType.FeaturedListingWidget, Settings.Default.DefaultHostName, "/widget/featuredlistings?callback=?&accountkey="));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}';", WidgetType.FeaturedAgentWidget, Settings.Default.DefaultHostName, "/widget/featuredagents?callback=?&accountkey="));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}';", WidgetType.MembersWidget, Settings.Default.DefaultHostName, "/widget/displayagents?callback=?&accountkey="));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}';", WidgetType.ContactsWidget, Settings.Default.DefaultHostName, "/widget/displaycontacts?callback=?&accountkey="));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}';", WidgetType.NewsWidget, Settings.Default.DefaultHostName, "/widget/displaynews?callback=?&accountkey="));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}';", WidgetType.MortgageCalculatorWidget, Settings.Default.DefaultHostName, "/widget/mortgagecalculator?callback=?&accountkey="));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}';", WidgetType.LatestListingWidget, Settings.Default.DefaultHostName, "/widget/latestlisting?callback=?&accountkey="));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}';", WidgetType.AgentProfileWidget, Settings.Default.DefaultHostName, "/widget/agentprofile?callback=?&accountkey="));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}{3}';", "_emailForm", Settings.Default.DefaultHostName,  Url.Action("EmailAgent", "Email"), "/"));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}{3}';", "_emailListingForm", Settings.Default.DefaultHostName,  Url.Action("EmailListing", "Email"), "/"));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}{3}';", "_newsItem", Settings.Default.DefaultHostName, Url.Action("DisplayNewsItem", "News"), "/"));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}{3}';", "_sendEmailAgent", Settings.Default.DefaultHostName, Url.Action("SendEmailAgent", "Email"), "/?callback=?"));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}{3}';", "_sendEmailListing", Settings.Default.DefaultHostName, Url.Action("SendEmailListing", "Email"), "/?callback=?"));
            //new widget custom seacrh
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}';", WidgetType.CustomSearchWidget, Settings.Default.DefaultHostName, "/widget/CustomSearch?callback=?&accountkey="));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}';", WidgetType.NearMeWidget, Settings.Default.DefaultHostName, "/widget/NearMeListings?callback=?&accountkey="));
            js.AppendLine(string.Format("_vdcopts.{0} = '{1}{2}';", WidgetType.InPageListingDetails, Settings.Default.DefaultHostName, "/widget/InPageListingDetails?callback=?&accountkey="));
            //
            js.AppendLine(_applicationControllerHelper.ReadListingWidgetJavascript());

            return new ContentResult
            {
                Content = js.ToString(),
                ContentType = "application/x-javascript"
            };
        }
    }
}
