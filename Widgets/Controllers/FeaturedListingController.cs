﻿using System.Linq;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Helpers;
using Widgets.Infrastructure;
using Widgets.Infrastructure.Abstract;
using Widgets.Models;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    [Authorize(Roles = "Super, Admin")]
    public class FeaturedListingController : BaseController
    {
        private readonly IAuthProvider _auth;
        private readonly ISession _session;
        private readonly IListingService _listingService;

        public FeaturedListingController(IAuthProvider auth, ISession session, IListingService listingService) : base(auth, session)
        {
            _auth = auth;
            _session = session;
            _listingService = listingService;
        }

        /// <summary>
        /// Render a list of listings marked as featured for the account
        /// </summary>
        [HttpGet]
        public ViewResult Index()
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var listings = ControllerHelpers.GetFeaturedListings(account, _session, _listingService);
            var model = new FeaturedListingIndexViewModel
                {
                    Listings = listings,
                    AutoSelect = account.Features.AutoSelectFeaturedListings
                };
            return View("Index", model);
        }

        /// <summary>
        /// Render the create featured listings view
        /// </summary>
        [HttpGet]
        public ViewResult Create()
        {
            var model = new FeaturedListingsCreateViewModel();
            return View("Create", model);
        }

        /// <summary>
        /// Create new featured listing
        /// </summary>
        /// <param name="mlsnumber"></param>
        [HttpPost]
        public ActionResult Create(int? mlsnumber)
        {
            if(mlsnumber == null)
            {
                return RedirectToAction("Create");
            }

            var account = Account.GetById(_session, _auth.GetAccountKey());
            var featuredListing = new FeaturedListing
                                      {
                                          Mlsnumber = (int) mlsnumber,
                                          Account = account
                                       };

            _session.Save(featuredListing);
            _session.Flush();

            return RedirectToAction("Index", "FeaturedListing");
        }

        /// <summary>
        /// AJAX update for featured listings auto selection
        /// </summary>
        /// <param name="autoSelect">Whether or not to auto select listings</param>
        [HttpPost]
        public JsonResult AutoSelect(bool autoSelect)
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            account.Features.AutoSelectFeaturedListings = autoSelect;
            _session.Save(account);
            if (autoSelect)
            {
                var listings = _session.Query<FeaturedListing>().Where(x => x.Account == account);
                foreach (var featuredListing in listings)
                {
                    _session.Delete(featuredListing);
                }
            }
            _session.Flush();
            return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.DenyGet,
                    Data = new {isChecked = autoSelect}
                };
        }

        /// <summary>
        /// AJAX search used currently on the featured listings / edit coordinages page. 
        /// </summary>
        /// <param name="address">The address to filter on</param>
        [HttpGet]
        [JsonpFilter]
        public JsonResult SearchListings(string address)
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var searchCriteria = new SearchCriteria
                                     {
                                         Address = address,
                                         Account = account
                                     };
            var helper = new WidgetControllerHelper(_listingService, searchCriteria, false); // has been changed to false to allow adding listing from the whole board

            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = new { listings = helper.GetAllListings(account.BoardCode, account.Features.NumberOfListingsToDisplay, false) }
            };
            

        }

        /// <summary>
        /// Admin function - Delete featured listing
        /// </summary>
        /// <param name="mlsNumber">The listing to delete</param>
        [HttpGet]
        public ActionResult Delete(int mlsNumber)
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var featuredListing = _session.Query<FeaturedListing>().FirstOrDefault(x => x.Mlsnumber == mlsNumber && x.Account == account);

            _session.Delete(featuredListing);
            _session.Flush();
            return RedirectToAction("Index", "FeaturedListing");
        }
    }
}