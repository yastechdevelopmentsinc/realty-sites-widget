﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Extensions;
using Widgets.Helpers;
using Widgets.Infrastructure;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    public class NewsController : BaseController
    {
        private readonly IAuthProvider _auth;
        private readonly ISession _session;

        public NewsController(IAuthProvider auth, ISession session)  : base(auth, session)
        {
            _auth = auth;
            _session = session;
        }

        /// <summary>
        /// Render the index view which displays list of bulleting board & news item messages
        /// </summary>
        [HttpGet]
        [Authorize(Roles = "Super, Admin, News")]
        public ViewResult Index()
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var bulletins = Mapper.Map<IEnumerable<BulletinBoard>, IEnumerable<NewsItemViewModel>>(
                _session.Query<BulletinBoard>().Where(x => x.Account == account && x.IsNews))
                .OrderByDescending(x => x.Id)
                .OrderByDescending(x => x.StartDate);

            var model = new NewsIndexViewModel
            {
                Bulletins = bulletins
            };


            return View("Index", model);
        }

        /// <summary>
        /// Render the create news item view
        /// </summary>
        [HttpGet]
        [Authorize(Roles = "Super, Admin, News")]
        public ViewResult Create()
        {
            var model = new NewsItemViewModel
                            {
                                IsNews = true
                            };
            return View("Create", model);
        }

        /// <summary>
        /// Create a news item
        /// </summary>
        /// <param name="bulletin">Posted news item</param>
        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Super, Admin, News")]
        public ActionResult Create(NewsItemViewModel bulletin)
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var agent = _session.Query<Agent>().SingleOrDefault(x => x.UserName == User.Identity.Name);

            var model = Mapper.Map<NewsItemViewModel, BulletinBoard>(bulletin);
            model.Account = account;
            model.Agent = agent;

            _session.Save(model);
            _session.Flush();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Render the edit view to edit an existing news item
        /// </summary>
        /// <param name="id">The id of the news item to edit</param>
        [HttpGet]
        [Authorize(Roles = "Super, Admin, News")]
        public ViewResult Edit(int id)
        {
            var model = Mapper.Map<BulletinBoard, NewsItemViewModel>(_session.Get<BulletinBoard>(id));
            return View("Edit", model);
        }

        /// <summary>
        /// Save changes to a news item made on the edit form
        /// </summary>
        /// <param name="bulletin">posted news item</param>
        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Super, Admin, News")]
        public ActionResult Edit(NewsItemViewModel bulletin)
        {
            if (ModelState.IsValid)
            {
                var model = _session.Get<BulletinBoard>(bulletin.Id);
                TryUpdateModel(model);

                _session.Save(model);
                _session.Flush();

                return RedirectToAction("Index", "News");
            }

            return View("Edit", bulletin);
        }

        /// <summary>
        /// Delete the specified news item
        /// </summary>
        /// <param name="id">id of the news item to delete</param>
        [HttpGet]
        public ActionResult Delete(int id)
        {
            var model = _session.Get<BulletinBoard>(id);
            if(model.Account.Id == _auth.GetAccountKey())
            {
                _session.Delete(model);
                _session.Flush();
            }
            
            return RedirectToAction("Index", "News");
        }

        /// <summary>
        /// Add photo gallery to news item
        /// </summary>
        /// <param name="id"></param>
        [HttpGet]
        [Authorize(Roles = "Super, Admin, News")]
        public ViewResult Photos(int id)
        {
            var bulletin = _session.Get<BulletinBoard>(id);
            var model = new PhotoGalleryViewModel
            {
                Photos = bulletin.Photos.OrderBy(x=>x.PhotoIndex)
            };

            return View("PhotoGallery", model);
        }

        /// <summary>
        /// Save photo
        /// </summary>
        /// <param name="id">Bulletin Board id</param>
        /// <param name="Filedata">The collection of posted files</param>
        [HttpPost]
        [Authorize(Roles = "Super, Admin, News")]
        public ActionResult Photos(int id, IEnumerable<HttpPostedFileBase> Filedata)
        {
            var bulletin = _session.Get<BulletinBoard>(id);
            if(ModelState.IsValid)
            {
                // get the index to use for the first uploaded photo
                var i = bulletin.Photos.Count() + 1;
                foreach (var file in Filedata)
                {
                    var photo = new BulletinBoardPhoto
                                    {
                                        BulletinBoard = bulletin,
                                        Photo = ControllerHelpers.GetImageBytes(file),
                                        PhotoMimeType = file.ContentType,
                                        PhotoCaption = string.Empty,
                                        PhotoIndex = i
                                    };
                    _session.Save(photo);
                    i++;
                }
                _session.Flush();

                return RedirectToAction("Photos", Index());
            }

            var modelerror = new PhotoGalleryViewModel
                                 {
                                     Photos = bulletin.Photos.OrderBy(x => x.PhotoIndex)
                                 };

            return View("PhotoGallery", modelerror);
        }

        /// <summary>
        /// Delete the specified photo
        /// </summary>
        /// <param name="id">The id of the photo to delete</param>
        [Authorize(Roles = "Super, Admin, News")]
        public ActionResult DeletePhoto(int id)
        {
            var photo = _session.Get<BulletinBoardPhoto>(id);
            _session.Delete(photo);
            _session.Flush();

            return RedirectToAction("Photos", "News", new {id = photo.BulletinBoard.Id});
        }

        /// <summary>
        /// Is opened from the news module as a light box window
        /// </summary>
        /// <param name="id">The bulletin board message to display</param>
        [HttpGet]
        [JsonpFilter]
        public JsonResult DisplayNewsItem(int id)
        {
            var model = Mapper.Map<BulletinBoard, NewsItemViewModel>(_session.Get<BulletinBoard>(id));

            var sharethis = Request.UrlReferrer.OriginalString;
            sharethis = sharethis + (Request.UrlReferrer.Query == string.Empty ? "?" : "&");

            model.ShareThisUrl = sharethis;

            return new JsonpResult
                       {
                           JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                           Data = new {html = this.RenderPartialViewToString("DisplayNewsItem", model)}
                       };
        }


        /// <summary>
        /// Retrieve news item photo
        /// </summary>
        /// <param name="id">The id of the photo to retreive</param>
        [HttpGet]
        public ActionResult GetImage(int id)
        {
            var photo = _session.Get<BulletinBoardPhoto>(id);
            return photo.Photo == null ? null : new FileContentResult(photo.Photo, photo.PhotoMimeType);
        }

        /// <summary>
        /// Retreive news item photo as a thumbnail
        /// </summary>
        /// <param name="id">The id of the photo to retreive</param>
        [HttpGet]
        public ActionResult GetImageThumb(int id)
        {
            var photo = _session.Get<BulletinBoardPhoto>(id);
            var thumb = ImageHelper.CreateThumbnail(photo.Photo, 100);

            return thumb == null ? null : new FileContentResult(thumb, photo.PhotoMimeType);
        }

        /// <summary>
        /// Order the news gallery & update captions
        /// </summary>
        /// <param name="gallery"></param>
        [HttpPost]
        public ActionResult Order(IEnumerable<OrderedNewsGallery> gallery )
        {
            foreach (var item in gallery)
            {
                var photo = _session.Get<BulletinBoardPhoto>(item.value);
                
                if(string.IsNullOrEmpty(item.caption) || item.caption == "Enter photo caption...")
                {
                    photo.PhotoCaption = string.Empty;
                }
                else
                {
                    photo.PhotoCaption = item.caption;
                }

                photo.PhotoIndex = item.index;
                _session.Update(photo);
            }
            _session.Flush();

            return new JsonResult
                       {
                           JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                           Data = new {message = "Order & Captions Updated"}
                       };
        }
    }

    public class OrderedNewsGallery
    {
        public int index { get; set; }
        public int value { get; set; }
        public string caption { get; set; }
    }
}
