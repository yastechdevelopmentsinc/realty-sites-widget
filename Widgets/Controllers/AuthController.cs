﻿using System;
using System.Linq;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    /// <summary>
    /// Controller to handle website authentication
    /// </summary>
    public class AuthController : Controller
    {
        private readonly ISession _session;
        private readonly IAuthProvider _authProvider;
        private readonly IEmailProcessor _emailProcessor;

        public AuthController(ISession session, IAuthProvider auth, IEmailProcessor emailProcessor)
        {
            _session = session;
            _authProvider = auth;
            _emailProcessor = emailProcessor;
        }

        /// <summary>
        /// Render the sign in page
        /// </summary>
        [HttpGet]
        public ViewResult SignIn()
        {
            var model = new SignInViewModel();

            return View("SignIn", model);
        }

        /// <summary>
        /// Validate the credentials and sign user into admin area if credentials valid
        /// </summary>
        /// <param name="credentials">Username / Password</param>
        /// <param name="returnurl">Url to redirect to after successfull authentication</param>
        [HttpPost]
        public ActionResult SignIn(SignInViewModel credentials, string returnurl)
        {
            if (ModelState.IsValid)
            {
                // get agent
                var agent = _session.Query<Agent>()
                    .SingleOrDefault(x => x.UserName == credentials.UserName);

                if (agent != null && string.Equals(agent.UserName, credentials.UserName) && string.Equals(agent.Password, credentials.Password))
                {
                    HttpContext.Cache.Remove(agent.UserName);

                    var role = agent.Role;
                    var apiKey = agent.Account == null ? Guid.Empty : agent.Account.Id;
                    var accountType = agent.Account == null ? string.Empty: agent.Account.AccountType.ToString();
                    _authProvider.SetAuthCookie(agent.UserName, role, apiKey, accountType);

                    if (string.IsNullOrEmpty(returnurl))
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    return Redirect(returnurl);
                }
            }
            ModelState.AddModelError("", "Incorrect User Name or Password");
            return View("SignIn");
        }

        /// <summary>
        /// Current user displays information associated to the current authenticated user along with a sign out link
        /// </summary>
        [Authorize]
        public PartialViewResult CurrentUser()
        {
            var model = new CurrentUserViewModel
                            {
                                UserName = HttpContext.User.Identity.Name
                            };
           
            return PartialView("CurrentUser", model);
        }

        /// <summary>
        /// Signs the user out of the admin section and redirects to the sign in page
        /// </summary>
        [HttpGet]
        [Authorize]
        public ActionResult SignOut()
        {
            _authProvider.SignOut();
            return RedirectToAction("SignIn", "Auth");
        }

        /// <summary>
        /// Render the forgot password page
        /// </summary>
        [HttpGet]
        public ViewResult ForgotPassword()
        {
            var model = new ForgotPasswordViewModel();
            return View("ForgotPassword", model);
        }

        [HttpPost]
        public ViewResult ForgotPassword(ForgotPasswordViewModel forgotPassword)
        {
            if (ModelState.IsValid)
            {
                // get agent
                var agent = _session.Query<Agent>()
                    .SingleOrDefault(x => x.UserName == forgotPassword.UserName);

                if (agent != null)
                {
                    var email = new EmailPassword
                    {
                        Email = agent.Email,
                        UserName = agent.UserName,
                        Password = agent.Password
                    };
                    _emailProcessor.SendForgotPasswordEmail(email);
                    return View("Success");
                }
            }
            ModelState.AddModelError("", "Please check and re-enter your User Name");
            return View("ForgotPassword", forgotPassword);
        }
    }
}
