﻿using System.Linq;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IAuthProvider _auth;
        private readonly ISession _session;

        public HomeController(IAuthProvider auth, ISession session) : base(auth, session)
        {
            _auth = auth;
            _session = session;
        }

        [HttpGet]
        [Authorize]
        public ViewResult Index()
        {
            return View("Index");
        }

        /// <summary>
        /// Current account displays information associated to the current authenticated users account
        /// </summary>
        [Authorize]
        public ActionResult CurrentAccount()
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());

            if (account == null)
            {
                return new EmptyResult();
            }
            var model = new CurrentAccountViewModel
                            {
                                AccountName = account.Name,
                                AccountType = account.AccountType.ToString()
                            };

            return PartialView("CurrentAccount", model);
        }

        [HttpGet]
        [Authorize]
        public ViewResult ChangePassword()
        {
            var model = new ChangePassword();
            return View("ChangePassword", model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ChangePassword(ChangePassword change)
        {
            if (ModelState.IsValid)
            {
                var agent = _session.Query<Agent>().SingleOrDefault(x => x.UserName == User.Identity.Name);
                if (agent != null)
                {
                    agent.Password = change.Password;
                    _session.Save(agent);
                    _session.Flush();

                    return RedirectToAction("Index", "Home");
                }
            }

            return View("ChangePassword", change);
        }
    }
}
