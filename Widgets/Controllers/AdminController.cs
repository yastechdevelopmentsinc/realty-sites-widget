﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    [Authorize(Roles = "Super")]
    public class AdminController : BaseController
    {
        private readonly ISession _session;

        public AdminController(IAuthProvider auth, ISession session) : base(auth, session)
        {
            _session = session;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new AdminIndexViewModel
                            {
                                Admins = Mapper.Map<IEnumerable<Agent>, IEnumerable<AdminInfo>>(
                                        _session.Query<Agent>().Where(x => x.Role == UserRole.Super.ToString()))
                            };
            return View("Index", model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new AdminInfo
                            {
                                Role = UserRole.Super.ToString()
                            };

            return View("Create", model);
        }

        [HttpPost]
        public ActionResult Create(AdminInfo admin)
        {
            if (CheckUsername(admin.UserName))
            {
                if (ModelState.IsValid)
                {
                    var model = Mapper.Map<AdminInfo, Agent>(admin);
                    _session.Save(model);
                    _session.Flush();

                    return RedirectToAction("Index", "Admin");
                }
            }
            else
            {
                ModelState.AddModelError("UserName", "User Name is already in use.");
            }

            return View("Create", admin);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var model = Mapper.Map<Agent, AdminInfo>(_session.Get<Agent>(id));

            return View("Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(AdminInfo admin)
        {
            if (CheckUsername(admin.UserName, admin.AgentID))
            {
                if (ModelState.IsValid)
                {
                    var model = _session.Get<Agent>(admin.AgentID);
                    if (TryUpdateModel(model))
                    {
                        _session.Save(model);
                        _session.Flush();
                        return RedirectToAction("Index", "Admin");
                    }
                }
            }
            else
            {
                ModelState.AddModelError("UserName", "User Name is already in use.");
            }

            return View("Edit", admin);
        }

        /// <summary>
        /// Checks to see if a user name is in user
        /// </summary>
        /// <param name="username">username to check</param>
        /// <param name="id">when editing ensure, that the username that exists is not for the current user</param>
        private bool CheckUsername(string username, int? id = null)
        {
            var agent = _session.Query<Agent>().SingleOrDefault(x=>x.UserName == username);
            if (agent == null) return true;

            // when editing check that the user id found is different than the user being edited
            if(id != null)
            {
                if(agent.AgentID == id)
                {
                    return true;
                }
            }

            return false;
        }

        public ActionResult Delete(int id)
        {
            var agent = _session.Get<Agent>(id);
            _session.Delete(agent);
            _session.Flush();

            return RedirectToAction("Index", "Admin");
        }
    }
}