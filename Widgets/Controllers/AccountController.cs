﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Helpers;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using AutoMapper;
using System.Collections.Generic;
using Widgets.Extensions;

namespace Widgets.Controllers
{
    public class AccountController : BaseController
    {
        private readonly ISession _session;
        private readonly IAuthProvider _auth;

        public AccountController(ISession session, IAuthProvider auth) : base(auth, session)
        {
            _session = session;
            _auth = auth;
        }

        /// <summary>
        /// Render the manage accounts view which displays a list of accounts
        /// </summary>
        [HttpGet]
        [Authorize(Roles = "Super")]
        public ViewResult ManageAccounts(string sortby)
        {
            ViewBag.AccountSort = sortby == "account" ? "account desc" : "account";
            ViewBag.AccountTypeSort = sortby == "accounttype" ? "accounttype desc" : "accounttype";

            var accounts = _session.Query<Account>();

            // apply sorting
            switch (sortby)
            {
                case "account":
                    accounts = accounts.OrderBy(x => x.Name);
                    break;
                case "account desc":
                    accounts = accounts.OrderByDescending(x => x.Name);
                    break;
                case "accounttype":
                    accounts = accounts.OrderBy(x => x.AccountType);
                    break;                
                
                case "accounttype desc":
                    accounts = accounts.OrderByDescending(x => x.AccountType);
                    break;
            }

            var model = new ManageAccountsViewModel
                            {
                                Accounts = accounts
                            };

             return View("ManageAccounts", model);
        }

        /// <summary>
        /// Render the create account view
        /// </summary>
        [HttpGet]
        [Authorize(Roles = "Super")]
        public ViewResult Create()
        {
            IEnumerable<Board> boards = _session.Query<Board>();
            var model = new AccountViewModel
                            {
                                AccountType = null, BoardCodes = boards.ToSelectList(x => x.BoardCode, x => x.BoardCode, "Select Board")
                            };

            return View("Create", model);
        }

        /// <summary>
        /// Create a new account
        /// </summary>
        /// <param name="account">Account to create</param>
        [HttpPost]
        [Authorize(Roles = "Super")]
        public ActionResult Create(AccountViewModel account, HttpPostedFileBase postedFile, HttpPostedFileBase postedMarker, HttpPostedFileBase postedMarkerMore)
        {
            if (ModelState.IsValid)
            {
                var model = Mapper.Map<AccountViewModel, Account>(account);

                if (postedFile != null)
                {
                    var imgBytes = ControllerHelpers.GetImageBytes(postedFile);
                    model.LogoMimeType = postedFile.ContentType;
                    model.Logo = imgBytes;
                }

                if (postedMarker != null)
                {
                    var imgBytes = ControllerHelpers.GetImageBytes(postedMarker);
                    account.MapMarkerMimeType = postedMarker.ContentType;
                    account.MapMarker = imgBytes;
                }
                else
                {
                    account.MapMarker = null;
                }

                if (postedMarkerMore != null)
                {
                    var imgBytes = ControllerHelpers.GetImageBytes(postedMarkerMore);
                    account.MapMarkerMoreMimeType = postedMarkerMore.ContentType;
                    account.MapMarkerMore = imgBytes;
                }
                else
                {
                    account.MapMarkerMore = null;
                }

                // replace new line characters
                account.AgentCode = RemoveNewLine(account.AgentCode);
                account.BrokerCode = RemoveNewLine(account.BrokerCode);

                _session.Save(model);
                _session.Flush();

                return RedirectToAction("ManageAccounts", "Account");
            }
            
            return View("Create");
        }

        /// <summary>
        /// Render the edit account view
        /// </summary>
        /// <param name="id">Account ID to edit</param>
        [HttpGet]
        [Authorize(Roles = "Super")]
        public ViewResult Edit(Guid id)
        {
            var model = Mapper.Map<Account, AccountViewModel>(_session.Get<Account>(id));
            IEnumerable<Board> boards = _session.Query<Board>();
            model.BoardCodes = boards.ToSelectList(x => x.BoardCode, x => x.BoardCode, "Select Board");
            if(!string.IsNullOrEmpty(model.AgentCode))
            {
                model.AgentCode = model.AgentCode.Replace("|", "\r\n");
            }
            if (!string.IsNullOrEmpty(model.BrokerCode))
            {
                model.BrokerCode = model.BrokerCode.Replace("|", "\r\n");
            }

            return View("Edit", model);
        }

        /// <summary>
        /// Edit account
        /// </summary>
        /// <param name="modifiedAccount"></param>
        [HttpPost]
        [Authorize(Roles = "Super")]
        public ActionResult Edit(AccountViewModel modifiedAccount, HttpPostedFileBase postedFile, HttpPostedFileBase postedMarker, HttpPostedFileBase postedMarkerMore)
        {
            if (ModelState.IsValid)
            {
                var account = _session.Get<Account>(modifiedAccount.Id); 
                if(TryUpdateModel(account))
                {
                    // account image
                    if (postedFile != null)
                    {
                        var imgBytes = ControllerHelpers.GetImageBytes(postedFile);
                        account.LogoMimeType = postedFile.ContentType;
                        account.Logo = imgBytes;
                    }

                    // account google map marker
                    if (postedMarker != null)
                    {
                        var imgBytes = ControllerHelpers.GetImageBytes(postedMarker);
                        account.MapMarkerMimeType = postedMarker.ContentType;
                        account.MapMarker = imgBytes;
                    }

                    // account google map cluster
                    if (postedMarkerMore != null)
                    {
                        var imgBytes = ControllerHelpers.GetImageBytes(postedMarkerMore);
                        account.MapMarkerMoreMimeType = postedMarkerMore.ContentType;
                        account.MapMarkerMore = imgBytes;
                    }

                    // replace new line characters
                    account.AgentCode = RemoveNewLine(account.AgentCode);
                    account.BrokerCode = RemoveNewLine(account.BrokerCode);

                    _session.Save(account);
                    _session.Flush();

                    return RedirectToAction("Index", "Home");   
                }
            }

            return View("Edit", modifiedAccount);
        }

        /// <summary>
        /// Allows an administrator to assume the identity of an account
        /// </summary>
        /// <param name="apikey">The account api key to assume</param>
        /// <param name="accountType">The account type p</param>
        [Authorize(Roles = "Super")]
        public ActionResult Assume(Guid apikey, string accountType)
        {
            // remove the cached principal object set within the base controller
            HttpContext.Cache.Remove(HttpContext.User.Identity.Name);

            // assume account
            _auth.SetAuthCookie(_auth.GetAuthTicketName(), _auth.GetUserRole(), apikey, accountType);

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Retrieve account logo for the given account id
        /// </summary>
        /// <param name="apiKey">The account id to get the logo of</param>
        [HttpGet]
        public ActionResult GetAccountLogo(Guid apiKey)
        {
            var account = _session.Get<Account>(apiKey);
            try
            {
                return account.Logo == null ? null : new FileContentResult(account.Logo, account.LogoMimeType);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Retrieve account google map marker for the given account id
        /// </summary>
        /// <param name="apiKey">The account id to get the marker icon of</param>
        [HttpGet]
        public ActionResult GetAccountMapMarker(Guid apiKey)
        {
            var account = _session.Get<Account>(apiKey);
            return account.MapMarker == null || account.MapMarkerMimeType == null ? null : new FileContentResult(account.MapMarker, account.MapMarkerMimeType);
        }

        /// <summary>
        /// Retrieve account google map cluster icon for the given account id
        /// </summary>
        /// <param name="apiKey">The account id to get the cluster icon of</param>
        [HttpGet]
        public ActionResult GetAccountMapMarkerMore(Guid apiKey)
        {
            var account = _session.Get<Account>(apiKey);
            return account.MapMarkerMore == null || account.MapMarkerMoreMimeType == null ? null : new FileContentResult(account.MapMarkerMore, account.MapMarkerMoreMimeType);
        }

        /// <summary>
        /// Delete the specified account
        /// </summary>
        /// <param name="id">Id of the account to delete</param>
        [Authorize(Roles = "Super")]
        public ActionResult Delete(Guid id)
        {
            var account = _session.Get<Account>(id);
            _session.Delete(account);
            _session.Flush();

            return RedirectToAction("ManageAccounts");
        }

        /// <summary>
        /// Delete the google map marker for the specified account
        /// </summary>
        /// <param name="id">Account id</param>
        [HttpGet]
        public ActionResult DeleteAccountMapMarker(Guid id)
        {
            var account = _session.Get<Account>(id);
            account.MapMarker = null;
            account.MapMarkerMimeType = null;

            _session.Save(account);
            _session.Flush();

            return RedirectToAction("Edit", new { id });
        }

        /// <summary>
        /// Delete the google map cluster for the specified account
        /// </summary>
        /// <param name="id">Account id</param>
        [HttpGet]
        public ActionResult DeleteAccountMapMarkerMore(Guid id)
        {
            var account = _session.Get<Account>(id);
            account.MapMarkerMore = null;
            account.MapMarkerMoreMimeType = null;

            _session.Save(account);
            _session.Flush();

            return RedirectToAction("Edit", new { id });
        }

        private static string RemoveNewLine(string replace)
        {
            if (replace == null) return string.Empty;
            const string replaceWith = "|";
            return replace.Replace("\r\n", replaceWith).Replace("\n", replaceWith).Replace("\r", replaceWith);
        }
    }
}