﻿using System.Linq;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    [Authorize(Roles = "Super, Admin")]
    public class EditCoordinatesController : BaseController
    {
        private readonly ISession _session;
        private readonly IListingService _listingService;

        public EditCoordinatesController(IAuthProvider auth, ISession session, IListingService listingService) : base(auth, session)
        {
            _session = session;
            _listingService = listingService;
        }

        /// <summary>
        /// Render the edit coordinate view
        /// </summary>
        [HttpGet]
        public ViewResult Index()
        {
            var model = new EditCoordinatesViewModel();

            return View("Index", model);
        }

        /// <summary>
        /// Save modifications to latitude / longitude
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        public JsonResult Index(EditCoordinatesViewModel model)
        {
            int propertyRef = 0;
            if (model.BoardCode != "VIREB")
            {
                switch (model.PropertyType)
                {
                    case PropertyTypeConstants.Residential:
                        var resi = _listingService.GetResidentialListing(model.MlsNumber);
                        propertyRef = resi.ResiID;
                        break;

                    case PropertyTypeConstants.ForLease:
                        var lease = _listingService.GetForLeaseListing(model.MlsNumber);
                        propertyRef = lease.ForLeaseID;
                        break;

                    case PropertyTypeConstants.Commercial:
                        var commercial = _listingService.GetCommercialListing(model.MlsNumber);
                        propertyRef = commercial.CommID;
                        break;

                    case PropertyTypeConstants.FarmsAndLand:
                        var farmsandland = _listingService.GetFarmsAndLandListing(model.MlsNumber);
                        propertyRef = farmsandland.FarmsAndLandID;
                        break;

                    case PropertyTypeConstants.MultiFamily:
                        var multifamily = _listingService.GetMultiFamilyListing(model.MlsNumber);
                        propertyRef = multifamily.MultFamID;
                        break;
                }
            }
            else
            {
                switch (model.PropertyType)
                {
                    case PropertyTypeConstants.Residential:
                        var resi = _listingService.GetVResidentialListing(model.MlsNumber);
                        propertyRef = resi.ID;
                        _session.Evict(resi);
                        break;

                    case PropertyTypeConstants.Commercial:
                        var commercial = _listingService.GetVResidentialListing(model.MlsNumber);
                        propertyRef = commercial.ID;
                        _session.Evict(commercial);
                        break;
                }
            }

            var replacement = _session.Query<ReplacementCoord>().SingleOrDefault(x=> x.PropertyRef == propertyRef) ?? new ReplacementCoord
                                                               {
                                                                   PropertyRef = propertyRef,BoardCode = model.BoardCode
                                                               };

            replacement.Latitude = model.Latitude;
            replacement.Longitude = model.Longitude;

            _session.Save(replacement);
            _session.Flush();
            
            return new JsonResult
                       {
                           JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                           Data = new {html = "<p>Coordinates updated succesfully</p>"}
                       };
        }
    }
}