﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    [Authorize(Roles = "Super, Admin, User, Listings")]
    public class ReferralController : BaseController
    {
        private readonly IAuthProvider _auth;
        private readonly ISession _session;

        public ReferralController(IAuthProvider authProvider, ISession session) : base(authProvider, session)
        {
            _auth = authProvider;
            _session = session;
        }

        /// <summary>
        /// Render referral list for current account
        /// </summary>
        [HttpGet]
        public ViewResult Index(string sortby = "position")
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var referrals = AutoMapper.Mapper.Map<IEnumerable<Referral>, IEnumerable<ReferralInfo>>(_session.Query<Referral>().Where(x=>x.Account == account))
                .OrderBy(x => x.PositionNumber);

            // apply sorting
            ViewBag.PositionSort = sortby == "position" ? "position desc" : "position";
            ViewBag.MemberSort = sortby == "member" ? "member desc" : "member";

            switch(sortby)
            {
                case("position"):
                    referrals = referrals.OrderBy(x => x.PositionNumber);
                    break;                
                case("position desc"):
                    referrals = referrals.OrderByDescending(x => x.PositionNumber);
                    break;
                case ("member"):
                    referrals = referrals.OrderBy(x => x.Agent.FullName);
                    break;
                case ("member desc"):
                    referrals = referrals.OrderByDescending(x => x.Agent.FullName);
                    break;                
            }

            var model = new ReferralIndexViewModel
                            {
                                Referrals = referrals
                            };

            return View("Index", model);
        }

        /// <summary>
        /// Display lead history for specified agent
        /// </summary>
        /// <param name="id">Agent id to retreive lead history for</param>
        [HttpGet]
        public PartialViewResult History(int id)
        {
            var agent = _session.Get<Agent>(id);
            var leads = AutoMapper.Mapper.Map<IEnumerable<Lead>, IEnumerable<LeadInfo>>(_session.Query<Lead>().Where(x=>x.Agent == agent));

            return PartialView("History", leads);
        }

        /// <summary>
        /// Render add lead view for the specific agent
        /// </summary>
        /// <param name="id">Agent id to add a lead too</param>
        [HttpGet]
        public PartialViewResult AddLead(int id)
        {
            var model = new LeadInfo();
            return PartialView("AddLead", model);
        }

        /// <summary>
        /// Adds a new lead to the specified referral
        /// </summary>
        /// <param name="lead">The lead to add</param>
        /// <param name="id">Referral id</param>
        [HttpPost]
        public ActionResult AddLead(LeadInfo lead, int id)
        {
            var agent = _session.Get<Agent>(id);

            var model = AutoMapper.Mapper.Map<LeadInfo, Lead>(lead);
            model.Agent = agent;
            model.Created = DateTime.Now;

            _session.Save(model);
            _session.Flush();

            MoveToLastPosition(agent.Account, id);

            return RedirectToAction("Index", "Referral");
        }

        /// <summary>
        /// Moves the referral the lead was added to, to the bottom of the lead system
        /// </summary>
        /// <param name="account">The account of the referral that had a lead add too</param>
        /// <param name="agentid">The agentid of the referral</param>
        private void MoveToLastPosition(Account account, int agentid)
        {
            var referrals = _session.Query<Referral>().Where(x => x.Account == account).ToList();

            var agent = referrals.SingleOrDefault(x => x.Agent.AgentID == agentid);
            agent.PositionNumber = referrals.Max(x => x.PositionNumber) + 1;
            agent.LastReferralDate = DateTime.Now;

            var i = 1;
            foreach (var referral in referrals.OrderBy(x => x.PositionNumber))
            {
                referral.PositionNumber = i;
                _session.Save(referral);
                i++;
            }
            _session.Flush();
        }
    }
}
