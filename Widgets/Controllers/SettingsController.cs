﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    [Authorize(Roles = "Super")]
    public class SettingsController : BaseController
    {
        private readonly ISession _session;

        public SettingsController(IAuthProvider auth, ISession session) : base(auth, session)
        {
            _session = session;
        }

        [HttpGet]
        public ViewResult ManageSettings()
        {
            var model = Mapper.Map<CalculatorDefaults, ManageSettingsViewModel>(_session.Query<CalculatorDefaults>().FirstOrDefault());
            return View("ManageSettings", model);
        }

        [HttpPost]
        public ActionResult ManageSettings(ManageSettingsViewModel manage)
        {
            if (ModelState.IsValid)
            {
                var settings = _session.Query<CalculatorDefaults>().FirstOrDefault() ?? new CalculatorDefaults();
                
                if(TryUpdateModel(settings))
                {
                    _session.Save(settings);
                    _session.Flush();
                }

                return RedirectToAction("Index", "Home");
            }

            return View("ManageSettings", manage);
        }
    }
}
