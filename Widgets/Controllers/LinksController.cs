﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using NHibernate;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    [Authorize]
    public class LinksController : BaseController
    {
        private readonly ISession _session;
        private readonly IAuthProvider _auth;

        public LinksController(IAuthProvider auth, ISession session) : base(auth, session)
        {
            _auth = auth;
            _session = session;
        }

        [HttpGet]
        public ViewResult ManageLinks()
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var model = new ManageLinksViewModel
                {
                    Links = Mapper.Map<IEnumerable<AccountLink>, IEnumerable<AccountLinkViewModel>>(AccountLink.GetByAccount(_session, account))
                };
            return View("ManageLinks", model);
        }

        [HttpGet]
        public ViewResult Create()
        {
            var model = new AccountLinkViewModel();
            return View("Create", model);
        }
        
        [HttpPost]
        public ActionResult Create(AccountLinkViewModel create)
        {
            if (ModelState.IsValid)
            {
                var model = Mapper.Map<AccountLinkViewModel, AccountLink>(create);
                model.Account = Account.GetById(_session, _auth.GetAccountKey());
                _session.Save(model);
                _session.Flush();

                return RedirectToAction("ManageLinks", "Links");
            }

            return View("Create", create);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var link = Mapper.Map<AccountLink, AccountLinkViewModel>(AccountLink.GetById(_session, id));
            return View("Edit", link);
        }

        [HttpPost]
        public ActionResult Edit(AccountLinkViewModel link)
        {
            if (ModelState.IsValid)
            {
                var model = AccountLink.GetById(_session, link.Id);
                if (TryUpdateModel(model))
                {
                    _session.Save(model);
                    _session.Flush();

                    return RedirectToAction("ManageLinks", "Links");
                }
            }

            return View("Edit", link);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var link = AccountLink.GetById(_session, id);
            if (link.Account.Id == _auth.GetAccountKey())
            {
                _session.Delete(link);
                _session.Flush();
            }

            return RedirectToAction("ManageLinks", "Links");
        }
    }
}
