﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using NHibernate;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Extensions;
using Widgets.Helpers;
using Widgets.Infrastructure;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    public class EmailController : Controller
    {
        private readonly IEmailProcessor _emailProcessor;
        private readonly IListingService _listingService;
        private readonly ISession _session;

        public EmailController(IEmailProcessor emailProcessor, ISession session, IListingService listingService)
        {
            _emailProcessor = emailProcessor;
            _session = session;
            _listingService = listingService;
        }

        /// <summary>
        /// Render the email form as a light box pop up windows
        /// </summary>
        /// <param name="id">The id of the agent to email</param>
        /// <param name="propertyType">The property type of the listingId</param>
        /// <param name="listingId">The listingId referenced in the email</param>
        /// <param name="accountkey">The account key to email in place of the agent</param>
        [HttpGet]
        [JsonpFilter]
        public JsonResult EmailAgent(int id, string propertyType, int? listingId, Guid? accountkey, int? officeid)
        {
            Email model;
            if(id > 0)
            {
                var agent = _session.Get<Agent>(id);
                model = new Email
                {
                    AgentID = (int) id,
                    Contact = agent.FullName,
                    ToAddress = agent.Email
                };
            }
            else if (officeid != null && officeid > 0)
            {
                var office = _session.Get<AccountOffice>(officeid);
                model = new Email
                {
                    OfficeID = officeid.Value,
                    Contact = office.Name,
                    ToAddress = office.Email
                };
            }
            else
            {
                var account = _session.Get<Account>((Guid)accountkey);
                model = new Email
                            {
                                AccountKey = (Guid)accountkey,
                                Contact = account.Name,
                                ToAddress = account.Email
                            };
            }

            if (listingId != null && listingId > 0)
            {
                model.PropertyType = propertyType;
                model.ListingId = (int) listingId;
                model.Comments = "Regarding Listing ID# " + listingId;
            }

            return new JsonResult
                       {
                           JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                           Data = new {html = this.RenderPartialViewToString("EmailAgent", model)}
                       };
        }


        /// <summary>
        /// Send an email to the specified agent
        /// </summary>
        /// <param name="email">Email to send</param>
        [JsonpFilter]
        public JsonResult SendEmailAgent(Email email)
        {
            if (ModelState.IsValid)
            {
                if (email.ListingId > 0)
                {
                     var account = _session.Get<Account>((Guid)email.AccountKey);
                    var helper = new WidgetControllerHelper(_listingService);
                    var listing = helper.GetListingByPropertyTypeAndMlsNumber(email.PropertyType, email.ListingId,account.BoardCode);
                    var sharethis = ShareThis.MakeUrl(listing, Request);
                    _emailProcessor.SendEmail(email, listing, sharethis);
                }
                else
                {
                    _emailProcessor.SendEmail(email);
                }
                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { message = "success" }
                };
            }

            // return modelstate errors as json
            Dictionary<string, string[]> errorList = ModelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
                );

            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = new { message = "error", errorList }
            };

        }


        /// <summary>
        /// Render the email listing form as a light box pop up window
        /// </summary>
        /// <param name="propertyType">The Property type of the listing to be emailed</param>
        /// <param name="listingId">The listingId of the listing to be emailed</param>
        [HttpGet]
        [JsonpFilter]
        public JsonResult EmailListing(string propertyType, int listingId, Guid? accountkey)
        {
            string boardcode = "";
            if (accountkey != null)
            {
                var account = _session.Get<Account>((Guid)accountkey);
                boardcode = account.BoardCode;
            }
            var model = new EmailListing
            {
                PropertyType = propertyType,
                ListingId = listingId, BoardCode=boardcode,
                Comments = "Regarding Listing ID# " + listingId
            };

            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = new { html = this.RenderPartialViewToString("EmailListing", model) }
            };
        }


        /// <summary>
        /// Send the listing email
        /// </summary>
        /// <param name="email">Email to send</param>
        [JsonpFilter]
        public JsonResult SendEmailListing(EmailListing email)
        {
            if (ModelState.IsValid)
            {
                var helper = new WidgetControllerHelper(_listingService);
                var listing = helper.GetListingByPropertyTypeAndMlsNumber(email.PropertyType, email.ListingId,email.BoardCode);
                var sharethis = ShareThis.MakeUrl(listing, Request);
                _emailProcessor.SendListingEmail(email, listing, sharethis);

                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { message = "success" }
                };
            }

            // return modelstate errors as json
            Dictionary<string, string[]> errorList = ModelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
                );

            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = new { message = "error", errorList }
            };

        }
    }
}