﻿using System.Web.Mvc;
using AutoMapper;
using NHibernate;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    [Authorize(Roles = "Super")]
    public class SiteFeaturesController : BaseController
    {
        private readonly IAuthProvider _auth;
        private readonly ISession _session;

        public SiteFeaturesController(IAuthProvider auth, ISession session) : base(auth, session)
        {
            _auth = auth;
            _session = session;
        }

        /// <summary>
        /// Render manage features view which displays a view to enable / disabled the features associated to an account
        /// </summary>
        [HttpGet]
        public ViewResult ManageFeatures()
        {
            var features = _session.Get<Account>(_auth.GetAccountKey()).Features;
            var model = features == null ? new AccountFeaturesViewModel() : Mapper.Map<AccountFeatures, AccountFeaturesViewModel>(features);

            return View("ManageFeatures", model);
        }

        /// <summary>
        /// Create new / save changes made to features for account
        /// </summary>
        /// <param name="features"></param>
        [HttpPost]
        public ActionResult ManageFeatures(AccountFeaturesViewModel features)
        {
            AccountFeatures model;
            if (features.Id > 0)
            {
                model = _session.Get<AccountFeatures>(features.Id);
                if (model.EnableFeaturedAgentsSchedule != features.EnableFeaturedAgentsSchedule)
                {
                     var currentagents = _session.QueryOver<Agent>().Where(x=>x.Account == model.Account && x.FeaturedAgent && x.InProgress == true).List();
                     if (currentagents != null && currentagents.Count > 0)
                     {
                         for (int i = 0; i < currentagents.Count; i++)
                         {
                             var currentfeaturedagent = currentagents[i];
                             currentfeaturedagent.InProgress = false;
                             currentfeaturedagent.StartDate = null;
                             _session.Update(currentfeaturedagent);
                             _session.Flush();
                         }
                     }
                }
                TryUpdateModel(model);
            }
            else
            {
                model = Mapper.Map<AccountFeaturesViewModel, AccountFeatures>(features);
                model.Account = Account.GetById(_session, _auth.GetAccountKey());
            }

            _session.Save(model);
            _session.Flush();

            return RedirectToAction("Index", "Home");
        }

    }
}
