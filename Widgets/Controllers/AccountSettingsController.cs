﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    public class AccountSettingsController : BaseController
    {
        private readonly IAuthProvider _auth;
        private readonly ISession _session;

        public AccountSettingsController(IAuthProvider auth, ISession session) : base(auth, session)
        {
            _auth = auth;
            _session = session;
        }

        /// <summary>
        /// Render manage urls view which displays list of accounts "authorized" urls
        /// </summary>
        [HttpGet]
        public ViewResult ManageUrls()
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var model = new ManageUrlsViewModel
            {
                AccountUrls = Mapper.Map<IEnumerable<AccountUrl>, IEnumerable<AccountUrlViewModel>>(_session.Query<AccountUrl>().Where(x=>x.Account == account)),
                AccountKey = account.Id
            };

            return View("ManageUrls", model);
        }

        /// <summary>
        /// AddUrl is an lightbox window, render add new url partial view
        /// </summary>
        [HttpGet]
        public PartialViewResult AddUrl()
        {
            var model = new AccountUrlViewModel();
            return PartialView("AddUrl", model);
        }

        /// <summary>
        /// Save Authorized URL (called from Add Url & Edit Url methods
        /// </summary>
        /// <param name="accountUrlViewModel"></param>
        [HttpPost]
        public ActionResult SaveUrl(AccountUrlViewModel accountUrlViewModel)
        {
            if (ModelState.IsValid)
            {
                AccountUrl model;

                if (accountUrlViewModel.Id == null)
                {
                    var account = Account.GetById(_session, _auth.GetAccountKey());
                    model = Mapper.Map<AccountUrlViewModel, AccountUrl>(accountUrlViewModel);
                    model.Account = account;
                }
                else
                {
                    model = _session.Get<AccountUrl>((int)accountUrlViewModel.Id);
                    TryUpdateModel(model);
                }

                _session.Save(model);
                _session.Flush();

                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = "success"
                };
            }

            // return modelstate errors as json
            Dictionary<string, string[]> errorList = ModelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
                );

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = errorList
            };
        }

        /// <summary>
        /// EditUrl opens in a lightbox window, render edit url partial view
        /// </summary>
        /// <param name="id"></param>
        [HttpGet]
        public PartialViewResult EditUrl(int id)
        {
            var url = Mapper.Map<AccountUrl, AccountUrlViewModel>(_session.Get<AccountUrl>(id));
            return PartialView("EditUrl", url);
        }

        /// <summary>
        /// Delete an account url
        /// </summary>
        /// <param name="id"></param>
        [HttpGet]
        public ActionResult DeleteUrl(int id)
        {
            var url = _session.Get<AccountUrl>(id);
            if(url.Account.Id == _auth.GetAccountKey())
            {
                _session.Delete(url);
                _session.Flush();
            }

            return RedirectToAction("ManageUrls", "AccountSettings");
        }

    }
}
