﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Caching;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using AutoMapper;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Mapping;
using NHibernate.Transform;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Extensions;
using Widgets.Helpers;
using Widgets.Infrastructure;
using Widgets.Models;
using Widgets.Models.ViewModels;
using Widgets.Properties;
using System.Threading.Tasks;
using System.Collections;
using Newtonsoft.Json;

namespace Widgets.Controllers
{

    /// <summary>
    /// Controller for searching and displaying listing data
    /// </summary>
    /// 
    public class WidgetController : Controller
    {
        private readonly IListingService _listingService;
        private readonly ISession _session;

        public WidgetController(IListingService service, ISession session)
        {
            _listingService = service;
            _session = session;
        }

        /// <summary>
        /// Displays a list of all listings that meet the search criteria
        /// </summary>
        /// <param name="accountkey">account key associated to the request</param>
        /// <param name="listings">listing display options (all, brokerage, agent, openhouse)</param>
        /// <param name="showfilter">show the mini search filter</param>
        /// <param name="filter">the default mini search filter</param>
        /// <param name="id">current page of the results to display</param>
        /// <param name="sort">the sort options for how the listings should display</param>
        /// <param name="sessionid">sessionid associated to request</param>
        /// <param name="mostrecent">if set display the the specfied number of the most recent listings on the current sort</param>
        /// <param name="agentid">the agentid to display listings for</param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        [AuthorizedWidget(WidgetType.ListingWidget)]
        public JsonResult DisplayListings(Guid accountkey, string listings = WidgetOptions.AllListings, bool showfilter = false, string filter = "", int id = 1, string sort = "", string sessionid = "", int mostrecent = 0, int agentid = 0, string status = "")
        {
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;

            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
                var account = Account.GetById(_session, accountkey);

                if (listings != "rentals" && string.IsNullOrEmpty(status))
                {
                    status = PropertyStatusConstants.ForSaleOrLease;
                }
                SearchCriteria searchCriteria = new SearchCriteria();
                // setup the search criteria model used within the widget controller helper for filtering listings
                searchCriteria = SetupSearchCriteria(account, listings, false, sessionid, filter);
                // setup sorting
                if (string.IsNullOrEmpty(searchCriteria.Sort))
                {
                    searchCriteria.Sort = sort;
                }

                if (string.IsNullOrEmpty(searchCriteria.Status))
                {
                    searchCriteria.Status = status;
                }

                // configure how many listings to display
                var features = account.Features;
                int restrictListings = features.NumberOfListingsToDisplay;
                if (mostrecent > 0)
                {
                    restrictListings = mostrecent;
                }

                // restrict the listings returned by the agent id if specified
                if (agentid > 0)
                {
                    var agent = Agent.GetByID(_session, agentid);
                    if (agent != null)
                    {
                        searchCriteria.AgentID = agent.AgentIndCode;
                    }
                }

                // get listing data
                var helper = new WidgetControllerHelper(_listingService, id, features.ListingWidgetPageSize, searchCriteria, false, restrictListings);
                var model = helper.GetListingWidgetData(account.BoardCode, account.Features.NumberOfListingsToDisplay);

                model.Account = Mapper.Map<Account, AccountViewModel>(account);
                model.Disclaimer = Mapper.Map<Disclaimer, DisclaimerViewModel>(_session.Query<Disclaimer>().SingleOrDefault(x => x.BoardCode == account.BoardCode));
                model.SessionID = searchCriteria.SessionID;

                // ensure that if the widget is configured to display open house listings the "open house" feature is enabled
                VerifyOpenHouseDisplay(searchCriteria.WidgetOptions, features, model);

                // the mini search is used on the listings widget with the "search" option enabled
                SetupMiniSearch(showfilter, model, sessionid, filter);

                // setup sort options
                if (!string.IsNullOrEmpty(sort))
                {
                    if (string.IsNullOrEmpty(searchCriteria.Sort))
                    {
                        searchCriteria.Sort = sort;
                    }

                    if (model.MiniSearch != null && string.IsNullOrEmpty(model.MiniSearch.Sort))
                    {
                        model.MiniSearch.Sort = sort;
                    }
                }

                if (!string.IsNullOrEmpty(status))
                {
                    if (string.IsNullOrEmpty(searchCriteria.Status))
                    {
                        searchCriteria.Status = status;
                    }

                    if (model.MiniSearch != null && string.IsNullOrEmpty(model.MiniSearch.Status))
                    {
                        model.MiniSearch.Status = status;
                    }
                }

                var cacheKey = CreateCacheKey("SearchCriteria", searchCriteria.SessionID);
                searchCriteria.PageId = id;
                HttpContext.Cache.Insert(cacheKey, searchCriteria, null, Cache.NoAbsoluteExpiration, TimeSpan.FromDays(1));
                // check if account can display open house information 
                model.DisplayOpenHouseInfo = features.OpenHouses;
                if (account.Features.ShowSoldLeasedWatermark)
                {
                    ViewBag.LeasedBanner = string.Format("{0}/Content/images/leased.png", Properties.Settings.Default.DefaultHostName);

                    ViewBag.SoldBanner = string.Format("{0}/Content/images/sold.png", Properties.Settings.Default.DefaultHostName);
                }
                //return View("DisplayListings", model);
                return new JsonResult
                           {
                               JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                               Data = new { html = this.RenderPartialViewToString("DisplayListings", model) }
                           };
            }

            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        private void Parse_Multiple_Listing_Types(string listings, SearchCriteria searchCriteria)
        {
            try
            {
                var serialzer = new JavaScriptSerializer();
                var listingList = serialzer.Deserialize<IEnumerable<string>>(listings).ToList();
                if (listingList.Count > 1)
                {
                    searchCriteria.WidgetOptions = WidgetOptions.AllListings;
                    for (int i = 0; i < listingList.Count; i++)
                    {
                        switch (listingList[i])
                        {
                            case WidgetOptions.ResidentialListings:
                                searchCriteria.Residential = true;
                                break;

                            case WidgetOptions.CommercialListings:
                                searchCriteria.Commercial = true;
                                break;

                            case WidgetOptions.FarmsAndLandListings:
                                searchCriteria.FarmsAndLand = true;
                                break;

                            case WidgetOptions.ForLeaseListings:
                                searchCriteria.ForLease = true;
                                break;

                            case WidgetOptions.MultiFamListings:
                                searchCriteria.MultiFam = true;
                                break;
                            default: break;
                        }
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Renders the search form that will be embedded within a clients website
        /// </summary>
        /// <param name="accountkey">account key associated to the request</param>
        /// <param name="autoload">widget option, that if true should load property data</param>
        /// <param name="redirecturl">if present, this is the url to redirect too after a search is made</param>
        /// <param name="listings">listing display options (all, brokerage, agent, openhouse)</param>
        /// <param name="cities">an optional list of cities to display at the top of the city list</param>
        /// <param name="limitCities">an optional boolean that when true restricts the cities to that of those specified in the cities param</param>
        /// <param name="sort">default sort option </param>
        /// <param name="sessionid">sessionid associated to a previous search that was made to allow the default search options to be set</param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        [AuthorizedWidget(WidgetType.SearchWidget)]
        public JsonResult Search(Guid accountkey, bool autoload = false, string redirecturl = "", string listings = WidgetOptions.AllListings, string cities = "", bool limitCities = false, string sort = "", string sessionid = "", string filter = "")
        {
            _session.Clear();
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;

            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
            // setup the search criteria model used within the widget controller helper for filtering listings
            var account = Account.GetById(_session, accountkey);
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = SetupSearchCriteria(account, listings, !autoload, sessionid, filter);

            searchCriteria.RedirectUrl = redirecturl;

            // if search if for open house show date fields for open house from / to date
            if (listings == WidgetOptions.OpenHouseListings)
            {
                searchCriteria.DateFrom = DateTime.Today;
                searchCriteria.DateTo = DateTime.Today.AddDays(7);
            }

            searchCriteria.Bathrooms = 0;
            searchCriteria.Bedrooms = 0;

            // setup search price low / hi lists
            var prices = new[]
                                 {
                                     "25000", "50000", "75000", "100000", "125000",
                                     "150000", "175000", "200000", "225000", "250000",
                                     "275000", "300000", "325000", "350000", "375000",
                                     "400000", "425000", "450000", "475000", "500000", "550000", "600000",
                                     "650000", "700000", "750000", "800000", "850000",
                                     "900000", "950000", "1000000", "1500000", "2000000"
                                 };

            // setup sort options
            var sortOptions = new[] { "New Listings", "Address", "Lowest Price", "Highest Price" };
            if (!string.IsNullOrEmpty(sort) && string.IsNullOrEmpty(searchCriteria.Sort))
            {
                searchCriteria.Sort = sort;
            }

            // parse the list of cities passed in as they should appear on the top of the city list
            var serialzer = new JavaScriptSerializer();
            var cityList = serialzer.Deserialize<IEnumerable<string>>(cities);

            List<SearchCity> savedcities = Get_Cities(account, listings, searchCriteria);
            // select the locations that have listings 
            /*var locations = _session.Query<Location>().Where(x=>x.BoardCode == account.BoardCode)
                .Cacheable()
                .CacheRegion("LongTerm")
                .AsEnumerable();

            var distinctLocations =
                (from l in locations
                    group l by l.CityName into g
                    select new {CityName = g.Key, Listings = g.Sum(x=>x.Listings)})
                        .OrderBy(x => x.CityName)
                        .Where(x => x.Listings > 0).ToList();*/
            /*var distinctLocations =
                    (from l in locations
                     group l by l.CityName into g
                     select new { CityName = g.Key, Listings = g.Sum(x => x.Listings) })
                            .OrderBy(x => x.CityName).ToList();*/

            int allPosition;
            if (cityList != null && cityList.Any())
            {
                var cityCount = cityList.Count();
                foreach (var city in cityList.Reverse())
                {
                    if (city.Contains('/'))
                    {
                        string[] groupcities = city.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                        if (groupcities.Length > 0)
                        {
                            int numberlisting = 0;
                            for (int i = 0; i < groupcities.Length; i++)
                            {
                                var existcity = (from loc in savedcities where loc.CityName.ToLower() == groupcities[i].ToLower() select loc).FirstOrDefault();
                                if (existcity != null)
                                {
                                    numberlisting += existcity.Listings;
                                }
                            }
                            savedcities.Insert(0, new SearchCity { CityName = city, Listings = numberlisting });
                        }
                    }
                    else
                    {
                        var existcity = (from loc in savedcities where loc.CityName.ToLower() == city.ToLower() select loc).FirstOrDefault();
                        savedcities.Remove(existcity);
                        if (existcity == null)
                        {
                            existcity = new SearchCity { CityName = city, Listings = 0 };
                        }
                        savedcities.Insert(0, new SearchCity { CityName = city, Listings = existcity.Listings });
                    }
                }

                if (limitCities)
                {
                    // remove all cities that are not explicitly set 
                    savedcities.RemoveRange(cityCount, savedcities.Count() - cityCount);
                }
                allPosition = cityCount;
            }
            else
            {
                allPosition = 0;
            }

            if (!limitCities)
            {
                // add an all cities option
                if (!savedcities.Exists(x => x.CityName == "All"))
                {
                    savedcities.Insert(allPosition, new SearchCity { CityName = "All", Listings = 0 });
                }
            }
            object locationlist = null;
            List<string> locationliststring = null;
            // setup the model
            var model = new ListingSearchViewModel
                            {
                                BoardCode = account.BoardCode,
                                City = savedcities.ToSelectList(value => value.CityName, text => text.CityName == "All" ? "All" : String.Format("{0} ({1})", text.CityName.Contains('/') ? text.CityName.Substring(0, text.CityName.IndexOf("/", 20) == -1 ? text.CityName.Length - 1 : text.CityName.IndexOf("/", 20)) : text.CityName, text.Listings.ToString()), ""),
                                //City = distinctLocations.ToSelectList(value => value.CityName, text => text.CityName == "All" ? "All" : String.Format("{0}", text.CityName), ""),
                                Area = searchCriteria.City == null
                                           ? GetAreas(savedcities.ElementAt(0).CityName, null, account.BoardCode, account, listings, out locationlist, out locationliststring)
                                           : GetAreas(searchCriteria.City, null, account.BoardCode, account, listings, out locationlist, out locationliststring),
                                PriceLow = prices.ToSelectList(x => x.ToString(CultureInfo.InvariantCulture),
                                                               x => x.ToString(CultureInfo.InvariantCulture), "0"),
                                PriceHigh = prices.ToSelectList(x => x.ToString(CultureInfo.InvariantCulture),
                                                                x => x.ToString(CultureInfo.InvariantCulture),
                                                                "unlimited"),
                                Bathrooms = Enumerable.Range(0, 6).ToSelectList(
                                    x => x.ToString(CultureInfo.InvariantCulture),
                                    x => x.ToString(CultureInfo.InvariantCulture) + "+", ""),
                                Bedrooms = Enumerable.Range(0, 6).ToSelectList(
                                    x => x.ToString(CultureInfo.InvariantCulture),
                                    x => x.ToString(CultureInfo.InvariantCulture) + "+", ""),
                                Sort = sortOptions.ToSelectList(x => x.ToString(CultureInfo.InvariantCulture),
                                                         x => x.ToString(CultureInfo.InvariantCulture), ""),
                                AutoLoad = autoload,
                                MLSSearch = account.Features.MLSSearch,
                                SearchCriteria = searchCriteria,
                                ListingAccess = Mapper.Map<AccountListingAccess, ListingAccessViewModel>(
                                        _session.Query<AccountListingAccess>()
                                            .Cacheable()
                                            .CacheRegion("LongTerm")
                                            .SingleOrDefault(x => x.Account.Id == accountkey))
                            };
            ViewBag.loading = string.Format("{0}/Content/images/loading_widget.gif", Properties.Settings.Default.DefaultHostName);
            return new JsonResult
                       {
                           JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                           Data = new { html = this.RenderPartialViewToString("Search", model) }
                       };

             }

            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        public JsonResult Get_AutoComplete_List(Guid accountkey)
        {
            List<string> autocomplete = new List<string>();
            var account = Account.GetById(_session, accountkey);
            /*object locationlist = null;
            List<string> locationliststring = null;

            GetAreas(null, null, account.BoardCode, account, out locationlist, out locationliststring).ToList();
            autocomplete.AddRange(locationliststring);
            List<SearchCity> savedcities = Get_Cities(account);
            autocomplete.AddRange(savedcities.Select(x=>x.CityName).ToList());
            object zipcodelist = null;
            List<string> zipcodesliststring = null;
            GetZipcodes( null, account.BoardCode, account, out zipcodelist, out zipcodesliststring).ToList();
            autocomplete.AddRange(zipcodesliststring);
            autocomplete.AddRange(GetAddresses(account.BoardCode));
            autocomplete = autocomplete.Distinct().ToList();*/
            try
            {
                autocomplete = Get_AutoComplete_List(account);
                return Json(autocomplete, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString());
            }
        }

        private List<string> Get_AutoComplete_List(Account account)
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            List<string> filteredautocompletestrings = new List<string>();
            if (account.BoardCode == "VIREB")
            {
                var filteredautocompleteobjects = _session.Query<VResidential>().ToList().Select(x =>
                              new
                              {
                                  ZipCode = x.PostalCode,
                                  Address = x.Address + ", " + x.City,
                                  CityName = x.City,
                                  AreaName = x.AreaName
                              }
                      ).ToList();
                filteredautocompleteobjects.AddRange(_session.Query<VCommercial>().ToList().Select(x =>
                               new
                               {
                                   ZipCode = x.PostalCode,
                                   Address = x.Address + ", " + x.City,
                                   CityName = x.City,
                                   AreaName = x.AreaName
                               }
                       ).ToList());
                filteredautocompleteobjects.AddRange(_session.Query<Exclusive>().Where(x => x.AgentBoardCode == account.BoardCode && x.Account.Id == account.Id).Select(x =>
                    new
                    {
                        ZipCode = x.PostalCode,
                        Address = x.Address + ", " + x.City,
                        CityName = x.City,
                        AreaName = (x.SubAreaName.Length > 3 && x.SubAreaName[2] == '-') ? x.SubAreaName.Remove(0, 3) : x.SubAreaName
                    }
                    ).ToList());
                filteredautocompletestrings.AddRange(filteredautocompleteobjects.Select(x => x.Address));
                filteredautocompletestrings.AddRange(filteredautocompleteobjects.Select(x => x.AreaName));
                filteredautocompletestrings.AddRange(filteredautocompleteobjects.Select(x => x.CityName));
                filteredautocompletestrings.AddRange(filteredautocompleteobjects.Select(x => x.ZipCode));
                filteredautocompletestrings.RemoveAll(x => string.IsNullOrEmpty(x));
                filteredautocompletestrings.Sort();
            }
            else if (_session.Query<CBoardID>().Count(x => x.Name == account.BoardCode) > 0)
            {
                var filteredautocompleteobjects = _session.Query<CListing>().Select(x =>
                              new
                              {
                                  ZipCode = x.PostalCode,
                                  Address = x.Address + ", " + x.City,
                                  CityName = x.City,
                                  AreaName = x.AreaName
                              }
                      ).ToList();
                filteredautocompleteobjects.AddRange(_session.Query<Exclusive>().Where(x => x.AgentBoardCode == account.BoardCode && x.Account.Id == account.Id).Select(x =>
                    new
                    {
                        ZipCode = x.PostalCode,
                        Address = x.Address + ", " + x.City,
                        CityName = x.City,
                        AreaName = x.SubAreaName != null ? ((x.SubAreaName.Length > 3 && x.SubAreaName[2] == '-') ? x.SubAreaName.Remove(0, 3) : x.SubAreaName) : null
                    }
                    ).ToList());
                filteredautocompletestrings.AddRange(filteredautocompleteobjects.Select(x => x.Address));
                filteredautocompletestrings.AddRange(filteredautocompleteobjects.Select(x => x.AreaName));
                filteredautocompletestrings.AddRange(filteredautocompleteobjects.Select(x => x.CityName));
                filteredautocompletestrings.AddRange(filteredautocompleteobjects.Select(x => x.ZipCode));
                filteredautocompletestrings.RemoveAll(x => string.IsNullOrEmpty(x));
                filteredautocompletestrings.Sort();
            }
            else
            {

                var filteredautocompleteobjects = _session.Query<Residential>().Options(account, searchCriteria, PropertyTypeConstants.Residential).Where(x => x.AgentBoardCode == account.BoardCode).Select(x =>
                               new
                               {
                                   ZipCode = string.IsNullOrEmpty(x.PostalCode) ? "" : x.PostalCode.Trim().Replace(" ", ""),
                                   Address = x.Address + ", " + x.City,
                                   CityName = x.City,
                                   AreaName = string.IsNullOrEmpty(x.SubAreaName) ? "" : (x.SubAreaName.Length > 3 && x.SubAreaName[2] == '-') ? x.SubAreaName.Remove(0, 3) : x.SubAreaName
                               }
                       ).ToList();
                filteredautocompleteobjects.AddRange(_session.Query<Commercial>().Options(account, searchCriteria, PropertyTypeConstants.Commercial).Where(x => x.AgentBoardCode == account.BoardCode).Select(x =>
                               new
                               {
                                   ZipCode = string.IsNullOrEmpty(x.PostalCode) ? "" : x.PostalCode.Trim().Replace(" ", ""),
                                   Address = x.Address + ", " + x.City,
                                   CityName = x.City,
                                   AreaName = string.IsNullOrEmpty(x.SubAreaName) ? "" : (x.SubAreaName.Length > 3 && x.SubAreaName[2] == '-') ? x.SubAreaName.Remove(0, 3) : x.SubAreaName
                               }
                       ).ToList());
                filteredautocompleteobjects.AddRange(_session.Query<FarmsAndLand>().Options(account, searchCriteria, PropertyTypeConstants.FarmsAndLand).Where(x => x.AgentBoardCode == account.BoardCode).Select(x =>
                               new
                               {
                                   ZipCode = string.IsNullOrEmpty(x.PostalCode) ? "" : x.PostalCode.Trim().Replace(" ", ""),
                                   Address = x.Address + ", " + x.City,
                                   CityName = x.City,
                                   AreaName = string.IsNullOrEmpty(x.SubAreaName) ? "" : (x.SubAreaName.Length > 3 && x.SubAreaName[2] == '-') ? x.SubAreaName.Remove(0, 3) : x.SubAreaName
                               }
                       ).ToList());
                filteredautocompleteobjects.AddRange(_session.Query<ForLease>().Options(account, searchCriteria, PropertyTypeConstants.ForLease).Where(x => x.AgentBoardCode == account.BoardCode).Select(x =>
                               new
                               {
                                   ZipCode = string.IsNullOrEmpty(x.PostalCode) ? "" : x.PostalCode.Trim().Replace(" ", ""),
                                   Address = x.Address + ", " + x.City,
                                   CityName = x.City,
                                   AreaName = string.IsNullOrEmpty(x.SubAreaName) ? "" : (x.SubAreaName.Length > 3 && x.SubAreaName[2] == '-') ? x.SubAreaName.Remove(0, 3) : x.SubAreaName
                               }
                       ).ToList());
                filteredautocompleteobjects.AddRange(_session.Query<MultiFam>().Options(account, searchCriteria, PropertyTypeConstants.MultiFamily).Where(x => x.AgentBoardCode == account.BoardCode).Select(x =>
                               new
                               {
                                   ZipCode = string.IsNullOrEmpty(x.PostalCode) ? "" : x.PostalCode.Trim().Replace(" ", ""),
                                   Address = x.Address + ", " + x.City,
                                   CityName = x.City,
                                   AreaName = string.IsNullOrEmpty(x.SubAreaName) ? "" : (x.SubAreaName.Length > 3 && x.SubAreaName[2] == '-') ? x.SubAreaName.Remove(0, 3) : x.SubAreaName
                               }
                       ).ToList());
                filteredautocompleteobjects.AddRange(_session.Query<Exclusive>().Options(account, searchCriteria, ExclusivePropertyType.Exclusive).Where(x => x.AgentBoardCode == account.BoardCode && x.Account.Id == account.Id).Select(x =>
                    new
                    {
                        ZipCode = string.IsNullOrEmpty(x.PostalCode) ? "" : x.PostalCode.Trim().Replace(" ", ""),
                        Address = x.Address + ", " + x.City,
                        CityName = x.City,
                        AreaName = string.IsNullOrEmpty(x.SubAreaName) ? "" : (x.SubAreaName.Length > 3 && x.SubAreaName[2] == '-') ? x.SubAreaName.Remove(0, 3) : x.SubAreaName
                    }
                    ).ToList());
                filteredautocompletestrings.AddRange(filteredautocompleteobjects.Select(x => x.Address));
                filteredautocompletestrings.AddRange(filteredautocompleteobjects.Select(x => x.AreaName));
                filteredautocompletestrings.AddRange(filteredautocompleteobjects.Select(x => x.CityName));
                filteredautocompletestrings.AddRange(filteredautocompleteobjects.Select(x => x.ZipCode));

                filteredautocompletestrings.RemoveAll(x => string.IsNullOrEmpty(x));
                filteredautocompletestrings.Sort();

            }
            filteredautocompletestrings = filteredautocompletestrings.Distinct().ToList();
            return filteredautocompletestrings;
        }

        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        [AuthorizedWidget(WidgetType.SearchWidget)]
        public JsonResult CustomSearch(Guid accountkey, bool autoload = false, string redirecturl = "", string listings = WidgetOptions.AllListings, string cities = "", bool limitCities = false, string sort = "", string sessionid = "", bool showcityinfo = true, bool isswitch = false, string filter = "")
        {
            _session.Clear();
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;
            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
                //if (isswitch)
                //{
                //    foreach (var key in HttpContext.Cache)
                //    {
                //        var elemnet = (DictionaryEntry)key;
                //        if (elemnet.Key.ToString().StartsWith("SearchCriteria"))
                //        {
                //            sessionid = elemnet.Key.ToString().Split('-')[1];
                //        }
                //    }
                //    autoload = true;
                //}

                // setup the search criteria model used within the widget controller helper for filtering listings
                var account = Account.GetById(_session, accountkey);
                SearchCriteria searchCriteria = new SearchCriteria();
                //searchCriteria = SetupSearchCriteria(account, listings, !autoload, sessionid);
                if (isswitch == false || string.IsNullOrEmpty(sessionid))
                {
                    Task<SearchCriteria> searchtask = new Task<SearchCriteria>(() => this.SetupSearchCriteria(account, listings, !autoload, sessionid, filter));
                    searchtask.RunSynchronously();
                    searchCriteria = searchtask.Result;
                    searchCriteria.PageId = 1;
                }
                else
                {
                    var cacheKey = CreateCacheKey("SearchCriteria", sessionid);
                    var cachsearch = (SearchCriteria)HttpContext.Cache.Get(cacheKey);
                    if (cachsearch != null)
                    {
                        searchCriteria = cachsearch;
                        searchCriteria.Account = account;
                        HttpContext.Cache.Insert(cacheKey, searchCriteria, null, Cache.NoAbsoluteExpiration, TimeSpan.FromDays(1));
                    }

                }

                searchCriteria.RedirectUrl = redirecturl;

                // if search if for open house show date fields for open house from / to date
                if (listings == WidgetOptions.OpenHouseListings)
                {
                    searchCriteria.DateFrom = DateTime.Today;
                    searchCriteria.DateTo = DateTime.Today.AddDays(7);
                }

                // setup search price low / hi lists
                var prices = new[]
                                 {
                                     "25000", "50000", "75000", "100000", "125000",
                                     "150000", "175000", "200000", "225000", "250000",
                                     "275000", "300000", "325000", "350000", "375000",
                                     "400000", "425000", "450000", "475000", "500000", "550000", "600000",
                                     "650000", "700000", "750000", "800000", "850000",
                                     "900000", "950000", "1000000", "1500000", "2000000"
                                 };

                // setup sort options
                var sortOptions = new[] { "New Listings", "Address", "Lowest Price", "Highest Price" };
                if (!string.IsNullOrEmpty(sort) && string.IsNullOrEmpty(searchCriteria.Sort))
                {
                    searchCriteria.Sort = sort;
                }

                // parse the list of cities passed in as they should appear on the top of the city list
                var serialzer = new JavaScriptSerializer();
                var cityList = serialzer.Deserialize<IEnumerable<string>>(cities);
                List<SearchCity> savedcities = new List<SearchCity>();

                if (showcityinfo == true)
                {
                    savedcities = Get_Cities(account, listings, searchCriteria);

                    int allPosition;
                    if (cityList != null && cityList.Any())
                    {
                        if (string.IsNullOrEmpty(searchCriteria.City))
                        {
                            searchCriteria.City = cityList.ElementAt(0);
                        }
                        var cityCount = cityList.Count();
                        foreach (var city in cityList.Reverse())
                        {
                            if (city.Contains('/'))
                            {
                                string[] groupcities = city.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                                if (groupcities.Length > 0)
                                {
                                    int numberlisting = 0;
                                    for (int i = 0; i < groupcities.Length; i++)
                                    {
                                        var existcity = (from loc in savedcities where loc.CityName.ToLower() == groupcities[i].ToLower() select loc).FirstOrDefault();
                                        if (existcity != null)
                                        {
                                            numberlisting += existcity.Listings;
                                        }
                                    }
                                    savedcities.Insert(0, new SearchCity { CityName = city, Listings = numberlisting });
                                }
                            }
                            else
                            {
                                var existcity = (from loc in savedcities where loc.CityName.ToLower() == city.ToLower() select loc).FirstOrDefault();
                                savedcities.Remove(existcity);
                                if (existcity == null)
                                {
                                    existcity = new SearchCity { CityName = city, Listings = 0 };
                                }
                                savedcities.Insert(0, new SearchCity { CityName = city, Listings = existcity.Listings });
                            }
                        }

                        if (limitCities)
                        {
                            // remove all cities that are not explicitly set 
                            savedcities.RemoveRange(cityCount, savedcities.Count() - cityCount);
                        }
                        allPosition = cityCount;
                    }
                    else
                    {
                        allPosition = 0;
                    }

                    if (!limitCities)
                    {
                        // add an all cities option
                        if (!savedcities.Exists(x => x.CityName == "All"))
                        {
                            savedcities.Insert(allPosition, new SearchCity { CityName = "All", Listings = 0 });
                        }
                    }
                }
                object districtlist = null;
                object locationlist = null;
                List<string> locationliststring = null;
                // setup the model
                var model = new ListingSearchViewModel
                {
                    BoardCode = account.BoardCode,
                    City = null,
                    District = null,
                    Area = null,
                    PriceLow = prices.ToSelectList(x => x.ToString(CultureInfo.InvariantCulture),
                                                   x => x.ToString(CultureInfo.InvariantCulture), "0"),
                    PriceHigh = prices.ToSelectList(x => x.ToString(CultureInfo.InvariantCulture),
                                                    x => x.ToString(CultureInfo.InvariantCulture),
                                                    "unlimited"),
                    Bathrooms = Enumerable.Range(0, 6).ToSelectList(
                        x => x.ToString(CultureInfo.InvariantCulture),
                        x => x.ToString(CultureInfo.InvariantCulture) + "+", null),
                    Bedrooms = Enumerable.Range(0, 6).ToSelectList(
                        x => x.ToString(CultureInfo.InvariantCulture),
                        x => x.ToString(CultureInfo.InvariantCulture) + "+", null),
                    Sort = sortOptions.ToSelectList(x => x.ToString(CultureInfo.InvariantCulture),
                                             x => x.ToString(CultureInfo.InvariantCulture), ""),
                    AutoLoad = autoload,
                    MLSSearch = account.Features.MLSSearch,
                    SearchCriteria = searchCriteria,
                    ListingAccess = Mapper.Map<AccountListingAccess, ListingAccessViewModel>(
                            _session.Query<AccountListingAccess>()
                                .Cacheable()
                                .CacheRegion("LongTerm")
                                .SingleOrDefault(x => x.Account.Id == accountkey))
                };
                if (showcityinfo == true)
                {
                    model.City = savedcities.ToSelectList(value => value.CityName, text => text.CityName == "All" ? "All" : String.Format("{0} ({1})", text.CityName.Contains('/') ? text.CityName.Substring(0, text.CityName.IndexOf("/", 20) == -1 ? text.CityName.Length - 1 : text.CityName.IndexOf("/", 20)) : text.CityName, text.Listings.ToString()), savedcities.Count > 0 && cityList != null && cityList.Count() > 0 ?
                        String.Format("{0} ({1})", savedcities[0].CityName.Contains('/') ? savedcities[0].CityName.Substring(0, savedcities[0].CityName.IndexOf("/", 20) == -1 ? savedcities[0].CityName.Length - 1 : savedcities[0].CityName.IndexOf("/", 20)) : savedcities[0].CityName, savedcities[0].Listings.ToString()) : "city");
                    model.District = searchCriteria.City == null
                               ? GetDistricts(savedcities.ElementAt(0).CityName, account.BoardCode, account, listings, out districtlist)
                               : GetDistricts(searchCriteria.City, account.BoardCode, account, listings, out districtlist);
                    model.Area = searchCriteria.City == null
                               ? GetAreas(savedcities.ElementAt(0).CityName, null, account.BoardCode, account, listings, out locationlist, out locationliststring)
                               : GetAreas(searchCriteria.City, null, account.BoardCode, account, listings, out locationlist, out locationliststring);
                }
                ViewBag.loading = string.Format("{0}/Content/images/loadingdrop.gif", Properties.Settings.Default.DefaultHostName);

                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { html = this.RenderPartialViewToString("CustomSearch", model) }
                };
            }
            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        /// <summary>
        /// Set the search criteria to display listing results for, and return the listings that match
        /// If the map widget is displaying a search side bar, return an indicator to refresh the map
        /// </summary>
        /// <param name="searchCriteria">The listing search criteria that the user specified on the search form</param>
        /// <param name="accountkey">account key associated to the request</param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        public ActionResult SearchListings(SearchCriteria searchCriteria, Guid accountkey, bool isswitch = false, string widgettype = null)
        {
            _session.Clear();

            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;
            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
                var cacheKey = CreateCacheKey("SearchCriteria", searchCriteria.SessionID);

                var cachsearch = (SearchCriteria)HttpContext.Cache.Get(cacheKey);
                if (cachsearch != null)
                {
                    searchCriteria.WidgetOptions = cachsearch.WidgetOptions;
                    searchCriteria.Cottage = cachsearch.Cottage;
                    searchCriteria.PageId = cachsearch.PageId;
                    searchCriteria.FarmLandType = cachsearch.FarmLandType;
                }
                if (!string.IsNullOrEmpty(widgettype))
                {
                    searchCriteria.WidgetType = widgettype;
                }

                // save the search that was made

                var miniSearchCacheKey = CreateCacheKey("MiniSearch", searchCriteria.SessionID);
                HttpContext.Cache.Insert(cacheKey, searchCriteria, null, Cache.NoAbsoluteExpiration, TimeSpan.FromDays(1));

                // if the search was made for the map search widget return a json result signifying the map should refresh its data set
                if (searchCriteria.WidgetType == WidgetType.MapSearchWidget && string.IsNullOrEmpty(searchCriteria.RedirectUrl))
                {
                    return new JsonResult
                               {
                                   JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                                   Data = new { action = "RefreshMap" }
                               };
                }

                var account = Account.GetById(_session, accountkey);
                searchCriteria.Account = account;
                // if the redirect url of the search has not been set search for listings
                if (string.IsNullOrEmpty(searchCriteria.RedirectUrl))
                {
                    int pageid = 1;
                    // get account
                    if (isswitch)
                    {
                        if (searchCriteria.PageId > 0)
                        {
                            pageid = searchCriteria.PageId;
                        }
                    }

                    // get listing data based on the search
                    var features = account.Features;
                    var helper = new WidgetControllerHelper(_listingService, pageid, features.ListingWidgetPageSize, searchCriteria, false, features.NumberOfListingsToDisplay);
                    var model = helper.GetListingWidgetData(account.BoardCode, account.Features.NumberOfListingsToDisplay);
                    model.Account = Mapper.Map<Account, AccountViewModel>(account);
                    model.Disclaimer = Mapper.Map<Disclaimer, DisclaimerViewModel>(_session.Query<Disclaimer>().SingleOrDefault(x => x.BoardCode == account.BoardCode));
                    model.SessionID = searchCriteria.SessionID;

                    // ensure that if the widget is configured to display open house listings the "open house" feature is enabled
                    VerifyOpenHouseDisplay(searchCriteria.WidgetOptions, features, model);

                    // check if account can display open house information                 
                    model.DisplayOpenHouseInfo = features.OpenHouses;

                    // the mini search is used on the listings widget with the "search" option enabled
                    if (HttpContext.Cache[miniSearchCacheKey] == null)
                    {
                        model.MiniSearch = new MiniSearch
                                               {
                                                   ShowFilter = false
                                               };
                    }
                    else
                    {
                        model.MiniSearch = (MiniSearch)HttpContext.Cache[miniSearchCacheKey];
                        model.MiniSearch.Filter = "All Listings";
                        model.MiniSearch.Sort = searchCriteria.Sort;
                        model.MiniSearch.Status = searchCriteria.Status;
                        SetSearchFilter(searchCriteria, model);
                    }
                    if (account.Features.ShowSoldLeasedWatermark)
                    {
                        ViewBag.LeasedBanner = string.Format("{0}/Content/images/leased.png", Properties.Settings.Default.DefaultHostName);

                        ViewBag.SoldBanner = string.Format("{0}/Content/images/sold.png", Properties.Settings.Default.DefaultHostName);
                    }
                    // return listing data
                    return new JsonResult
                           {
                               JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                               Data = new { html = this.RenderPartialViewToString("DisplayListings", model) }
                           };
                }

                // redirect url provide, return redirect url to calling javascript widget to perfrom the redirect
                string sessionid;
                if (searchCriteria.RedirectUrl.Contains("?"))
                {
                    sessionid = "&vdcsessionid=" + searchCriteria.SessionID;
                }
                else
                {
                    sessionid = "?vdcsessionid=" + searchCriteria.SessionID;
                }

                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { action = "Redirect", url = searchCriteria.RedirectUrl + sessionid }
                };
            }
            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        /// <summary>
        /// Populate the locations drop down menu on the search form when user changes selected city
        /// </summary>
        /// <param name="city">The city to query sub areas for</param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        public JsonResult PopulateLocations(string city, string district, Guid accountkey, string listings = WidgetOptions.AllListings)
        {
            var account = Account.GetById(_session, accountkey);
            object districtlist = null;
            List<string> districtliststring = null;
            GetAreas(city, district, account.BoardCode, account, listings, out districtlist, out districtliststring).ToList();
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = new { filteredLocations = districtlist }
            };
        }

        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        public JsonResult PopulateDistricts(string city, Guid accountkey, string listings = WidgetOptions.AllListings)
        {
            var account = Account.GetById(_session, accountkey);
            object districtlist = null;
            GetDistricts(city, account.BoardCode, account, listings, out districtlist).ToList();
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = new { districtlist }
            };
        }

        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        public JsonResult Get_BoardCode(Guid accountkey)
        {
            var account = Account.GetById(_session, accountkey);
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = new { account.BoardCode }
            };
        }

        private List<SelectListItem> GetZipcodes(string zipcode, string boardcode, Account account, out object zipcodelist, out List<string> zipcodeliststring)
        {
            if (boardcode == "VIREB")
            {
                List<VResidential> vres = _session.Query<VResidential>().Where(x => (string.IsNullOrEmpty(zipcode) ? true : x.PostalCode == zipcode)).ToList();
                List<VCommercial> vcomm = _session.Query<VCommercial>().Where(x => (string.IsNullOrEmpty(zipcode) ? true : x.PostalCode == zipcode)).ToList();
                var filteredzipcodes = vres.Select(x =>
                               new
                               {
                                   ZipCode = x.PostalCode,
                                   Listings = vres.Count(y => y.PostalCode.Trim().Replace(" ", "") == x.PostalCode.Trim().Replace(" ", ""))
                               }
                       ).ToList().Distinct().ToList();
                filteredzipcodes.AddRange(vcomm.Select(x =>
                               new
                               {
                                   ZipCode = x.PostalCode,
                                   Listings = vcomm.Count(y => y.PostalCode.Trim().Replace(" ", "") == x.PostalCode.Trim().Replace(" ", ""))
                               }
                       ).ToList().Distinct().ToList());
                filteredzipcodes = filteredzipcodes.Select(x => new
                {
                    ZipCode = x.ZipCode.Trim(),
                    Listings = filteredzipcodes.Where(y => y.ZipCode.Trim() == x.ZipCode.Trim()).Sum(y => y.Listings)
                }).Distinct().ToList();

                filteredzipcodes = filteredzipcodes.OrderBy(arg => arg.ZipCode).ToList();
                filteredzipcodes.RemoveAll(x => x.ZipCode == null);

                zipcodelist = filteredzipcodes;
                zipcodeliststring = filteredzipcodes.Select(x => x.ZipCode).ToList();
                List<SelectListItem> dropdownlist = filteredzipcodes.ToSelectList(value => value.ZipCode, text => (text.ZipCode + " (" + text.Listings.ToString(CultureInfo.InvariantCulture) + ")"), "All");
                filteredzipcodes.Insert(0, new { ZipCode = "All", Listings = 0 });
                return dropdownlist;
            }
            else if (_session.Query<CBoardID>().Count(x => x.Name == account.BoardCode) > 0)
            {
                List<CListing> vres = _session.Query<CListing>().Where(x => (string.IsNullOrEmpty(zipcode) ? true : x.PostalCode == zipcode)).ToList();
                var filteredzipcodes = vres.Select(x =>
                               new
                               {
                                   ZipCode = x.PostalCode,
                                   Listings = vres.Count(y => y.PostalCode.Trim().Replace(" ", "") == x.PostalCode.Trim().Replace(" ", ""))
                               }
                       ).ToList().Distinct().ToList();
                filteredzipcodes = filteredzipcodes.Select(x => new
                {
                    ZipCode = x.ZipCode.Trim(),
                    Listings = filteredzipcodes.Where(y => y.ZipCode.Trim() == x.ZipCode.Trim()).Sum(y => y.Listings)
                }).Distinct().ToList();

                filteredzipcodes = filteredzipcodes.OrderBy(arg => arg.ZipCode).ToList();
                filteredzipcodes.RemoveAll(x => x.ZipCode == null);

                zipcodelist = filteredzipcodes;
                zipcodeliststring = filteredzipcodes.Select(x => x.ZipCode).ToList();
                List<SelectListItem> dropdownlist = filteredzipcodes.ToSelectList(value => value.ZipCode, text => (text.ZipCode + " (" + text.Listings.ToString(CultureInfo.InvariantCulture) + ")"), "All");
                filteredzipcodes.Insert(0, new { ZipCode = "All", Listings = 0 });
                return dropdownlist;
            }
            else
            {
                List<Residential> tempresidential = _session.Query<Residential>().Where(x => x.AgentBoardCode == boardcode && (string.IsNullOrEmpty(zipcode) ? true : x.PostalCode == zipcode)).ToList();
                var filteredzipcodes = tempresidential.Select(x =>
                               new
                               {
                                   ZipCode = x.PostalCode.Trim().Replace(" ", ""),
                                   Listings = tempresidential.Count(y => y.PostalCode.Trim().Replace(" ", "") == x.PostalCode.Trim().Replace(" ", ""))
                               }
                       ).Distinct().ToList();
                List<Commercial> tempcommercial = _session.Query<Commercial>().Where(x => x.AgentBoardCode == boardcode && (string.IsNullOrEmpty(zipcode) ? true : x.PostalCode == zipcode)).ToList();
                filteredzipcodes.AddRange(tempcommercial.Select(x =>
                               new
                               {
                                   ZipCode = x.PostalCode.Trim().Replace(" ", ""),
                                   Listings = tempresidential.Count(y => y.PostalCode.Trim().Replace(" ", "") == x.PostalCode.Trim().Replace(" ", ""))
                               }
                       ).Distinct().ToList());
                List<FarmsAndLand> tempfarmsland = _session.Query<FarmsAndLand>().Where(x => x.AgentBoardCode == boardcode && (string.IsNullOrEmpty(zipcode) ? true : x.PostalCode == zipcode)).ToList();
                filteredzipcodes.AddRange(tempfarmsland.Select(x =>
                               new
                               {
                                   ZipCode = x.PostalCode.Trim().Replace(" ", ""),
                                   Listings = tempresidential.Count(y => y.PostalCode.Trim().Replace(" ", "") == x.PostalCode.Trim().Replace(" ", ""))
                               }
                       ).Distinct().ToList());
                List<ForLease> tempforlease = _session.Query<ForLease>().Where(x => x.AgentBoardCode == boardcode && (string.IsNullOrEmpty(zipcode) ? true : x.PostalCode == zipcode)).ToList();
                filteredzipcodes.AddRange(tempforlease.Select(x =>
                               new
                               {
                                   ZipCode = x.PostalCode.Trim().Replace(" ", ""),
                                   Listings = tempresidential.Count(y => y.PostalCode.Trim().Replace(" ", "") == x.PostalCode.Trim().Replace(" ", ""))
                               }
                       ).Distinct().ToList());
                List<MultiFam> tempmultifam = _session.Query<MultiFam>().Where(x => x.AgentBoardCode == boardcode && (string.IsNullOrEmpty(zipcode) ? true : x.PostalCode == zipcode)).ToList();
                filteredzipcodes.AddRange(tempmultifam.Select(x =>
                               new
                               {
                                   ZipCode = x.PostalCode.Trim().Replace(" ", ""),
                                   Listings = tempresidential.Count(y => y.PostalCode.Trim().Replace(" ", "") == x.PostalCode.Trim().Replace(" ", ""))
                               }
                       ).Distinct().ToList());

                filteredzipcodes = filteredzipcodes.Select(x => new
                {
                    ZipCode = x.ZipCode,
                    Listings = filteredzipcodes.Where(y => y.ZipCode == x.ZipCode).Sum(y => y.Listings)
                }).Distinct().ToList();
                filteredzipcodes = filteredzipcodes.OrderBy(arg => arg.ZipCode).ToList();
                filteredzipcodes.RemoveAll(x => x.ZipCode == null);
                zipcodelist = filteredzipcodes;
                zipcodeliststring = filteredzipcodes.Select(x => x.ZipCode).ToList();
                List<SelectListItem> dropdownlist = filteredzipcodes.ToSelectList(value => value.ZipCode, text => (text.ZipCode + " (" + text.Listings.ToString(CultureInfo.InvariantCulture) + ")"), "All");
                filteredzipcodes.Insert(0, new { ZipCode = "All", Listings = 0 });
                return dropdownlist;
            }
        }

        private List<string> GetAddresses(string boardcode)
        {
            List<string> addresslist = new List<string>();
            if (boardcode == "VIREB")
            {
                addresslist.AddRange(_session.Query<VResidential>().Select(x => x.Address + ", " + x.City).ToList());
                addresslist.AddRange(_session.Query<VCommercial>().Select(x => x.Address + ", " + x.City).ToList());
                return addresslist;

            }
            else if (_session.Query<CBoardID>().Count(x => x.Name == boardcode) > 0)
            {
                addresslist.AddRange(_session.Query<CListing>().Select(x => x.Address + ", " + x.City).ToList());
                return addresslist;
            }
            else
            {
                addresslist.AddRange(_session.Query<Residential>().Where(x => x.AgentBoardCode == boardcode).Select(x => x.Address + ", " + x.City).ToList());
                addresslist.AddRange(_session.Query<Commercial>().Where(x => x.AgentBoardCode == boardcode).Select(x => x.Address + ", " + x.City).ToList());
                addresslist.AddRange(_session.Query<FarmsAndLand>().Where(x => x.AgentBoardCode == boardcode).Select(x => x.Address + ", " + x.City).ToList());
                addresslist.AddRange(_session.Query<ForLease>().Where(x => x.AgentBoardCode == boardcode).Select(x => x.Address + ", " + x.City).ToList());
                addresslist.AddRange(_session.Query<MultiFam>().Where(x => x.AgentBoardCode == boardcode).Select(x => x.Address + ", " + x.City).ToList());

                return addresslist;
            }
        }

        /// <summary>
        /// Populate the locations drop down on the initial load of the search form
        /// </summary>
        /// <param name="city">The city to query sub areas for</param>
        private List<SelectListItem> GetAreas(string city, string district, string boardcode, Account account, string listings, out object locationslist, out List<string> locationliststring)
        {
            string[] groupcities = string.IsNullOrEmpty(city) ? new string[0] : city.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            ListingAccessViewModel ListingAccess = Mapper.Map<AccountListingAccess, ListingAccessViewModel>(
                                        _session.Query<AccountListingAccess>()
                                            .Cacheable()
                                            .CacheRegion("LongTerm")
                                            .SingleOrDefault(x => x.Account.Id == account.Id));
            if (boardcode == "VIREB")
            {
                string districtshortcut = null;
                string islandshortcut = null;
                if (!(String.IsNullOrEmpty(district) || district == "All"))
                {
                    List<string> splitdistrict = district.Split('-').ToList();
                    if (splitdistrict.Count > 0)
                    {
                        splitdistrict = splitdistrict[0].Split(' ').ToList();
                        if (splitdistrict.Count == 2)
                        {
                            districtshortcut = splitdistrict[1].Length > 1 ? splitdistrict[1] : "Z" + splitdistrict[1];
                            islandshortcut = splitdistrict[1].Length > 1 ? splitdistrict[1] : "(Zone " + splitdistrict[1] + ")";
                        }
                    }
                }
                List<AreaListingCount> filteredLocations = new List<AreaListingCount>();
                List<VResidential> vres = new List<VResidential>();
                List<VCommercial> vcomm = new List<VCommercial>();
                if (ListingAccess.Residential != ListingDisplayAccess.None)
                {
                    vres = _session.Query<VResidential>().Where(x => (string.IsNullOrEmpty(city) ? true : groupcities.Contains(x.City)) && (x.Map_Area != null && (x.Map_Area.AreaName.StartsWith(districtshortcut) || x.Map_Area.AreaName.EndsWith(islandshortcut)))).ToList();
                }
                if (ListingAccess.Commercial != ListingDisplayAccess.None)
                {
                    vcomm = _session.Query<VCommercial>().Where(x => (string.IsNullOrEmpty(city) ? true : groupcities.Contains(x.City)) && (x.Map_Area != null && (x.Map_Area.AreaName.StartsWith(districtshortcut) || x.Map_Area.AreaName.EndsWith(islandshortcut)))).ToList();
                }
                filteredLocations.AddRange(vres.GroupBy(a => a.SubAreaName)
                    .Select(group => new AreaListingCount()
                    {
                        AreaName = group.Key,
                        Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                    }).ToList());
                filteredLocations.AddRange(vcomm.GroupBy(a => a.SubAreaName)
                    .Select(group => new AreaListingCount()
                    {
                        AreaName = group.Key,
                        Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                    }).ToList());
                filteredLocations.AddRange(_session.Query<Exclusive>().Where(x => x.AgentBoardCode == boardcode && groupcities.Contains(x.City) && x.Account.Id == account.Id && !(x.SubAreaName == null || x.SubAreaName.Trim() == ""))
                    .GroupBy(a => a.SubAreaName)
                    .Select(group => new AreaListingCount()
                    {
                        AreaName = (group.Key.Length > 3 && group.Key[2] == '-') ? group.Key.Remove(0, 3) : group.Key,
                        Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                    }).ToList());
                filteredLocations = filteredLocations.Select(x => new AreaListingCount()
                {
                    AreaName = string.IsNullOrEmpty(x.AreaName)? null : x.AreaName.Trim(),
                    Listings = string.IsNullOrEmpty(x.AreaName)? 0 : filteredLocations.Where(y => !string.IsNullOrEmpty(y.AreaName) && y.AreaName.Trim() == x.AreaName.Trim()).Sum(y => y.Listings)
                }).Distinct().ToList();
                filteredLocations.RemoveAll(x => x.AreaName == null);
                filteredLocations = filteredLocations.OrderBy(arg => arg.AreaName).ToList();

                locationslist = filteredLocations;
                locationliststring = filteredLocations.Select(x => x.AreaName).ToList();
                List<SelectListItem> dropdownlist = filteredLocations.ToSelectList(value => value.AreaName, text => (text.AreaName + " (" + text.Listings.ToString(CultureInfo.InvariantCulture) + ")"), "All");
                filteredLocations.Insert(0, new AreaListingCount() { AreaName = "All", Listings = 0 });
                return dropdownlist;
            }
            else if (_session.Query<CBoardID>().Count(x => x.Name == account.BoardCode) > 0)
            {
                List<AreaListingCount> filteredLocations = new List<AreaListingCount>();
                IQueryable<CListing> vres = _session.Query<CListing>().Where(x => (string.IsNullOrEmpty(city) ? true : groupcities.Contains(x.City)) && !(x.SubAreaName == null) && !(x.SubAreaName.Contains("ZZZ")));
                filteredLocations.AddRange(vres.GroupBy(a => a.SubAreaName)
                    .Select(group => new AreaListingCount()
                    {
                        AreaName = group.Key,
                        Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                    }).ToList());

                filteredLocations.AddRange(_session.Query<Exclusive>().Where(x => x.AgentBoardCode == boardcode && groupcities.Contains(x.City) && x.Account.Id == account.Id && !(x.SubAreaName == null || x.SubAreaName.Trim() == ""))
                    .GroupBy(a => a.SubAreaName)
                    .Select(group => new AreaListingCount()
                    {
                        AreaName = (group.Key.Length > 3 && group.Key[2] == '-') ? group.Key.Remove(0, 3) : group.Key,
                        Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                    }).ToList());

                filteredLocations = filteredLocations.Select(x => new AreaListingCount()
                {
                    AreaName = x.AreaName.Trim(),
                    Listings = filteredLocations.Where(y => y.AreaName.Trim() == x.AreaName.Trim()).Sum(y => y.Listings)
                }).Distinct().ToList();
                filteredLocations = filteredLocations.OrderBy(arg => arg.AreaName).ToList();
                filteredLocations.RemoveAll(x => x.AreaName == null);

                locationslist = filteredLocations;
                locationliststring = filteredLocations.Select(x => x.AreaName).ToList();
                List<SelectListItem> dropdownlist = filteredLocations.ToSelectList(value => value.AreaName, text => (text.AreaName + " (" + text.Listings.ToString(CultureInfo.InvariantCulture) + ")"), "All");
                filteredLocations.Insert(0, new AreaListingCount() { AreaName = "All", Listings = 0 });
                return dropdownlist;
            }
            else
            {
                List<AreaListingCount> filteredLocations = new List<AreaListingCount>();
                if (ListingAccess.Residential != ListingDisplayAccess.None)
                {
                    IQueryable<Residential> tempresidential = _session.Query<Residential>().Where(x => x.AgentBoardCode == boardcode && (string.IsNullOrEmpty(city) ? true : groupcities.Contains(x.City)) && !(x.SubAreaName == null || x.SubAreaName.Trim() == "") && (!string.IsNullOrEmpty(district) ? x.AreaName.Contains(district) : true));
                    filteredLocations.AddRange(tempresidential.GroupBy(a => a.SubAreaName)
                        .Select(group => new AreaListingCount()
                        {
                            AreaName = (group.Key.Length > 3 && group.Key[2] == '-') ? group.Key.Remove(0, 3) : group.Key,
                            Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                        }).ToList());
                }
                if (ListingAccess.Commercial != ListingDisplayAccess.None)
                {
                    IQueryable<Commercial> tempcommercial = _session.Query<Commercial>().Where(x => x.AgentBoardCode == boardcode && (string.IsNullOrEmpty(city) ? true : groupcities.Contains(x.City)) && !(x.SubAreaName == null || x.SubAreaName.Trim() == "") && (!string.IsNullOrEmpty(district) ? x.AreaName.Contains(district) : true));
                    filteredLocations.AddRange(tempcommercial.GroupBy(a => a.SubAreaName)
                        .Select(group => new AreaListingCount()
                        {
                            AreaName = (group.Key.Length > 3 && group.Key[2] == '-') ? group.Key.Remove(0, 3) : group.Key,
                            Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                        }).ToList());
                }
                if (ListingAccess.FarmsAndLand != ListingDisplayAccess.None)
                {
                    IQueryable<FarmsAndLand> tempfarmsland = _session.Query<FarmsAndLand>().Where(x => x.AgentBoardCode == boardcode && (string.IsNullOrEmpty(city) ? true : groupcities.Contains(x.City)) && !(x.SubAreaName == null || x.SubAreaName.Trim() == "") && (!string.IsNullOrEmpty(district) ? x.AreaName.Contains(district) : true));
                    filteredLocations.AddRange(tempfarmsland.GroupBy(a => a.SubAreaName)
                        .Select(group => new AreaListingCount()
                        {
                            AreaName = (group.Key.Length > 3 && group.Key[2] == '-') ? group.Key.Remove(0, 3) : group.Key,
                            Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                        }).ToList());
                }
                if (ListingAccess.ForLease != ListingDisplayAccess.None)
                {
                    IQueryable<ForLease> tempforlease = _session.Query<ForLease>().Where(x => x.AgentBoardCode == boardcode && (string.IsNullOrEmpty(city) ? true : groupcities.Contains(x.City)) && !(x.SubAreaName == null || x.SubAreaName.Trim() == "") && (!string.IsNullOrEmpty(district) ? x.AreaName.Contains(district) : true));
                    filteredLocations.AddRange(tempforlease.GroupBy(a => a.SubAreaName)
                        .Select(group => new AreaListingCount()
                        {
                            AreaName = (group.Key.Length > 3 && group.Key[2] == '-') ? group.Key.Remove(0, 3) : group.Key,
                            Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                        }).ToList());
                }
                if (ListingAccess.MultiFam != ListingDisplayAccess.None)
                {
                    IQueryable<MultiFam> tempmultifam = _session.Query<MultiFam>().Where(x => x.AgentBoardCode == boardcode && (string.IsNullOrEmpty(city) ? true : groupcities.Contains(x.City)) && !(x.SubAreaName == null || x.SubAreaName.Trim() == "") && (!string.IsNullOrEmpty(district) ? x.AreaName.Contains(district) : true));
                    filteredLocations.AddRange(tempmultifam.GroupBy(a => a.SubAreaName)
                        .Select(group => new AreaListingCount()
                        {
                            AreaName = (group.Key.Length > 3 && group.Key[2] == '-') ? group.Key.Remove(0, 3) : group.Key,
                            Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                        }).ToList());
                }
                IQueryable<Exclusive> tempexclusive = _session.Query<Exclusive>().Where(x => x.AgentBoardCode == boardcode && (string.IsNullOrEmpty(city) ? true : groupcities.Contains(x.City)) && x.Account.Id == account.Id && !(x.SubAreaName == null || x.SubAreaName.Trim() == "") && (!string.IsNullOrEmpty(district) ? x.AreaName.Contains(district) : true));
                filteredLocations.AddRange(tempexclusive.GroupBy(a => a.SubAreaName)
                    .Select(group => new AreaListingCount()
                    {
                        AreaName = (group.Key.Length > 3 && group.Key[2] == '-') ? group.Key.Remove(0, 3) : group.Key,
                        Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                    }).ToList());
                filteredLocations = filteredLocations.Select(x => new AreaListingCount()
                {
                    AreaName = x.AreaName,
                    Listings = filteredLocations.Where(y => y.AreaName == x.AreaName).Sum(y => y.Listings)
                }).Distinct().ToList();
                filteredLocations = filteredLocations.OrderBy(arg => arg.AreaName).ToList();
                filteredLocations.RemoveAll(x => x.AreaName == null);
                /*var locations = _session.Query<Location>()
                        .Cacheable()
                        .CacheRegion("LongTerm")
                        .Where(x => x.CityName == city && x.AreaName != city && x.BoardCode == boardcode).AsEnumerable();

                var filteredLocations = (
                                            from l in locations
                                            select new {l.AreaName, l.Listings}
                                        )
                    .Distinct()
                    .OrderBy(arg => arg.AreaName);*/
                locationslist = filteredLocations;
                locationliststring = filteredLocations.Select(x => x.AreaName).ToList();
                List<SelectListItem> dropdownlist = filteredLocations.ToSelectList(value => value.AreaName, text => (text.AreaName + " (" + text.Listings.ToString(CultureInfo.InvariantCulture) + ")"), "All");
                filteredLocations.Insert(0, new AreaListingCount() { AreaName = "All", Listings = 0 });
                return dropdownlist;
            }
        }

        private List<SelectListItem> GetDistricts(string city, string boardcode, Account account, string listings, out object districtlist)
        {
            districtlist = null;
            string[] groupcities = city.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            ListingAccessViewModel ListingAccess = Mapper.Map<AccountListingAccess, ListingAccessViewModel>(
                                        _session.Query<AccountListingAccess>()
                                            .Cacheable()
                                            .CacheRegion("LongTerm")
                                            .SingleOrDefault(x => x.Account.Id == account.Id));
            if (boardcode == "VIREB")
            {
                List<AreaListingCount> filteredLocations = new List<AreaListingCount>();
                List<VResidential> vres = new List<VResidential>();
                List<VCommercial> vcomm = new List<VCommercial>();
                if (ListingAccess.Residential != ListingDisplayAccess.None)
                {
                    vres = _session.Query<VResidential>().Where(x => groupcities.Contains(x.City) && !(x.Map_Area == null)).ToList();
                }
                if (ListingAccess.Commercial != ListingDisplayAccess.None)
                {
                    vcomm = _session.Query<VCommercial>().Where(x => groupcities.Contains(x.City) && !(x.Map_Area == null)).ToList();
                }
                List<VDistrict> vdistrict = _session.Query<VDistrict>().ToList();

                filteredLocations.AddRange(vres.Select(x =>
                               new AreaListingCount()
                               {
                                   AreaName = x.Map_Area != null ? vdistrict.Find(z => z.AreaDescription.StartsWith("Zone " + (x.Map_Area.AreaName.Substring(0, 2).Contains('Z') ? x.Map_Area.AreaName.Substring(1, 1) : x.Map_Area.AreaName.Substring(x.Map_Area.AreaName.Length-2, 1)))).AreaDescription : null,
                                   Listings = x.Map_Area != null ? listings == WidgetOptions.OpenHouseListings ? 0 : vres.Count(y => y.Map_Area != null && y.Map_Area.AreaName.StartsWith(x.Map_Area.AreaName.Substring(0, 2))) : 0
                               }
                       ).ToList().Distinct().ToList());
                filteredLocations.AddRange(vcomm.Select(x =>
                               new AreaListingCount()
                               {
                                   AreaName = x.Map_Area != null ? vdistrict.Find(z => z.AreaDescription.StartsWith("Zone " + (x.Map_Area.AreaName.Substring(0, 2).Contains('Z') ? x.Map_Area.AreaName.Substring(1, 1) : x.Map_Area.AreaName.Substring(x.Map_Area.AreaName.Length - 2, 1)))).AreaDescription : null,
                                   Listings = x.Map_Area != null ? listings == WidgetOptions.OpenHouseListings ? 0 : vres.Count(y => y.Map_Area != null && y.Map_Area.AreaName.StartsWith(x.Map_Area.AreaName.Substring(0, 2))) : 0
                               }
                       ).ToList().Distinct().ToList());
                filteredLocations.AddRange(_session.Query<Exclusive>().Where(x => x.AgentBoardCode == boardcode && groupcities.Contains(x.City) && x.Account.Id == account.Id && !(x.AreaName == null || x.AreaName.Trim() == "")).GroupBy(a => a.AreaName)
                       .Select(group => new AreaListingCount()
                       {
                           AreaName = group.Key,
                           Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                       }).ToList());
                filteredLocations.RemoveAll(x => x.AreaName == null);
                filteredLocations = filteredLocations.Select(x => new AreaListingCount()
                {
                    AreaName = x.AreaName.Trim(),
                    Listings = filteredLocations.Where(y => y.AreaName.Trim() == x.AreaName.Trim()).Sum(y => y.Listings)
                }).Distinct().ToList();
                filteredLocations = filteredLocations.OrderBy(arg => arg.AreaName).ToList();
                districtlist = filteredLocations;
                List<SelectListItem> dropdownlist = filteredLocations.ToSelectList(value => value.AreaName, text => (text.AreaName + " (" + text.Listings.ToString(CultureInfo.InvariantCulture) + ")"), "All");
                filteredLocations.Insert(0, new AreaListingCount() { AreaName = "All", Listings = 0 });
                return dropdownlist;
            }
            else if (_session.Query<CBoardID>().Count(x => x.Name == account.BoardCode) > 0)
            {
                List<AreaListingCount> filteredLocations = new List<AreaListingCount>();
                IQueryable<CListing> vres = _session.Query<CListing>().Where(x => groupcities.Contains(x.City) && !(x.AreaName == null) && !(x.AreaName.Contains("ZZZ")));
                filteredLocations.AddRange(vres.GroupBy(a => a.AreaName)
                    .Select(group => new AreaListingCount()
                    {
                        AreaName = group.Key,
                        Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                    }).ToList());
                filteredLocations.AddRange(_session.Query<Exclusive>().Where(x => x.AgentBoardCode == boardcode && groupcities.Contains(x.City) && x.Account.Id == account.Id && !(x.AreaName == null || x.AreaName.Trim() == "")).GroupBy(a => a.AreaName)
                    .Select(group => new AreaListingCount()
                    {
                        AreaName = group.Key,
                        Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                    }).ToList());
                filteredLocations = filteredLocations.Select(x => new AreaListingCount()
                {
                    AreaName = x.AreaName.Trim(),
                    Listings = filteredLocations.Where(y => y.AreaName.Trim() == x.AreaName.Trim()).Sum(y => y.Listings)
                }).Distinct().ToList();
                filteredLocations = filteredLocations.OrderBy(arg => arg.AreaName).ToList();
                filteredLocations.RemoveAll(x => x.AreaName == null);
                districtlist = filteredLocations;
                List<SelectListItem> dropdownlist = filteredLocations.ToSelectList(value => value.AreaName, text => (text.AreaName + " (" + text.Listings.ToString(CultureInfo.InvariantCulture) + ")"), "All");
                filteredLocations.Insert(0, new AreaListingCount() { AreaName = "All", Listings = 0 });
                return dropdownlist;
            }
            else
            {
                List<AreaListingCount> filteredLocations = new List<AreaListingCount>();
                if (ListingAccess.Residential != ListingDisplayAccess.None)
                {
                    IQueryable<Residential> tempresidential = _session.Query<Residential>().Where(x => x.AgentBoardCode == boardcode && groupcities.Contains(x.City) && !(x.AreaName == null || x.AreaName.Trim() == ""));
                    filteredLocations.AddRange(tempresidential.GroupBy(a => a.AreaName)
                        .Select(group => new AreaListingCount()
                        {
                            AreaName = group.Key,
                            Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                        }).ToList());
                }
                if (ListingAccess.Commercial != ListingDisplayAccess.None)
                {
                    IQueryable<Commercial> tempcommercial = _session.Query<Commercial>().Where(x => x.AgentBoardCode == boardcode && groupcities.Contains(x.City) && !(x.AreaName == null || x.AreaName.Trim() == ""));
                    filteredLocations.AddRange(tempcommercial.GroupBy(a => a.AreaName)
                        .Select(group => new AreaListingCount()
                        {
                            AreaName = group.Key,
                            Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                        }).ToList());
                }
                if (ListingAccess.FarmsAndLand != ListingDisplayAccess.None)
                {
                    IQueryable<FarmsAndLand> tempfarmsland = _session.Query<FarmsAndLand>().Where(x => x.AgentBoardCode == boardcode && groupcities.Contains(x.City) && !(x.AreaName == null || x.AreaName.Trim() == ""));
                    filteredLocations.AddRange(tempfarmsland.GroupBy(a => a.AreaName)
                        .Select(group => new AreaListingCount()
                        {
                            AreaName = group.Key,
                            Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                        }).ToList());
                }
                if (ListingAccess.ForLease != ListingDisplayAccess.None)
                {
                    IQueryable<ForLease> tempforlease = _session.Query<ForLease>().Where(x => x.AgentBoardCode == boardcode && groupcities.Contains(x.City) && !(x.AreaName == null || x.AreaName.Trim() == ""));
                    filteredLocations.AddRange(tempforlease.GroupBy(a => a.AreaName)
                        .Select(group => new AreaListingCount()
                        {
                            AreaName = group.Key,
                            Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                        }).ToList());
                }
                if (ListingAccess.MultiFam != ListingDisplayAccess.None)
                {
                    IQueryable<MultiFam> tempmultifam = _session.Query<MultiFam>().Where(x => x.AgentBoardCode == boardcode && groupcities.Contains(x.City) && !(x.AreaName == null || x.AreaName.Trim() == ""));
                    filteredLocations.AddRange(tempmultifam.GroupBy(a => a.AreaName)
                        .Select(group => new AreaListingCount()
                        {
                            AreaName = group.Key,
                            Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                        }).ToList());
                }
                IQueryable<Exclusive> tempexclusive = _session.Query<Exclusive>().Where(x => x.AgentBoardCode == boardcode && groupcities.Contains(x.City) && x.Account.Id == account.Id && !(x.AreaName == null || x.AreaName.Trim() == ""));

                filteredLocations.AddRange(tempexclusive.GroupBy(a => a.AreaName)
                    .Select(group => new AreaListingCount()
                    {
                        AreaName = group.Key,
                        Listings = listings == WidgetOptions.OpenHouseListings ? 0 : group.Count()
                    }).ToList());

                filteredLocations = filteredLocations.Select(x => new AreaListingCount()
                {
                    AreaName = x.AreaName,
                    Listings = filteredLocations.Where(y => y.AreaName == x.AreaName).Sum(y => y.Listings)
                }).Distinct().ToList();
                filteredLocations = filteredLocations.OrderBy(arg => arg.AreaName).ToList();
                filteredLocations.RemoveAll(x => x.AreaName == null);
                districtlist = filteredLocations;
                List<SelectListItem> dropdownlist = filteredLocations.ToSelectList(value => value.AreaName, text => ((text.AreaName.Length > 3 && text.AreaName[2] == ' ' ? text.AreaName.Substring(2) : text.AreaName) + " (" + text.Listings.ToString(CultureInfo.InvariantCulture) + ")"), "All");
                filteredLocations.Insert(0, new AreaListingCount() { AreaName = "All", Listings = 0 });
                return dropdownlist;
            }
        }

        /// <summary>
        /// Renders the a detailed view for the specific listing
        /// </summary>
        /// <param name="accountkey">The account key associated to the request</param>
        /// <param name="property">The property type of the listing</param>
        /// <param name="mlsnumber">The associated MLS number of the listing</param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        public JsonResult DetailedListing(Guid accountkey, string property, int mlsnumber, string vdcsessionid)
        {
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;
            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
                if (!string.IsNullOrEmpty(vdcsessionid))
                {
                    var cacheKey = CreateCacheKey("SearchCriteria", vdcsessionid);
                    SearchCriteria searchCriteria = new SearchCriteria();
                    searchCriteria = (SearchCriteria)HttpContext.Cache[cacheKey];

                    HttpContext.Cache.Insert(cacheKey, searchCriteria, null, Cache.NoAbsoluteExpiration, TimeSpan.FromDays(1));
                }

                var helper = new WidgetControllerHelper(_listingService);
                var account = Account.GetById(_session, accountkey);
                //var account = _session.Get<Account>(new Guid(HttpContext.Request.QueryString["accountkey"]));
                var listing = helper.GetListingByPropertyTypeAndMlsNumber(property, mlsnumber, account.BoardCode);
                if (listing != null)
                {
                    var agent = GetAgentInfo(account, listing.AgentIndCode);
                    var coAgent = listing.CoAgentIndCode != null ? GetAgentInfo(account, listing.CoAgentIndCode) : null;
                    var coAgent2 = listing.CoAgentIndCode2 != null ? GetAgentInfo(account, listing.CoAgentIndCode2) : null;
                    AccountOfficeInfo selectedoffice = null;
                    if (agent == null && coAgent == null)
                    {
                        List<AgentInfo> agents = ControllerHelpers.GetAgents(account, _session, 1);
                        if (agents != null && agents.Count > 0)
                        {
                            agent = agents[0];
                        }
                        else
                        {
                            List<AccountOfficeInfo> offices = ControllerHelpers.GetAgentOffice(account, _session, 1);
                            if (offices != null && offices.Count() > 0)
                            {
                                selectedoffice = offices[0];
                            }
                        }
                    }

                    List<ListingFiles> listingFiles = _session.Query<ListingFiles>().Where(x => x.Account == account && x.Mlsnumber == listing.MlsNumber).ToList();

                    var model = new DetailedListingViewModel
                                    {
                                        Listing = listing,
                                        Agent = agent,
                                        CoAgent = coAgent,
                                        CoAgent2 = coAgent2,
                                        Account = Mapper.Map<Account, AccountViewModel>(account),
                                        ApiKey = Settings.Default.GoogleMapsApiKey,
                                        Disclaimer = Mapper.Map<Disclaimer, DisclaimerViewModel>(_session.Query<Disclaimer>().SingleOrDefault(x => x.BoardCode == account.BoardCode)),
                                        Office = selectedoffice,
                                        ListingFiles = listingFiles != null && listingFiles.Count > 0 ? listingFiles : null,
                                        SessionID = vdcsessionid
                                    };

                    // check if account can display open house information 
                    var features = account.Features;
                    model.DisplayOpenHouseInfo = features.OpenHouses;
                    model.ShareThisUrl = ShareThis.MakeUrl(listing, Request);

                    // check and see if account is setup to use the simple detailed listings page
                    var detailedListingViewName = features.UseSimpleDetailedListing ? "SimpleDetailedListing" : "DetailedListing";

                    // check and see if account has mortgage calculator enabled
                    model.ShowMortgageCalculator = features.MortgageCalculator;

                    return new JsonResult
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { html = this.RenderPartialViewToString(detailedListingViewName, model) }
                    };
                }

                return new JsonResult
                           {
                               JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                               Data = new { html = @"<a class=""close"" href=""#"">close</a> Listing Not Found" }
                           };
            }
            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        [AuthorizedWidget(WidgetType.InPageListingDetails)]
        public JsonResult InPageListingDetails(Guid accountkey, string property, int mlsnumber, string vdcsessionid)
        {
            return DetailedListing(accountkey, property, mlsnumber, vdcsessionid);
        }

        /// <summary>
        /// Renders the google map listing search widget
        /// </summary>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        [AuthorizedWidget(WidgetType.MapSearchWidget)]
        public JsonResult MapSearch(string latitude, string longitude)
        {
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;

            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
                double mapSearchLatitude;
                double mapSearchLongitude;
                if (!string.IsNullOrEmpty(latitude) && !string.IsNullOrEmpty(longitude))
                {
                    mapSearchLatitude = Double.Parse(latitude);
                    mapSearchLongitude = Double.Parse(longitude);
                }
                else
                {
                    mapSearchLatitude = Settings.Default.Latitude;
                    mapSearchLongitude = Settings.Default.Longitude;
                }

                var model = new MapSearchViewModel
                                {
                                    ApiKey = Settings.Default.GoogleMapsApiKey,
                                    MapSearchLatitude = mapSearchLatitude,
                                    MapSearchLongitude = mapSearchLongitude
                                };

                return new JsonResult
                           {
                               JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                               Data = new { html = this.RenderPartialViewToString("MapSearch", model) }
                           };
            }

            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        [AuthorizedWidget(WidgetType.NearMeWidget)]
        public JsonResult NearMeListings(string latitude, string longitude, bool islocated)
        {
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;

            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
                double mapSearchLatitude;
                double mapSearchLongitude;
                if (!string.IsNullOrEmpty(latitude) && !string.IsNullOrEmpty(longitude) && longitude != "null" && latitude != "null")
                {
                    mapSearchLatitude = Double.Parse(latitude);
                    mapSearchLongitude = Double.Parse(longitude);
                }
                else
                {
                    mapSearchLatitude = Settings.Default.Latitude;
                    mapSearchLongitude = Settings.Default.Longitude;
                }

                var model = new MapSearchViewModel
                {
                    ApiKey = Settings.Default.GoogleMapsApiKey,
                    MapSearchLatitude = mapSearchLatitude,
                    MapSearchLongitude = mapSearchLongitude,
                    IsLocated = islocated
                };

                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { html = this.RenderPartialViewToString("MapSearch", model) }
                };
            }

            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        /// <summary>
        /// The Google map widgets performs a get json request to this method when it loads
        /// </summary>
        /// <param name="accountkey">account key associated to the request</param>
        /// <param name="listings">allows the listings to be filtered by valid options (all, brokerage, agent, openhouse)</param>
        /// <param name="sessionid">sessionid associated to request</param>
        /// <returns>json object of filtered listings</returns>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        public JsonResult GetMapListings(Guid accountkey, string listings = WidgetOptions.AllListings, string sessionid = "", string cities = "", bool islocated = false, double latitude = 0, double longitude = 0, bool isswitch = false)
        {
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;
            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
                var account = Account.GetById(_session, accountkey);
                var serialzer = new JavaScriptSerializer();
                var cityList = (List<string>)serialzer.Deserialize<IEnumerable<string>>(cities);
                //if (isswitch)
                //{
                //    foreach (var key in HttpContext.Cache)
                //    {
                //        var elemnet = (DictionaryEntry)key;
                //        if (elemnet.Key.ToString().StartsWith("SearchCriteria"))
                //        {
                //            sessionid = elemnet.Key.ToString().Split('-')[1];
                //        }
                //    }
                //}

                string city = null;
                var cacheKey = CreateCacheKey("SearchCriteria", sessionid);
                SearchCriteria searchCriteria = new SearchCriteria();
                searchCriteria = (SearchCriteria)HttpContext.Cache[cacheKey];

                if (cityList != null && cityList.Count() > 0 && (searchCriteria == null || string.IsNullOrEmpty(searchCriteria.City)))
                {
                    city = cityList[0];
                }
                //searchCriteria = SetupSearchCriteria(account, listings, false, sessionid, city: city);
                if (isswitch == false)
                {
                    Task<SearchCriteria> searchtask = new Task<SearchCriteria>(() => this.SetupSearchCriteria(account, listings, false, sessionid, city: city));
                    searchtask.Start();
                    searchCriteria = searchtask.Result;
                }
                searchCriteria.Account = account;

                // setup the search criteria model used within the widget controller helper for filtering listings



                // get listings
                var helper = new WidgetControllerHelper(_listingService, searchCriteria);

                var allListings = helper.GetAllListings(account.BoardCode, account.Features.NumberOfListingsToDisplay, true);

                if (islocated)
                {
                    allListings = allListings.Where(x => x.Latitude != null && x.Longitude != null ? Math.Acos(Math.Sin(latitude) * Math.Sin(x.Latitude.Value) + Math.Cos(latitude) * Math.Cos(x.Latitude.Value) * Math.Cos(x.Longitude.Value - (longitude))) * 6371 <= 20 : false);
                }

                // if display openhouse data ensure that site has access to the openhouse feature
                var features = account.Features;
                if (listings == WidgetOptions.OpenHouseListings && !features.OpenHouses)
                {
                    allListings = null;
                }

                DateTime start = DateTime.Now;
                allListings = allListings.ToList();
                List<MapSearchListing> mapListings = new List<MapSearchListing>();
                Parallel.ForEach(allListings, i => mapListings.Add(i.Get_MapObject()));
                var model = new MapSearchListingsViewModel
                                {
                                    Listings = mapListings,
                                    Disclaimer = Mapper.Map<Disclaimer, DisclaimerViewModel>(_session.Query<Disclaimer>().SingleOrDefault(x => x.BoardCode == account.BoardCode)),
                                    ShowListingDetailsInPage = account.Features.ShowListingDetailsInPage
                                };

                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = model
                };
            }
            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        /// <summary>
        /// Return the map icons for the map search
        /// </summary>
        /// <param name="accountkey">account key associated to the request</param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        public JsonResult GetMapIcons(Guid accountkey)
        {
            var account = Account.GetById(_session, accountkey);

            return new JsonResult
                       {
                           JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                           Data = new { customMarker = account.MapMarker != null && account.MapMarker.Length > 0, customCluster = account.MapMarkerMore != null && account.MapMarkerMore.Length > 0 }
                       };

        }

        /// <summary>
        /// Renders the featured listings widget that can be embedded within a clients site
        /// </summary>
        /// <param name="accountkey">account key associated to the request</param>
        /// <param name="display">the number of listings to display</param>
        /// <param name="listings">the listings to select from if auto select is enabled</param>
        /// <param name="city">an optional city to filter listings for</param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        [AuthorizedWidget(WidgetType.FeaturedListingWidget)]
        public JsonResult FeaturedListings(Guid accountkey, int display = 1, string listings = WidgetOptions.AllListings, string city = "", string sort = "", bool isjson = false)
        {
            _session.Clear();
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;
            try
            {
                if (string.IsNullOrEmpty(authorizedErrorMessage))
                {
                    var account = Account.GetById(_session, accountkey);
                    ViewBag.Account = account;
                    IEnumerable<Listing> featuredListings;
                    if (account.Features.AutoSelectFeaturedListings)
                    {
                        // setup the search criteria model used within the widget controller helper for filtering listings
                        SearchCriteria searchCriteria = new SearchCriteria();
                        searchCriteria = SetupSearchCriteria(account, listings, false, "", city: city);
                        var helper = new WidgetControllerHelper(_listingService, searchCriteria);
                        featuredListings = helper.GetAllListings(account.BoardCode, account.Features.NumberOfListingsToDisplay, false).Where(x => x.PhotoCount > 1).GetRandomElements(display).ToList();
                        featuredListings = WidgetControllerHelper.ApplyOrderBy(featuredListings, sort).ToList();
                    }
                    else
                    {
                        var featuredlistquery = ControllerHelpers.GetFeaturedListings(account, _session, _listingService).GetRandomElements(display).ToList();
                        featuredListings = WidgetControllerHelper.ApplyOrderBy(featuredlistquery, sort).ToList();
                        if (sort == "Agent")
                        {
                            featuredListings = OrderListingByAccount(featuredListings, account);
                        }
                    }

                    // multiple results
                    if (featuredListings.Count() > 0)
                    {
                        if (!isjson)
                        {
                            return new JsonResult
                                {
                                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                                    Data = new
                                        {
                                            html = this.RenderPartialViewToString("FeaturedListings",
                                                                                  new FeaturedListingViewModel
                                                                                      {
                                                                                          Listings = featuredListings
                                                                                      })
                                        }
                                };
                        }
                        else
                        {
                            return new JsonResult
                            {
                                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                                Data = new
                                {
                                    html = JsonConvert.SerializeObject(featuredListings.Select(x =>
                                    {
                                        if (x != null)
                                        {
                                            var agent = GetAgentInfo(account, x.AgentIndCode);
                                            var coAgent = x.CoAgentIndCode != null ? GetAgentInfo(account, x.CoAgentIndCode) : null;
                                            var coAgent2 = x.CoAgentIndCode2 != null ? GetAgentInfo(account, x.CoAgentIndCode2) : null;
                                            AccountOfficeInfo selectedoffice = null;
                                            if (agent == null && coAgent == null)
                                            {
                                                List<AgentInfo> agents = ControllerHelpers.GetAgents(account, _session, 1);
                                                if (agents != null && agents.Count > 0)
                                                {
                                                    agent = agents[0];
                                                }
                                                else
                                                {
                                                    List<AccountOfficeInfo> offices = ControllerHelpers.GetAgentOffice(account, _session, 1);
                                                    if (offices != null && offices.Count() > 0)
                                                    {
                                                        selectedoffice = offices[0];
                                                    }
                                                }
                                            }
                                            if (agent != null)
                                            {
                                                agent.Account = null;
                                                if(agent.Referral!= null)
                                                {
                                                    agent.Referral.Account = null;
                                                    agent.Referral.Agent = null;
                                                }
                                                if (agent.Office != null)
                                                {
                                                    agent.Office.Account = null;
                                                }
                                                agent.Leads = null;
                                            }
                                            if (coAgent != null)
                                            {
                                                coAgent.Account = null;
                                                if (coAgent.Referral != null)
                                                {
                                                    coAgent.Referral.Account = null;
                                                    coAgent.Referral.Agent = null;
                                                }
                                                if (coAgent.Office != null)
                                                {
                                                    coAgent.Office.Account = null;
                                                }
                                                coAgent.Leads = null;
                                            }
                                            if (coAgent2 != null)
                                            {
                                                coAgent2.Account = null;
                                                if (coAgent2.Referral != null)
                                                {
                                                    coAgent2.Referral.Account = null;
                                                    coAgent2.Referral.Agent = null;
                                                }
                                                if (coAgent2.Office != null)
                                                {
                                                    coAgent2.Office.Account = null;
                                                }
                                                coAgent2.Leads = null;
                                            }
                                            if (selectedoffice != null)
                                            {
                                                selectedoffice.Account = null;
                                            }
                                            return new DetailedListingViewModel
                                            {
                                                Listing = x,
                                                Agent = agent,
                                                CoAgent = coAgent,
                                                CoAgent2 = coAgent2,
                                                Office = selectedoffice,
                                            };
                                        };
                                        return null;
                                    }))
                                }
                            };
                        }
                    }

                    // single result
                    /*if (featuredListings.Count() == 1)
                    {
                        return new JsonResult
                            {
                                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                                Data = new
                                    {
                                        html = this.RenderPartialViewToString("FeaturedListing",
                                                                              new FeaturedListingViewModel
                                                                                  {
                                                                                      Listing = featuredListings.ElementAt(0)
                                                                                  })
                                    }
                            };
                    }*/

                    // empty result
                    return new JsonResult
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new
                                {
                                    html = ""
                                }
                        };
                }
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new
                    {
                        html = ex.Message
                    }
                };
            }

            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        /// <summary>
        /// Renders the latest listings widget that can be embedded within a clients site
        /// </summary>
        /// <param name="accountkey">account key associated to the request</param>
        /// <param name="display">the number of listings to display</param>
        /// <param name="listings">the types of listings to display</param>
        /// <param name="city">the city to restrict listings too</param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        [AuthorizedWidget(WidgetType.LatestListingWidget)]
        public JsonResult LatestListing(Guid accountkey, int display = 1, string listings = WidgetOptions.AllListings, string city = "")
        {
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;

            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
                var account = Account.GetById(_session, accountkey);
                ViewBag.Account = account;
                // setup the search criteria model used within the widget controller helper for filtering listings
                SearchCriteria searchCriteria = new SearchCriteria();
                searchCriteria = SetupSearchCriteria(account, listings, false, "", city: city);
                var helper = new WidgetControllerHelper(_listingService, searchCriteria);

                //photocount always 0 for exclusive 
                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new
                    {
                        html = this.RenderPartialViewToString("LatestListing", new LatestListingViewModel
                                                                                    {
                                                                                        Listings = helper.GetAllListings(account.BoardCode, account.Features.NumberOfListingsToDisplay, false).Where(x => x.PhotoCount > 1).Take(display)
                                                                                    })
                    }
                };
            }

            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        /// <summary>
        /// Renders the featured agents widget that can be embedded within a clients site
        /// </summary>
        /// <param name="accountkey">account key associated to the request</param>
        /// <param name="display">the number of featured agents to display on the page</param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        [AuthorizedWidget(WidgetType.FeaturedAgentWidget)]
        public JsonResult
            FeaturedAgents(Guid accountkey, int display = 1, string profilePageUrl = "")
        {
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;

            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
                var account = Account.GetById(_session, accountkey);
                var agents = ControllerHelpers.GetFeaturedAgents(account, _session, display);


                // multiple results
                if (agents.Count() > 1)
                {
                    return new JsonResult
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new
                                {
                                    html = this.RenderPartialViewToString("FeaturedAgents", new FeaturedAgentViewModel
                                        {
                                            Agents = agents,
                                            ProfilePageUrl = profilePageUrl
                                        })
                                }
                        };
                }

                // single result
                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new
                    {
                        html = this.RenderPartialViewToString("FeaturedAgent", new FeaturedAgentViewModel
                        {
                            Agent = agents.ElementAt(0),
                            ProfilePageUrl = profilePageUrl
                        })
                    }
                };

            }

            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        /// <summary>
        /// Renders the members widget that can be embedded within a clients site
        /// </summary>
        /// <param name="accountkey">account key associated to the request</param>
        /// <param name="office">the office id to filter agents on</param>
        /// <param name="sort">first name / last name / asc / desc </param>
        /// <param name="search">optional search text</param>
        /// <param name="profilePageUrl">base url for the agent profile page</param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        [AuthorizedWidget(WidgetType.MembersWidget)]
        public JsonResult DisplayAgents(Guid accountkey, string office = "", string sort = MembersOptions.AscLastName, string search = "", string profilePageUrl = "", string Specialty = null)
        {
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;
            search = search != null ? search.Trim() : search;
            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
                var account = Account.GetById(_session, accountkey);

                // filter by office if set
                IEnumerable<Agent> agents;
                if (!string.IsNullOrEmpty(office))
                {
                    var officeId = int.Parse(office);
                    agents = _session.QueryOver<Agent>()
                                     .Fetch(l => l.Leads).Eager
                                     .TransformUsing(Transformers.DistinctRootEntity)
                                     .Where(x => x.Account == account && x.RealtorPage == true && x.Office.Id == officeId)
                                     .List();

                }
                else
                {
                    agents = _session.QueryOver<Agent>()
                                     .Fetch(l => l.Leads).Eager
                                     .TransformUsing(Transformers.DistinctRootEntity)
                                     .Where(x => x.Account == account && x.RealtorPage == true).List();
                }

                if(Specialty != null)
                {
                    agents = agents.Where(x => x.Specialty == Specialty).ToList();
                }

                // filter the list by the agent name
                if (!string.IsNullOrEmpty(search))
                {
                    agents = agents.Where(x => x.FullName.ToLower().Contains(search.ToLower()));
                }

                // apply sorting
                string sortBy = "";
                switch (sort)
                {
                    case MembersOptions.AscFirstName:
                        agents = agents.OrderBy(x => x.FirstName);
                        sortBy = MembersOptions.AscFirstName;
                        break;
                    case MembersOptions.DescFirstName:
                        agents = agents.OrderByDescending(x => x.FirstName);
                        sortBy = MembersOptions.DescFirstName;
                        break;
                    case MembersOptions.AscLastName:
                        agents = agents.OrderBy(x => x.LastName);
                        sortBy = MembersOptions.AscLastName;
                        break;
                    case MembersOptions.DescLastName:
                        agents = agents.OrderByDescending(x => x.LastName);
                        sortBy = MembersOptions.DescLastName;
                        break;
                    case MembersOptions.Default:
                        var rand = new Random();
                        agents = agents.OrderBy(x => rand.Next());
                        sortBy = MembersOptions.Default;
                        break;
                }

                var offices = AccountOffice.GetByAccount(_session, account);
                var model = new MembersWidget
                {
                    ProfilePageUrl = profilePageUrl,
                    Agents = Mapper.Map<IEnumerable<Agent>, IEnumerable<AgentInfo>>(agents),
                    SortyBy = sortBy,
                    Search = search,
                    Offices = offices.ToSelectList(x => x.Id.ToString(CultureInfo.InvariantCulture), x => x.Name, "All Offices"),
                    Sort = new List<SelectListItem>
                    {
                        new SelectListItem
                            {
                                Text = "Sort",
                                Value = MembersOptions.Default,
                                Selected = (sortBy == MembersOptions.Default)
                            },  
                        new SelectListItem
                            {
                                Text = "First Name ↑",
                                Value = MembersOptions.AscFirstName,
                                Selected = (sortBy == MembersOptions.AscFirstName)
                            },                        
                        new SelectListItem
                        {
                            Text = "First Name ↓",
                            Value = MembersOptions.DescFirstName,
                            Selected = (sortBy == MembersOptions.DescFirstName)
                        },                        
                        new SelectListItem
                        {
                            Text = "Last Name ↑",
                            Value = MembersOptions.AscLastName,
                            Selected = (sortBy == MembersOptions.AscLastName)
                        },                        
                        new SelectListItem
                        {
                            Text = "Last Name ↓",
                            Value = MembersOptions.DescLastName,
                            Selected = (sortBy == MembersOptions.DescLastName)
                        },
                    }
                };

                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { html = this.RenderPartialViewToString("DisplayAgents", model) }
                };
            }

            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        /// <summary>
        /// Renders the contacts widget that can be embedded within a clients site
        /// </summary>
        /// <param name="accountkey">account key associated to the request</param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        [AuthorizedWidget(WidgetType.ContactsWidget)]
        public JsonResult DisplayContacts(Guid accountkey)
        {
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;

            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
                var account = Account.GetById(_session, accountkey);
                var contacts = _session.QueryOver<Contact>()
                                       .Where(x => x.Account == account)
                                       .OrderBy(x => x.PositionNumber).Desc
                                       .List();

                var model = new ContactsWidget
                {
                    Contacts = Mapper.Map<IEnumerable<Contact>, IEnumerable<ContactsViewModel>>(contacts)
                };

                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { html = this.RenderPartialViewToString("DisplayContacts", model) }
                };
            }

            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        /// <summary>
        /// Renders the news widget that can be embedded within a clients site
        /// </summary>
        /// <param name="accountkey">account key associated to the request</param>
        /// <param name="news">the type of news widget to render</param>
        /// <param name="pagesize">the number of news items to display</param>
        /// <param name="length">the length of the comment to display</param>
        /// <param name="archiveyear">the year of news messages to display</param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        [AuthorizedWidget(WidgetType.NewsWidget)]
        public JsonResult DisplayNews(Guid accountkey, string news = NewsOptions.Headline, int pagesize = 10, int length = 250, int? archiveyear = null)
        {
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;

            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
                var account = Account.GetById(_session, accountkey);

                // get news items
                var newsitems = Mapper.Map<IEnumerable<BulletinBoard>, IEnumerable<NewsItemViewModel>>(
                    _session.Query<BulletinBoard>()
                        .Cacheable()
                        .CacheRegion("LongTerm")
                        .Where(x => x.Account == account && x.IsNews))
                        .Where(x => x.StartDate <= DateTime.Today)
                        .OrderByDescending(x => x.Id)
                        .ThenByDescending(x => x.StartDate);

                var model = new NewsWidgetViewModel
                                {
                                    News = newsitems,
                                    Length = length,
                                    Options = news,
                                };

                // if the widget is in "arhive" model display a drop down for year selection and filter items by the current year
                if (news == NewsOptions.Archive)
                {
                    model.Archives = newsitems.Select(x => x.StartDate.Year)
                        .Distinct()
                        .ToSelectList(value => value.ToString(CultureInfo.InvariantCulture), text => text.ToString(CultureInfo.InvariantCulture), "");

                    var year = archiveyear ?? Convert.ToInt32(model.Archives.ElementAt(0).Value);
                    model.Archive = year;
                    model.News = model.News.Where(x => x.StartDate.Year == year);
                }

                // restrict the number of news items by the page size
                model.News = model.News.Take(pagesize);

                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { html = this.RenderPartialViewToString("DisplayNews", model) }
                };
            }

            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        /// <summary>
        /// Renders the mortgage calculator widget that can be embedded into clients site
        /// </summary>
        /// <param name="accountkey">account key associated to the request</param>
        /// <param name="principal">optional principal of property</param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        [AuthorizedWidget(WidgetType.MortgageCalculatorWidget)]
        public JsonResult MortgageCalculator(Guid accountkey, double principal = 0)
        {
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;

            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
                var calculatorDefaults = _session.Query<CalculatorDefaults>()
                    .Cacheable()
                    .CacheRegion("LongTerm")
                    .FirstOrDefault();

                if (calculatorDefaults != null)
                {
                    var model = new MortgageCalculatorViewModel
                                    {
                                        Amortization = calculatorDefaults.Amortization,
                                        Term = calculatorDefaults.Term,
                                        InterestRate = calculatorDefaults.InterestRate,
                                        Principal = principal <= 1 ? calculatorDefaults.Principal : principal,
                                        DownPayment = calculatorDefaults.DownPayment
                                    };

                    return new JsonResult
                               {
                                   JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                                   Data =
                                       new { html = this.RenderPartialViewToString("MortgageCalculator", model) }
                               };
                }
            }

            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        /// <summary>
        /// Renders the agent profile widget that can be embedded within a clients site
        /// </summary>
        /// <param name="accountkey">account key associated to the request</param>
        /// <param name="agentId">the agent id to render </param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        [AuthorizedWidget(WidgetType.AgentProfileWidget)]
        public JsonResult AgentProfile(Guid accountkey, int agentId)
        {
            var authorizedErrorMessage = HttpContext.Items["errormessage"] as string;

            if (string.IsNullOrEmpty(authorizedErrorMessage))
            {
                var agent = Mapper.Map<Agent, AgentInfo>(Agent.GetByID(_session, agentId));
                var account = Account.GetById(_session, accountkey);
                if (agent != null && agent.Account.Id == account.Id)
                {
                    var model = new AgentProfileViewModel
                    {
                        Agent = agent
                    };

                    return new JsonResult
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data =
                            new { html = this.RenderPartialViewToString("AgentProfile", model) }
                    };
                }

            }

            return ControllerHelpers.RenderErrorMessage(authorizedErrorMessage);
        }

        /// <summary>
        /// Is called from the mortgage calculator widget
        /// Calculates canadian mortgage information 
        /// </summary>
        /// <param name="accountkey">Account key associated to request</param>
        /// <param name="mortgage">Entered mortgage information</param>
        [HttpGet]
        [JsonpFilter]
        [AuthorizedUrl]
        public JsonResult Calculate(Guid accountkey, MortgageCalculatorViewModel mortgage)
        {
            if (ModelState.IsValid)
            {
                mortgage.DownPaymentRequired = mortgage.Principal * (mortgage.DownPayment / 100);
                mortgage.MortgagePrincipal = mortgage.Principal - mortgage.DownPaymentRequired;

                /*
                canadian monthly Pmt = http://www.themortgagestoreonline.com/canadiancommercialmortgagepaymentformula.html 
                */

                var step1 = mortgage.MortgagePrincipal;
                var step2 = mortgage.Amortization;
                var step3 = mortgage.InterestRate;
                var step4 = step3 / 100;
                var step5 = step2 * 12;
                var step6 = step4 / 2;
                var step7 = step6 + 1;
                double step8 = Math.Pow(step7, 0.166666666);
                var step9 = step8 - 1;
                var step10 = step9 + 1;
                double step11 = Math.Pow(step10, step5);
                var step12 = step1 * step9 * step11;
                var step13 = step11 - 1;
                var step14 = step12 / step13;

                mortgage.MonthlyPayment = Math.Round(step14, 2);

                var monthlyInterest = Math.Pow(1 + mortgage.InterestRate / 100 / 2, (double)1 / 6);
                var v = 1 / monthlyInterest;

                var term = (mortgage.MortgagePrincipal - (v * mortgage.MonthlyPayment * (1 - Math.Pow(v, 12 * mortgage.Term))) / (1 - v)) / Math.Pow(v, 12 * mortgage.Term);
                mortgage.OwingAtTerm = Math.Round(term, 2);
                mortgage.ShowResults = true;
            }

            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = new { html = this.RenderPartialViewToString("MortgageCalculator", mortgage) }
            };

        }

        /// <summary>
        /// The search criteria object is used primary for filtering listing data and is stored in a session cookie
        /// </summary>
        /// <param name="account">The account associated to the request</param>
        /// <param name="widgetOptions">The javascript widget configuration</param>
        /// <param name="resetSearch">If true don't use the save search</param>
        /// <param name="sessionid">sessionid associated to request</param>
        /// <param name="filter">The default property type filter to apply</param>
        /// <param name="city">An optional city to restrict results too</param>
        private SearchCriteria SetupSearchCriteria(Account account, string widgetOptions, bool resetSearch, string sessionid, string filter = "", string city = "")
        {
            // create cache key
            if (string.IsNullOrEmpty(sessionid))
            {
                sessionid = Session.SessionID;
            }
            var cacheKey = CreateCacheKey("SearchCriteria", sessionid);
            bool newsearch = false;
            // get existing search and reset if required

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = (SearchCriteria)HttpContext.Cache[cacheKey];
            if (searchCriteria == null || resetSearch)
            {
                searchCriteria = new SearchCriteria();
                newsearch = true;
            }
            searchCriteria.Account = account;
            searchCriteria.WidgetOptions = widgetOptions;
            searchCriteria.SessionID = sessionid;

            if (!string.IsNullOrEmpty(city))
            {
                searchCriteria.City = city;
            }

            Parse_Multiple_Listing_Types(widgetOptions, searchCriteria);

            if (!string.IsNullOrEmpty(filter))
            {
                switch (filter)
                {
                    case TypeDwelling.Acreage:
                        searchCriteria.Acreage = true;
                        break;

                    case TypeDwelling.Condo:
                        searchCriteria.Condominium = true;
                        break;

                    case TypeDwelling.VacantLot:
                        searchCriteria.Vacant = true;
                        break;

                    case TypeDwelling.CottageRecreation:
                        searchCriteria.Cottage = true;
                        break;

                    case CommercialPropertyType.Retail:
                        searchCriteria.Retail = true;
                        break;

                    case CommercialPropertyType.Land:
                        searchCriteria.Land = true;
                        break;

                    case CommercialPropertyType.Office:
                        searchCriteria.Office = true;
                        break;

                    case CommercialPropertyType.Industrial:
                        searchCriteria.Industrial = true;
                        break;
                    case FarmsAndLandPropertyType.Beef:
                        searchCriteria.FarmLandType = FarmsAndLandPropertyType.Beef;
                        break;
                    case FarmsAndLandPropertyType.Cashcrop:
                        searchCriteria.FarmLandType = FarmsAndLandPropertyType.Cashcrop;
                        break;
                    case FarmsAndLandPropertyType.Dary:
                        searchCriteria.FarmLandType = FarmsAndLandPropertyType.Dary;
                        break;
                    case FarmsAndLandPropertyType.Game:
                        searchCriteria.FarmLandType = FarmsAndLandPropertyType.Game;
                        break;
                    case FarmsAndLandPropertyType.Grain:
                        searchCriteria.FarmLandType = FarmsAndLandPropertyType.Grain;
                        break;
                    case FarmsAndLandPropertyType.Hay:
                        searchCriteria.FarmLandType = FarmsAndLandPropertyType.Hay;
                        break;
                    case FarmsAndLandPropertyType.Hobby:
                        searchCriteria.FarmLandType = FarmsAndLandPropertyType.Hobby;
                        break;
                    case FarmsAndLandPropertyType.Mixed:
                        searchCriteria.FarmLandType = FarmsAndLandPropertyType.Mixed;
                        break;
                    case FarmsAndLandPropertyType.Other:
                        searchCriteria.FarmLandType = FarmsAndLandPropertyType.Other;
                        break;
                }
            }

            // save to cache
            if (newsearch)
            {
                //foreach (var key in HttpContext.Cache)
                //{
                //    var elemnet = (DictionaryEntry)key;
                //    if (elemnet.Key.ToString().StartsWith("SearchCriteria"))
                //    {
                //        HttpContext.Cache.Remove(elemnet.Key.ToString());
                //    }
                //}
                HttpContext.Cache.Insert(cacheKey, searchCriteria, null, Cache.NoAbsoluteExpiration, TimeSpan.FromDays(1));

            }
            return searchCriteria;
        }

        /// <summary>
        /// The mini search object is used on the listings widget where the "filter" parameter is set to true
        /// </summary>
        /// <param name="showfilter">If the filter is enabled configure the mini search options</param>
        /// <param name="model">The listing widget view model</param>
        /// <param name="sessionid">The current session id</param>
        /// <param name="filter">The default filter</param>
        private void SetupMiniSearch(bool showfilter, ListingWidgetViewModel model, string sessionid, string filter = "")
        {
            if (!showfilter) return;

            var cacheKey = CreateCacheKey("MiniSearch", sessionid);
            if (HttpContext.Cache[cacheKey] == null)
            {
                var minisearch = new MiniSearch
                                     {
                                         ShowFilter = true,
                                         Filter = filter
                                     };

                model.MiniSearch = minisearch;
                HttpContext.Cache.Insert(cacheKey, minisearch, null, Cache.NoAbsoluteExpiration, TimeSpan.FromDays(1));
            }
            else
            {
                model.MiniSearch = (MiniSearch)HttpContext.Cache[cacheKey];
            }
        }

        /// <summary>
        /// Set the mini search filter default to the type of search being performed
        /// </summary>
        /// <param name="searchCriteria">The active search</param>
        /// <param name="model">The active listing widget model from the current search</param>
        private static void SetSearchFilter(SearchCriteria searchCriteria, ListingWidgetViewModel model)
        {
            if (searchCriteria.Acreage) model.MiniSearch.Filter = TypeDwelling.Acreage;
            if (searchCriteria.Condominium) model.MiniSearch.Filter = TypeDwelling.Condo;
            if (searchCriteria.Vacant) model.MiniSearch.Filter = TypeDwelling.VacantLot;
            if (searchCriteria.Cottage) model.MiniSearch.Filter = TypeDwelling.CottageRecreation;
            if (searchCriteria.Residential) model.MiniSearch.Filter = PropertyTypeConstants.Residential;
            if (searchCriteria.Commercial) model.MiniSearch.Filter = PropertyTypeConstants.Commercial;
            if (searchCriteria.ForLease) model.MiniSearch.Filter = PropertyTypeConstants.ForLease;
            if (searchCriteria.FarmsAndLand) model.MiniSearch.Filter = PropertyTypeConstants.FarmsAndLand;
            if (searchCriteria.MultiFam) model.MiniSearch.Filter = PropertyTypeConstants.MultiFamily;
            if (searchCriteria.Retail) model.MiniSearch.Filter = CommercialPropertyType.Retail;
            if (searchCriteria.Land) model.MiniSearch.Filter = CommercialPropertyType.Land;
            if (searchCriteria.Office) model.MiniSearch.Filter = CommercialPropertyType.Office;
            if (searchCriteria.Industrial) model.MiniSearch.Filter = CommercialPropertyType.Industrial;
        }

        /// <summary>
        /// create a cache key to use for the httpcontext cache
        /// </summary>
        /// <param name="name">Name of the key</param>
        /// <param name="sessionid">Current session</param>
        private string CreateCacheKey(string name, string sessionid)
        {
            if (string.IsNullOrEmpty(sessionid))
            {
                sessionid = Session.SessionID;
            }

            var cacheKey = string.Format("{0}-{1}", name, sessionid);
            return cacheKey;
        }

        /// <summary>
        ///  ensure that if the widget is configured to display open house listings the "open house" feature is enabled
        /// </summary>
        private void VerifyOpenHouseDisplay(string options, AccountFeatures features, ListingWidgetViewModel model)
        {
            if (options == WidgetOptions.OpenHouseListings && !features.OpenHouses)
            {
                model.Listings = null;
            }
        }

        /// <summary>
        /// Get the agent info for the specified account / code
        /// </summary>
        private AgentInfo GetAgentInfo(Account account, string industryCode)
        {
            //if (account.BoardCode != "VIREB")
            //{
            return Mapper.Map<Agent, AgentInfo>(_session.Query<Agent>()
                                                        .FirstOrDefault(x => x.AgentIndCode == industryCode && x.Account == account));
            //}
            /*else
            {
                var agent = Mapper.Map<VAgent, AgentInfo>(_session.Query<VAgent>()
                                                            .FirstOrDefault(x => x.AgentID == Convert.ToInt32(industryCode)));
                if(agent != null)
                {
                    agent.Account =  Mapper.Map<Account, AccountViewModel>(account);
                    agent.Office = Mapper.Map<VOffice, AccountOfficeInfo>((_session.Query<VAgent>()
                                                            .FirstOrDefault(x => x.AgentID == Convert.ToInt32(industryCode))).AgentOffice);
                }
                return agent;
            }*/
        }

        private List<SearchCity> Get_Cities(Account account, string listings, SearchCriteria searchCriteria)
        {
            ListingAccessViewModel ListingAccess = Mapper.Map<AccountListingAccess, ListingAccessViewModel>(
                                       _session.Query<AccountListingAccess>()
                                           .Cacheable()
                                           .CacheRegion("LongTerm")
                                           .SingleOrDefault(x => x.Account.Id == account.Id));
            List<SearchCity> savedcities = new List<SearchCity>();
            if (account.BoardCode == "VIREB")
            {
                if (ListingAccess.Residential != ListingDisplayAccess.None)
                {
                    IQueryable<VResidential> queryVResidential = _session.Query<VResidential>();
                    if (listings == WidgetOptions.OpenHouseListings)
                    {
                        if (searchCriteria.DateFrom != null && searchCriteria.DateTo != null && searchCriteria.DateFrom.Value.Year != 1 && searchCriteria.DateTo.Value.Year != 1)
                        {
                            queryVResidential = queryVResidential.Options(account, searchCriteria, PropertyTypeConstants.Residential);
                        }
                    }

                    savedcities.AddRange(queryVResidential.GroupBy(a => a.City)
                        .Select(group => new SearchCity
                        {
                            CityName = group.Key,
                            Listings = group.Count()
                        }).ToList().OrderBy(arg => arg.CityName).ToList());
                }
                if (ListingAccess.Commercial != ListingDisplayAccess.None)
                {
                    IQueryable<VCommercial> queryVCommercial = _session.Query<VCommercial>();
                    if (listings == WidgetOptions.OpenHouseListings)
                    {
                        if (searchCriteria.DateFrom != null && searchCriteria.DateTo != null && searchCriteria.DateFrom.Value.Year != 1 && searchCriteria.DateTo.Value.Year != 1)
                        {
                            queryVCommercial = queryVCommercial.Options(account, searchCriteria, PropertyTypeConstants.Commercial);
                        }
                    }
                    savedcities.AddRange(queryVCommercial.GroupBy(a => a.City)
                        .Select(group => new SearchCity
                        {
                            CityName = group.Key,
                            Listings = group.Count()
                        }).ToList().OrderBy(arg => arg.CityName).ToList());
                }
            }
            else if (_session.Query<CBoardID>().Count(x => x.Name == account.BoardCode) > 0)
            {
                IQueryable<CListing> queryVResidential = _session.Query<CListing>().Where(x => !(x.City.Contains("ZZZ")));
                if (listings == WidgetOptions.OpenHouseListings)
                {
                    if (searchCriteria.DateFrom != null && searchCriteria.DateTo != null && searchCriteria.DateFrom.Value.Year != 1 && searchCriteria.DateTo.Value.Year != 1)
                    {
                        queryVResidential = queryVResidential.Options(account, searchCriteria, PropertyTypeConstants.Residential);
                    }
                }
                savedcities.AddRange(queryVResidential.GroupBy(a => a.City)
                    .Select(group => new SearchCity
                    {
                        CityName = group.Key,
                        Listings = group.Count()
                    }).ToList().OrderBy(arg => arg.CityName).ToList());
            }
            else
            {
                if (ListingAccess.Residential != ListingDisplayAccess.None)
                {
                    IQueryable<Residential> queryResidential = _session.Query<Residential>().Where(x => x.AgentBoardCode == account.BoardCode);
                    if (listings == WidgetOptions.OpenHouseListings)
                    {
                        if (searchCriteria.DateFrom != null && searchCriteria.DateTo != null && searchCriteria.DateFrom.Value.Year != 1 && searchCriteria.DateTo.Value.Year != 1)
                        {
                            queryResidential = queryResidential.Options(account, searchCriteria, PropertyTypeConstants.Residential);
                        }
                    }
                    savedcities.AddRange(queryResidential.GroupBy(a => a.City)
                        .Select(group => new SearchCity
                        {
                            CityName = group.Key,
                            Listings = group.Count()
                        }).ToList().OrderBy(arg => arg.CityName).ToList());
                }
                if (ListingAccess.Commercial != ListingDisplayAccess.None)
                {
                    IQueryable<Commercial> queryCommercial = _session.Query<Commercial>().Where(x => x.AgentBoardCode == account.BoardCode);
                    if (listings == WidgetOptions.OpenHouseListings)
                    {
                        if (searchCriteria.DateFrom != null && searchCriteria.DateTo != null && searchCriteria.DateFrom.Value.Year != 1 && searchCriteria.DateTo.Value.Year != 1)
                        {
                            queryCommercial = queryCommercial.Options(account, searchCriteria, PropertyTypeConstants.Commercial);
                        }
                    }
                    savedcities.AddRange(queryCommercial.GroupBy(a => a.City)
                        .Select(group => new SearchCity
                        {
                            CityName = group.Key,
                            Listings = group.Count()
                        }).ToList().OrderBy(arg => arg.CityName).ToList());
                }
                if (ListingAccess.FarmsAndLand != ListingDisplayAccess.None)
                {
                    IQueryable<FarmsAndLand> queryFarmsAndLand = _session.Query<FarmsAndLand>().Where(x => x.AgentBoardCode == account.BoardCode);
                    if (listings == WidgetOptions.OpenHouseListings)
                    {
                        if (searchCriteria.DateFrom != null && searchCriteria.DateTo != null && searchCriteria.DateFrom.Value.Year != 1 && searchCriteria.DateTo.Value.Year != 1)
                        {
                            queryFarmsAndLand = queryFarmsAndLand.Options(account, searchCriteria, PropertyTypeConstants.FarmsAndLand);
                        }
                    }
                    savedcities.AddRange(queryFarmsAndLand.GroupBy(a => a.City)
                        .Select(group => new SearchCity
                        {
                            CityName = group.Key,
                            Listings = group.Count()
                        }).ToList().OrderBy(arg => arg.CityName).ToList());
                }
                //
                if (ListingAccess.ForLease != ListingDisplayAccess.None)
                {
                    IQueryable<ForLease> queryForLease = _session.Query<ForLease>().Where(x => x.AgentBoardCode == account.BoardCode);
                    if (listings == WidgetOptions.OpenHouseListings)
                    {
                        if (searchCriteria.DateFrom != null && searchCriteria.DateTo != null && searchCriteria.DateFrom.Value.Year != 1 && searchCriteria.DateTo.Value.Year != 1)
                        {
                            queryForLease = queryForLease.Options(account, searchCriteria, PropertyTypeConstants.ForLease);
                        }
                    }
                    savedcities.AddRange(queryForLease.GroupBy(a => a.City)
                        .Select(group => new SearchCity
                        {
                            CityName = group.Key,
                            Listings = group.Count()
                        }).ToList().OrderBy(arg => arg.CityName).ToList());
                }
                //
                if (ListingAccess.MultiFam != ListingDisplayAccess.None)
                {
                    IQueryable<MultiFam> queryMultiFam = _session.Query<MultiFam>().Where(x => x.AgentBoardCode == account.BoardCode);
                    if (listings == WidgetOptions.OpenHouseListings)
                    {
                        if (searchCriteria.DateFrom != null && searchCriteria.DateTo != null && searchCriteria.DateFrom.Value.Year != 1 && searchCriteria.DateTo.Value.Year != 1)
                        {
                            queryMultiFam = queryMultiFam.Options(account, searchCriteria, PropertyTypeConstants.MultiFamily);
                        }
                    }
                    savedcities.AddRange(queryMultiFam.GroupBy(a => a.City)
                        .Select(group => new SearchCity
                        {
                            CityName = group.Key,
                            Listings = group.Count()
                        }).ToList().OrderBy(arg => arg.CityName).ToList());
                }
            }
            IQueryable<Exclusive> queryExclusive = _session.Query<Exclusive>().Where(x => x.AgentBoardCode == account.BoardCode && x.Account.Id == account.Id);
            if (listings == WidgetOptions.OpenHouseListings)
            {
                if (searchCriteria.DateFrom != null && searchCriteria.DateTo != null && searchCriteria.DateFrom.Value.Year != 1 && searchCriteria.DateTo.Value.Year != 1)
                {
                    queryExclusive = queryExclusive.Options(account, searchCriteria, ExclusivePropertyType.Exclusive);
                }
            }
            savedcities.AddRange(queryExclusive.GroupBy(a => a.City)
                    .Select(group => new SearchCity
                    {
                        CityName = group.Key,
                        Listings = group.Count()
                    }).ToList().OrderBy(arg => arg.CityName).ToList());

            savedcities = savedcities.Select(x => new SearchCity
            {
                CityName = Fix_City(x.CityName),
                Listings = savedcities.Where(y => Fix_City(y.CityName) == Fix_City(x.CityName)).Sum(y => y.Listings)
            }).Distinct().ToList();
            savedcities = savedcities.OrderBy(arg => arg.CityName).ToList();
            savedcities.RemoveAll(x => x.CityName == null);

            var count = savedcities.Select(x => new { count = savedcities.Sum(y => y.Listings) });

            return savedcities;
        }

        private IEnumerable<Listing> OrderListingByAccount(IEnumerable<Listing> alllisting, Account account)
        {
            List<Listing> agentlisting = new List<Listing>();
            switch (account.AccountType)
            {
                // only display open houses for the accounts agent
                case AccountType.Agent:
                    agentlisting = alllisting.Where(x => x.AgentIndCode == account.AgentCode).ToList();
                    break;

                // only display open houses for the accounts brokerage
                case AccountType.Broker:
                    var brokercodes = account.BrokerCode.Split('|');
                    agentlisting = alllisting.Where(x => brokercodes.Contains(x.AgentBrokerCode)).ToList();
                    break;

                // only display open houses for the accounts agents
                case AccountType.Team:
                    var agentcodes = account.AgentCode.Split('|');
                    agentlisting = alllisting.Where(x => agentcodes.Contains(x.AgentIndCode)).ToList();
                    break;
            }
            alllisting = alllisting.Except(agentlisting);
            alllisting = agentlisting.Concat(alllisting);

            return alllisting;
        }

        private string Fix_City(string cityname)
        {
            if (cityname != null)
            {
                string[] citysplit = cityname.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                string city = "";
                for (int i = 0; i < citysplit.Length; i++)
                {
                    city += char.ToUpper(citysplit[i][0]) + citysplit[i].Substring(1).ToLower() + " ";
                }
                city = city.Trim();
                return city;
            }
            return cityname;
        }

        //Vancouver Widgets

    }
}
