﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Extensions;

namespace Widgets.Controllers
{
    [Authorize(Roles = "Super")]
    public class DisclaimersController : BaseController
    {
        private readonly ISession _session;

        public DisclaimersController(IAuthProvider auth, ISession session) : base(auth, session)
        {
            _session = session;
        }

        /// <summary>
        /// Display list of board disclaimers
        /// </summary>
        [HttpGet]
        public ViewResult ManageDisclaimers()
        {
            var model = new ManageDisclaimersViewModel
                            {
                                Disclaimers = Mapper.Map<IEnumerable<Disclaimer>, IEnumerable<DisclaimerViewModel>>(
                                        _session.Query<Disclaimer>())
                            };
            return View("ManageDisclaimers", model);
        }

        /// <summary>
        /// Render the create new board disclaimer view
        /// </summary>
        [HttpGet]
        public ViewResult Create()
        {
            var model = new DisclaimerViewModel();
            IEnumerable<Board> boards = _session.Query<Board>();
            ViewBag.BoardCode = boards.ToSelectList(x => x.BoardCode, x => x.BoardCode, "Select Board");
            return View("Create", model);
        }

        /// <summary>
        /// Create new disclaimer entry
        /// </summary>
        /// <param name="disclaimer"></param>
        [HttpPost]
        public ActionResult Create(DisclaimerViewModel disclaimer)
        {
            if(ModelState.IsValid)
            {
                var model = Mapper.Map<DisclaimerViewModel, Disclaimer>(disclaimer);
                _session.Save(model);
                _session.Flush();

                return RedirectToAction("ManageDisclaimers");
            }
            IEnumerable<Board> boards = _session.Query<Board>();
            ViewBag.BoardCode = boards.ToSelectList(x => x.BoardCode, x => x.BoardCode, disclaimer.BoardCode);
            return View("Create", disclaimer);
        }

        /// <summary>
        /// Render edit disclaimer view
        /// </summary>
        /// <param name="id">ID, of the disclaimer to edit</param>
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var disclaimer = _session.Get<Disclaimer>(id);
            if(disclaimer != null)
            {
                IEnumerable<Board> boards = _session.Query<Board>();
                ViewBag.BoardCode = boards.ToSelectList(x => x.BoardCode, x => x.BoardCode, disclaimer.BoardCode);
                return View("Edit", Mapper.Map<Disclaimer, DisclaimerViewModel>(disclaimer));
            }

            return RedirectToAction("ManageDisclaimers");
        }

        /// <summary>
        /// Save changes made to the disclaimer
        /// </summary>
        /// <param name="disclaimer"></param>
        [HttpPost]
        public ActionResult Edit(DisclaimerViewModel disclaimer)
        {
            if(ModelState.IsValid)
            {
                var model = _session.Get<Disclaimer>(disclaimer.ID);
                if(TryUpdateModel(model))
                {
                    _session.Save(model);
                    _session.Flush();
                    return RedirectToAction("ManageDisclaimers");
                }
            }
            IEnumerable<Board> boards = _session.Query<Board>();
            ViewBag.BoardCode = boards.ToSelectList(x => x.BoardCode, x => x.BoardCode, disclaimer.BoardCode);
            return View("Edit", disclaimer);
        }

        /// <summary>
        /// Delete the specified disclaimer
        /// </summary>
        /// <param name="id">ID, of the disclaimer to delete</param>
        [HttpGet]
        public ActionResult Delete(int id)
        {
            var model = _session.Get<Disclaimer>(id);
            if (model != null)
            {
                _session.Delete(model);
                _session.Flush();
            }

            return RedirectToAction("ManageDisclaimers");
        }
    }
}
