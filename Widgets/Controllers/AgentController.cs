﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Helpers;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Extensions;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Drawing.Imaging;

namespace Widgets.Controllers
{
    public class AgentController : BaseController
    {
        private readonly IAuthProvider _auth;
        private readonly ISession _session;

        public AgentController(IAuthProvider auth, ISession session) : base(auth, session)
        {
            _auth = auth;
            _session = session;
        }

        /// <summary>
        /// Render manage agents view which displays list of agents for account
        /// </summary>
        [HttpGet]
        [Authorize(Roles = "Super, Admin")]
        public ViewResult ManageAgents(string sortby = "name")
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var agents = Mapper.Map<IEnumerable<Agent>, IEnumerable<AgentInfo>>(Agent.GetByAccount(_session, account));

            // sorting
            ViewBag.NameSort = sortby == "name" ? "name desc" : "name";
            switch (sortby)
            {
                case "name":
                    agents = agents.OrderBy(x => x.FullName);
                    break;
                case "name desc":
                    agents = agents.OrderByDescending(x => x.FullName);
                    break;
            }

            var model = new ManageAgentsViewModel
                            {
                                Account = account,
                                Agents = agents
                            };

            return View("ManageAgents", model);
        }

        /// <summary>
        /// Render Edit Agent View
        /// </summary>
        /// <param name="id">Agent Id to edit</param>
        [HttpGet]
        [Authorize(Roles = "Super, Admin")]
        public ActionResult Edit(int id)
        {
            var model = Mapper.Map<Agent, AgentInfo>(Agent.GetByID(_session, id));
            if (model != null)
            {
                AddOfficesToViewBag(model);
                AddSpecialtyToViewBag(model);
                return View("Edit", model);
            }
            return RedirectToAction("ManageAgents", "Agent");
        }

        /// <summary>
        /// Render Create Agent View
        /// </summary>
        [HttpGet]
        [Authorize(Roles = "Super, Admin")]
        public ViewResult Create()
        {
            var model = new AgentInfo();
            AddOfficesToViewBag(model);
            AddSpecialtyToViewBag(model);
            return View("Create", model);
        }

        /// <summary>
        /// Save Change made on edit agent view
        /// </summary>
        /// <param name="modifiedAgent">view model of agent information</param>
        /// <param name="postedFile">agentsphoto</param>
        /// <param name="officeId">office associated to agent</param>/// 
        [HttpPost]
        [Authorize(Roles = "Super, Admin")]
        public ActionResult Edit(AgentInfo modifiedAgent, HttpPostedFileBase postedFile, int? officeId = null)
        {
            if (CheckUsername(modifiedAgent.UserName, modifiedAgent.AgentID))
            {
                if (ModelState.IsValid)
                {
                    var model = Agent.GetByID(_session, modifiedAgent.AgentID);
                    if (TryUpdateModel(model, string.Empty, null, new string[] { "Office"}))
                    {
                        if (postedFile != null)
                        {
                            Image agentimage = Image.FromStream(postedFile.InputStream, true, true);
                            byte[] imagearray = Crop_Agent_Image(agentimage);
                            if (imagearray == null)
                            {
                                ModelState.AddModelError("postedFile", "We couldn't save your image. Please, Try Again");
                            }
                            model.PhotoMimeType = postedFile.ContentType;
                            model.Photo = imagearray;
                            //var imgBytes = ControllerHelpers.GetImageBytes(postedFile);
                            
                        }
                        if (officeId != null)
                        {
                            model.Office = AccountOffice.GetById(_session, (int) officeId);
                        }
                        else
                        {
                            model.Office = null;
                        }
                        _session.Update(model);
                        _session.Flush();

                        CheckContactsPage(model);
                        CheckFeaturedAgents(model);
                        CheckReferralSystem(model);
                        OrderReferrals(model.Account, _session);

                        return RedirectToAction("ManageAgents", "Agent");
                    }
                }
            }
            else
            {
                ModelState.AddModelError("UserName", "User Name is already in use.");
            }
            AddOfficesToViewBag(modifiedAgent);
            AddSpecialtyToViewBag(modifiedAgent);
            return View("Edit", modifiedAgent);
        }

        /// <summary>
        /// Create new agent
        /// </summary>
        /// <param name="agent"></param>
        /// <param name="postedFile"> </param>
        /// <param name="officeId">office associated to agent</param>/// 
        [HttpPost]
        [Authorize(Roles = "Super, Admin")]
        public ActionResult Create(AgentInfo agent, HttpPostedFileBase postedFile, int? officeId = null)
        {
            if (CheckUsername(agent.UserName))
            {
                if (ModelState.IsValid)
                {
                    var account = Account.GetById(_session, _auth.GetAccountKey());
                    var model = Mapper.Map<AgentInfo, Agent>(agent);
                    model.Account = account;
                    if (postedFile != null)
                    {
                        Image agentimage = Image.FromStream(postedFile.InputStream, true, true);
                        byte[] imagearray = Crop_Agent_Image(agentimage);
                        if (imagearray == null)
                        {
                            ModelState.AddModelError("postedFile", "We couldn't save your image. Please, Try Again");
                        }
                        model.PhotoMimeType = postedFile.ContentType;
                        model.Photo = imagearray;
                        //var imgBytes = ControllerHelpers.GetImageBytes(postedFile);
                       
                    }
                    if (officeId != null)
                    {
                        model.Office = AccountOffice.GetById(_session, (int)officeId);
                    }
                    _session.Save(model);
                    _session.Flush();

                    CheckContactsPage(model);
                    CheckFeaturedAgents(model);
                    CheckReferralSystem(model);
                    OrderReferrals(account, _session);

                    return RedirectToAction("ManageAgents", "Agent");
                }
            }
            else
            {
                ModelState.AddModelError("UserName", "User Name is already in use.");
            }
            AddOfficesToViewBag(agent);
            AddSpecialtyToViewBag(agent);
            return View("Create", agent);
        }

        /// <summary>
        /// Checks to see if a user name is in user
        /// </summary>
        /// <param name="username">username to check</param>
        /// <param name="id">when editing ensure, that the username that exists is not for the current user</param>
        private bool CheckUsername(string username, int? id = null)
        {
            var agent = _session.Query<Agent>().SingleOrDefault(x=>x.UserName == username);

            // when editing check that the user id found is different than the user being edited
            if (id != null && agent != null)
            {
                if (agent.AgentID == id)
                {
                    return true;
                }
            }

            return agent == null;
        }

        /// <summary>
        /// Checks to see if the user is supposed to be in the contact system or not
        /// </summary>
        /// <param name="model"></param>
        private void CheckContactsPage(Agent model)
        {
            var contact = _session.Query<Contact>().SingleOrDefault(x => x.Agent == model);

            if (model.ContactsPage)
            {
                if (contact == null)
                {
                    var create = new Contact
                    {
                        Account = model.Account,
                        Agent = model,
                        AgentTitle = model.AgentTitle,
                    };
                    _session.Save(create);
                }
            }
            else
            {
                if (contact != null)
                {
                    _session.Delete(contact);
                }
            }
            _session.Flush();
        }


        /// <summary>
        /// Checks to see if the user is supposed to be a featured agent or not
        /// </summary>
        /// <param name="model"></param>
        private void CheckFeaturedAgents(Agent model)
        {
            var agent = _session.Query<FeaturedAgent>().SingleOrDefault(x => x.Agent == model);

            if (model.FeaturedAgent)
            {
                if (agent == null)
                {
                    var create = new FeaturedAgent
                    {
                        Account = model.Account,
                        Agent = model,
                    };
                    _session.Save(create);
                }
            }
            else
            {
                if (agent != null)
                {
                    _session.Delete(agent);
                }
            }
            _session.Flush();
        }


        /// <summary>
        /// Checks to see if the user is supposed to be in the referral system or not
        /// </summary>
        /// <param name="model"></param>
        private void CheckReferralSystem(Agent model)
        {
            var referral = _session.Query<Referral>().SingleOrDefault(x => x.Agent == model);

            if (model.ReferralSystem)
            {
                if (referral == null)
                {
                    var create = new Referral
                    {
                        Account = model.Account,
                        Agent = model,
                        PositionNumber = 100
                    };
                    _session.Save(create);
                    OrderReferrals(model.Account, _session);
                }
            }
            else
            {
                if (referral != null)
                {
                    _session.Delete(referral);
                }
            }
            _session.Flush();
        }

        /// <summary>
        /// Retrieve agent photo for the given agent id
        /// </summary>
        /// <param name="id">The agent id to get the photo of</param>
        [HttpGet]
        public ActionResult GetImage(int id)
        {
            var agent = Agent.GetByID(_session, id);
            return agent.Photo == null ? null : new FileContentResult(agent.Photo, agent.PhotoMimeType);
        }

        /// <summary>
        /// Delete agent 
        /// </summary>
        /// <param name="id">Agent id to delete</param>
        [HttpGet]
        public ActionResult Delete(int id)
        {
            var agent = Agent.GetByID(_session, id);
            if(agent.Account.Id == _auth.GetAccountKey())
            {
                _session.Delete(agent);
                _session.Flush();    
            }

            return RedirectToAction("ManageAgents", "Agent");
        }

        /// <summary>
        /// Order the referrals by position number
        /// </summary>
        private static void OrderReferrals(Account account, ISession session)
        {
            var referals = session.Query<Referral>().Where(x => x.Account == account)
                .OrderBy(x => x.PositionNumber).ThenBy(x => x.LastReferralDate).ToList();

            for (var i = 0; i < referals.Count(); i++)
            {
                var referral = referals.ElementAt(i);
                referral.PositionNumber = i + 1;
                session.Save(referral);
            }
            session.Flush();
        }

        /// <summary>
        /// Add account offices to viewbag for create / edit views
        /// </summary>
        private void AddOfficesToViewBag(AgentInfo model)
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var offices = Mapper.Map<IEnumerable<AccountOffice>, IEnumerable<AccountOfficeInfo>>(AccountOffice.GetByAccount(_session, account));
            var selectList = offices.ToSelectList(value => value.Id.ToString(CultureInfo.InvariantCulture), text => text.Name, "Select Office");

            if (model.Office != null)
            {
                ViewBag.Office = model.Office.Id;
                foreach (var selectListItem in selectList.Where(selectListItem => selectListItem.Value == model.Office.Id.ToString(CultureInfo.InvariantCulture)))
                {
                    selectListItem.Selected = true;
                }
            }
            ViewBag.Offices = selectList;
        }

        private void AddSpecialtyToViewBag(AgentInfo model)
        {
            var AgentSpecialties = _session.Query<AgentSpecialty>();
            var selectList = new SelectList(AgentSpecialties, "Specialty", "Specialty", model.Specialty);
            ViewBag.Specialty = selectList;
        }

        private byte[] Crop_Agent_Image(Image agentimage)
        {
            double height = (220 * agentimage.Height) / agentimage.Width;
            var agentbitmap = new Bitmap(agentimage, new Size(220, (int)height));
            agentimage = (Image)(agentbitmap);
            try
            {
               // if ((int)height > 288)
                //{
                    var bitmap = new Bitmap(220, 288);
                    using (Graphics g = Graphics.FromImage(bitmap))
                    {
                        g.SmoothingMode = SmoothingMode.Default;
                        g.PixelOffsetMode = PixelOffsetMode.Default;
                        g.CompositingQuality = CompositingQuality.Default;
                        g.InterpolationMode = InterpolationMode.Default;
                        g.DrawImage(agentimage,
                            new Rectangle(0, 0, 220, 288), new Rectangle(0, 0, 220, 288), GraphicsUnit.Pixel);
                    }
                    byte[] imgarray = null;
                    using (MemoryStream stream = new MemoryStream())
                    {
                        bitmap.Save(stream, ImageFormat.Png);
                        imgarray = stream.ToArray();
                    }
                    ImageConverter converter = new ImageConverter();
                   // byte[] imgarray = (byte[])converter.ConvertTo(bitmap, typeof(byte[]));
                    return imgarray;
               // }
               // else
               // {
              //      ImageConverter converter = new ImageConverter();
              //      byte[] imgarray = (byte[])converter.ConvertTo(agentbitmap, typeof(byte[]));
              //      return imgarray;
              //  }

            }
            catch
            {
                return null;
            }
        }
    }
}