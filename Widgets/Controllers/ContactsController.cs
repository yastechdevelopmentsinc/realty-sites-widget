﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    [Authorize(Roles = "Super, Admin")]
    public class ContactsController : BaseController
    {
        private readonly IAuthProvider _auth;
        private readonly ISession _session;

        public ContactsController(IAuthProvider auth, ISession session) : base(auth, session)
        {
            _auth = auth;
            _session = session;
        }

        /// <summary>
        /// Render the index view which displays the accounts users, that have been selecte to appear within the contacts widget
        /// </summary>
        /// <param name="message">Successs message to display</param>
        [HttpGet]
        public ViewResult Index(string message)
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());

            var contacts = _session.Query<Contact>().Where(x => x.Account == account);
            var model = new ContactsIndexViewModel
                            {
                                Contacts = Mapper.Map<IEnumerable<Contact>, IEnumerable<ContactsViewModel>>(contacts).OrderBy(x => x.PositionNumber)
                            };

            if (!string.IsNullOrEmpty(message))
            {
                ViewBag.Message = message;
            }

            return View("Index", model);
        }

        /// <summary>
        /// Update the order of the contacts
        /// </summary>
        /// <param name="items">Posted Index Form</param>
        [HttpPost]
        public JsonResult Index(IEnumerable<ContactsOrder> items)
        {
            foreach (var contacts in items)
            {
                var contact = _session.Get<Contact>(contacts.id);
                contact.PositionNumber = contacts.order;
                _session.Save(contact);
                _session.Flush();
            }

            return new JsonResult
                       {
                           JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                           Data = new { message = "Contacts order updated" }
                       };

        }

    }

    public class ContactsOrder
    {
        public int id { get; set; }
        public int order { get; set; }
    }
}
