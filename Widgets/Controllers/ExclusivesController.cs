﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Helpers;
using Widgets.Infrastructure.Abstract;
using Widgets.Models;
using Widgets.Models.ViewModels;
using Widgets.Extensions;
using AutoMapper;

namespace Widgets.Controllers
{
    [Authorize(Roles = "Super, Admin, Listings")]
    public class ExclusivesController : BaseController
    {
        private readonly IAuthProvider _auth;
        private readonly ISession _session;

        public ExclusivesController(IAuthProvider auth, ISession session) : base(auth, session)
        {
            _auth = auth;
            _session = session;
        }

        /// <summary>
        /// Render index view which display list of all exclusive listings for the account
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var exclusives = _session.Query<Exclusive>().Where(x=>x.Account == account);

            var model = new ExclusiveIndexViewModel
                            {
                                Account = AutoMapper.Mapper.Map<Account, AccountViewModel>(account),
                                Exclusives = AutoMapper.Mapper.Map<IEnumerable<Exclusive>, IEnumerable<ExclusiveListing>>(exclusives)
                            };
            ViewBag.exclusivelist = model;
            return View("Index");
        }

        [HttpPost]
        public ActionResult Search(string serachtext)
        {
            serachtext = serachtext.ToLower();
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var exclusives = _session.Query<Exclusive>().Where(x => x.Account == account);
            if (!string.IsNullOrEmpty(serachtext))
            {
                exclusives = exclusives.Where(x => (x.Address.ToLower().Contains(serachtext) || x.City.ToLower().Contains(serachtext) || x.AreaName.ToLower().Contains(serachtext) || x.SubAreaName.ToLower().Contains(serachtext) || x.InternetComm.ToLower().Contains(serachtext)));
            }
            var model = new ExclusiveIndexViewModel
            {
                Account = AutoMapper.Mapper.Map<Account, AccountViewModel>(account),
                Exclusives = AutoMapper.Mapper.Map<IEnumerable<Exclusive>, IEnumerable<ExclusiveListing>>(exclusives)
            };
            return PartialView("ExclusiveListTemplate",model);
        }

        /// <summary>
        /// Render create exclusive listing view
        /// </summary>
        [HttpGet]
        public ViewResult Create()
        {
            var model = SetupExclusivesViewModel();
            model.Exclusive = new ExclusiveListing();

            return View("Create", model);
        }

        /// <summary>
        /// Create exclusive listing
        /// </summary>
        /// <param name="exclusive">view model of exclusive listing information</param>
        [HttpPost]
        public ActionResult Create(ExclusiveListing exclusive, string otherarea, string othersubarea)
        {
            if (ModelState.IsValid)
            {
                var account = Account.GetById(_session, _auth.GetAccountKey());
                var model = AutoMapper.Mapper.Map<ExclusiveListing, Exclusive>(exclusive);
                
                model.Account = account;
                model.AgentBoardCode = account.BoardCode;
                model.AgentBrokerCode = account.BrokerCode;
                model.DateEntered = DateTime.Now;
                model.CoAgentIndCode = exclusive.CoAgentIndCode;
                model.CoAgentIndCode2 = exclusive.CoAgentIndCode2;
                SetPropertyStatus(exclusive, model);
                if (model.AreaCode == -1 && string.IsNullOrEmpty(otherarea))
                {
                    var errorModel2 = SetupExclusivesViewModel();
                    errorModel2.Exclusive = exclusive;

                    return View("Create", errorModel2);
                }
                SetAreaAndSubArea(exclusive, model,otherarea , othersubarea);
                

                _session.Save(model);
                _session.Flush();

                return RedirectToAction("Index", "Exclusives");
            }

            var errorModel = SetupExclusivesViewModel();
            errorModel.Exclusive = exclusive;

            return View("Create", errorModel);
        }

        /// <summary>
        /// Render Edit Exclusive Listing View
        /// </summary>
        /// <param name="id">The id of the exclusive listing to edit</param>
        [HttpGet]
        public ViewResult Edit(int id)
        {
            var exclusive = AutoMapper.Mapper.Map<Exclusive, ExclusiveListing>(_session.Get<Exclusive>(id));

            var model = SetupExclusivesViewModel();
            model.Exclusive = exclusive;
                
            return View("Edit", model);
        }

        /// <summary>
        /// Save changes made on edit view
        /// </summary>
        /// <param name="exclusive">view model of exclusive listing information</param>
        [HttpPost]
        public ActionResult Edit(ExclusiveListing exclusive, string otherarea, string othersubarea)
        {
            if(ModelState.IsValid)
            {
                var model = _session.Get<Exclusive>(exclusive.ExclusiveID);
                if (TryUpdateModel(model, "Exclusive"))
                {
                    SetPropertyStatus(exclusive, model);
                    if (model.AreaCode == -1 && string.IsNullOrEmpty(otherarea))
                    {
                        var errorModel2 = SetupExclusivesViewModel();
                        errorModel2.Exclusive = exclusive;

                        return View("Edit", errorModel2);
                    }
                    SetAreaAndSubArea(exclusive, model,otherarea,othersubarea);
                   

                    _session.Save(model);
                    _session.Flush();

                    return RedirectToAction("Index", "Exclusives");
                }
            }

            var errorModel = SetupExclusivesViewModel();
            errorModel.Exclusive = exclusive;

            return View("Edit", errorModel);
        }


        /// <summary>
        /// Get sub locations base upon the selected area 
        /// </summary>
        /// <param name="areacode">selected area</param>
        [HttpGet]
        public JsonResult GetLocations(int areacode)
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            List<Location> locations = null;
            if (account.BoardCode != "VIREB")
            {
                locations = _session.Query<Location>().Where(x => x.AreaCode == areacode).ToList();
                locations.Add(new Location() { AreaID = -1, AreaName = "Other", DistrictCode = "-1" });
            }
            else
            {
                VDistrict currentdistrict = _session.Query<VDistrict>().FirstOrDefault(x => x.AreaID == areacode);
                string code ="";
                List<string> areanamesplit = currentdistrict.AreaDescription.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if(areanamesplit.Count >2)
                {
                    areanamesplit[1] = areanamesplit[1].Replace("-","");
                    if(areanamesplit[1] != "10")
                    {
                        code ="Z"+areanamesplit[1];
                    }
                    else
                    {
                        code ="10";
                    }
                }
                var vancouverlocations = _session.Query<VMapArea>().Where(x=>x.AreaName.Substring(0,2) == code).ToList();
                locations = Mapper.Map<IEnumerable<VMapArea>, IEnumerable<Location>>(vancouverlocations).ToList();
            }

           

            return new JsonResult
                       {
                           JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                           Data = locations
                       };
        }

        /// <summary>
        /// Setup the exclusive listings view model
        /// </summary>
        /// <returns></returns>
        private ExclusivesViewModel SetupExclusivesViewModel()
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            IEnumerable<Agent> agents = _session.Query<Agent>().Where(x => x.Account == account);
            List<Area> areas = new List<Area>();
            if (account.BoardCode != "VIREB")
            {
                areas = _session.Query<Area>().Where(x => x.BoardCode == account.BoardCode || x.Account.Id == account.Id).ToList();
                areas.Add(new Area() { AreaID = -1, AreaDescription = "Other" });
            }
            else
            {
                var vancouverareas = _session.Query<VDistrict>().ToList();
                areas = Mapper.Map<IEnumerable<VDistrict>,IEnumerable<Area>>(vancouverareas).ToList();
            }
            

            List<SelectListItem> propertyTypes;
            if (account.AccountType == AccountType.Commercial)
            {
                var obj = new CommercialPropertyTypeViewModel();
                propertyTypes = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance)
                    .ToSelectList(x => (ControllerHelpers.GetPropValue(obj, x.Name).ToString()), x => (ControllerHelpers.GetName(x)), "Select");
            }
            else
            {
                var obj = new PropertyTypeViewModel();
                propertyTypes = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance)
                    .ToSelectList(x => (ControllerHelpers.GetPropValue(obj, x.Name).ToString()), x => (ControllerHelpers.GetName(x)), "Select");
            }

            IEnumerable<Style> styles = _session.Query<Style>();
            IEnumerable<DwellingType> types = _session.Query<DwellingType>();

            return new ExclusivesViewModel
            {
                Account = account,
                Agents = agents.ToSelectList(x => x.AgentIndCode, x => x.FullName, "Select Agent"),
                CoAgents = agents.ToSelectList(x => x.AgentIndCode, x => x.FullName, "Select Co-Listing Agent"),
                CoAgents2 = agents.ToSelectList(x => x.AgentIndCode, x => x.FullName, "Select Co-Listing Agent"),
                Areas = areas.ToSelectList(x => x.AreaID.ToString(), x => x.AreaDescription, "Select Area"),
                PropertyTypes = propertyTypes,
                Styles = styles.ToSelectList( x => x.StyleName, x => x.StyleName, "Select Style"),
                Type = types.ToSelectList( x => x.Type, x => x.Type, "Select Type")
            };
        }

        /// <summary>
        /// Render the create / edit photos view for exclusive listings
        /// </summary>
        /// <param name="id">id of the exclusive listing to add photos too</param>
        [HttpGet]
        public ActionResult Photos(int id)
        {
            var exclusivenotlisting = _session.Get<Exclusive>(id);
            var exclusive = AutoMapper.Mapper.Map<Exclusive, ExclusiveListing>(exclusivenotlisting);
            var path = string.Format("{0}\\{1}\\{2}", Properties.Settings.Default.AbsoluteMediaPath, "ExclusivePhotos", id);
            if (Directory.Exists(path))
            {
                var files = Directory.GetFiles(path).ToList();
                exclusivenotlisting.PhotoCount = files.Count;
                _session.Save(exclusivenotlisting);
                _session.Flush();
            }
            return View("Photos", exclusive);
        }

        /// <summary>
        /// Save exclusive listing photos
        /// </summary>
        /// <param name="id">Exclusive Listing id</param>
        /// <param name="Filedata">collection of posted files</param>
        [HttpPost]
        public ActionResult Photos(int id, IEnumerable<HttpPostedFileBase> Filedata)
        {
            var path = string.Format("{0}\\{1}\\{2}\\", Properties.Settings.Default.AbsoluteMediaPath, "ExclusivePhotos", id);

            if(!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var existingFiles = Directory.GetFiles(path);
            int i = existingFiles.Count();

            foreach (var file in Filedata)
            {
                i++;
                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        int start = file.FileName.LastIndexOf(".", StringComparison.Ordinal);
                        int end = file.FileName.Length - start;
                        
                        var ext = file.FileName.Substring(start,end);
                        var fileName = string.Format("{0}_{1}{2}", id, i, ext);
                        file.SaveAs(path + fileName);
                        
                       

                    }
                }

            }
            var exclusive = _session.Get<Exclusive>(id);
            exclusive.PhotoCount = i;
            _session.Save(exclusive);
            _session.Flush();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Delete an exclusive listing
        /// </summary>
        /// <param name="id">Exclusive listing id</param>
        [HttpGet]
        public ActionResult Delete(int id)
        {
            var exclusive = _session.Get<Exclusive>(id);
            if(exclusive.Account.Id == _auth.GetAccountKey())
            {
                _session.Delete(exclusive);
                _session.Flush();

                var photoPath = string.Format("{0}\\{1}\\{2}\\", Properties.Settings.Default.AbsoluteMediaPath, "ExclusivePhotos", id);
                if (Directory.Exists(photoPath))
                {
                    Directory.Delete(photoPath, true);
                }

                var documentPath = string.Format("{0}\\{1}\\{2}\\", Properties.Settings.Default.AbsoluteMediaPath, "ExclusiveDocuments", id);
                if (Directory.Exists(documentPath))
                {
                    Directory.Delete(documentPath, true);
                }
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Delete a photo for an exclsuvie listing
        /// </summary>
        /// <param name="id">exclusive listing id</param>
        /// <param name="photopath">the path to the photo to delete</param>
        [HttpGet]
        public ActionResult DeletePhoto(int id, string photopath)
        {
            if(System.IO.File.Exists(photopath))
            {
                System.IO.File.Delete(photopath);

                // get all exclusive photos for current listing and sort by photo index
                var path = string.Format("{0}\\{1}\\{2}", Properties.Settings.Default.AbsoluteMediaPath, "ExclusivePhotos", id);
                var files = Directory.GetFiles(path).ToList();
                var photos = files.Select(file => new OrderedExclusivePhoto
                    {
                        index = ControllerHelpers.GetImageIndex(file), value = file
                    }).ToList();
                ReOrderExclusiveListingPhotos(id, photos);
                var exclusive = _session.Get<Exclusive>(id);
                exclusive.PhotoCount = files.Count;
                _session.Save(exclusive);
                _session.Flush();
            }

            return RedirectToAction("Photos", "Exclusives", new { id });
        }

        /// <summary>
        /// Render View to add a new open house to an exclusive listing
        /// </summary>
        /// <param name="id">Exclusive Listing ID</param>
        [HttpGet]
        public ViewResult AddOpenHouse(int id)
        {
            var openHouse = new OpenHouseViewModel
                                {
                                    MlsNumber = id,
                                    FromDate = DateTime.Today
                                };

            SetupOpenHouseTimes(openHouse);

            return View("AddOpenHouse", openHouse);
        }

        /// <summary>
        /// Add new open house to exclusive listing
        /// </summary>
        /// <param name="openhouse"></param>
        /// <param name="id"></param>
        [HttpPost]
        public ActionResult AddOpenHouse(OpenHouseViewModel openhouse, int id)
        {
            if(ModelState.IsValid)
            {
                var model = AutoMapper.Mapper.Map<OpenHouseViewModel, OpenHouse>(openhouse);
                model.MlsNumber = id;

                _session.Save(model);
                _session.Flush();

                return RedirectToAction("Index", "Exclusives");
            }

            return View("AddOpenHouse", openhouse);
        }

        /// <summary>
        /// Render Edit View for exclusive house open house
        /// </summary>
        /// <param name="id">The Exclusive Listing Id</param>
        [HttpGet]
        public ActionResult EditOpenHouse(int id)
        {
            var model = AutoMapper.Mapper.Map<OpenHouse, OpenHouseViewModel>(_session.Get<OpenHouse>(id));
            SetupOpenHouseTimes(model);

            return View("EditOpenHouse", model);
        }

        /// <summary>
        /// Save Change Made to an open house
        /// </summary>
        /// <param name="openhouse">Open House View model</param>
        /// <param name="id">The Exclusive Listing id</param>
        [HttpPost]
        public ActionResult EditOpenHouse(OpenHouseViewModel openhouse, int id)
        {
            if (ModelState.IsValid)
            {
                var model = _session.Get<OpenHouse>(id);
                if(TryUpdateModel(model))
                {
                    _session.Save(model);
                    _session.Flush();
                    return RedirectToAction("Index", "Exclusives"); 
                }
            }
            return View("EditOpenHouse", openhouse); 
        }


        /// <summary>
        /// Add document to the exclusive listing 
        /// </summary>
        /// <param name="id">Exclusive Listing Id</param>
        [HttpGet]        
        public ActionResult AddDocument(int id)
        {
            var documentViewModel = new DocumentViewModel(id);
            return View("AddDocument", documentViewModel);
        }

        /// <summary>
        /// Save document
        /// </summary>
        /// <param name="id">Exclusive Listing Id</param>
        /// <param name="postedFile">Document to save</param>
        [HttpPost]
        public ActionResult AddDocument(int id, HttpPostedFileBase postedFile)
        {
            var path = string.Format("{0}\\{1}\\{2}\\", Properties.Settings.Default.AbsoluteMediaPath, "ExclusiveDocuments", id);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var fileName = string.Format("{0}{1}", path, postedFile.FileName);
            postedFile.SaveAs(fileName);

            return RedirectToAction("Index", "Exclusives");
        }

        /// <summary>
        /// Delete a document for an excluisve listing
        /// </summary>
        /// <param name="id">Exclusive listing id</param>
        /// <param name="documentpath">The path to the document to delete</param>
        [HttpGet]
        public ActionResult DeleteDocument(int id, string documentpath)
        {
            if (System.IO.File.Exists(documentpath))
            {
                System.IO.File.Delete(documentpath);
            }

            return RedirectToAction("Index", "Exclusives", new { id });
        }

        /// <summary>
        /// Reorder exclusive listing photos for the listing
        /// </summary>
        [HttpPost]
        public ActionResult Order(int id, IEnumerable<OrderedExclusivePhoto> photos)
        {
            ReOrderExclusiveListingPhotos(id, photos);
            return new JsonResult()
                       {
                           JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                           Data = new { message = "Order updated" }
                       };
        }

        /// <summary>
        /// When a exclusive listing photo is deleted, keep the filenames in sync
        /// </summary>
        /// <param name="id">The listing id</param>
        /// <param name="photos">The list of photos to reorder</param>
        private static void ReOrderExclusiveListingPhotos(int id, IEnumerable<OrderedExclusivePhoto> photos)
        {
            var path = string.Format("{0}\\{1}\\{2}\\", Properties.Settings.Default.AbsoluteMediaPath, "ExclusivePhotos", id);
            
            // create a temp directory to perform the ordering
            var tempDirectory = path + "temp";
            Directory.CreateDirectory(tempDirectory);

            // rename photos with the new index
            foreach (var photo in photos)
            {
                if (System.IO.File.Exists(photo.value))
                {
                    var moveTo = GetMoveToFileName(id, photo.index, tempDirectory, photo.value);
                    System.IO.File.Move(photo.value, moveTo);
                }
            }

            // move back to the normal directory
            foreach (var file in Directory.GetFiles(tempDirectory))
            {
                System.IO.File.Copy(file, Path.Combine(path, Path.GetFileName(file)), true);
            }

            // cleanup
            Directory.Delete(tempDirectory, true);
        }

        /// <summary>
        /// Setup the properties of an openhouse view model
        /// </summary>
        /// <param name="openHouse"></param>
        private static void SetupOpenHouseTimes(OpenHouseViewModel openHouse)
        {
            var start = DateTime.Today;
            List<SelectListItem> times = (from offset in Enumerable.Range(16, 32)
                                          select start.AddMinutes(30 * offset)).ToSelectList(x => x.ToString("Hmm"),
                                                                                           x => x.ToShortTimeString(),
                                                                                           "Select Time");

            openHouse.FromTimes = times;
            openHouse.ToTimes = times;
        }

        private static string GetMoveToFileName(int exclusiveID, int photoIndex, string path, string file)
        {
            int start = file.LastIndexOf(".", StringComparison.Ordinal);
            int end = file.Length - start;
            var ext = file.Substring(start, end);

            var fileName = string.Format("{0}_{1}{2}", exclusiveID, photoIndex, ext);
            var moveTo = string.Format("{0}\\{1}", path, fileName);
            return moveTo;
        }

        private void SetAreaAndSubArea(ExclusiveListing exclusive, Exclusive model, string otherarea, string othersubarea)
        {
            if (exclusive.AreaCode != -1)
            {
                if (model.Account.BoardCode != "VIREB")
                {
                    var area = _session.Query<Area>().SingleOrDefault(x => x.AreaID == exclusive.AreaCode);
                    var location = _session.Query<Location>().SingleOrDefault(x => x.DistrictCode == exclusive.DistrictCode && x.BoardCode == model.Account.BoardCode && x.AreaCode == area.AreaID);
                    if (location != null)
                    {
                        model.AreaName = location.AreaName;
                        model.SubAreaName = location.SubAreaName;
                    }
                    else if (location == null && exclusive.DistrictCode == "-1")
                    {
                        model.AreaName = area.AreaDescription;
                        model.SubAreaName = othersubarea;
                        model.DistrictCode = -1;
                        if (!string.IsNullOrEmpty(othersubarea))
                        {
                            Location newlocation = new Location() { CityName = exclusive.City, AreaName = othersubarea, AreaCode = area.AreaID, SubAreaName = othersubarea, BoardCode = model.Account.BoardCode };
                            _session.Save(newlocation);
                            model.DistrictCode = newlocation.AreaID;
                        }
                    }
                }
                else
                {
                     var area = _session.Query<VDistrict>().SingleOrDefault(x => x.AreaID == exclusive.AreaCode);
                    var location = _session.Query<VMapArea>().SingleOrDefault(x => x.AreaID.ToString() == exclusive.DistrictCode);
                    if (location != null)
                    {
                        model.AreaName = area.AreaDescription;
                        model.SubAreaName = location.AreaName.Substring(2);
                    }
                }
            }
            else
            {
                if (model.Account.BoardCode != "VIREB")
                {
                    model.AreaName = otherarea;
                    model.SubAreaName = othersubarea;
                    model.DistrictCode = -1;
                    //
                    Area newarea = new Area() { AreaID = 0, BoardCode = null, AreaDescription = otherarea, Account = model.Account };
                    _session.Save(newarea);
                    model.AreaCode = newarea.AreaID;
                    //
                    if (!string.IsNullOrEmpty(othersubarea))
                    {
                        Location newlocation = new Location() { CityName = exclusive.City, AreaName = othersubarea, AreaCode = newarea.AreaID, SubAreaName = othersubarea, BoardCode = model.Account.BoardCode };
                        _session.Save(newlocation);
                        model.DistrictCode = newlocation.AreaID;
                    }
                }
                else
                {
 
                }
            }
        }

        private static void SetPropertyStatus(ExclusiveListing exclusive, Exclusive model)
        {
            String status = string.Empty;
            if (!string.IsNullOrEmpty(exclusive.ForLease) && !string.IsNullOrEmpty(exclusive.ForSale))
            {
                status = PropertyStatusConstants.ForSaleOrLease;
            }
            else if (!string.IsNullOrEmpty(exclusive.ForLease))
            {
                status = PropertyStatusConstants.ForLease;
            }
            else if (!string.IsNullOrEmpty(exclusive.ForSale))
            {
                status = PropertyStatusConstants.ForSale;
            }
            else if (!string.IsNullOrEmpty(exclusive.Sold) && !string.IsNullOrEmpty(exclusive.Leased))
            {
                status = PropertyStatusConstants.SoldOrLeased;
            }
            else if (!string.IsNullOrEmpty(exclusive.Sold))
            {
                status = PropertyStatusConstants.Sold;
            }
            else if (!string.IsNullOrEmpty(exclusive.Leased))
            {
                status = PropertyStatusConstants.Leased;
            }

            model.Status = status;
            exclusive.Status = status;
        }
    }

    public class OrderedExclusivePhoto
    {
        public int index { get; set; }
        public string value { get; set; }
    }
}
