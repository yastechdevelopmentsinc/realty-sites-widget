﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using AutoMapper;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Helpers;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    [Authorize(Roles = "Super")]
    public class ListingAccessController : BaseController
    {
        private readonly IAuthProvider _auth;
        private readonly ISession _session;

        public ListingAccessController(IAuthProvider auth, ISession session) : base(auth, session)
        {
            _auth = auth;
            _session = session;
        }

        /// <summary>
        /// Render manage listing access view which is used to set the listing display access for property types
        /// </summary>
        [HttpGet]
        public ViewResult ManageListingAccess()
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());

            // get types of properties
            var obj = new PropertyTypeViewModel();
            var properties = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);

            var dic = new Dictionary<string, string>();
            foreach (var property in properties)
            {
                var prop = typeof(PropertyTypeViewModel).GetProperty(property.Name);
                dic.Add(ControllerHelpers.GetName(prop), ControllerHelpers.GetPropValue(obj, property.Name).ToString());
            }

            // get access types
            var displayaccess = Enum.GetValues(typeof(ListingDisplayAccess)).Cast<ListingDisplayAccess>()
                .Select(access => access.ToString()).ToList();

            var model = new ManageListingAccessViewModel
            {
                PropertyTypes = dic.ToList(),
                Access = displayaccess,
                ListingDisplayAccess = Mapper.Map<AccountListingAccess, ListingAccessViewModel>(_session.Query<AccountListingAccess>().SingleOrDefault(x => x.Account == account))
            };

            return View("ManageListingAccess", model);
        }

        /// <summary>
        /// Create new / save changes made to listing access levels for account
        /// </summary>
        /// <param name="listingAccess"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageListingAccess(ListingAccessViewModel listingAccess)
        {
            AccountListingAccess model;
            if (listingAccess.Id > 0)
            {
                model = _session.Get<AccountListingAccess>(listingAccess.Id);
                TryUpdateModel(model);
            }
            else
            {
                model = Mapper.Map<ListingAccessViewModel, AccountListingAccess>(listingAccess);
                var account = Account.GetById(_session, _auth.GetAccountKey());
                model.Account = account;
            }

            _session.Save(model);
            _session.Flush();

            return RedirectToAction("Index", "Home");
        }

    }
}
