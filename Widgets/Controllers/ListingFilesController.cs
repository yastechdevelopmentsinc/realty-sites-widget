﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Helpers;
using Widgets.Infrastructure;
using Widgets.Infrastructure.Abstract;
using Widgets.Models;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    [Authorize(Roles = "Super, Admin")]
    public class ListingFilesController : BaseController
    {
        private readonly ISession _session;
        private readonly IAuthProvider _auth;
        private readonly IListingService _listingService;

        public ListingFilesController(IAuthProvider auth, ISession session, IListingService listingService)
            : base(auth, session)
        {
            _auth = auth;
            _session = session;
            _listingService = listingService;
        }

        [HttpGet]
        public ViewResult Index(string sortby)
        {
            ViewBag.UploadDateSort = sortby == "UploadDate desc" ? "UploadDate" : "UploadDate desc";
            ViewBag.FileNameSort = sortby == "FileName desc" ? "FileName" : "FileName desc";

            var account = Account.GetById(_session, _auth.GetAccountKey());

            var listingFiles = Mapper.Map<IEnumerable<ListingFiles>, IEnumerable<ListingFilesViewModel>>(
                _session.Query<ListingFiles>()
                    .Where(x => x.Account == account)
                );

            switch(sortby)
            {
                case "FileName desc":
                    listingFiles = listingFiles.OrderByDescending(x => x.FileName);
                    ViewBag.FileNameSort = "FileName";
                    break;

                case "FileName":
                    listingFiles = listingFiles.OrderBy(x => x.FileName);
                    ViewBag.FileNameSort = "FileName desc";
                    break;

                case "UploadDate desc":
                    listingFiles = listingFiles.OrderByDescending(x => x.UploadDate);
                    ViewBag.UploadDateSort = "UploadDate";
                    break;

                case "UploadDate":
                    listingFiles = listingFiles.OrderBy(x => x.UploadDate);
                    ViewBag.UploadDateSort = "UploadDate desc";
                    break;
            }

            var model = new ListingFilesIndexViewModel
                            {
                                ListingFiles = listingFiles
                            };
                           
            return View("Index", model);
        }

        [HttpGet]
        public ViewResult Create()
        {
            if(TempData["error"] != null)
            {
                ViewBag.FileSizeError = TempData["error"];
            }

            var model = new ListingFilesViewModel();
            return View("Create", model);
        }

        [HttpPost]
        public ActionResult Create(HttpPostedFileBase file, ListingFilesViewModel model)
        {
            if (model.Mlsnumber == 0)
            {
                ViewBag.FileSizeError = "MLS number is required.";
                return View("Create", model);
            }
            if (file != null && file.ContentLength > 0)
            {
                double fileSize = (file.ContentLength / 1024.0) / 1024.0;
                if (fileSize > 25.0)
                {
                    ViewBag.FileSizeError = "File must be 25MB or smaller";
                    return View("Create", model);
                }

                var account = Account.GetById(_session, _auth.GetAccountKey());

                // read file
                var contentlength = file.ContentLength;
                var tempFile = new byte[contentlength];
                file.InputStream.Position = 0;
                file.InputStream.Read(tempFile, 0, contentlength);
                Create_Account_Folder(account);
                string filepath = Save_File(file, account);
                var listingFile = new ListingFiles
                                      {
                                          Account = account,
                                          FileName = file.FileName,
                                          MimeType = file.ContentType,
                                          FilePath = filepath,
                                          UploadDate = DateTime.UtcNow,
                                          Mlsnumber = model.Mlsnumber,
                                          Name = model.Name
                                      };
                try
                {
                    _session.Save(listingFile);
                    _session.Flush();
                    return Redirect("/ListingFiles/Index");
                }
                catch
                {
                    ViewBag.FileSizeError = "Something wrong happened we couldn't save your file";
                    return View("Create", model);
                }
            }
            else
            {
                ViewBag.FileSizeError = "File is required";
            }

            return View("Create", model);
        }

        public ActionResult Delete(int id)
        {
            var file = _session.Get<ListingFiles>(id);
            if (file.Account.Id == _auth.GetAccountKey())
            {
                _session.Delete(file);
                _session.Flush();
                string filepath = Server.MapPath("~" + file.FilePath);
                if (System.IO.File.Exists(filepath))
                {
                    System.IO.File.Delete(filepath);
                }
            }

            return Redirect("/ListingFiles/Index");
        }

        private string Create_Account_Folder(Account selectedaccount )
        {
            string documentfolder = Server.MapPath("~/ListingFiles");
            if (!System.IO.Directory.Exists(documentfolder))
            {
                System.IO.Directory.CreateDirectory(documentfolder);
            }
            string accountlocation = Server.MapPath("~/ListingFiles/" + selectedaccount.Id);
            if (!System.IO.Directory.Exists(accountlocation))
            {
                System.IO.Directory.CreateDirectory(accountlocation);
            }
            return accountlocation;
        }

        private string Save_File(HttpPostedFileBase file, Account selectedaccount)
        {
            var fileName = Path.GetFileName(file.FileName);
            var path = Path.Combine(Server.MapPath("~/ListingFiles/" + selectedaccount.Id), fileName);
            string tempfileName = fileName;
            if (System.IO.File.Exists(path))
            {
                int counter = 2;
                while (System.IO.File.Exists(path))
                {
                    tempfileName = counter.ToString() + fileName;
                    path = Path.Combine(Server.MapPath("~/ListingFiles/" + selectedaccount.Id), tempfileName);
                    counter++;
                }
            }
            file.SaveAs(path);
            return "/ListingFiles/" + selectedaccount.Id + "/" + tempfileName;
        }

    }
}
