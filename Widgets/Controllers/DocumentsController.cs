﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    [Authorize]
    public class DocumentsController : BaseController
    {
        private readonly ISession _session;
        private readonly IAuthProvider _auth;

        public DocumentsController(IAuthProvider auth, ISession session) : base(auth, session)
        {
            _auth = auth;
            _session = session;
        }

        [HttpGet]
        public ViewResult Index(string sortby)
        {
            ViewBag.UploadDateSort = sortby == "UploadDate desc" ? "UploadDate" : "UploadDate desc";
            ViewBag.FileNameSort = sortby == "FileName desc" ? "FileName" : "FileName desc";

            var account = Account.GetById(_session, _auth.GetAccountKey());

            var accountFiles = Mapper.Map<IEnumerable<AccountFile>, IEnumerable<AccountFileViewModel>>(
                _session.Query<AccountFile>()
                    .Where(x => x.Account == account)
                );

            switch(sortby)
            {
                case "FileName desc":
                    accountFiles = accountFiles.OrderByDescending(x => x.FileName);
                    ViewBag.FileNameSort = "FileName";
                    break;

                case "FileName":
                    accountFiles = accountFiles.OrderBy(x => x.FileName);
                    ViewBag.FileNameSort = "FileName desc";
                    break;

                case "UploadDate desc":
                    accountFiles = accountFiles.OrderByDescending(x => x.UploadDate);
                    ViewBag.UploadDateSort = "UploadDate";
                    break;

                case "UploadDate":
                    accountFiles = accountFiles.OrderBy(x => x.UploadDate);
                    ViewBag.UploadDateSort = "UploadDate desc";
                    break;
            }

            var model = new AccountFilesIndexViewModel
                            {
                                AccountFiles = accountFiles
                            };
                           
            return View("Index", model);
        }

        [HttpGet]
        public ViewResult Create()
        {
            if(TempData["error"] != null)
            {
                ViewBag.FileSizeError = TempData["error"];
            }

            return View("Create");
        }

        [HttpPost]
        public ActionResult Create(HttpPostedFileBase file)
        {
            if(file != null && file.ContentLength > 0)
            {
                double fileSize = (file.ContentLength / 1024.0) / 1024.0;
                if(fileSize > 25.0)
                {
                    TempData["error"] = "File must be 25MB or smaller";
                    return RedirectToAction("Create");
                }

                var account = Account.GetById(_session, _auth.GetAccountKey());

                // read file
                var contentlength = file.ContentLength;
                var tempFile = new byte[contentlength];
                file.InputStream.Position = 0;
                file.InputStream.Read(tempFile, 0, contentlength);
                Create_Account_Folder(account);
                string filepath = Save_File(file, account);
                var accountFile = new AccountFile
                                      {
                                          Account = account,
                                          FileName = file.FileName,
                                          MimeType = file.ContentType,
                                          FilePath = filepath,
                                          UploadDate = DateTime.UtcNow
                                      };

                _session.Save(accountFile);
                _session.Flush();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Create");
        }

        /*[HttpGet]
        public ActionResult Details(Guid id)
        {
            var file = _session.Get<AccountFile>(id);
            /*if(file.Account.Id == _auth.GetAccountKey())
            {
                return File(file.FileContent, file.MimeType, file.FileName );
            }

            return RedirectToAction("Index");
        }*/

        public ActionResult Delete(Guid id)
        {
            var file = _session.Get<AccountFile>(id);
            if (file.Account.Id == _auth.GetAccountKey())
            {
                _session.Delete(file);
                _session.Flush();
                string filepath = Server.MapPath("~" + file.FilePath);
                if (System.IO.File.Exists(filepath))
                {
                    System.IO.File.Delete(filepath);
                }
            }

            return RedirectToAction("Index");
        }

        private string Create_Account_Folder(Account selectedaccount )
        {
            string documentfolder = Server.MapPath("~/DocumentFiles");
            if (!System.IO.Directory.Exists(documentfolder))
            {
                System.IO.Directory.CreateDirectory(documentfolder);
            }
            string accountlocation = Server.MapPath("~/DocumentFiles/" + selectedaccount.Id);
            if (!System.IO.Directory.Exists(accountlocation))
            {
                System.IO.Directory.CreateDirectory(accountlocation);
            }
            return accountlocation;
        }

        private string Save_File(HttpPostedFileBase file, Account selectedaccount)
        {
            var fileName = Path.GetFileName(file.FileName);
            var path = Path.Combine(Server.MapPath("~/DocumentFiles/" + selectedaccount.Id), fileName);
            string tempfileName = fileName;
            if (System.IO.File.Exists(path))
            {
                int counter = 2;
                while (System.IO.File.Exists(path))
                {
                    tempfileName = counter.ToString() + fileName;
                    path = Path.Combine(Server.MapPath("~/DocumentFiles/" + selectedaccount.Id), tempfileName);
                    counter++;
                }
            }
            file.SaveAs(path);
            return "/DocumentFiles/" + selectedaccount.Id+"/" +tempfileName;
        }
    }
}
