﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using NHibernate;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;

namespace Widgets.Controllers
{
    public class OfficeController : BaseController
    {
        private readonly IAuthProvider _auth;
        private readonly ISession _session;

        public OfficeController(IAuthProvider auth, ISession session) : base(auth, session)
        {
            _auth = auth;
            _session = session;
        }

        /// <summary>
        /// Render Manage Offices view which displays list of offices for account
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Super, Admin")]
        public ViewResult ManageOffices()
        {
            var account = Account.GetById(_session, _auth.GetAccountKey());
            var offices = Mapper.Map<IEnumerable<AccountOffice>, IEnumerable<AccountOfficeInfo>>(AccountOffice.GetByAccount(_session, account));

            var model = new ManageOfficesViewModel
            {
                Account = account,
                Offices = offices
            };

            return View("ManageOffices", model);
        }

        /// <summary>
        /// Render Create Office View
        /// </summary>
        [HttpGet]
        [Authorize(Roles = "Super, Admin")]
        public ViewResult Create()
        {
            var model = new AccountOfficeInfo();
            return View("Create", model);
        }

        /// <summary>
        /// Create new office
        /// </summary>
        /// <param name="office"></param>
        [HttpPost]
        [Authorize(Roles = "Super, Admin")]
        public ActionResult Create(AccountOfficeInfo office)
        {
            if (ModelState.IsValid)
            {
                var account = Account.GetById(_session, _auth.GetAccountKey());
                var model = Mapper.Map<AccountOfficeInfo, AccountOffice>(office);
                model.Account = account;

                _session.Save(model);
                _session.Flush();

                return RedirectToAction("ManageOffices", "Office");
            }

            return View("Create", office);
        }

        /// <summary>
        /// Render Edit Office View
        /// </summary>
        /// <param name="id">Office Id to edit</param>
        [HttpGet]
        [Authorize(Roles = "Super, Admin")]
        public ViewResult Edit(int id)
        {
            var model = Mapper.Map<AccountOffice, AccountOfficeInfo>(_session.Get<AccountOffice>(id));
            return View("Edit", model);
        }

        /// <summary>
        /// Save Change made on edit office view
        /// </summary>
        /// <param name="modifiedOffice">view model of office information</param>
        [HttpPost]
        [Authorize(Roles = "Super, Admin")]
        public ActionResult Edit(AccountOfficeInfo modifiedOffice)
        {
            if (ModelState.IsValid)
            {
                var model = AccountOffice.GetById(_session, modifiedOffice.Id);
                if (TryUpdateModel(model))
                {
                    _session.Update(model);
                    _session.Flush();

                    return RedirectToAction("ManageOffices", "Office");
                }
            }

            return View("Edit", modifiedOffice);
        }

        /// <summary>
        /// Delete office 
        /// </summary>
        /// <param name="id">Office id to delete</param>
        [HttpGet]
        public ActionResult Delete(int id)
        {
            var office = AccountOffice.GetById(_session, id);
            if (office.Account.Id == _auth.GetAccountKey())
            {
                _session.Delete(office);
                _session.Flush();
            }

            return RedirectToAction("ManageOffices", "Office");
        }
    }
}
