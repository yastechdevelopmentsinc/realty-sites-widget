﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Widgets.Infrastructure
{
    public class UrlAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            //may want more here for https, etc
            var regex = new Regex(@"((file|gopher|news|nntp|telnet|http|ftp|https|ftps|sftp)://)+(([a-zA-Z0-9\._-]+\.[a-zA-Z]{2,15})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(/[a-zA-Z0-9\&amp;%_\./-~-]*)?", RegexOptions.Singleline | RegexOptions.IgnoreCase);

            if (value == null) return false;

            if (!regex.IsMatch(value.ToString())) return false;

            return true;
        }
    }
}