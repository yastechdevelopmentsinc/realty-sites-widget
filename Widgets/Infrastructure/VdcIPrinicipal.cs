﻿using System;
using System.Security.Principal;

namespace Widgets.Infrastructure
{
    public class VdcIPrinicipal : IPrincipal
    {
        public VdcIPrinicipal(VdcIdentity identity)
        {
            Identity = identity;
            ApiKey = identity.ApiKey;
            AccountType = identity.AccountType;
        }
        
        public bool IsInRole(string role)
        {
            return string.Equals(Identity.AuthenticationType, role);
        }

        public IIdentity Identity { get; private set; }
        public Guid ApiKey { get; private set; }
        public string AccountType { get; private set; }
    }
}