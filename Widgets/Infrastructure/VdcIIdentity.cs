﻿using System;
using System.Security.Principal;

namespace Widgets.Infrastructure
{
    public class VdcIdentity : IIdentity
    {
        public VdcIdentity(string name, string userData)
        {
            Name = name;
            AuthenticationType = userData.Split('|')[0];
            ApiKey = new Guid(userData.Split('|')[1]);
            AccountType = userData.Split('|')[2];
        }

        public string Name { get; private set; }
        public string AuthenticationType { get; private set; }
        public Guid ApiKey { get; private set; }
        public string AccountType { get; private set; }

        public bool IsAuthenticated
        {
            get { return !string.IsNullOrEmpty(Name); }
        }
    }
}