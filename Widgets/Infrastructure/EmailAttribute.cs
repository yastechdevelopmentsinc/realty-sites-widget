﻿using System.ComponentModel.DataAnnotations;

namespace Widgets.Infrastructure
{
    public class EmailAttribute : RegularExpressionAttribute
    {
        private const string EmailReg = @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$";

        public EmailAttribute() : base(EmailReg)
        {
        }
    }
}