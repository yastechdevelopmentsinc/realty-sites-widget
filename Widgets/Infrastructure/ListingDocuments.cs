﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Widgets.Infrastructure
{
    public class ListingDocuments
    {
        public ListingDocuments(int? id)
        {
            ListingId = id;
        }

        public int? ListingId { get; set; }

        public IEnumerable<StaticMedia> Documents
        {
            get
            {
                var listingDocuments = new List<StaticMedia>();
                var path = string.Format("{0}\\{1}\\{2}\\", Properties.Settings.Default.AbsoluteMediaPath, "ExclusiveDocuments", ListingId);

                try
                {
                    var documents = Directory.GetFiles(path).ToList();
                    listingDocuments.AddRange(from t in documents
                                              let filename = Path.GetFileName(t)
                                              select new StaticMedia
                                              {
                                                  AbsoluteFilePath = Path.GetFullPath(t),
                                                  RelativeFilePath = string.Format("{0}/{1}/{2}/{3}", Properties.Settings.Default.MediaHostName, "ExclusiveDocuments", ListingId, filename),
                                                  FileName = filename,
                                                  Extension = Path.GetExtension(t)
                                              });
                }
                catch
                {
                    // no listing documents
                }

                return listingDocuments;
            }
        } 

    }
}