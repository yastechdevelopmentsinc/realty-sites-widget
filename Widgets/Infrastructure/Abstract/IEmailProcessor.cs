﻿using Widgets.Models.ViewModels;

namespace Widgets.Infrastructure.Abstract
{
    public interface IEmailProcessor
    {
        void SendEmail(Email email);
        void SendEmail(Email email, Listing listing, string sharethis);
        void SendListingEmail(EmailListing email, Listing listing, string sharethis);
        void SendForgotPasswordEmail(EmailPassword email);
    }
}
