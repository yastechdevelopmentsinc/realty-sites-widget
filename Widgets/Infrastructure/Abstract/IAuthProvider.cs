﻿using System;
using System.Web.Security;

namespace Widgets.Infrastructure.Abstract
{
    public interface IAuthProvider
    {
        void SetAuthCookie(string userName, string role, Guid apiKey, string accountType);
        FormsAuthenticationTicket GetAuthTicket();
        string GetAuthTicketName();
        string GetUserRole();
        Guid GetAccountKey();
        string GetAccountType();
        void SignOut();
    }
}
