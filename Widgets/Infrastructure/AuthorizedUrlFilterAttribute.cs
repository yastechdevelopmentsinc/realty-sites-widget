﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Linq;
using Ninject;
using Widgets.Domain.Entities;

namespace Widgets.Infrastructure
{
    public class AuthorizedUrl : ActionFilterAttribute
    {
        [Inject]
        public ISession Session { get; set; } 

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException("filterContext");

            var accountkey = new Guid(filterContext.HttpContext.Request.QueryString["accountkey"]);

            try
            {
                var host = HttpContext.Current.Request.UrlReferrer.DnsSafeHost;
                var accountUrls = Session.Query<AccountUrl>()
                    .Cacheable()
                    .CacheRegion("LongTerm")
                    .Where(x=>x.Account.Id == accountkey).ToList();

                var result = accountUrls.Find(url => url.Url == host);
                if (result == null)
                {
                    filterContext.HttpContext.Items["errormessage"] = "Url Not Authorized";
                }
            }
            catch (NullReferenceException)
            {
                filterContext.HttpContext.Items["errormessage"] = "Invalid Url Referrer";
            }

        }
    }
}