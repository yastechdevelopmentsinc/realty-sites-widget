﻿using System;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Widgets.Infrastructure
{
    public class JsonpResult : JsonResult
    {
        public string Callback { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            var response = context.HttpContext.Response;
            response.ContentType = !string.IsNullOrEmpty(ContentType) ? ContentType : "application/json";

            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;

            if (Data == null) return;
            var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue, RecursionLimit = 100 };
            var ser = serializer.Serialize(Data);
            response.Write(Callback + "(" + ser + ");");
        }
    }
}