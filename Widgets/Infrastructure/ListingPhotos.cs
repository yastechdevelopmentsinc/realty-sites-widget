﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NHibernate;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Helpers;
using NHibernate.Linq;

namespace Widgets.Infrastructure
{
    public class ListingPhotos
    {
        private readonly IListingService _listingService;
        private readonly ISession _session;

        public ListingPhotos(int? id, string mediaFolder,string boardcode)
        {
            ListingId = id;
            MediaFolder = mediaFolder;
            BoardCode = boardcode;
        }

        public int? ListingId { get; set; }
        public string MediaFolder { get; set; }
        public string BoardCode { get; set; }

        public IEnumerable<StaticMedia> Photos
        {
            get
            {

                var listingPhotos = new List<StaticMedia>();
                var path = string.Format("{0}\\{1}\\{2}\\", Properties.Settings.Default.AbsoluteMediaPath, MediaFolder,
                                         ListingId);

                if (BoardCode == "VIREB")
                {
                     path = string.Format("{0}\\{1}\\V{2}\\", Properties.Settings.Default.AbsoluteMediaPath, MediaFolder,
                                         ListingId);
                }
                else if (BoardCode == "Calgary")
                {
                    path = string.Format("{0}\\{1}\\C{2}\\", Properties.Settings.Default.AbsoluteMediaPath, MediaFolder,
                                         ListingId);
                }

                try
                {
                    if (Directory.Exists(path))
                    {
                        var images = Directory.GetFiles(path).ToList();
                        images.Sort(
                            (a, b) => ControllerHelpers.GetImageIndex(a).CompareTo(ControllerHelpers.GetImageIndex(b)));

                        listingPhotos.AddRange(from t in images
                                               let filename = Path.GetFileName(t)
                                               select new StaticMedia
                                                   {
                                                       AbsoluteFilePath = Path.GetFullPath(t),
                                                       RelativeFilePath = (BoardCode == "VIREB") ? string.Format("{0}/{1}/V{2}/{3}", Properties.Settings.Default.MediaHostName, MediaFolder, ListingId, filename) :(
                                                       BoardCode == "Calgary" ? string.Format("{0}/{1}/C{2}/{3}", Properties.Settings.Default.MediaHostName, MediaFolder, ListingId, filename) :
                                                       string.Format("{0}/{1}/{2}/{3}", Properties.Settings.Default.MediaHostName, MediaFolder, ListingId, filename))
                                                   });
                    }
                    else
                    {
                        listingPhotos.Add(new StaticMedia
                        {
                            RelativeFilePath =
                                string.Format("{0}/Content/images/noimageavailable.jpg",
                                              Properties.Settings.Default.DefaultHostName)
                        });
                    }
                }
                catch
                {
                    listingPhotos.Add(new StaticMedia
                        {
                            RelativeFilePath =
                                string.Format("{0}/Content/images/noimageavailable.jpg",
                                              Properties.Settings.Default.DefaultHostName)
                        });
                }

                return listingPhotos;
            }
        }
    }
}