﻿using System;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using Ninject;
using Widgets.Domain.Entities;
using Widgets.Models;

namespace Widgets.Infrastructure
{
    public class AuthorizedWidget : ActionFilterAttribute
    {
        public string Widget;

        [Inject]
        public ISession Session { get; set; } 

        public AuthorizedWidget(string widget)
        {
            Widget = widget;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException("filterContext");

            var accountkey = new Guid(HttpContext.Current.Request.QueryString["accountkey"]);
            var features = Session.Get<Account>(accountkey).Features;

            switch (Widget)
            {
                case WidgetType.ListingWidget:
                    if (!features.Listing)
                    {
                        filterContext.HttpContext.Items["errormessage"] = "Listing widget is not enabled.";
                    }
                    break;
                case WidgetType.SearchWidget:
                    if (!features.Search)
                    {
                         filterContext.HttpContext.Items["errormessage"] = "Search widget is not enabled.";
                    }
                    break;
                case WidgetType.FeaturedListingWidget:
                    if (!features.FeaturedListing)
                    {
                         filterContext.HttpContext.Items["errormessage"] = "Featured Listing widget is not enabled.";
                    }
                    break;
                case WidgetType.MapSearchWidget:
                    if (!features.Map)
                    {
                        filterContext.HttpContext.Items["errormessage"] = "Map widget is not enabled.";
                    }
                    break;
                case WidgetType.MembersWidget:
                    if (!features.Members)
                    {
                        filterContext.HttpContext.Items["errormessage"] = "Members widget is not enabled.";
                    }
                    break;
                case WidgetType.ContactsWidget:
                    if (!features.Contacts)
                    {
                        filterContext.HttpContext.Items["errormessage"] = "Contacts widget is not enabled.";
                    }
                    break;
                case WidgetType.NewsWidget:
                    if (!features.News)
                    {
                        filterContext.HttpContext.Items["errormessage"] = "News widget is not enabled.";
                    }
                    break;                
                case WidgetType.MortgageCalculatorWidget:
                    if (!features.MortgageCalculator)
                    {
                        filterContext.HttpContext.Items["errormessage"] = "Mortgage Calculator widget is not enabled.";
                    }
                    break;                
                case WidgetType.FeaturedAgentWidget:
                    if (!features.FeaturedAgent)
                    {
                        filterContext.HttpContext.Items["errormessage"] = "Featured Agent widget is not enabled.";
                    }
                    break;
                case WidgetType.LatestListingWidget:
                    if (!features.LatestListing)
                    {
                        filterContext.HttpContext.Items["errormessage"] = "Latest Listing widget is not enabled.";
                    }
                    break;
                case WidgetType.AgentProfileWidget:
                    if (!features.AgentProfile)
                    {
                        filterContext.HttpContext.Items["errormessage"] = "Agent Profile widget is not enabled.";
                    }
                    break;
            }
        }
    }
}