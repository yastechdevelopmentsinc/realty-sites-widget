﻿using AutoMapper;
using Widgets.Domain.Entities;
using Widgets.Models.ViewModels;

namespace Widgets.Infrastructure
{
    public static class AutoMapperBootStrapper
    {
        public static void Bootstrap()
        {
            Mapper.CreateMap<Residential, Listing>()
                .ForMember(x => x.BusName, opt => opt.Ignore())
                .ForMember(x => x.ExclusiveID, opt => opt.Ignore())
                .ForMember(x => x.MajorType, opt => opt.Ignore())
                .ForMember(x => x.OfficeArea, opt => opt.Ignore())
                .ForMember(x => x.RetailArea, opt => opt.Ignore())
                .ForMember(x => x.Topography, opt => opt.Ignore())
                .ForMember(x => x.TotalArea, opt => opt.Ignore())
                .ForMember(x => x.ListingPhotos, opt => opt.Ignore());

            Mapper.CreateMap<Commercial, Listing>()
                .ForMember(x => x.ExclusiveID, opt => opt.Ignore())
                .ForMember(x => x.SqFootage, opt => opt.Ignore())
                .ForMember(x => x.TypeDwelling, opt => opt.Ignore())
                .ForMember(x => x.Exterior, opt => opt.Ignore())
                .ForMember(x => x.Roof, opt => opt.Ignore())
                .ForMember(x => x.Basement, opt => opt.Ignore())
                .ForMember(x => x.Garages, opt => opt.Ignore())
                .ForMember(x => x.Frontage, opt => opt.Ignore())
                .ForMember(x => x.DepthInformation, opt => opt.Ignore())
                .ForMember(x => x.Parking, opt => opt.Ignore())
                .ForMember(x => x.Style, opt => opt.Ignore())
                .ForMember(x => x.Topography, opt => opt.Ignore())
                .ForMember(x => x.TotalArea, opt => opt.Ignore())
                .ForMember(x => x.ListingPhotos, opt => opt.Ignore());

            Mapper.CreateMap<FarmsAndLand, Listing>()
                .ForMember(x => x.ExclusiveID, opt => opt.Ignore())
                .ForMember(x => x.TypeDwelling, opt => opt.Ignore())
                .ForMember(x => x.Frontage, opt => opt.Ignore())
                .ForMember(x => x.DepthInformation, opt => opt.Ignore())
                .ForMember(x => x.Parking, opt => opt.Ignore())
                .ForMember(x => x.BusName, opt => opt.Ignore())
                .ForMember(x => x.OfficeArea, opt => opt.Ignore())
                .ForMember(x => x.RetailArea, opt => opt.Ignore())
                .ForMember(x => x.ListingPhotos, opt => opt.Ignore());

            Mapper.CreateMap<ForLease, Listing>()
                .ForMember(x => x.ExclusiveID, opt => opt.Ignore())
                .ForMember(x => x.SqFootage, opt => opt.Ignore())
                .ForMember(x => x.TypeDwelling, opt => opt.Ignore())
                .ForMember(x => x.Exterior, opt => opt.Ignore())
                .ForMember(x => x.Roof, opt => opt.Ignore())
                .ForMember(x => x.Basement, opt => opt.Ignore())
                .ForMember(x => x.Garages, opt => opt.Ignore())
                .ForMember(x => x.Frontage, opt => opt.Ignore())
                .ForMember(x => x.DepthInformation, opt => opt.Ignore())
                .ForMember(x => x.Parking, opt => opt.Ignore())
                .ForMember(x => x.Style, opt => opt.Ignore())
                .ForMember(x => x.Topography, opt => opt.Ignore())
                .ForMember(x => x.TotalArea, opt => opt.Ignore())
                .ForMember(x => x.ListingPhotos, opt => opt.Ignore());

            Mapper.CreateMap<MultiFam, Listing>()
                .ForMember(x => x.ExclusiveID, opt => opt.Ignore())
                .ForMember(x => x.SqFootage, opt => opt.Ignore())
                .ForMember(x => x.TypeDwelling, opt => opt.Ignore())
                .ForMember(x => x.Basement, opt => opt.Ignore())
                .ForMember(x => x.Garages, opt => opt.Ignore())
                .ForMember(x => x.OfficeArea, opt => opt.Ignore())
                .ForMember(x => x.RetailArea, opt => opt.Ignore())
                .ForMember(x => x.Topography, opt => opt.Ignore())
                .ForMember(x => x.TotalArea, opt => opt.Ignore())
                .ForMember(x => x.VirtualTour, opt => opt.Ignore())
                .ForMember(x => x.Parking, opt => opt.Ignore())
                .ForMember(x => x.BusName, opt => opt.Ignore())
                .ForMember(x => x.ListingPhotos, opt => opt.Ignore())
                .ForMember(x => x.DateEntered, opt => opt.Ignore());

            Mapper.CreateMap<Exclusive, Listing>()
                .ForMember(x => x.BusName, opt => opt.Ignore())
                .ForMember(x => x.MajorType, opt => opt.Ignore())
                .ForMember(x => x.OfficeArea, opt => opt.Ignore())
                .ForMember(x => x.RetailArea, opt => opt.Ignore())
                .ForMember(x => x.Taxes, opt => opt.Ignore())
                .ForMember(x => x.Topography, opt => opt.Ignore())
                .ForMember(x => x.TotalArea, opt => opt.Ignore())
                .ForMember(x => x.ListingDocuments, opt => opt.Ignore())
                .ForMember(x => x.ListingPhotos, opt => opt.Ignore())
                .ForMember(x => x.PostalCode, opt => opt.Ignore());

            Mapper.CreateMap<Listing, MapSearchListing>();

            Mapper.CreateMap<Lead, LeadInfo>();
            Mapper.CreateMap<LeadInfo, Lead>();
            Mapper.CreateMap<Referral, ReferralInfo>();
            Mapper.CreateMap<ReferralInfo, Referral>();

            Mapper.CreateMap<Agent, AgentInfo>()
                .ForMember(x => x.Roles, opt => opt.Ignore());

            Mapper.CreateMap<AgentInfo, Agent>()
                  .ForMember(x => x.Photo, opt => opt.Ignore())
                  .ForMember(x => x.Contact, opt => opt.Ignore());

            Mapper.CreateMap<Agent, AdminInfo>();
            Mapper.CreateMap<AdminInfo, Agent>()
                .ForMember(x => x.Photo, opt => opt.Ignore())
                .ForMember(x => x.AgentIndCode, opt => opt.Ignore())
                .ForMember(x => x.Logo, opt => opt.Ignore())
                .ForMember(x => x.Company, opt => opt.Ignore())
                .ForMember(x => x.Phone, opt => opt.Ignore())
                .ForMember(x => x.Cell, opt => opt.Ignore())
                .ForMember(x => x.Fax, opt => opt.Ignore())
                .ForMember(x => x.Website, opt => opt.Ignore())
                .ForMember(x => x.Active, opt => opt.Ignore())
                .ForMember(x => x.UserRank, opt => opt.Ignore())
                .ForMember(x => x.Notes, opt => opt.Ignore())
                .ForMember(x => x.AgentTitle, opt => opt.Ignore())
                .ForMember(x => x.Staff, opt => opt.Ignore())
                .ForMember(x => x.PhotoMimeType, opt => opt.Ignore())
                .ForMember(x => x.ContactsPage, opt => opt.Ignore())
                .ForMember(x => x.ReferralSystem, opt => opt.Ignore())
                .ForMember(x => x.FeaturedAgent, opt => opt.Ignore())
                .ForMember(x => x.Leads, opt => opt.Ignore())
                .ForMember(x => x.FacebookUrl, opt => opt.Ignore())
                .ForMember(x => x.LinkedInUrl, opt => opt.Ignore())
                .ForMember(x => x.YouTubeUrl, opt => opt.Ignore())
                .ForMember(x => x.Account, opt => opt.Ignore())
                .ForMember(x => x.RealtorPage, opt => opt.Ignore())
                .ForMember(x => x.Referral, opt => opt.Ignore())
                .ForMember(x => x.TwitterUrl, opt => opt.Ignore())
                .ForMember(x => x.Office, opt => opt.Ignore())
                .ForMember(x => x.AgentProfile, opt => opt.Ignore())
                .ForMember(x => x.Contact, opt => opt.Ignore());

            Mapper.CreateMap<Account, AccountViewModel>()
                .ForMember(x => x.AccountTypes, opt => opt.Ignore());

            Mapper.CreateMap<AccountViewModel, Account>()
                .ForMember(x => x.Features, opt => opt.Ignore())
                .ForMember(x => x.Access, opt => opt.Ignore()); 

            Mapper.CreateMap<AccountUrlViewModel, AccountUrl>()
                .ForMember(x => x.Account, opt => opt.Ignore());

            Mapper.CreateMap<AccountUrl, AccountUrlViewModel>();
            Mapper.CreateMap<AccountFeatures, AccountFeaturesViewModel>();
            Mapper.CreateMap<AccountFeaturesViewModel, AccountFeatures>();

            Mapper.CreateMap<AccountListingAccess, ListingAccessViewModel>();
            Mapper.CreateMap<ListingAccessViewModel, AccountListingAccess>()
                .ForMember(x => x.Account, opt => opt.Ignore());

            Mapper.CreateMap<AccountOffice, AccountOfficeInfo>();
            Mapper.CreateMap<AccountOfficeInfo, AccountOffice>();            
            
            Mapper.CreateMap<AccountLink, AccountLinkViewModel>();
            Mapper.CreateMap<AccountLinkViewModel, AccountLink>();

            Mapper.CreateMap<BulletinBoardViewModel, BulletinBoard>()
                .ForMember(x => x.Photos, opt => opt.Ignore());
            Mapper.CreateMap<BulletinBoard, BulletinBoardViewModel>();

            Mapper.CreateMap<NewsItemViewModel, BulletinBoard>();
            Mapper.CreateMap<BulletinBoard, NewsItemViewModel>()
                .ForMember(x=>x.DefaultHostName, opt=>opt.Ignore())
                .ForMember(x=>x.ShareThisUrl, opt=>opt.Ignore());

            Mapper.CreateMap<ExclusiveListing, Exclusive>()
                .ForMember(x=>x.AgentBoardProv, opt=>opt.Ignore())
                .ForMember(x=>x.CoAgentIndCode, opt=>opt.Ignore())
                .ForMember(x=>x.ReplacementCoordinates, opt=>opt.Ignore());
            Mapper.CreateMap<Exclusive, ExclusiveListing>()
                  .ForMember(x => x.ForSale, opt => opt.Ignore())
                  .ForMember(x => x.ForLease, opt => opt.Ignore());

            Mapper.CreateMap<OpenHouseViewModel, OpenHouse>();
            Mapper.CreateMap<OpenHouse, OpenHouseViewModel>()
                .ForMember(x => x.FromTimes, opt => opt.Ignore())
                .ForMember(x => x.ToTimes, opt => opt.Ignore());

            Mapper.CreateMap<Contact, ContactsViewModel>();
            Mapper.CreateMap<ContactsViewModel, Contact>();            
            
            Mapper.CreateMap<Disclaimer, DisclaimerViewModel>();
            Mapper.CreateMap<DisclaimerViewModel, Disclaimer>();

            Mapper.CreateMap<ReplacementCoord, ReplacementCoordinates>();
            Mapper.CreateMap<ReplacementCoordinates, ReplacementCoord>();

            Mapper.CreateMap<AccountFile, AccountFileViewModel>();
            Mapper.CreateMap<AccountFileViewModel, AccountFile>();

            Mapper.CreateMap<ListingFiles, ListingFilesViewModel>();
            Mapper.CreateMap<ListingFilesViewModel, ListingFiles>();

            Mapper.CreateMap<CalculatorDefaults, ManageSettingsViewModel>();
            Mapper.CreateMap<ManageSettingsViewModel, CalculatorDefaults>();

            //Vancouver

            Mapper.CreateMap<VResidential, Listing>()
                .ForMember(x => x.BusName, opt => opt.Ignore())
                .ForMember(x => x.ExclusiveID, opt => opt.Ignore())
                .ForMember(x => x.MajorType, opt => opt.Ignore())
                .ForMember(x => x.OfficeArea, opt => opt.Ignore())
                .ForMember(x => x.RetailArea, opt => opt.Ignore())
                .ForMember(x => x.Topography, opt => opt.Ignore())
                .ForMember(x => x.TotalArea, opt => opt.Ignore())
                .ForMember(x => x.ListingPhotos, opt => opt.Ignore())
            .ForMember(x => x.AgentBrokerCode, opt => opt.Ignore()); //.ForMember(x=>x.PropertyType, opt => opt.UseValue("Residential"))

            Mapper.CreateMap<VCommercial, Listing>()
                .ForMember(x => x.ExclusiveID, opt => opt.Ignore())
                .ForMember(x => x.TypeDwelling, opt => opt.Ignore())
                .ForMember(x => x.Exterior, opt => opt.Ignore())
                .ForMember(x => x.Roof, opt => opt.Ignore())
                .ForMember(x => x.Basement, opt => opt.Ignore())
                .ForMember(x => x.Garages, opt => opt.Ignore())
                .ForMember(x => x.Frontage, opt => opt.Ignore())
                .ForMember(x => x.DepthInformation, opt => opt.Ignore())
                .ForMember(x => x.Parking, opt => opt.Ignore())
                .ForMember(x => x.Style, opt => opt.Ignore())
                .ForMember(x => x.Topography, opt => opt.Ignore())
                .ForMember(x => x.TotalArea, opt => opt.Ignore())
                .ForMember(x => x.ListingPhotos, opt => opt.Ignore())
                .ForMember(x => x.PropertyType, opt => opt.UseValue("Commercial"))
                .ForMember(x => x.AgentBrokerCode, opt => opt.Ignore());

            Mapper.CreateMap<VAgent, AgentInfo>()
                .ForMember(x => x.Roles, opt => opt.Ignore());

            Mapper.CreateMap<VOffice, AccountOfficeInfo>();

            Mapper.CreateMap<VDistrict, Area>().ForMember(x => x.AreaCode, opt => opt.Ignore()).ForMember(x => x.Locations, opt => opt.Ignore());

            Mapper.CreateMap<VMapArea, Location>().ForMember(x => x.AreaCode, opt => opt.Ignore()).ForMember(x => x.SubAreaName, opt => opt.Ignore())
                .ForMember(x => x.Listings, opt => opt.Ignore())
                .ForMember(x => x.CityName, opt => opt.Ignore())
                .ForMember(x => x.DistrictCode, opt => opt.Ignore());

            //Calgary
            Mapper.CreateMap<CListing, Listing>()
                .ForMember(x => x.BusName, opt => opt.Ignore())
                .ForMember(x => x.ExclusiveID, opt => opt.Ignore())
                .ForMember(x => x.MajorType, opt => opt.Ignore())
                .ForMember(x => x.OfficeArea, opt => opt.Ignore())
                .ForMember(x => x.RetailArea, opt => opt.Ignore())
                .ForMember(x => x.Topography, opt => opt.Ignore())
                .ForMember(x => x.TotalArea, opt => opt.Ignore())
                .ForMember(x => x.ListingPhotos, opt => opt.Ignore())
                .ForMember(x => x.AgentBrokerCode, opt => opt.Ignore());

            Mapper.CreateMap<CAgent, AgentInfo>()
                .ForMember(x => x.Roles, opt => opt.Ignore());

            Mapper.CreateMap<COffice, AccountOfficeInfo>();

            Mapper.CreateMap<CRegion, Area>().ForMember(x => x.AreaCode, opt => opt.Ignore()).ForMember(x => x.Locations, opt => opt.Ignore());

            Mapper.CreateMap<CCommunity, Location>().ForMember(x => x.AreaCode, opt => opt.Ignore()).ForMember(x => x.SubAreaName, opt => opt.Ignore())
                .ForMember(x => x.Listings, opt => opt.Ignore())
                .ForMember(x => x.CityName, opt => opt.Ignore())
                .ForMember(x => x.DistrictCode, opt => opt.Ignore());

        }
    }
}