﻿using System;
using System.Web.Mvc;

namespace Widgets.Infrastructure
{
    public class JsonpFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException("filterContext");

            var callback = filterContext.HttpContext.Request.QueryString["callback"];
            if (string.IsNullOrEmpty(callback)) return;

            if (filterContext.Exception == null)
            {
                var result = filterContext.Result as JsonResult;
                if (result == null)
                {
                    throw new InvalidOperationException("JsonpFilterAttribute must be applied only " +
                                                        "on controllers and actions that return a JsonResult object.");
                }

                filterContext.Result = new JsonpResult
                {
                    ContentEncoding = result.ContentEncoding,
                    ContentType = result.ContentType,
                    Data = result.Data,
                    Callback = callback
                };
            } 
            else
            {
                var result = filterContext.Exception;
                filterContext.Result = new JsonpResult
                {
                    Data = result.Data,
                    Callback = callback
                };
            }

        }
    }
}