﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using Widgets.Domain.Entities;
using Widgets.Infrastructure.Abstract;
using Widgets.Models.ViewModels;
using Widgets.Properties;

namespace Widgets.Infrastructure.Concrete
{
    public class EmailProcessor : IEmailProcessor
    {
        public EmailProcessor()
        {
            EmailSettings.Host = Settings.Default.SmtpHost;
            EmailSettings.Port = Settings.Default.SmtpPort;
            EmailSettings.Ssl = Settings.Default.SmtpSsl;
            EmailSettings.UserName = Settings.Default.SmtpUserName;
            EmailSettings.Password = Settings.Default.SmtpPassword;
            EmailSettings.WriteToFile = Settings.Default.WriteToFile;
            EmailSettings.WriteToFilePath = Settings.Default.WriteToFilePath;
            EmailSettings.SendFromAddress = Settings.Default.SendFromAddress;

        }

        public void SendEmail(Email email, Listing listing, string sharethis)
        {
            using (var smtpClient = new SmtpClient())
            {
                EmailSettings.ConfigureSmtpClient(smtpClient);
                var body = new StringBuilder();
                body.AppendLine("From: " + email.FirstName + " " + email.LastName);
                body.AppendLine("Email: " + email.EmailAddress);
                body.AppendLine("Phone: " + email.Phone);
                body.AppendLine("Listing ID#: " + listing.ListingID);
                body.AppendLine("Price: " + listing.Price);
                body.AppendLine("Address: " + listing.Address);
                body.AppendLine("View Listing: " + sharethis);
                body.AppendLine("Comments: " + email.Comments);
                body.AppendLine(string.Format("Date Sent: {0}", DateTime.Now.ToString("MMMM dd, yyyy hh:mm tt")));

                Send(email.ToAddress, email.Subject, body.ToString(), smtpClient, email.EmailAddress);
            }
        }


        public void SendEmail(Email email)
        {
            using(var smtpClient = new SmtpClient())
            {
                EmailSettings.ConfigureSmtpClient(smtpClient);
                var body = new StringBuilder();
                body.AppendLine("From: " + email.FirstName + " " + email.LastName);
                body.AppendLine("Email: " + email.EmailAddress);
                body.AppendLine("Phone: " + email.Phone);
                body.AppendLine("Comments: " + email.Comments);

                Send(email.ToAddress, email.Subject, body.ToString(), smtpClient, email.EmailAddress);
            }
        }

        public void SendListingEmail(EmailListing email, Listing listing, string sharethis)
        {
            using (var smtpClient = new SmtpClient())
            {
                EmailSettings.ConfigureSmtpClient(smtpClient);
                var body = new StringBuilder();
                body.AppendLine("From: " + email.FromFirstName + " " + email.FromLastName);
                body.AppendLine("Email: " + email.FromEmail);
                body.AppendLine("Listing ID#: " + listing.ListingID);
                body.AppendLine("Price: " + listing.Price);
                body.AppendLine("Address: " + listing.Address);
                body.AppendLine("View Listing: " + sharethis.Replace(" ","%20"));
                body.AppendLine("Comments: " + email.Comments);
                body.AppendLine(string.Format("Date Sent: {0}", DateTime.Now.ToString("MMMM dd, yyyy hh:mm tt")));

                var subject = string.Format("{0} {1} has emailed you a listing.",
                                            email.FromFirstName, email.FromLastName);

                Send(email.ToEmail, subject, body.ToString(), smtpClient, email.FromEmail);
            }
        }

        public void SendForgotPasswordEmail(EmailPassword email)
        {
            using (var smtpClient = new SmtpClient())
            {
                EmailSettings.ConfigureSmtpClient(smtpClient);
                var body = new StringBuilder();
                body.AppendLine("This is an automated password recovery message sent from Virtual Data Corp");
                body.AppendLine("Your User Name: " + email.UserName);
                body.AppendLine("Your Password: " + email.Password);
                Send(email.Email, "Realty Sites Forgot Password", body.ToString(), smtpClient);
            }
        }


        private static void Send(string toAddress, string subject, string body, SmtpClient smtpClient, string replyToAddress = null)
        {
            var message = new MailMessage(EmailSettings.SendFromAddress, toAddress, subject, body);
            if (!string.IsNullOrEmpty(replyToAddress))
            {
                message.ReplyToList.Add(replyToAddress); 
            }
            if (EmailSettings.WriteToFile)
            {
                message.BodyEncoding = Encoding.ASCII;
            }
            smtpClient.Send(message);
        }

        public class EmailSettings
        {
            public static string Host;
            public static bool Ssl;
            public static bool UseDefaultCredentials;
            public static int Port;
            public static string UserName;
            public static string Password;
            public static bool WriteToFile;
            public static string WriteToFilePath;
            public static string SendFromAddress;

            public static void ConfigureSmtpClient(SmtpClient smtpClient)
            {
                smtpClient.EnableSsl = Ssl;
                smtpClient.Host = Host;
                smtpClient.Port = Port;
                smtpClient.UseDefaultCredentials = UseDefaultCredentials;
                smtpClient.Credentials = new NetworkCredential(UserName, Password);

                if (WriteToFile)
                {
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                    smtpClient.PickupDirectoryLocation = WriteToFilePath;
                    smtpClient.EnableSsl = false;
                }
            }
        }

    }
}