﻿using System;
using System.Web;
using System.Web.Security;
using Widgets.Infrastructure.Abstract;

namespace Widgets.Infrastructure.Concrete
{
    public class AuthProvider : IAuthProvider
    {
        /// <summary>
        /// Create the forms authentication ticket
        /// </summary>
        /// <param name="userName">username</param>
        /// <param name="role">roles of authenticated user</param>
        /// <param name="apiKey">api key</param>
        /// <param name="accountType">account type</param>
        public void SetAuthCookie(string userName, string role, Guid apiKey, string accountType)
        {
            var ticket = new FormsAuthenticationTicket
                (
                    2,
                    userName,
                    DateTime.Now,
                    DateTime.Now.AddHours(2),
                    true,
                    role + "|" + apiKey + "|" + accountType,
                    FormsAuthentication.FormsCookiePath
                );

            var encryptedTicket = FormsAuthentication.Encrypt(ticket);
            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket)
                {
                    Expires = ticket.Expiration,
                    Path = FormsAuthentication.FormsCookiePath,
                    HttpOnly = true
                };

            // if cookie already exists update it, otherwise add it
            HttpContext.Current.Response.Cookies.Add(authCookie);

        }

        /// <summary>
        /// Get the forms authentication ticket
        /// </summary>
        public FormsAuthenticationTicket GetAuthTicket()
        {
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

            return authTicket;
        }

        /// <summary>
        /// Get the user rolestored within the userdata of the authentication ticket
        /// </summary>
        public string GetUserRole()
        {
            FormsAuthenticationTicket ticket = GetAuthTicket();
            var role = ticket.UserData.Split('|')[0];

            return role;
        }

        /// <summary>
        /// Get the ticket name
        /// </summary>
        public string GetAuthTicketName()
        {
            FormsAuthenticationTicket ticket = GetAuthTicket();
            return ticket.Name;
        }

        /// <summary>
        /// Get the account api key stored within the userdata of the authentication ticket
        /// </summary>
        public Guid GetAccountKey()
        {
            FormsAuthenticationTicket ticket = GetAuthTicket();
            var accountKey = ticket.UserData.Split('|')[1];

            return new Guid(accountKey);
        }

        /// <summary>
        /// Get the account type stored within the userdata of the authentication ticket
        /// </summary>
        public string GetAccountType()
        {
            FormsAuthenticationTicket ticket = GetAuthTicket();
            var accountType = ticket.UserData.Split('|')[2];

            return accountType;
        }

        /// <summary>
        /// Sign the user out / destroy the auth ticket
        /// </summary>
        public void SignOut()
        {
           FormsAuthentication.SignOut();
        }
    }
}