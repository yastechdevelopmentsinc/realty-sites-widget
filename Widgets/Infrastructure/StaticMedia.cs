namespace Widgets.Infrastructure
{
    public class StaticMedia
    {
        public string AbsoluteFilePath { get; set; }
        public string RelativeFilePath { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
    }
}