﻿using System.Web.Mvc;
using System.Web.Routing;
using NHibernate;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using Ninject.Web.Mvc.FilterBindingSyntax;
using Widgets.Domain;
using Widgets.Domain.Abstract;
using Widgets.Domain.Concrete;
using Widgets.Infrastructure;
using Widgets.Infrastructure.Abstract;
using Widgets.Infrastructure.Concrete;

namespace Widgets
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : NinjectHttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ElmahHandledErrorLoggerFilter());
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("favicon.ico");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            routes.MapRoute(
                "apiKey", // Route name
                "Account/{action}/{apiKey}", // URL with parameters
                new { controller = "Account", action = "ManageAccounts", apiKey = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "emailRoute", // Route name
                "Email/{action}/{id}/{info}/{accountkey}", // URL with parameters
                new { controller = "Email", action = "Index", id = UrlParameter.Optional, info = UrlParameter.Optional, accountkey = UrlParameter.Optional } // Parameter defaults
            );

            //Default MVC route (fallback)
            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected override IKernel CreateKernel()
        {
            return Container;
        }

        static IKernel _container;
        public static IKernel Container
        {
            get
            {
                if (_container == null)
                {
                    _container = new StandardKernel(new SiteModule());
                }
                return _container;
            }
        }

        internal class SiteModule : NinjectModule
        {
            public override void Load()
            {
                //Set up ninject bindings here.
                Bind<IListingService>().To<ListingService>();

                // infrastrcture 
                Bind<IAuthProvider>().To<AuthProvider>();
                Bind<IEmailProcessor>().To<EmailProcessor>();

                // action filter attributes
                this.BindFilter<AuthorizedUrl>(FilterScope.Action, 0)
                    .WhenActionMethodHas<AuthorizedUrl>();

                this.BindFilter<AuthorizedWidget>(FilterScope.Action, 0)
                    .WhenActionMethodHas<AuthorizedWidget>()
                    .WithConstructorArgumentFromActionAttribute<AuthorizedWidget>("widget", o => o.Widget);

                // nhibernate session
                Bind<ISession>().ToMethod(x => NHibernateHelper.OpenSession()).InRequestScope();
            }
        }

        protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            // setup automapper mappings
            AutoMapperBootStrapper.Bootstrap();
        }
    }
}