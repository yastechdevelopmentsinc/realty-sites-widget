﻿using System.IO;
using System.Web.Mvc;

namespace Widgets.Extensions
{
    public static class ControllerExtensions
    {
        /// <summary>
        /// Extension method to render a view to string
        /// </summary>
        /// <param name="controller">the controller</param>
        /// <param name="viewName">the viewname to render</param>
        /// <param name="model">the model associated to the view</param>
        /// <param name="masterName">the layout page</param>
        public static string RenderViewToString(this Controller controller, string viewName = null, object model = null, string masterName = null)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");
            }

            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindView(controller.ControllerContext, viewName, masterName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }

        /// <summary>
        /// Extension method to render a partial view to string
        /// </summary>
        /// <param name="controller">the controller</param>
        /// <param name="viewName">the viewname to render</param>
        /// <param name="model">the model associated to the view</param>
        public static string RenderPartialViewToString(this Controller controller, string viewName = null, object model = null)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");
            }

            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}