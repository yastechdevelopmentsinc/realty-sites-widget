﻿using System;
using System.Collections.Generic;
using System.Linq;
using Widgets.Domain.Entities;
using Widgets.Models;

namespace Widgets.Extensions
{
    public static class IQueryableExtensions
    {
        /// <summary>
        /// Extension of IQueryable that filters on the mls number passed
        /// </summary>
        /// <typeparam name="T">Base Entity (Common fields)</typeparam>
        /// <param name="query">The query to extend</param>
        /// <param name="mlsNumber">The listing price to query for</param>
        /// <returns></returns>
        public static IQueryable<T> MlsNumber<T>(this IQueryable<T> query, int? mlsNumber) where T : BaseEntity
        {
            var results = mlsNumber.HasValue ? query.Where(o => (o.MlsNumber == mlsNumber.Value || o.MlsNumber.ToString().Contains(mlsNumber.Value.ToString()))) : query;
            return results;
        }

        /// <summary>
        /// Extension of IQueryable that filters on listing price greater than the value passed
        /// </summary>
        /// <typeparam name="T">Base Entity (Common fields)</typeparam>
        /// <param name="query">The query to extend</param>
        /// <param name="priceLow">The listing price to query for</param>
        /// <returns></returns>
        public static IQueryable<T> PriceLow<T>(this IQueryable<T> query, decimal? priceLow) where T : BaseEntity
        {
            var results = priceLow.HasValue ? query.Where(o => (o.ListPrice >= priceLow.Value)) : query;
            return results;
        }

        /// <summary>
        /// Extension of IQueryable that filters on listing price less than the value passed
        /// </summary>
        /// <typeparam name="T">Base Entity (Common fields)</typeparam>
        /// <param name="query">The query to extend</param>
        /// <param name="priceHigh">The listing price to query for</param>
        /// <returns></returns>
        public static IQueryable<T> PriceHigh<T>(this IQueryable<T> query, decimal? priceHigh) where T : BaseEntity
        {
            if (priceHigh <= 0) return query;

            var results = priceHigh.HasValue ? query.Where(o => (o.ListPrice <= priceHigh.Value)) : query;
            return results;
        }

        /// <summary>
        /// Extension of IQueryable that filters on the number of bedrooms
        /// </summary>
        /// <typeparam name="T">Base Entity (Common fields)</typeparam>
        /// <param name="query">The query to extend</param>
        /// <param name="bedrooms">The number of bedrooms to query for</param>
        /// <returns></returns>
        public static IQueryable<T> Bedrooms<T>(this IQueryable<T> query, int? bedrooms) where T : BaseEntity
        {
            var results = (bedrooms.HasValue && bedrooms.Value >0) ? query.Where(o => o.NumbBeds >= bedrooms.Value) : query;
            return results;
        }

        /// <summary>
        /// Extension of IQueryable that filters on the number of bathrooms
        /// </summary>
        /// <typeparam name="T">Base Entity (Common fields)</typeparam>
        /// <param name="query">The query to extend</param>
        /// <param name="bathrooms">The number of bathrooms to query for</param>
        /// <returns></returns>
        public static IQueryable<T> Bathrooms<T>(this IQueryable<T> query, int? bathrooms) where T : BaseEntity
        {
            var results = (bathrooms.HasValue && bathrooms.Value >0) ? query.Where(o => o.Bathrooms >= bathrooms.Value) : query;
            return results;
        }

        /// <summary>
        /// Extension of IQueryable that filters on the city 
        /// </summary>
        /// <typeparam name="T">Base Entity (Common fields)</typeparam>
        /// <param name="query">The query to extend</param>
        /// <param name="city">The city to filter for</param>
        /// <returns></returns>
        public static IQueryable<T> City<T>(this IQueryable<T> query, string city) where T : BaseEntity
        {
            if (String.IsNullOrEmpty(city) || city == "All") return query;
            string[] groupcities = city.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            var results = query.Where(o => groupcities.Contains(o.City));
            return results;
        }

        public static IQueryable<T> VCity<T>(this IQueryable<T> query, string city) where T : BaseEntity
        {
            if (String.IsNullOrEmpty(city) || city == "All") return query;
            string[] groupcities = city.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            var results = query.Where(o => groupcities.Contains(o.City));
            return results;
        }

        public static IQueryable<T> District<T>(this IQueryable<T> query, string district) where T : BaseEntity
        {
            if (String.IsNullOrEmpty(district) || district == "All") return query;

            var results = query.Where(o => o.AreaName == district);
            return results;
        }

        public static IQueryable<T> VDistrict<T>(this IQueryable<T> query, string district) where T : BaseEntity
        {
            if (String.IsNullOrEmpty(district) || district == "All") return query;

            List<string> splitdistrict = district.Split('-').ToList();
            if(splitdistrict.Count >0)
            {
                splitdistrict = splitdistrict[0].Split(' ').ToList();
                if(splitdistrict.Count == 2)
                {
                    string districtshortcut = splitdistrict[1].Length > 1 ? splitdistrict[1] : "Z" + splitdistrict[1];
                    string islandshortcut = splitdistrict[1].Length > 1 ? splitdistrict[1] : "(Zone " + splitdistrict[1]+")";
                    var results = query.Where(o => o.Map_Area != null && (o.Map_Area.AreaName.StartsWith(districtshortcut) || o.Map_Area.AreaName.EndsWith(islandshortcut)));
                    return results;
                }
                else
                {
                    return query;
                }
            }
            else
            {
                return query;
            }
            
        }

        /// <summary>
        /// Extension of IQueryable that filters on the sub area 
        /// </summary>
        /// <typeparam name="T">Base Entity (Common fields)</typeparam>
        /// <param name="query">The query to extend</param>
        /// <param name="area">The sub area to filter for</param>
        /// <returns></returns>
        public static IQueryable<T> Area<T>(this IQueryable<T> query, string area) where T : BaseEntity
        {
            if (String.IsNullOrEmpty(area) || area == "All") return query;

            var results = query.Where(o => o.SubAreaName.EndsWith(area));
            return results;
        }

        public static IQueryable<T> VArea<T>(this IQueryable<T> query, string area) where T : BaseEntity
        {
            if (String.IsNullOrEmpty(area) || area == "All") return query;

            var results = query.Where(o => o.Map_Area != null && o.Map_Area.AreaName.Contains(area));
            return results;
        }

        /// <summary>
        /// Extension of IQueryable that filters by the accounts listing display access 
        /// </summary>
        /// <typeparam name="T">Base Entity (Common fields)</typeparam>
        /// <param name="query">The query to extend</param>
        /// <param name="account">The account to filter by listing display access</param>
        /// <param name="propertyType">The property type to apply the accounts listings access too </param>
        /// <param name="restrictByAccountType">Within the admin area users can only make modifications to the listings they represent</param>
        public static IQueryable<T> ListingAccess<T>(this IQueryable<T> query, Account account, string propertyType, bool restrictByAccountType = false) where T : BaseEntity
        {
            if (account != null && account.Access != null )
            {
                switch (propertyType)
                {
                    case PropertyTypeConstants.Residential:
                        return ListingAccess(query, account, restrictByAccountType, account.Access.Residential, propertyType);
                    case PropertyTypeConstants.Commercial:
                        return ListingAccess(query, account, restrictByAccountType, account.Access.Commercial, propertyType);
                    case PropertyTypeConstants.FarmsAndLand:
                        return ListingAccess(query, account, restrictByAccountType, account.Access.FarmsAndLand, propertyType);
                    case PropertyTypeConstants.ForLease:
                        return ListingAccess(query, account, restrictByAccountType, account.Access.ForLease, propertyType);
                    case PropertyTypeConstants.MultiFamily:
                        return ListingAccess(query, account, restrictByAccountType, account.Access.MultiFam, propertyType);
                }
            }

            return query;
        }

        private static IQueryable<T> ListingAccess<T>(IQueryable<T> query, Account account, bool restrictByAccountType, ListingDisplayAccess access, string propertyType) where T : BaseEntity
        {
            IQueryable<T> result;

            switch (access)
            {
                case ListingDisplayAccess.Brokerage:
                    var brokerCodes = account.BrokerCode.Split('|');
                    result = query.Where(x => brokerCodes.Contains(x.AgentBrokerCode));
                    break;
                case ListingDisplayAccess.Agent:
                    var agentCodes = account.AgentCode.Split('|');

                    if(propertyType == PropertyTypeConstants.Residential || propertyType == PropertyTypeConstants.MultiFamily)
                    {
                        result = query.Where(agent => agentCodes.Contains(agent.CoAgentIndCode) || agentCodes.Contains(agent.AgentIndCode));                    
                    }
                    else
                    {
                        result = query.Where(agent => agentCodes.Contains(agent.AgentIndCode));                    
                    }

                    break;
                case ListingDisplayAccess.IDX:
                    result = query.Where(x => x.AgentBoardCode == account.BoardCode);
                    break;
                default:
                    return Enumerable.Empty<T>().AsQueryable();
            }

            // filter by account type if required
            if (restrictByAccountType)
            {
                result = FilterByAccountType(query, account, result);
            }

            return result;
        }

        /// <summary>
        /// Extension of IQueryable that filters by the listings address
        /// </summary>
        /// <typeparam name="T">Base Entity (Common fields)</typeparam>
        /// <param name="query">The query to extend</param>
        /// <param name="address">The address to filter on</param>
        public static IQueryable<T> Address<T>(this IQueryable<T> query, string address) where T : BaseEntity
        {
            if (String.IsNullOrEmpty(address)) return query;

            var result = query.Where(x => x.Address.Contains(address) || x.MlsNumber.ToString().Contains(address));
            return result;
        }

        public static IQueryable<T> VAddress<T>(this IQueryable<T> query, string address) where T : BaseEntity
        {
            if (String.IsNullOrEmpty(address)) return query;

            var result = query.Where(x => x.Street_Name.Contains(address) || x.Street_Number.ToString().Contains(address) || (x.Street_Number.ToString()+" "+x.Street_Name).ToString().Contains(address) || x.MlsNumber.ToString().Contains(address));
            return result;
        }

        public static IQueryable<T> CustomSearch<T>(this IQueryable<T> query, string searchtext) where T : BaseEntity
        {
            if (String.IsNullOrEmpty(searchtext)) return query;

            int mlsnumber = -1;
            if (int.TryParse(searchtext.Trim().Replace(" ", ""), out mlsnumber))
            {
                var results = query.Where(o => (o.MlsNumber == mlsnumber));
                return results;
            }
            else
            {
                var result = query.Where(x => (x.AreaName.ToLower().Contains(searchtext.Trim().ToLower())) || (x.SubAreaName.ToLower().Contains(searchtext.Trim().ToLower()))
                    || (x.City.ToLower().Contains(searchtext.Trim().ToLower())) || ((searchtext.Trim().ToLower().Contains(x.Address.ToLower())))
                    || ((x.PostalCode.ToLower().Replace(" ", "").Contains(searchtext.Trim().Replace(" ", "").ToLower()))));

                return result;
            }
        }

       /* public static IQueryable<T> VCustomSearch<T>(this IQueryable<T> query, string searchtext) where T : BaseEntity
        {
            if (String.IsNullOrEmpty(address)) return query;

            var result = query.Where(x => x.Street_Name.Contains(address) || x.Street_Number.ToString().Contains(address) || (x.Street_Number.ToString() + " " + x.Street_Name).ToString().Contains(address) || x.MlsNumber.ToString().Contains(address));
            return result;
        }*/

        /// <summary>
        /// Extension of IQueryable that filters by the listings status
        /// </summary>
        /// <typeparam name="T">Base Entity (Common fields)</typeparam>
        /// <param name="query">The query to extend</param>
        /// <param name="status">The status to filter on</param>
        public static IQueryable<T> Status<T>(this IQueryable<T> query, string status) where T : BaseEntity
        {
            if (String.IsNullOrEmpty(status) || status == "All"|| status=="undefined")
            {
                return query;
            }
            else if (status == PropertyStatusConstants.ForSaleOrLease)
            {
                var result = query.Where(x => x.Status.Contains(PropertyStatusConstants.ForSaleOrLease) || x.Status.Contains(PropertyStatusConstants.ForSale) || x.Status.Contains(PropertyStatusConstants.ForLease));
                return result;
            }
            else if (status == PropertyStatusConstants.SoldOrLeased)
            {
                var result = query.Where(x => x.Status.Contains(status) || x.Status.Contains(PropertyStatusConstants.Sold) || x.Status.Contains(PropertyStatusConstants.Leased));
                return result;
            }
            else
            {
                var result = query.Where(x => x.Status.Contains(status));
                return result;
            }
            
        }

        /// <summary>
        /// Extension of IQueryable that filters by the listings agent / co agent
        /// </summary>
        /// <typeparam name="T">Base Entity (Common fields)</typeparam>
        /// <param name="query">The query to extend</param>
        /// <param name="agentid">The agentid to filter on</param>
        /// <param name="propertyType">The property type to apply the agent filtering too</param>
        public static IQueryable<T> Agent<T>(this IQueryable<T> query, string agentid, string propertyType) where T : BaseEntity
        {
            if (String.IsNullOrEmpty(agentid)) return query;

            IQueryable<T> result;
            if (propertyType == PropertyTypeConstants.Residential || propertyType == PropertyTypeConstants.MultiFamily || propertyType == ExclusivePropertyType.Exclusive)
            {
                result = query.Where(listing => listing.CoAgentIndCode.Contains(agentid) || listing.AgentIndCode.Contains(agentid));
            }
            else
            {
                result = query.Where(listing => listing.AgentIndCode.Contains(agentid));
            }  
            return result;
        }

        /// <summary>
        /// Extension of IQueryable that filters by the options passed into the widgets
        /// </summary>
        /// <typeparam name="T">Base Entity (Common fields)</typeparam>
        /// <param name="query">The query to extend</param>
        /// <param name="account">The account to filter upon</param>
        /// <param name="searchCriteria">The options passed into the widget</param>
        /// <param name="propertyType">The property type that is being filtered on </param>
        public static IQueryable<T> Options<T>(this IQueryable<T> query, Account account, SearchCriteria searchCriteria, string propertyType ,List<CBoardID> abboards = null) where T : BaseEntity
        {
            var options = searchCriteria.WidgetOptions;
            if (String.IsNullOrEmpty(options) ||
                String.Compare(options, WidgetOptions.AllListings, StringComparison.InvariantCultureIgnoreCase) == 0) return query;

            // filter on broker code
            if (String.Compare(options, WidgetOptions.BrokerageListing, StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                var brokercodes = account.BrokerCode.Split('|');
                var result = query.Where(listing =>brokercodes.Contains(listing.AgentBrokerCode));
                return result;
            }

           
            // filter on agent code
            if (String.Compare(options, WidgetOptions.AgentListings, StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                var agentcodes = account.AgentCode.Split('|');
                IQueryable<T> result;
                switch (propertyType)
                {
                    case PropertyTypeConstants.MultiFamily:
                    case PropertyTypeConstants.Residential:
                        result = query.Where(listing => agentcodes.Contains(listing.CoAgentIndCode) || agentcodes.Contains(listing.AgentIndCode));
                        break;
                    case ExclusivePropertyType.Exclusive:
                        result = query.Where(listing => agentcodes.Contains(listing.CoAgentIndCode) || agentcodes.Contains(listing.CoAgentIndCode2) || agentcodes.Contains(listing.AgentIndCode));
                        break;
                    default:
                        result = query.Where(listing => agentcodes.Contains(listing.AgentIndCode));
                        break;
                }
                return result;
            }

            // filter on openhouses
            if (String.Compare(options, WidgetOptions.OpenHouseListings, StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                var result = query;

                if (!account.Features.IDXOpenHouses)
                {
                    result = FilterByAccountType(query, account, result);
                }

                // set the date range to a week if it has not been set
                if (searchCriteria.DateFrom == null || searchCriteria.DateTo == null || searchCriteria.DateFrom.Value.Year == 1 || searchCriteria.DateTo.Value.Year == 1)
                {
                    searchCriteria.DateFrom = DateTime.Today;
                    searchCriteria.DateTo = DateTime.Today.AddDays(7);
                }
                if (account.BoardCode == "VIREB")
                {
                    result = result.Where(x => x.VOpenHouses.Any(opho => opho.StartTime >= searchCriteria.DateFrom && opho.StartTime <= searchCriteria.DateTo));
                }
                else if (abboards != null && abboards.Count(x => x.Name == account.BoardCode) > 0)
                {
                    result = result.Where(x => x.COpenHouses.Any(opho => opho.OpenHouseDate >= searchCriteria.DateFrom && opho.OpenHouseDate <= searchCriteria.DateTo));
                }
                else
                {
                    result = result.Where(x => x.OpenHouses.Any(opho => opho.FromDate >= searchCriteria.DateFrom && opho.FromDate <= searchCriteria.DateTo));
                }
                return result;
            }

            // filter on virtualtours
            if(string.Compare(options, WidgetOptions.VirtualTourListings, StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                var result = query;
                result = FilterByAccountType(query, account, result);

                return result.Where(x => x.VirtualTour != string.Empty);
            }

            return query;
        }

        public static IQueryable<T> FilterByAccountType<T>(IQueryable<T> query, Account account, IQueryable<T> result)where T : BaseEntity
        {
            switch (account.AccountType)
            {
                // only display open houses for the accounts agent
                case AccountType.Agent:
                    result = result.Where(x => x.AgentIndCode == account.AgentCode);
                    break;

                // only display open houses for the accounts brokerage
                case AccountType.Broker:
                    var brokercodes = account.BrokerCode.Split('|');
                    result = result.Where(x => brokercodes.Contains(x.AgentBrokerCode));
                    break;

                // only display open houses for the accounts agents
                case AccountType.Team:
                    var agentcodes = account.AgentCode.Split('|');
                    result = query.Where(x => agentcodes.Contains(x.AgentIndCode));
                    break;
            }
            return result;
        }

        public static IQueryable<T> IsActive<T>(this IQueryable<T> query) where T : BaseEntity
        {
            var results = query.Where(o => (o.IsActive == true));
            return results;
        }

        public static IQueryable<T> IsRental<T>(this IQueryable<T> query, SearchCriteria searchCriteria,string propertyType) where T : BaseEntity
        {
            var options = searchCriteria.WidgetOptions;
            if (String.Compare(options, WidgetOptions.Rentals, StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                if (propertyType == ExclusivePropertyType.Exclusive)
                {
                    var result = query.Where(x => x.IsRental == true);
                    return result;
                }
                else
                {
                    return Enumerable.Empty<T>().AsQueryable();
                }
            }
            else
            {
                if (propertyType == ExclusivePropertyType.Exclusive)
                {
                    var result = query.Where(x => x.IsRental == false);
                    return result;
                }
                return query;
            }
        }

        public static IQueryable<T> Sort<T>(this IQueryable<T> query, string orderBy) where T : BaseEntity
        {
            switch (orderBy)
            {
                case "Address":
                    return query.OrderBy(x => x.Street_Number).ThenBy(x=>x.Street_Name);
                case "Lowest Price":
                    return query.OrderBy(x => x.ListPrice);
                case "Highest Price":
                    return query.OrderByDescending(x => x.ListPrice);
                default:
                    return query.OrderByDescending(x => x.MlsNumber);
            }
        }
    }
}