﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Widgets.Extensions
{
    public static class IEnumberableExtensions
    {
        /// <summary>
        /// Extension method to easily render a drop down list within a view page
        /// </summary>
        /// <typeparam name="T">Entity Type</typeparam>
        /// <param name="enumerable">Collection of items</param>
        /// <param name="value">the value of the select list item</param>
        /// <param name="text">the textual display of the selct list item</param>
        /// <param name="defaultOption">the default option that is set</param>
        public static List<SelectListItem> ToSelectList<T>( this IEnumerable<T> enumerable, Func<T, string> value, Func<T, string> text, string defaultOption)
        {
            var items = enumerable.Select(f => new SelectListItem()
                                                  {
                                                      Text = text(f) ,
                                                      Value = value(f)
                                                  }).ToList();

            if (!String.IsNullOrEmpty(defaultOption))
            {
                            items.Insert(0, new SelectListItem()
                                {
                                    Text = defaultOption,
                                    Value = String.Empty
                                });
            }

            return items;
        }

        /// <summary>
        /// Extension that will return a random item from a collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="take">take, the number o felemnts to return</param>
        /// <returns></returns>
        public static IEnumerable<T> GetRandomElements<T>(this IEnumerable<T> list, int take = 1)
        {
            if (list == null)
                throw new ArgumentNullException("list");

            var r = new Random();
            return list.OrderBy(x => r.Next()).Take(take);
        }
    }
}