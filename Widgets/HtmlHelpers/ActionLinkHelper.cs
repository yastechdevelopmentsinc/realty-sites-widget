﻿using System;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Widgets.HtmlHelpers
{
    public static class ActionLinkHelpers
    {
        public static MvcHtmlString SelectedActionLink(this HtmlHelper helper, string linkText, string actionName, string controllerName, Guid? identifier = null)
        {
            var controller = (string) helper.ViewContext.RouteData.Values["controller"];
            if (string.Compare(controller, controllerName, StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                if (identifier == null)
                {
                    return helper.ActionLink(linkText, actionName, controllerName, null, new { Class = "selected" }); 
                }
                return helper.ActionLink(linkText, actionName, controllerName, new { id = identifier }, new { Class = "selected" });
            }

            if(identifier==null)
            {
                return helper.ActionLink(linkText, actionName, controllerName);
            }
            return helper.ActionLink(linkText, actionName, controllerName, new {id = identifier}, null);
        }
    }
}