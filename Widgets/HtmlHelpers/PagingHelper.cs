﻿using System;
using System.Text;
using System.Web.Mvc;
using Widgets.Models;

namespace Widgets.HtmlHelpers
{
    /// <summary>
    /// Helper class to draw a pagination control for the listing widget 
    /// </summary>
    public static class PagingHelpers
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html, PagingInfo pagingInfo)
        {
            const int pageGroupSize = 10;
            var lastGroupNumber = pagingInfo.CurrentPage;
            while ((lastGroupNumber % pageGroupSize != 0)) lastGroupNumber++;
            var groupEnd = Math.Min(lastGroupNumber, pagingInfo.TotalPages);
            var groupStart = lastGroupNumber - (pageGroupSize - 1);

            var result = new StringBuilder();

            var pagePostion = new TagBuilder("span");
            pagePostion.InnerHtml = "Page " + pagingInfo.CurrentPage.ToString() + " of " + pagingInfo.TotalPages.ToString();
            result.Append(pagePostion.ToString());

            var tagFirst = new TagBuilder("a");
            tagFirst.MergeAttribute("href", "#");
            tagFirst.MergeAttribute("data-url", "1");
            tagFirst.MergeAttribute("onclick", "ScrollTop();");
            tagFirst.InnerHtml = "<<";
            tagFirst.AddCssClass("first");
            result.Append(tagFirst.ToString());

            var tagPrevious = new TagBuilder("a");
            if (pagingInfo.CurrentPage > 1)
            {
                tagPrevious.MergeAttribute("href", "#");
                tagPrevious.MergeAttribute("data-url", (pagingInfo.CurrentPage - 1).ToString());
                tagPrevious.MergeAttribute("onclick", "ScrollTop();");
            }
            tagPrevious.InnerHtml = "<";
            tagPrevious.AddCssClass("previous");
            result.Append(tagPrevious.ToString());

            for (int i = groupStart; i <= groupEnd; i++)
            {
                var tag = new TagBuilder("a"); // Construct an <a> tag
                tag.MergeAttribute("href", "#");
                tag.MergeAttribute("data-url", i.ToString());
                tag.MergeAttribute("onclick", "ScrollTop();");
                tag.InnerHtml = i.ToString();

                if (i == pagingInfo.CurrentPage)
                    tag.AddCssClass("selected");
                result.Append(tag.ToString());
            }

            var tagNext = new TagBuilder("a");
            if (pagingInfo.CurrentPage < pagingInfo.TotalPages)
            {
                tagNext.MergeAttribute("href", "#");
                tagNext.MergeAttribute("data-url", (pagingInfo.CurrentPage + 1).ToString());
                tagNext.MergeAttribute("onclick", "ScrollTop();");
            }
            tagNext.InnerHtml = ">";
            tagNext.AddCssClass("next");
            result.Append(tagNext.ToString());

            var tagLast = new TagBuilder("a");
            tagLast.MergeAttribute("href", "#");
            tagLast.MergeAttribute("data-url", pagingInfo.TotalPages.ToString());
            tagLast.MergeAttribute("onclick", "ScrollTop();");
            tagLast.InnerHtml = ">>";
            tagLast.AddCssClass("last");
            result.Append(tagLast.ToString());

            return MvcHtmlString.Create(result.ToString());
        }
    }
}