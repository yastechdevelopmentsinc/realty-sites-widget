﻿using System.Web;
using System.Web.Mvc;

namespace Widgets.HtmlHelpers
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString Nl2Br(this HtmlHelper htmlHelper, string text , int numberofcharindescription)
        {
            if (string.IsNullOrEmpty(text))
            {
                return MvcHtmlString.Create(text);
            }
            text = Truncate_Text(text, numberofcharindescription);

            return MvcHtmlString.Create(HttpUtility.HtmlEncode(text).Replace("\n", "<br />"));                
        }

        private static string Truncate_Text(string text, int numberofcharindescription)
        {
            string temptext = text;
            if (temptext.Length > numberofcharindescription)
            {
                int nextspace = temptext.IndexOf("\n", numberofcharindescription-1);
                if (nextspace > 0)
                {
                    temptext = temptext.Substring(0, nextspace).TrimEnd() + "... \n";
                    return temptext;
                }
                else
                {
                    nextspace = temptext.IndexOf(" ", numberofcharindescription - 1);
                    if (nextspace > 0)
                    {
                        temptext = temptext.Substring(0, nextspace).TrimEnd() + "... \n";
                        return temptext;
                    }
                    else
                    {
                        return text + "\n";
                    }
                }
            }
            else
            {
                return text +"\n";
            }
        }

        public static MvcHtmlString YesNoBool(this HtmlHelper htmlHelper, bool? status)
        {
            if (status == null)
            {
                return MvcHtmlString.Create("Not Available");
            }
            else if (status == true)
            {
                return MvcHtmlString.Create("Yes");
            }
            else
            {
                return MvcHtmlString.Create("No");
            }
        }
    }
}