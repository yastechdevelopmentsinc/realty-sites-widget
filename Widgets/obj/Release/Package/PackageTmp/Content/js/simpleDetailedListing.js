﻿var _vdcopts = _vdcopts || {};

_vdcopts.DetailedListing = {

    initialize: function() {
        _vdcopts.stLightLoaded = setInterval(function () {
            _vdcopts.DetailedListing.checkstlight();
        }, 100);
    },

    loadScripts: function() {     
        
        if (typeof _vdcopts.map === 'undefined') {
            var url = "https://maps.googleapis.com/maps/api/js?key=" + _vdcopts.GoogleApiKey + "&sensor=false&callback=_vdcopts.GoogleMaps.initMaps";
            var mapscript = document.createElement("script");
            mapscript.type = "text/javascript";
            mapscript.src = url;
            document.body.appendChild(mapscript);
        } else {
            _vdcopts.GoogleMaps.initMaps();
        }

        // setup share this scripts
        if (typeof stLight === 'undefined') {
            var stlightscript = document.createElement("script");
            stlightscript.type = "text/javascript";
            stlightscript.src = "http://w.sharethis.com/button/buttons.js";
            (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(stlightscript);
        }
        
        var slimboxcss = document.createElement("link");
        slimboxcss.type = "text/css";
        slimboxcss.rel = "stylesheet";
        slimboxcss.href = _vdcopts._defaultHostName + "/scripts/slimbox/css/slimbox2.css";
        document.getElementsByTagName("head")[0].appendChild(slimboxcss);
        
        var slimbox = document.createElement("script");
        slimbox.type = "text/javascript";
        slimbox.src = _vdcopts._defaultHostName + "/scripts/slimbox/js/slimbox2.js";
        document.body.appendChild(slimbox);
    },
    
    // check if share this scripts have been loaded
    checkstlight: function () {
        if (typeof stLight === 'undefined') {

        } else {
            clearInterval(_vdcopts.stLightLoaded);
            var switchTo5x = false;
            stLight.options({ publisher: "ur-ae4e32fd-1eb6-c01c-b37d-b07d781f6d5c" });
            stButtons.locateElements();
        }
    }
};

(function () {
    _vdcopts.DetailedListing.loadScripts();
})();