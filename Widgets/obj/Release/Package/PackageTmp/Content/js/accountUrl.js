﻿$(function() {
    $('form').submit(function(e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: _vdcopts.saveAccountUrl,
            data: $(this).serializeArray(),
            success: function(data, textStatus) {
                location.reload();
            },
            error: function(data, textStatus) {
                var responseData = JSON.parse(data.responseText);
                $.each(responseData, function(key, value) {
                    // highlight input
                    var input = '#' + key;
                    if (!$(input).hasClass('input-validation-error')) {
                        $(input).addClass('input-validation-error');
                    }

                    // validation field 
                    var field = '[data-valmsg-for="' + key + '"]';
                    var ele = $('<span>').text(value.toString());
                    $(field).html(ele);
                    if (!$(field).hasClass('field-validation-error')) {
                        $(field).addClass('field-validation-error');
                    }
                    $(field).show();
                });
            }
        });
    });
});