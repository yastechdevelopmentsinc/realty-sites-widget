﻿var _vdcopts = _vdcopts || {};

_vdcopts.GoogleMaps = {
    
    // initialize the listings google map
    initMaps: function() {

        setTimeout(function() {
            var myLatLng = new google.maps.LatLng(_vdcopts.Latitude, _vdcopts.Longitude);
            var myOptions = {
                scrollwheel: false,
                draggable: false,
                zoom: 16,
                center: myLatLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            // create map 
            _vdcopts.map = new google.maps.Map(document.getElementById("vdc-listing-details-Map"), myOptions);
            // add marker
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: _vdcopts.map,
                title: _vdcopts.Address
            });

            // Create the DIV to hold the control and call the StreetViewControl() constructor passing in this DIV.
            var controlDiv = document.createElement('div');
            var streetViewControl = new _vdcopts.GoogleMaps.StreetViewControl(controlDiv);

            controlDiv.index = 1;
            _vdcopts.map.controls[google.maps.ControlPosition.TOP_RIGHT].push(controlDiv);

        }, 500);
    },

    StreetViewControl: function(controlDiv) {
        // Set CSS styles for the DIV containing the control
        // Setting padding to 5 px will offset the control
        // from the edge of the map.
        controlDiv.style.padding = '5px';

        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = 'white';
        controlUI.style.borderStyle = 'solid';
        controlUI.style.borderWidth = '1px';
        controlUI.style.borderColor = '#717B87';
        controlUI.style.boxShadow = 'rgba(0, 0, 0, 0.398438) 0px 2px 4px';
        controlUI.style.cursor = 'pointer';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Show street view';
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.fontFamily = 'Arial,sans-serif';
        controlText.style.fontSize = '13px';
        controlText.style.paddingLeft = '6px';
        controlText.style.paddingRight = '6px';
        controlText.style.paddingTop = '1px';
        controlText.style.paddingBottom = '1px';
        controlText.innerHTML = 'Street View';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        google.maps.event.addDomListener(controlUI, 'click', function() {

            var streetViewMaxDistance = 100;
            var point = new google.maps.LatLng(_vdcopts.Latitude, _vdcopts.Longitude);
            var streetViewService = new google.maps.StreetViewService();
            var panorama = _vdcopts.map.getStreetView();

            // We get the map's default panorama and set up some defaults.
            streetViewService.getPanoramaByLocation(point, streetViewMaxDistance, function(streetViewPanoramaData, status) {

                if (status === google.maps.StreetViewStatus.OK) {

                    var oldPoint = point;
                    point = streetViewPanoramaData.location.latLng;

                    var heading = google.maps.geometry.spherical.computeHeading(point, oldPoint);

                    panorama.setPosition(point);
                    panorama.setPov({
                        heading: heading,
                        zoom: 1,
                        pitch: 0
                    });
                    panorama.setVisible(true);

                } else {
                    alert("Sorry! Street View is not available.");
                    // no street view available in this range, or some error occurred
                }
            });
        });
    }
};