﻿var _vdcopts = _vdcopts || {};

_vdcopts.SearchWidget = {
    loadScripts: function () {
        var jqueryuicss = document.createElement("link");
        jqueryuicss.type = "text/css";
        jqueryuicss.rel = "stylesheet";
        jqueryuicss.href = _vdcopts._defaultHostName + "/scripts/jqueryui/jquery-ui.css";
        document.getElementsByTagName("head")[0].appendChild(jqueryuicss);
        
        var jqueryui = document.createElement("script");
        jqueryui.type = "text/javascript";
        jqueryui.src = _vdcopts._defaultHostName + "/scripts/jqueryui/jquery-ui.min.js";
        jqueryui.async = true;
        document.body.appendChild(jqueryui);
    },
    initBindings: function() {
        // submit search form
        _vdcopts.jQuery('#vdc-search-container form').submit(function (event) {
            event.preventDefault();
            var jsonpUrl = _vdcopts._defaultHostName + "/widget/SearchListings/?callback=?&accountkey=" + _lw._setAccount;
            _vdcopts.jQuery.getJSON(jsonpUrl, _vdcopts.jQuery(this).serializeArray(), function (data) {
                if (data.action === "RefreshMap") {
                    _vdcopts.MapSearch.refreshMap();
                }
                else if (data.action === "Redirect") {
                    window.location = data.url;
                }
                else {
                    _vdcopts.jQuery('#vdc-listing-container').html(data.html);
                }
            });

            var mlsSearch = _vdcopts.jQuery('#SearchCriteria_MlsNumber');
            if (mlsSearch) {
                mlsSearch.val('');
            }

            return false;
        });

        // when city changes populate sub areas
        _vdcopts.jQuery('#SearchCriteria_City').on('change', function () {
            _vdcopts.jQuery('#img_loading').css("display", "inline-block");
            var jsonpUrl = _vdcopts._defaultHostName + "/widget/populatelocations/?callback=?&accountkey=" + _lw._setAccount;
            _vdcopts.jQuery.getJSON(jsonpUrl, { city: _vdcopts.jQuery("#SearchCriteria_City").val() }, function (data) {
                _vdcopts.jQuery("#SearchCriteria_AreaName").empty();
                _vdcopts.jQuery.each(data.filteredLocations, function (index, optionData) {
                    var areaname;
                    if (optionData.Listings === 0) {
                        areaname = optionData.AreaName;
                        
                    } else {
                        //areaname = optionData.AreaName ;
                        areaname = optionData.AreaName + " (" + optionData.Listings + ")";
                    }
                    var option = new Option(areaname, optionData.AreaName);
                    _vdcopts.jQuery("#SearchCriteria_AreaName").append(option);
                });
                _vdcopts.jQuery('#img_loading').css('display', 'none');
            });
        });

        // if open house search setup the datepicker
        if (_vdcopts.jQuery('#SearchCriteria_DateFrom').length > 0) {
            setTimeout(function () {
                _vdcopts.jQuery('#SearchCriteria_DateFrom, #SearchCriteria_DateTo').datepicker();
            }, 500);
        }

        // if the map search is one of the widgets open set the widget type
        for (var item in _lw._widgets) {
            if (_lw._widgets[item].type === '_listingmapsearch') {
                _vdcopts.jQuery('#SearchCriteria_WidgetType').val(_lw._widgets[item].type);
                break;
            }
        }

        // if widget set to autoload submit the default search form
        if (_vdcopts.jQuery('#vdc-search-container #AutoLoad').val() === "true") {
            _vdcopts.jQuery('#vdc-search-container input[type=submit]').click();
        }
    }
};

(function() {
    _vdcopts.SearchWidget.loadScripts();
    _vdcopts.SearchWidget.initBindings();
})();  