﻿var _vdcopts = _vdcopts || {};

_vdcopts.DisplayNewsItem = {
    
    loadScripts: function() {
        // setup share this scripts
        if (typeof stLight === 'undefined') {
            var stlightscript = document.createElement("script");
            stlightscript.type = "text/javascript";
            stlightscript.src = "http://w.sharethis.com/button/buttons.js";
            (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(stlightscript);
        }
    },

    initialize: function() {
        _vdcopts.galleriaLoaded = setInterval(function () {
            _vdcopts.DisplayNewsItem.checkGalleria();
        }, 100);

        _vdcopts.stLightLoaded = setInterval(function () {
            _vdcopts.DisplayNewsItem.checkstlight();
        }, 100);
    },

    checkGalleria: function() {
        if (typeof(Galleria) === 'undefined') {

        } else {
            clearInterval(_vdcopts.galleriaLoaded);
            _vdcopts.DisplayNewsItem.initGalleria();
        }
    },

    checkstlight: function() {
        if (typeof (_vdcopts.stLightLoaded) === 'undefined') {

        } else {
            clearInterval(_vdcopts.stLightLoaded);
            var switchTo5x = false;
            stLight.options({ publisher: "ur-ae4e32fd-1eb6-c01c-b37d-b07d781f6d5c" });
            stButtons.locateElements();
        }
    },

    initGalleria: function() {
        if (typeof(Galleria.theme) === 'undefined') {
            var url = _vdcopts._defaultHostName + '/scripts/galleria/themes/classic/galleria.classic.min.js?v1';
            Galleria.loadTheme(url);
        }

        Galleria.run('#vdc-newsitem-galleria', {
            debug: false,
            showInfo: false,
            width: 550,
            height: 400,
            dummy: _vdcopts._defaultHostName + '/Content/images/noimageavailable.jpg'
        });

        _vdcopts.jQuery('#vdc-newsitem-galleria').show();
    }
};

(function () {
    _vdcopts.DisplayNewsItem.loadScripts();
})();