﻿var _vdcopts = _vdcopts || {};

_vdcopts.InitializeEmailBindings = function() {
    _vdcopts.jQuery('#vdc-email-container form').submit(function(event) {
        event.preventDefault();

        var jsonpUrl;
        if (_vdcopts.EmailListing) {
            jsonpUrl = _vdcopts._sendEmailListing;
        } else {
            jsonpUrl = _vdcopts._sendEmailAgent;
        }

        _vdcopts.jQuery.getJSON(jsonpUrl, _vdcopts.jQuery(this).serializeArray(), function(data) {
            if (data.message === "error") {

                _vdcopts.jQuery('.field-validation-error span').remove();
                _vdcopts.jQuery('input').removeClass('input-validation-error');
                _vdcopts.jQuery('input').removeClass('field-validation-error');

                _vdcopts.jQuery.each(data.errorList, function(key, value) {
                    if (value.length > 0) {
                        // highlight input
                        var input = '#' + key;
                        if (!_vdcopts.jQuery(input).hasClass('input-validation-error')) {
                            _vdcopts.jQuery(input).addClass('input-validation-error');
                        }

                        // validation field 
                        var field = '[data-valmsg-for="' + key + '"]';
                        var ele = _vdcopts.jQuery('<span>').text(value.toString());
                        _vdcopts.jQuery(field).html(ele);
                        if (!_vdcopts.jQuery(field).hasClass('field-validation-error')) {
                            _vdcopts.jQuery(field).addClass('field-validation-error');
                        }
                        _vdcopts.jQuery(field).show();
                    }
                });
            }

            if (data.message === "success") {
                var ele = _vdcopts.jQuery('<div>').attr('id', 'success')
                    .addClass('success')
                    .html("Email Sent")
                    .hide();
                _vdcopts.jQuery('#vdc-email-container').prepend(ele);
                _vdcopts.jQuery('#success').fadeIn(500);

                _vdcopts.jQuery('#emailclose').delay(2000).queue(function() {
                    _vdcopts.jQuery(this).click();
                });
            }
        });
    });
};

_vdcopts.jQuery(function() {
    _vdcopts.InitializeEmailBindings();
});