﻿var _vdcopts = _vdcopts || {};

_vdcopts.NewsWidget = {
    loadScripts: function() {
        var slimboxcss = document.createElement("link");
        slimboxcss.type = "text/css";
        slimboxcss.rel = "stylesheet";
        slimboxcss.href = _vdcopts._defaultHostName + "/scripts/slimbox/css/slimbox2.css";
        document.getElementsByTagName("head")[0].appendChild(slimboxcss);

        var slimbox = document.createElement("script");
        slimbox.type = "text/javascript";
        slimbox.src = _vdcopts._defaultHostName + "/scripts/slimbox/js/slimbox2.js";
        document.body.appendChild(slimbox);
    },

    loadNewsItem: function(jsonpUrl) {
        _vdcopts.jQuery.getJSON(jsonpUrl, function(data) {
            var ele = _vdcopts.jQuery('<div>').attr('id', 'vdc-newsitem')
                .html(data.html);
            _vdcopts.jQuery('#vdc-news-container').append(ele);

            _vdcopts.jQuery('#vdc-newsitem').lightbox_me({
                destroyOnClose: true,
                onLoad: function() {
                    _vdcopts.DisplayNewsItem.initialize();
                }
            });
        });
    }
};
    
_vdcopts.jQuery(function () {
        
    var newsitem = _vdcopts.getParameterByName('newsitem');
    if( newsitem != undefined && newsitem != "") {
        var url = _vdcopts._newsItem + newsitem + "?callback=?";
        _vdcopts.NewsWidget.loadNewsItem(url); 
    }
        
    _vdcopts.jQuery('.vdc-news-readmore').on('click', function(e) {
        e.preventDefault();

        var jsonpUrl = _vdcopts._newsItem + _vdcopts.jQuery(this).data('newsid') + "?callback=?";
        _vdcopts.NewsWidget.loadNewsItem(jsonpUrl);
    });

    _vdcopts.jQuery('#Archive').on('change', function() {
        var jsonpUrl = _vdcopts._newswidget + _lw._setAccount + '&archiveyear=' + _vdcopts.jQuery(this).val();
        _vdcopts.jQuery.getJSON(jsonpUrl, _vdcopts.getWidgetOptionsByName('_newswidget'), function (data) {
            _vdcopts.jQuery('#vdc-news-container').html(data.html);
        });
    });

});

(function() {
    _vdcopts.NewsWidget.loadScripts();
})();