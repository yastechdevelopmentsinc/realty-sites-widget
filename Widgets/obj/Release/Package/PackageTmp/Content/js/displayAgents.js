﻿var _vdcopts = _vdcopts || {};

_vdcopts.MemberWidget = {
    getMembers: function (search, office, sort) {
        var widgetOptions = _vdcopts.getWidgetOptionsByName('_memberswidget') || {};
        if (search !== undefined) widgetOptions.search = search;
        if (office !== undefined) widgetOptions.office = office;
        if (sort !== undefined) widgetOptions.sort = sort;

        var jsonpUrl = _vdcopts._memberswidget + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, widgetOptions, function (data) {
            _vdcopts.jQuery('#vdc-members-container').html(data.html);
        });
    }
};

_vdcopts.jQuery('.vdc-email-member').on('click', function (e) {
    e.preventDefault();
    var jsonpUrl = _vdcopts._emailForm + _vdcopts.jQuery(this).data('agentid') + "?callback=?";
    _vdcopts.jQuery.getJSON(jsonpUrl, function (data) {
        var ele = _vdcopts.jQuery('<div>').attr('id', 'popup')
                .addClass('vdc-popup')
                .html(data.html);
        ele.lightbox_me({
            destroyOnClose: true,
        });
    });
});

_vdcopts.jQuery('.vdc-member-sort select, .vdc-member-filter-office select').on('change', function (e) {
    e.preventDefault();
    _vdcopts.MemberWidget.getMembers(_vdcopts.jQuery('.vdc-member-search input').val(), _vdcopts.jQuery('.vdc-member-filter-office select').val(), _vdcopts.jQuery('.vdc-member-sort select').val());
});

_vdcopts.jQuery('.vdc-member-search button').on('click', function (e) {
    e.preventDefault();
    _vdcopts.MemberWidget.getMembers(_vdcopts.jQuery('.vdc-member-search input').val(), _vdcopts.jQuery('.vdc-member-filter-office select').val(), _vdcopts.jQuery('.vdc-member-sort select').val());
});