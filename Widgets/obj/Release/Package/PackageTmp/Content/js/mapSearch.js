﻿var _vdcopts = _vdcopts || {};

var ShowListingDetailsInPage = false;

_vdcopts.MapSearch = {

    // append the google maps script to the dom if the map has not previously been initialized
    loadScripts: function () {
        if (typeof _vdcopts.map === 'undefined') {
            var url = "https://maps.googleapis.com/maps/api/js?key=" + _vdcopts.GoogleApiKey + "&sensor=false&callback=_vdcopts.MapSearch.initMaps";
            var mapscript = document.createElement("script");
            mapscript.type = "text/javascript";
            mapscript.src = url;
            document.body.appendChild(mapscript);
        } else {
            _vdcopts.MapSearch.initMaps();
        }
    },

    // refreshMap is called from the search widget to signify a map refresh
    refreshMap: function () {
        _vdcopts.jQuery('#vdc-map-loading').show();
        // clear all marker icons
        if (_vdcopts.markerCluster != undefined) {
            _vdcopts.markerCluster.clearMarkers();
        }
        _vdcopts.MapSearch.createMap();
        var jsonpUrl = _vdcopts._defaultHostName + "/widget/getmaplistings/?callback=?&accountkey=" + _lw._setAccount + "&sessionid=" + _vdcopts.jQuery('#SearchCriteria_SessionID').val();
        _vdcopts.jQuery.getJSON(jsonpUrl, _vdcopts.getWidgetOptionsByName('_listingmapsearch'), function (data) {
            _vdcopts.jQuery('#vdc-map-loading').hide();
            _vdcopts.MapSearch.setupLocationMarker(_vdcopts.map, data);
            if (_vdcopts.jQuery("#main .wrapper") != undefined && _vdcopts.jQuery(".m_map_mask") != undefined) {
                _vdcopts.jQuery("#main .wrapper").show();
                _vdcopts.jQuery(".m_map_mask").hide();
            }
        });
    },

    // google maps callback to setup the map
    initMaps: function () {

        var jsonpUrl = _vdcopts._defaultHostName + "/widget/getmapicons/?callback=?&accountkey=" + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, _vdcopts.getWidgetOptionsByName('_listingmapsearch'), function (data) {
            if (data.customMarker === true) {
                var iconUrl = _vdcopts._defaultHostName + '/Account/GetAccountMapMarker/' + _lw._setAccount;
                _vdcopts.markerImage = new google.maps.MarkerImage(
                    iconUrl,
                    new google.maps.Size(36, 36),
                    new google.maps.Point(0, 0),
                    new google.maps.Point(0, 36)
                );
            }
            if (data.customCluster === true) {
                _vdcopts.markerClusterImage = _vdcopts._defaultHostName + '/Account/GetAccountMapMarkerMore/' + _lw._setAccount;
            }
        });

        setTimeout(function () {
            _vdcopts.MapSearch.createMap();
            _vdcopts.jQuery('#vdc-map-loading').show();
            var widgetOptions = _vdcopts.getWidgetOptionsByName('_listingmapsearch');
            if (widgetOptions === null) {
                widgetOptions = _vdcopts.getWidgetOptionsByName('_nearmesearch');
                widgetOptions['islocated'] = _vdcopts.islocated;
                widgetOptions['latitude'] = _vdcopts.mapSearchLatitude;
                widgetOptions['longitude'] = _vdcopts.mapSearchLongitude;
            }
            var switchsessionid = "";
            if (_vdcopts.getParameterByName('vdcsessionid') != "") {
                switchsessionid = _vdcopts.getParameterByName('vdcsessionid')
            }
            var jsonpUrl = _vdcopts._defaultHostName + "/widget/getmaplistings/?callback=?&accountkey=" + _lw._setAccount+"&sessionid=" + switchsessionid;
            _vdcopts.jQuery.getJSON(jsonpUrl, widgetOptions, function (data) {
                _vdcopts.jQuery('#vdc-map-loading').hide();
                _vdcopts.MapSearch.setupLocationMarker(_vdcopts.map, data);
                if (_vdcopts.islocated != undefined && _vdcopts.islocated != false)
                {
                    _vdcopts.map.setOptions({ zoom: 14 });
                    var marker = new google.maps.Marker({ position: new google.maps.LatLng(_vdcopts.mapSearchLatitude, _vdcopts.mapSearchLongitude),map:_vdcopts.map });
                    
                   // _vdcopts.markerCluster = new MarkerClusterer(_vdcopts.map, marker);
                   
                }
                if (_vdcopts.jQuery("#main .wrapper") != undefined && _vdcopts.jQuery(".m_map_mask") != undefined) {
                    _vdcopts.jQuery("#main .wrapper").css("visibility", "visible");
                    _vdcopts.jQuery(".m_map_mask").hide();
                }
            });
        }, 500);
    },

    createMap: function () {
        var myOptions = {
            zoom: 8,
            center: new google.maps.LatLng(_vdcopts.mapSearchLatitude, _vdcopts.mapSearchLongitude),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        _vdcopts.map = new google.maps.Map(document.getElementById("vdc-map-search"), myOptions);
        google.maps.event.addDomListener(window, 'resize', function () {
            _vdcopts.map.setCenter(new google.maps.LatLng(_vdcopts.mapSearchLatitude, _vdcopts.mapSearchLongitude));
        });
        
    },

    setupLocationMarker: function (map, data) {
        var markers = [];
        var infowindow = new google.maps.InfoWindow({
            content: "placeholder..."
        });

        var latLng = null;

        for (var i = 0; i < data.Listings.length; i++) {
            if (data.Listings[i] != null && data.Listings[i].GetLatitude != null && data.Listings[i].GetLongitude != null)
            {
                // create a marker          
                var propertyType;
                var mlsNumber;
                var subAreaName;
                var style;
                var disclaimer;

                if (data.Listings[i].ExclusiveID == undefined) {
                    propertyType = data.Listings[i].PropertyType;
                    mlsNumber = data.Listings[i].MlsNumber;
                    disclaimer = data.Disclaimer.MLSListing;
                } else {
                    propertyType = "Exclusive";
                    mlsNumber = data.Listings[i].ExclusiveID;
                    disclaimer = data.Disclaimer.ExclusiveListing;
                }

                if (data.Listings[i].SubAreaName == undefined) {
                    subAreaName = data.Listings[i].SubAreaName;
                } else {
                    subAreaName = data.Listings[i].SubAreaName.substring(3, data.Listings[i].SubAreaName.length);
                }

                if (data.Listings[i].TypeDwelling != null) {
                    style = '<div class="vdc-map-style"><label>Style</label>' + data.Listings[i].TypeDwelling + '</div>';
                } else {
                    style = "";
                }
                var brokername = '';
                if (data.Listings[i].BrokerName != 'null') {
                    brokername = '<div class="vdc-map-broker"><label>Brokered by</label>' + data.Listings[i].BrokerName + '</div>'
                }
                var moredetailslinkinfo = 'href="#" onclick="_vdcopts.MapSearch.OpenDetailsWindow(\'' + propertyType + '\',' + mlsNumber + ')"';
                if (data.ShowListingDetailsInPage) {
                    ShowListingDetailsInPage = data.ShowListingDetailsInPage;
                    moredetailslinkinfo = 'class="moredetailspage" target="_blank" href="/ListingDetails?property=' + propertyType + '&id=' + mlsNumber + '"';
                }
                var contentString = '<div class="vdc-map-listing clearfix">' +
                    '<div class="vdc-map-address">' + data.Listings[i].Address + '</div>' +
                        '<div class="vdc-map-photo"><a '+moredetailslinkinfo+'><img src="' + data.Listings[i].DefaultPhoto + '"/></a></div><div class="vdc-map-details">' +
                            '<div class="vdc-map-price"><label>Price</label>' + data.Listings[i].Price + '</div>' +
                                '<div class="vdc-map-type"><label>Type</label>' + data.Listings[i].PropertyType + '</div>' + style +
                                    '<div class="vdc-map-area"><label>Area</label>' + subAreaName + '</div>' +
                                        '<div class="vdc-map-more"><a class="listing-details"' + moredetailslinkinfo + '>More Details...</a></div>' +
                                            '</div></div>' + brokername
                     +
                                                    '<div class="vdc-map-disclaimer">' + disclaimer + '</div>';
                latLng = new google.maps.LatLng(data.Listings[i].GetLatitude, data.Listings[i].GetLongitude);
                var marker = new google.maps.Marker({ icon: _vdcopts.markerImage, 'position': latLng, title: data.Listings[i].Address, html: contentString, propertype: propertyType, mlsnumber: mlsNumber });

                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(map, this);
                    infowindow.setContent(this.html);
                });

                markers.push(marker);
            }
           
        }
        // add markers to clusterer
        var mcOptions;
        if (_vdcopts.markerClusterImage != undefined) {
            var styles = [{
                url: _vdcopts.markerClusterImage,
                height: 36,
                width: 36
            }];

            mcOptions = {
                gridSize: 25,
                zoomOnClick: false,
                styles: styles
            };
        } else {
            mcOptions = { gridSize: 25, zoomOnClick: false };
        }
        _vdcopts.markerCluster = new MarkerClusterer(map, markers, mcOptions);

        try
        {
            google.maps.event.addListenerOnce(map, 'load', loadstyle());
           

        }
        catch (err){}

        // handle dblclick of a cluster
        google.maps.event.addListener(_vdcopts.markerCluster, 'click', function (mCluster) {

            var zoom = map.getZoom();
            var clustermarkers = mCluster.getMarkers();
            var center = mCluster.getCenter();
            latLng = new google.maps.LatLng(center.lat(), center.lng());

            if (zoom >= 15) {
                // show info window for cluster of properties
                var clusterString = '<div class="vdc-map-cluster">Please click on a address to view more details:</div>';
                
                for (var i = 0; i < clustermarkers.length; i++) {
                    var moredetailslinkinfo = 'href="#" onclick="_vdcopts.MapSearch.OpenDetailsWindow(\'' + clustermarkers[i].propertype + '\',' + clustermarkers[i].mlsnumber + ')"';
                    if (ShowListingDetailsInPage) {
                        moredetailslinkinfo = 'class="moredetailspage" target="_blank" href="/ListingDetails?property=' + clustermarkers[i].propertype + '&id=' + clustermarkers[i].mlsnumber + '"';
                    }
                        clusterString += '<div class="vdc-map-cluster-link"><a class="listing-details"' + moredetailslinkinfo +' >' + clustermarkers[i].title + '</a></div>';
                    }
                    infowindow.setContent(clusterString);
                    infowindow.setPosition(latLng);
                    infowindow.open(map);
            }
            else
            {
                map.setZoom(zoom + 2);
                map.setCenter(latLng);
            }

            //if (zoom >= 15) {
            //    // show info window for cluster of properties
            //    var clusterString = '<div class="vdc-map-cluster">Please click on a address to view more details:</div>';
            //    for (var i = 0; i < clustermarkers.length; i++) {
            //        clusterString += '<div class="vdc-map-cluster-link"><a class="listing-details" onclick="_vdcopts.MapSearch.OpenDetailsWindow(\'' + clustermarkers[i].propertype + '\',' + clustermarkers[i].mlsnumber + ')" href="#">' + clustermarkers[i].title + '</a></div>';
            //    }
            //    infowindow.setContent(clusterString);
            //    infowindow.setPosition(latLng);
            //    infowindow.open(map);
            //} else {
            //    // zoom and center
            //    map.setZoom(zoom + 2);
            //    map.setCenter(latLng);
            //}
        });

        var property = _vdcopts.getParameterByName('property');
        var mlsnumber = _vdcopts.getParameterByName('mlsnumber');
        if (property != undefined && property != "" && mlsnumber != undefined && mlsnumber != "") {
            _vdcopts.MapSearch.OpenDetailsWindow(property, mlsnumber);
        }
    },

    // check if details page has been loaded
    checkDetails: function () {
        if (typeof _vdcopts.DetailedListing === 'undefined') {

        } else {
            clearInterval(_vdcopts.detailsLoaded);
            _vdcopts.DetailedListing.initialize();
        }
    },

    OpenDetailsWindow: function (property, mlsnumber) {
        var jsonpUrl = _vdcopts._defaultHostName + "/widget/detailedlisting?property=" + property + "&mlsnumber= " + mlsnumber + "&callback=?&accountkey=" + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, function(data) {
            var ele = _vdcopts.jQuery('<div>').attr('id', 'vdc-listing-details')
                .html(data.html);
            ele.lightbox_me({
                centered: true,
                destroyOnClose: true,
                onLoad: function () {
                    _vdcopts.detailsLoaded = setInterval(function () {
                        _vdcopts.MapSearch.checkDetails();
                    }, 100);
                }
            });
        });
    }
};

(function () {
    _vdcopts.MapSearch.loadScripts();
})();