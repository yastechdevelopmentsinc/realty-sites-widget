﻿_vdcopts.jQuery('.vdc-email-contact').on('click', function (e) {
    e.preventDefault();

    var jsonpUrl = _vdcopts._emailForm + _vdcopts.jQuery(this).data('agentid') + "?callback=?";
    _vdcopts.jQuery.getJSON(jsonpUrl, function (data) {
        var ele = _vdcopts.jQuery('<div>').attr('id', 'popup')
                .addClass('vdc-popup')
                .html(data.html);
        ele.lightbox_me({
            destroyOnClose: true,
        });
    });
});