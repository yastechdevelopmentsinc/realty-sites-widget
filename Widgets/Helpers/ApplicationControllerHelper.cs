﻿using System.Globalization;

namespace Widgets.Helpers
{
    public class ApplicationControllerHelper
    {
        public virtual string ReadListingWidgetJavascript()
        {
            return System.IO.File.ReadAllText(
                System.Web.HttpContext.Current.Server.MapPath("~/Content/js/widget.js")
                );
        }

        public virtual string GetLastModified()
        {
            var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/widget.css");
            return System.IO.File.GetLastWriteTime(path).ToFileTimeUtc().ToString(CultureInfo.InvariantCulture);
        }
    }
}