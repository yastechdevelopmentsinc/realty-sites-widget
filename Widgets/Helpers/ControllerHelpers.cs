﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Extensions;
using Widgets.Models.ViewModels;

namespace Widgets.Helpers
{
    public static class ControllerHelpers
    {
        /// <summary>
        /// Converts the posted file to a byte array that can be saved to the database
        /// </summary>
        /// <param name="postedFile">The image uploaded within the get / create actions</param>
        public static byte[] GetImageBytes(HttpPostedFileBase postedFile)
        {
            int imageLength = postedFile.ContentLength;
            var tempImage = new byte[imageLength];
            postedFile.InputStream.Position = 0;
            postedFile.InputStream.Read(tempImage, 0, imageLength);

            return tempImage;
        }

        /// <summary>
        /// Renders JSON result for specified error message
        /// </summary>
        /// <param name="authorizedErrorMessage"></param>
        public static JsonResult RenderErrorMessage(string authorizedErrorMessage)
        {
            return new JsonResult
                       {
                           JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                           Data =
                               new
                                   {
                                       html = authorizedErrorMessage
                                   }
                       };
        }

        /// <summary>
        /// Get the name of a property via reflection
        /// </summary>
        /// <param name="propertyInfo"></param>
        public static string GetName(PropertyInfo propertyInfo)
        {
            var attr = (DisplayNameAttribute)propertyInfo.GetCustomAttributes(typeof(DisplayNameAttribute), false).SingleOrDefault();
            return attr == null ? propertyInfo.Name : attr.DisplayName;
        }

        /// <summary>
        /// Get the value of a property via reflection
        /// </summary>
        /// <param name="src"></param>
        /// <param name="propName"></param>
        public static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        /// <summary>
        /// Get featured listings for specified account
        /// </summary>
        /// <param name="account"></param>
        /// <param name="session"></param>
        /// <param name="listingService"></param>
        public static List<Listing> GetFeaturedListings(Account account, ISession session, IListingService listingService)
        {
            var featuredListings = session.QueryOver<FeaturedListing>().Where(x => x.Account == account).List();
            var listings = new List<Listing>();

            foreach (var featuredListing in featuredListings)
            {
                if (account.BoardCode != "VIREB")
                {
                    var resi = Mapper.Map<Residential, Listing>(listingService.GetResidentialListing(featuredListing.Mlsnumber));
                    if (resi != null) listings.Add(resi);

                    var comm = Mapper.Map<Commercial, Listing>(listingService.GetCommercialListing(featuredListing.Mlsnumber));
                    if (comm != null) listings.Add(comm);

                    var farms = Mapper.Map<FarmsAndLand, Listing>(listingService.GetFarmsAndLandListing(featuredListing.Mlsnumber));
                    if (farms != null) listings.Add(farms);

                    var lease = Mapper.Map<ForLease, Listing>(listingService.GetForLeaseListing(featuredListing.Mlsnumber));
                    if (lease != null) listings.Add(lease);

                    var multifam = Mapper.Map<MultiFam, Listing>(listingService.GetMultiFamilyListing(featuredListing.Mlsnumber));
                    if (multifam != null) listings.Add(multifam);
                }
                else
                {
                    var resi = Mapper.Map<VResidential, Listing>(listingService.GetVResidentialListing(featuredListing.Mlsnumber));
                    if (resi != null) listings.Add(resi);

                    var comm = Mapper.Map<VCommercial, Listing>(listingService.GetVCommercialListing(featuredListing.Mlsnumber));
                    if (comm != null) listings.Add(comm);
                }
            }

            return listings;
        }

        /// <summary>
        /// Gets the index of either a RETSPhoto or an Exclusive Photo
        /// </summary>
        /// <param name="s">The path to the photo</param>
        public static int GetImageIndex(string s)
        {
            var start = s.LastIndexOf('_') + 1;
            var end = s.LastIndexOf('.') - start;
            return Convert.ToInt32(s.Substring(start, end));
        }

        /// <summary>
        /// Get featured agents for specified account
        /// </summary>
        /// <param name="account">The account to query agents for</param>
        /// <param name="session">Nhibernate session</param>
        /// <param name="take">The number of agents to return</param>
        public static List<AgentInfo> GetFeaturedAgents(Account account, ISession session, int take)
        {
            if (account.Features.EnableFeaturedAgentsSchedule && take == 1)
            {
                var currentagents = session.QueryOver<Agent>().Where(x => x.Account == account && x.FeaturedAgent && x.InProgress == true).List();
                if (currentagents == null || currentagents.Count == 0)
                {
                    currentagents = session.QueryOver<Agent>().Where(x => x.Account == account && x.FeaturedAgent && x.FeaturedPositionNumber != null).OrderBy(x => x.FeaturedPositionNumber).Asc.List();
                    if (currentagents == null || currentagents.Count == 0)
                    {
                        var result = session.QueryOver<FeaturedAgent>()
               .Where(x => x.Account == account && x.Agent != null)
               .OrderByRandom()
               .Take(take)
               .List();
                        return result.Select(featuredAgent => Mapper.Map<Agent, AgentInfo>(featuredAgent.Agent)).ToList();
                    }
                    else
                    {
                        var currentfeaturedagent = currentagents[0];
                        currentfeaturedagent.InProgress = true;
                        currentfeaturedagent.StartDate = DateTime.Now.Date;
                        session.Update(currentfeaturedagent);
                        session.Flush();
                        return new List<AgentInfo>() { Mapper.Map<Agent, AgentInfo>(currentfeaturedagent) };
                    }
                }
                else
                {
                    var currentfeaturedagent = currentagents[0];
                    int numberofdays = currentfeaturedagent.NubmerOfDays.HasValue ? currentfeaturedagent.NubmerOfDays.Value : 1;
                    if (currentfeaturedagent.StartDate.HasValue && currentfeaturedagent.StartDate.Value.AddDays(numberofdays) > DateTime.Now)
                    {
                        return new List<AgentInfo>() { Mapper.Map<Agent, AgentInfo>(currentfeaturedagent) };
                    }
                    else
                    {
                        currentfeaturedagent.InProgress = false;
                        currentfeaturedagent.StartDate = null;
                        session.Update(currentfeaturedagent);
                        session.Flush();
                        //
                        currentagents = session.QueryOver<Agent>().Where(x => x.Account == account && x.FeaturedAgent && x.FeaturedPositionNumber != null && x.FeaturedPositionNumber > currentfeaturedagent.FeaturedPositionNumber).OrderBy(x => x.FeaturedPositionNumber).Asc.List();
                        if (currentagents == null || currentagents.Count == 0)
                        {
                            currentagents = session.QueryOver<Agent>().Where(x => x.Account == account && x.FeaturedAgent && x.FeaturedPositionNumber != null).OrderBy(x => x.FeaturedPositionNumber).Asc.List();
                            if (currentagents == null || currentagents.Count == 0)
                            {
                                var result = session.QueryOver<FeaturedAgent>()
               .Where(x => x.Account == account && x.Agent != null)
               .OrderByRandom()
               .Take(take)
               .List();
                                return result.Select(featuredAgent => Mapper.Map<Agent, AgentInfo>(featuredAgent.Agent)).ToList();
                            }
                            else
                            {
                                currentfeaturedagent = currentagents[0];
                                currentfeaturedagent.InProgress = true;
                                currentfeaturedagent.StartDate = DateTime.Now.Date;
                                session.Update(currentfeaturedagent);
                                session.Flush();
                                return new List<AgentInfo>() { Mapper.Map<Agent, AgentInfo>(currentfeaturedagent) };
                            }
                        }
                        else
                        {
                            currentfeaturedagent = currentagents[0];
                            currentfeaturedagent.InProgress = true;
                            currentfeaturedagent.StartDate = DateTime.Now.Date;
                            session.Update(currentfeaturedagent);
                            session.Flush();
                            return new List<AgentInfo>() { Mapper.Map<Agent, AgentInfo>(currentfeaturedagent) };
                        }
                    }
                }

            }
            else
            {
                var results = session.QueryOver<FeaturedAgent>()
                    .Where(x => x.Account == account && x.Agent != null)
                    .OrderByRandom()
                    .Take(take)
                    .List();
                return results.Select(featuredAgent => Mapper.Map<Agent, AgentInfo>(featuredAgent.Agent)).ToList();
            }

        }

        public static List<AgentInfo> GetAgents(Account account, ISession session, int take)
        {
            var results = session.QueryOver<Agent>()
                .Where(x => x.Account == account && x.IDXListings == true)
                .OrderByRandom()
                .Take(take)
                .List().ToList();
            return Mapper.Map<List<Agent>, List<AgentInfo>>(results);
        }

        public static List<AccountOfficeInfo> GetAgentOffice(Account account, ISession session, int take)
        {
            var results = session.QueryOver<AccountOffice>()
                .Where(x => x.Account == account && x.IDXListings == true)
                .OrderByRandom()
                .Take(take)
                .List().ToList();
            return Mapper.Map<List<AccountOffice>, List<AccountOfficeInfo>>(results);
        }
    }
}