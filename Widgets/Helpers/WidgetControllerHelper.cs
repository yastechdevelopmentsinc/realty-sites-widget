﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;
using Widgets.Extensions;
using Widgets.Models;
using Widgets.Models.ViewModels;

namespace Widgets.Helpers
{

    /// <summary>
    /// Helper that contains abstracted business logic for the widget controller
    /// </summary>
    public class WidgetControllerHelper
    {
        private readonly IListingService _service;
        private readonly int _currentPage;
        private readonly int _pageSize;
        private readonly SearchCriteria _searchCriteria;
        private readonly bool _restrictByAccountType;
        private readonly int _restrictNumberListings;

        private IEnumerable<Listing> _filteredListings;

        public WidgetControllerHelper(IListingService service, int page, int pageSize, SearchCriteria searchCriteria, bool restrictByAccountType = false, int restrictNumberListings = 0)
        {
            _service = service;
            _currentPage = page;
            _pageSize = pageSize;
            _searchCriteria = searchCriteria;
            _restrictByAccountType = restrictByAccountType;
            _restrictNumberListings = restrictNumberListings;

            if (_searchCriteria == null) _searchCriteria = new SearchCriteria();
        }

        public WidgetControllerHelper(IListingService service, SearchCriteria searchCriteria, bool restrictByAccountType = false)
        {
            _service = service;
            _searchCriteria = searchCriteria;
            _restrictByAccountType = restrictByAccountType;

            if (_searchCriteria == null) _searchCriteria = new SearchCriteria();
        }

        public WidgetControllerHelper(IListingService service)
        {
            _service = service;
        }

        /// <summary>
        /// Queries and filters listing data for the listing display widget.
        /// </summary>
        /// <returns>ListingWidgeView model which consists of a paginated collection of listings</returns>
        public ListingWidgetViewModel GetListingWidgetData(string boardcode, int maxcount)
        {
            // If the listing search is by a specific MLS number return a collection of listings filtered by that id vs all of the listings
            IEnumerable<Listing> listings = _searchCriteria.MlsNumber.HasValue ? GetListingByMlsNumber(boardcode) : GetAllListings(boardcode, maxcount,false);

            // limit the number of listings returned if the account is setup to restrict a maximum number to display
            if (_restrictNumberListings > 0)
            {
                listings = listings.Take(_restrictNumberListings);
            }

            var model = new ListingWidgetViewModel(_searchCriteria.Account)
            {
                Listings = listings.Skip((_currentPage - 1) * _pageSize)
                    .Take(_pageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = _currentPage,
                    ItemsPerPage = _pageSize,
                    TotalItems = listings.Count()
                }
            };

            return model;
        }

        /// <summary>
        /// Queries and filters listing data by mls number
        /// </summary>
        /// <returns>Collection of filtered listings</returns>
        public IEnumerable<Listing> GetListingByMlsNumber(string boardcode)
        {
            if (boardcode == "VIREB")
            {
                var residential = _service.GetAllVResidential()
                    .MlsNumber(_searchCriteria.MlsNumber)
                    .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.Residential);

                var commercial = _service.GetAllVCommercial()
                    .MlsNumber(_searchCriteria.MlsNumber)
                    .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.Commercial);

                return VExecuteListingQuery(residential, commercial, null);
            }
            else if (_service.GetAllCBoardID().Count(x => x.Name == boardcode) > 0)
            {
                var listings = _service.GetAllCListings()
                    .MlsNumber(_searchCriteria.MlsNumber)
                    .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.Residential, _service.GetAllCBoardID().ToList());
                return CExecuteListingQuery(listings,null);
            }
            else
            {
                var residential = _service.GetAllResidential()
                   .MlsNumber(_searchCriteria.MlsNumber)
                   .ListingAccess(_searchCriteria.Account, PropertyTypeConstants.Residential, _restrictByAccountType)
                   .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.Residential);

                var commercial = _service.GetAllCommercial()
                    .MlsNumber(_searchCriteria.MlsNumber)
                    .ListingAccess(_searchCriteria.Account, PropertyTypeConstants.Commercial, _restrictByAccountType)
                    .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.Commercial);

                var farmsandland = _service.GetAllFarmsAndLand()
                    .MlsNumber(_searchCriteria.MlsNumber)
                    .ListingAccess(_searchCriteria.Account, PropertyTypeConstants.FarmsAndLand, _restrictByAccountType)
                    .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.FarmsAndLand);

                var forlease = _service.GetAllForLease()
                    .MlsNumber(_searchCriteria.MlsNumber)
                    .ListingAccess(_searchCriteria.Account, PropertyTypeConstants.ForLease, _restrictByAccountType)
                    .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.ForLease);

                var multifamily = _service.GetAllMultiFamily()
                    .MlsNumber(_searchCriteria.MlsNumber)
                    .ListingAccess(_searchCriteria.Account, PropertyTypeConstants.MultiFamily, _restrictByAccountType)
                    .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.MultiFamily);

                return ExecuteListingQuery(residential, commercial, farmsandland, forlease, null, multifamily);
               
            }
        }

        /// <summary>
        /// Queries and filters listing data by property type mls number
        /// </summary>
        /// <returns>A matching listing</returns>
        public Listing GetListingByPropertyTypeAndMlsNumber(string property, int mlsnumber, string boardcode)
        {
            Listing listing;
            if (boardcode == "VIREB")
            {
                switch (property)
                {
                    case "Exclusive":
                        listing = Mapper.Map<Exclusive, Listing>(_service.GetExclusiveListing(mlsnumber));
                        break;

                    default:
                        listing = Mapper.Map<VResidential, Listing>(_service.GetVResidentialListing(mlsnumber));
                        if (listing == null)
                        {
                            listing = Mapper.Map<VCommercial, Listing>(_service.GetVCommercialListing(mlsnumber));
                        }
                        break;
                }
                
            }
            else if (_service.GetAllCBoardID().Count(x => x.Name == boardcode) > 0)
            {
                switch (property)
                {
                    case PropertyTypeConstants.Residential:
                        listing = Mapper.Map<CListing, Listing>(_service.GetCListings(mlsnumber));
                        break;

                    case PropertyTypeConstants.ForLease:
                        var result = _service.GetCListings(mlsnumber);
                        if (result.PropertyType == "Rental")
                        {
                            listing = Mapper.Map<CListing, Listing>(result);
                        }
                        else
                        {
                            listing = null;
                        }
                        break;

                    case PropertyTypeConstants.FarmsAndLand:
                        var landresult = _service.GetCListings(mlsnumber);
                        if (landresult.PropertyType == "Land")
                        {
                            listing = Mapper.Map<CListing, Listing>(landresult);
                        }
                        else
                        {
                            listing = null;
                        }
                        break;

                    case TypeDwelling.Acreage:
                        var acreageresult = _service.GetCListings(mlsnumber);
                        if (acreageresult.PropertyType == "Rural")
                        {
                            listing = Mapper.Map<CListing, Listing>(acreageresult);
                        }
                        else
                        {
                            listing = null;
                        }
                        break;
                    case "Exclusive":
                        listing = Mapper.Map<Exclusive, Listing>(_service.GetExclusiveListing(mlsnumber));
                        break;

                    default:
                        listing = Mapper.Map<CListing, Listing>(_service.GetCListings(mlsnumber));
                        
                        break;
                }
            }
            else
            {

                switch (property)
                {
                    case PropertyTypeConstants.Residential:
                        listing = Mapper.Map<Residential, Listing>(_service.GetResidentialListing(mlsnumber));
                        break;

                    case TypeDwelling.Condo:
                        listing = Mapper.Map<Residential, Listing>(_service.GetResidentialListing(mlsnumber));
                        break;

                    case PropertyTypeConstants.Commercial:
                        listing = Mapper.Map<Commercial, Listing>(_service.GetCommercialListing(mlsnumber));
                        break;

                    case PropertyTypeConstants.ForLease:
                        listing = Mapper.Map<ForLease, Listing>(_service.GetForLeaseListing(mlsnumber));
                        break;

                    case PropertyTypeConstants.MultiFamily:
                        listing = Mapper.Map<MultiFam, Listing>(_service.GetMultiFamilyListing(mlsnumber));
                        break;

                    case PropertyTypeConstants.FarmsAndLand:
                        listing = Mapper.Map<FarmsAndLand, Listing>(_service.GetFarmsAndLandListing(mlsnumber));
                        break;

                    case TypeDwelling.Acreage:
                        listing = Mapper.Map<FarmsAndLand, Listing>(_service.GetFarmsAndLandListing(mlsnumber));
                        break;

                    case "Exclusive":
                        listing = Mapper.Map<Exclusive, Listing>(_service.GetExclusiveListing(mlsnumber));
                        break;

                    default:
                        listing = null;
                        break;
                }
            }
            return listing;
        }

        /// <summary>
        /// Queries and filters listing data.
        /// </summary>
        /// <returns>Collection of filtered listings</returns>
        public IEnumerable<Listing> GetAllListings(string boardcode, int maxcount , bool ismap)
        {
            if (boardcode == "VIREB")
            {

                var residential = _service.GetAllVResidential()
                    .PriceLow(_searchCriteria.PriceLow)
                    .PriceHigh(_searchCriteria.PriceHigh)
                    .Bathrooms(_searchCriteria.Bathrooms)
                    .Bedrooms(_searchCriteria.Bedrooms)
                    .VCity(_searchCriteria.City)
                    .VDistrict(_searchCriteria.District)
                    .VArea(_searchCriteria.AreaName)
                    .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.Residential)
                    .VAddress(_searchCriteria.Address)
                    .Agent(_searchCriteria.AgentID, PropertyTypeConstants.Residential).IsRental(_searchCriteria, PropertyTypeConstants.Residential).Sort(_searchCriteria.Sort);

                var commercial = _service.GetAllVCommercial()
                    .PriceLow(_searchCriteria.PriceLow)
                    .PriceHigh(_searchCriteria.PriceHigh)
                    .VCity(_searchCriteria.City)
                    .VDistrict(_searchCriteria.District)
                    .VArea(_searchCriteria.AreaName)
                    .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.Commercial)
                    .VAddress(_searchCriteria.Address)
                    .Agent(_searchCriteria.AgentID, PropertyTypeConstants.Commercial).IsRental(_searchCriteria, PropertyTypeConstants.Commercial).Sort(_searchCriteria.Sort);

                var exclusives = _service.GetAllExclusives(_searchCriteria.Account)
                    .PriceLow(_searchCriteria.PriceLow)
                    .PriceHigh(_searchCriteria.PriceHigh)
                    .VCity(_searchCriteria.City)
                    .District(_searchCriteria.District)
                    .Area(_searchCriteria.AreaName)
                    .Options(_searchCriteria.Account, _searchCriteria, ExclusivePropertyType.Exclusive)
                    .Address(_searchCriteria.Address)
                    .Status(_searchCriteria.Status)
                    .Agent(_searchCriteria.AgentID, ExclusivePropertyType.Exclusive)
                    .IsActive().IsRental(_searchCriteria, ExclusivePropertyType.Exclusive);

                if (_searchCriteria.WidgetOptions != WidgetOptions.Rentals)
                {
                    if (_searchCriteria.Residential && !_searchCriteria.Commercial)
                    {
                        commercial = null;
                        exclusives = null;
                    }
                    else if (_searchCriteria.Commercial && !_searchCriteria.Residential)
                    {
                        residential = null;
                        exclusives = null;
                    }
                }

                if (!ismap)
                {
                    if (commercial != null && commercial.Count() > maxcount)
                    {
                        commercial = commercial.Take(maxcount);
                    }
                    if (residential != null && residential.Count() > maxcount)
                    {
                        residential = residential.Take(maxcount);
                    }
                    if (exclusives != null && exclusives.Count() > maxcount)
                    {
                        exclusives = exclusives.Take(maxcount);
                    }
                }


                var allListings = VExecuteListingQuery(residential, commercial, exclusives);

               // if (_searchCriteria.WidgetOptions == WidgetOptions.Rentals)
                //{
                    ApplyPropertyTypeFiltering(allListings , boardcode);
                //}



                // if no property types selected return all listings
                var listings = _filteredListings ?? allListings;
                if (String.Compare(_searchCriteria.WidgetOptions, WidgetOptions.OpenHouseListings, StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    List<Listing> templistings = listings.ToList();
                    templistings.ForEach(x => x.VOpenHouses = x.VOpenHouses.Where(opho => opho.StartTime >= _searchCriteria.DateFrom && opho.StartTime <= _searchCriteria.DateTo));
                    listings = templistings;
                }
                // apply ordering
                return ApplyOrderBy(listings, _searchCriteria.Sort);
            }
            else if (_service.GetAllCBoardID().Count(x => x.Name == boardcode) > 0)
            {
                var residential = _service.GetAllCListings()
                    .PriceLow(_searchCriteria.PriceLow)
                    .PriceHigh(_searchCriteria.PriceHigh)
                    .Bathrooms(_searchCriteria.Bathrooms)
                    .Bedrooms(_searchCriteria.Bedrooms)
                    .City(_searchCriteria.City)
                    .District(_searchCriteria.District)
                    .Area(_searchCriteria.AreaName)
                    .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.Residential,_service.GetAllCBoardID().ToList())
                    .Address(_searchCriteria.Address)
                    .Agent(_searchCriteria.AgentID, PropertyTypeConstants.Residential).IsRental(_searchCriteria, PropertyTypeConstants.Residential).Sort(_searchCriteria.Sort)
                    .CustomSearch(_searchCriteria.SearchText);

                var exclusives = _service.GetAllExclusives(_searchCriteria.Account)
                    .PriceLow(_searchCriteria.PriceLow)
                    .PriceHigh(_searchCriteria.PriceHigh)
                    .City(_searchCriteria.City)
                    .District(_searchCriteria.District)
                    .Area(_searchCriteria.AreaName)
                    .Options(_searchCriteria.Account, _searchCriteria, ExclusivePropertyType.Exclusive)
                    .Address(_searchCriteria.Address)
                    .Status(_searchCriteria.Status)
                    .Agent(_searchCriteria.AgentID, ExclusivePropertyType.Exclusive)
                    .IsActive().IsRental(_searchCriteria, ExclusivePropertyType.Exclusive)
                    .CustomSearch(_searchCriteria.SearchText);

                /*if (!ismap)
                {
                    if (residential != null && residential.Count() > maxcount)
                    {
                        residential = residential.Take(maxcount);
                    }
                    if (exclusives != null && exclusives.Count() > maxcount)
                    {
                        exclusives = exclusives.Take(maxcount);
                    }
                }*/

                residential = residential.Take(maxcount);

                var allListings = CExecuteListingQuery(residential, exclusives);

                ApplyPropertyTypeFiltering(allListings , boardcode);

                // if no property types selected return all listings
                var listings = _filteredListings ?? allListings;
                if (String.Compare(_searchCriteria.WidgetOptions, WidgetOptions.OpenHouseListings, StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    List<Listing> templistings = listings.ToList();
                    templistings.ForEach(x => x.COpenHouses = x.COpenHouses.Where(opho => opho.OpenHouseDate >= _searchCriteria.DateFrom && opho.OpenHouseDate <= _searchCriteria.DateTo));
                    listings = templistings;
                }
                // apply ordering
                return ApplyOrderBy(listings, _searchCriteria.Sort);
            }
            else
            {

                var residential = _service.GetAllResidential()
                   .PriceLow(_searchCriteria.PriceLow)
                   .PriceHigh(_searchCriteria.PriceHigh)
                   .Bathrooms(_searchCriteria.Bathrooms)
                   .Bedrooms(_searchCriteria.Bedrooms)
                   .City(_searchCriteria.City)
                   .District(_searchCriteria.District)
                   .Area(_searchCriteria.AreaName)
                   .ListingAccess(_searchCriteria.Account, PropertyTypeConstants.Residential, _restrictByAccountType)
                   .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.Residential)
                   .Address(_searchCriteria.Address)
                   .Agent(_searchCriteria.AgentID, PropertyTypeConstants.Residential)
                   .IsRental(_searchCriteria, PropertyTypeConstants.Residential)
                   .CustomSearch(_searchCriteria.SearchText);

                var commercial = _service.GetAllCommercial()
                    .PriceLow(_searchCriteria.PriceLow)
                    .PriceHigh(_searchCriteria.PriceHigh)
                    .City(_searchCriteria.City)
                    .District(_searchCriteria.District)
                    .Area(_searchCriteria.AreaName)
                    .ListingAccess(_searchCriteria.Account, PropertyTypeConstants.Commercial, _restrictByAccountType)
                    .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.Commercial)
                    .Address(_searchCriteria.Address)
                    .Agent(_searchCriteria.AgentID, PropertyTypeConstants.Commercial)
                    .IsRental(_searchCriteria, PropertyTypeConstants.Commercial)
                    .CustomSearch(_searchCriteria.SearchText);

                var farmsandland = _service.GetAllFarmsAndLand()
                    .PriceLow(_searchCriteria.PriceLow)
                    .PriceHigh(_searchCriteria.PriceHigh)
                    .Bathrooms(_searchCriteria.Bathrooms)
                    .Bedrooms(_searchCriteria.Bedrooms)
                    .City(_searchCriteria.City)
                    .District(_searchCriteria.District)
                    .Area(_searchCriteria.AreaName)
                    .ListingAccess(_searchCriteria.Account, PropertyTypeConstants.FarmsAndLand, _restrictByAccountType)
                    .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.FarmsAndLand)
                    .Address(_searchCriteria.Address)
                    .Agent(_searchCriteria.AgentID, PropertyTypeConstants.FarmsAndLand)
                    .IsRental(_searchCriteria, PropertyTypeConstants.FarmsAndLand)
                    .CustomSearch(_searchCriteria.SearchText);

                var forlease = _service.GetAllForLease()
                    .PriceLow(_searchCriteria.PriceLow)
                    .PriceHigh(_searchCriteria.PriceHigh)
                    .City(_searchCriteria.City)
                    .District(_searchCriteria.District)
                    .Area(_searchCriteria.AreaName)
                    .ListingAccess(_searchCriteria.Account, PropertyTypeConstants.ForLease, _restrictByAccountType)
                    .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.ForLease)
                    .Address(_searchCriteria.Address)
                    .Agent(_searchCriteria.AgentID, PropertyTypeConstants.ForLease)
                    .IsRental(_searchCriteria, PropertyTypeConstants.ForLease)
                    .CustomSearch(_searchCriteria.SearchText);

                var multifamily = _service.GetAllMultiFamily()
                    .PriceLow(_searchCriteria.PriceLow)
                    .PriceHigh(_searchCriteria.PriceHigh)
                    .City(_searchCriteria.City)
                    .District(_searchCriteria.District)
                    .Area(_searchCriteria.AreaName)
                    .ListingAccess(_searchCriteria.Account, PropertyTypeConstants.MultiFamily, _restrictByAccountType)
                    .Options(_searchCriteria.Account, _searchCriteria, PropertyTypeConstants.MultiFamily)
                    .Address(_searchCriteria.Address)
                    .Agent(_searchCriteria.AgentID, PropertyTypeConstants.MultiFamily)
                    .IsRental(_searchCriteria, PropertyTypeConstants.MultiFamily)
                    .CustomSearch(_searchCriteria.SearchText);

                var exclusives = _service.GetAllExclusives(_searchCriteria.Account)
                    .PriceLow(_searchCriteria.PriceLow)
                    .PriceHigh(_searchCriteria.PriceHigh)
                    .City(_searchCriteria.City)
                    .District(_searchCriteria.District)
                    .Area(_searchCriteria.AreaName)
                    .Options(_searchCriteria.Account, _searchCriteria, ExclusivePropertyType.Exclusive)
                    .Address(_searchCriteria.Address)
                    .Status(_searchCriteria.Status)
                    .Agent(_searchCriteria.AgentID, ExclusivePropertyType.Exclusive)
                    .IsActive().IsRental(_searchCriteria, ExclusivePropertyType.Exclusive)
                    .CustomSearch(_searchCriteria.SearchText);

                var allListings = ExecuteListingQuery(residential, commercial, farmsandland, forlease, exclusives, multifamily);

                // include only applicable property types
                ApplyPropertyTypeFiltering(allListings,boardcode);

                // if no property types selected return all listings
                var listings = _filteredListings ?? allListings;

                // apply ordering
                return ApplyOrderBy(listings, _searchCriteria.Sort);
            }
        }

        private IEnumerable<Listing> ExecuteListingQuery(IEnumerable<Residential> residential,
            IEnumerable<Commercial> commercial,
            IEnumerable<FarmsAndLand> farmsandland,
            IEnumerable<ForLease> forlease,
            IEnumerable<Exclusive> exclusives,
            IEnumerable<MultiFam> multifamily)
        {
            // filter on type of listing this is the section
            // note that the mapping process is what actually performs the database query
            IEnumerable<Listing> allListings;
            switch (_searchCriteria.WidgetOptions)
            {
                case WidgetOptions.ResidentialListings:
                    allListings = Mapper.Map<IEnumerable<Residential>, IEnumerable<Listing>>(residential);
                    break;

                case WidgetOptions.CommercialListings:
                    allListings = Mapper.Map<IEnumerable<Commercial>, IEnumerable<Listing>>(commercial);
                    break;

                case WidgetOptions.FarmsAndLandListings:
                    allListings = Mapper.Map<IEnumerable<FarmsAndLand>, IEnumerable<Listing>>(farmsandland);
                    break;

                case WidgetOptions.ForLeaseListings:
                    allListings = Mapper.Map<IEnumerable<ForLease>, IEnumerable<Listing>>(forlease);
                    break;

                case WidgetOptions.ExclusiveListings:
                    allListings = Mapper.Map<IEnumerable<Exclusive>, IEnumerable<Listing>>(exclusives);
                    break;

                case WidgetOptions.MultiFamListings:
                    allListings = Mapper.Map<IEnumerable<MultiFam>, IEnumerable<Listing>>(multifamily);
                    break;

                default:
                    allListings = Mapper.Map<IEnumerable<Residential>, IEnumerable<Listing>>(residential)
                                        .Concat(Mapper.Map<IEnumerable<Commercial>, IEnumerable<Listing>>(commercial))
                                        .Concat(Mapper.Map<IEnumerable<FarmsAndLand>, IEnumerable<Listing>>(farmsandland))
                                        .Concat(Mapper.Map<IEnumerable<ForLease>, IEnumerable<Listing>>(forlease))
                                        .Concat(Mapper.Map<IEnumerable<MultiFam>, IEnumerable<Listing>>(multifamily))
                                        .Concat(Mapper.Map<IEnumerable<Exclusive>, IEnumerable<Listing>>(exclusives));
                    break;
            }
            return allListings;
        }

        private IEnumerable<Listing> VExecuteListingQuery(IEnumerable<VResidential> residential,
           IEnumerable<VCommercial> commercial, IEnumerable<Exclusive> exclusive)
        {
            if (residential != null)
            {
                residential = residential.ToList();
            }
            if (commercial != null)
            {
                commercial = commercial.ToList();
            }
            if (exclusive != null)
            {
                exclusive = exclusive.ToList();
            }
            // filter on type of listing this is the section
            // note that the mapping process is what actually performs the database query
            IEnumerable<Listing> allListings;
            switch (_searchCriteria.WidgetOptions)
            {
                case WidgetOptions.ResidentialListings:
                    allListings = Mapper.Map<IEnumerable<VResidential>, IEnumerable<Listing>>(residential);
                    break;

                case WidgetOptions.CommercialListings:
                    allListings = Mapper.Map<IEnumerable<VCommercial>, IEnumerable<Listing>>(commercial);
                    break;

                default:
                    allListings = Mapper.Map<IEnumerable<VResidential>, IEnumerable<Listing>>(residential)
                                        .Concat(Mapper.Map<IEnumerable<VCommercial>, IEnumerable<Listing>>(commercial))
                                        .Concat(Mapper.Map<IEnumerable<Exclusive>, IEnumerable<Listing>>(exclusive));
                    break;
            }
            return allListings;
        }

        private IEnumerable<Listing> CExecuteListingQuery(IEnumerable<CListing> residential, IEnumerable<Exclusive> exclusive)
        {
            if (residential != null)
            {
                residential = residential.ToList();
            }
            // filter on type of listing this is the section
            // note that the mapping process is what actually performs the database query
            IEnumerable<Listing> allListings;

            switch (_searchCriteria.WidgetOptions)
            {
                case WidgetOptions.ResidentialListings:
                    allListings = Mapper.Map<IEnumerable<CListing>, IEnumerable<Listing>>(residential);
                    break;

                default:
                    allListings = Mapper.Map<IEnumerable<CListing>, IEnumerable<Listing>>(residential)
                                        .Concat(Mapper.Map<IEnumerable<Exclusive>, IEnumerable<Listing>>(exclusive));
                    break;
            }
            return allListings;
        }

        /// <summary>
        /// Property types searches are inclusive. Create a collection of properties that met the previous filtering criteria.
        /// </summary>
        /// <param name="allListings">the filtered collection of listings</param>
        private void ApplyPropertyTypeFiltering(IEnumerable<Listing> allListings , string boardcode)
        {
            List<string> propertytype = new List<string>();
            if (_searchCriteria.Residential)
            {
                propertytype.Add(PropertyTypeConstants.Residential.ToString().ToLower());
                if (_service.GetAllCBoardID().Count(x => x.Name == boardcode) > 0)
                {
                    
                    propertytype.Add("attached");
                    propertytype.Add("detached");
                    propertytype.Add("apartment");
                    propertytype.Add("mobile");
                }
                if (boardcode != "VIREB")
                {
                    FilterListingsByPropertyType(allListings, propertytype, null);
                }
            }

            if (_searchCriteria.Commercial)
            {
                propertytype.Add(PropertyTypeConstants.Commercial.ToString().ToLower());
                FilterListingsByPropertyType(allListings, propertytype, null);
            }

            if (_searchCriteria.ForLease)
            {
                if (_service.GetAllCBoardID().Count(x => x.Name == boardcode) > 0)
                {
                    propertytype.Add("rental");
                }
                else
                {
                    propertytype.Add(PropertyTypeConstants.ForLease.ToString().ToLower());
                }
                FilterListingsByPropertyType(allListings, propertytype, null);
            }

            if (_searchCriteria.FarmsAndLand)
            {
                if (_service.GetAllCBoardID().Count(x => x.Name == boardcode) > 0)
                {
                    propertytype.Add("land");
                }
                else if (boardcode == "VIREB")
                {
                    propertytype.Add(ResidentialPropertyType.Fram.ToString().ToLower());
                    FilterListingsByPropertyType(allListings, propertytype, null);
                }
                else
                {
                    propertytype.Add(PropertyTypeConstants.FarmsAndLand.ToString().ToLower());
                }
                FilterListingsByPropertyType(allListings, propertytype, null);
            }

            if (_searchCriteria.MultiFam)
            {
                propertytype.Add(PropertyTypeConstants.MultiFamily.ToString().ToLower());
                FilterListingsByPropertyType(allListings, propertytype, null);
            }

            if (_searchCriteria.Retail)
            {
                propertytype.Add(CommercialPropertyType.Retail.ToString().ToLower());
                FilterListingsByPropertyType(allListings, propertytype, null);
            }

            if (_searchCriteria.Industrial)
            {
                propertytype.Add(CommercialPropertyType.Industrial.ToString().ToLower());
                FilterListingsByPropertyType(allListings, propertytype, null);
            }

            if (_searchCriteria.Office)
            {
                propertytype.Add(CommercialPropertyType.Office.ToString().ToLower());
                FilterListingsByPropertyType(allListings, propertytype, null);
            }

            if (_searchCriteria.Land)
            {
                propertytype.Add(CommercialPropertyType.Land.ToString().ToLower());
                FilterListingsByPropertyType(allListings, propertytype, null);
            }

            if (_searchCriteria.Acreage)
            {
                if (_service.GetAllCBoardID().Count(x => x.Name == boardcode) > 0)
                {
                    propertytype.Add("rural");
                    FilterListingsByPropertyType(allListings, propertytype, null);
                }
                else if (boardcode == "VIREB")
                {
                    propertytype.Add(ResidentialPropertyType.Lot.ToString().ToLower());
                    FilterListingsByPropertyType(allListings, propertytype, null);
                }
                else
                {
                    FilterListingsByPropertyType(allListings, null, TypeDwelling.Acreage);
                }
            }

            if (_searchCriteria.Condominium)
            {
                if (_service.GetAllCBoardID().Count(x => x.Name == boardcode) > 0)
                {
                    IEnumerable<Listing> listings = allListings.Where(x => x.CondoType != "Not a Condo");
                    _filteredListings = _filteredListings == null ? listings : _filteredListings.Union(listings);
                }
                else if (boardcode == "VIREB")
                {
                    propertytype.Add(ResidentialPropertyType.Condo.ToString().ToLower());
                    FilterListingsByPropertyType(allListings, propertytype, null);
                }
                else
                {
                    FilterListingsByPropertyType(allListings, null, TypeDwelling.Condo);
                }

            }

            if (_searchCriteria.Vacant)
            {
                FilterListingsByPropertyType(allListings, null, TypeDwelling.VacantLot);
            }

            if (_searchCriteria.Cottage)
            {
                FilterListingsByPropertyType(allListings, null, TypeDwelling.CottageRecreation);
            }

            if(_searchCriteria.SingleFamily)
            {
                propertytype.Add(ResidentialPropertyType.SingleFamily.ToString().ToLower());
                FilterListingsByPropertyType(allListings, propertytype, null);
            }

            if (_searchCriteria.HalfDuplex)
            {
                propertytype.Add(ResidentialPropertyType.HalfDuplex.ToString().ToLower());
                FilterListingsByPropertyType(allListings, propertytype, null);
            }

            if (_searchCriteria.FullDuplex)
            {
                propertytype.Add(ResidentialPropertyType.FullDuplex.ToString().ToLower());
                FilterListingsByPropertyType(allListings, propertytype, null);
            }

            if (_searchCriteria.Triplex)
            {
                propertytype.Add(ResidentialPropertyType.Triplex.ToString().ToLower());
                FilterListingsByPropertyType(allListings, propertytype, null);
            }

            if (_searchCriteria.Fourplex)
            {
                propertytype.Add(ResidentialPropertyType.Fourplex.ToString().ToLower());
                FilterListingsByPropertyType(allListings, propertytype, null);
            }

            if (_searchCriteria.Mobile)
            {
                propertytype.Add(ResidentialPropertyType.Mobile.ToString().ToLower());
                FilterListingsByPropertyType(allListings, propertytype, null);
            }

            if(!string.IsNullOrEmpty(_searchCriteria.FarmLandType))
            {
                IEnumerable<Listing> listings = allListings.Where(x => x.MajorType == _searchCriteria.FarmLandType);
                _filteredListings = _filteredListings == null ? listings : _filteredListings.Union(listings);
            }
        }

        /// <summary>
        /// Filters a collection of listings by the listings property type
        /// </summary>
        /// <param name="allListings">the collection of listing to be filtered</param>
        /// <param name="propertyType">the type of property</param>
        /// <param name="typeDwelling">the type of dwelling, applicable to condo / acreage listings which are subsets of residential / farmsandland</param>
        private void FilterListingsByPropertyType(IEnumerable<Listing> allListings, List<string> propertyType, string typeDwelling)
        {
            IEnumerable<Listing> listings = propertyType != null ?
                                    allListings.Where(x => propertyType.Contains(x.PropertyType.ToLower())) :
                                    (typeDwelling != TypeDwelling.Condo?
                                    allListings.Where(x => x.TypeDwelling != null && x.TypeDwelling.Contains(typeDwelling)) : allListings.Where(x => (x.TypeDwelling != null && x.TypeDwelling.Contains(typeDwelling) && x.DateEntered < new DateTime(2014, 10, 1)) || (x.OwnershipTitle == "Condominium")));

            _filteredListings = _filteredListings == null ? listings : _filteredListings.Union(listings);
        }

        /// <summary>
        /// Applly ordering to the listing result set.
        /// </summary>
        /// <param name="query">Query to be ordered</param>
        /// <param name="orderBy">The Order to apply</param>
        public static IEnumerable<Listing> ApplyOrderBy(IEnumerable<Listing> query, string orderBy)
        {

            switch (orderBy)
            {
                case "Address":
                    return query.OrderBy(x => x.Address);
                case "Lowest Price":
                    return query.OrderBy(x => x.ListPrice);
                case "Highest Price":
                    return query.OrderByDescending(x => x.ListPrice);
                default:
                    return query.OrderByDescending(x => x.MlsNumber).ThenByDescending(x => x.DateEntered);
            }
        }
    }
}