﻿using System.Web;
using Widgets.Models.ViewModels;

namespace Widgets.Helpers
{
    public static class ShareThis
    {
        public static string MakeUrl(Listing listing, HttpRequestBase request)
        {
            // setup the sharing url to be specific to the listing / request being made
            var sharethis = request.UrlReferrer.OriginalString;
            sharethis = sharethis + (request.UrlReferrer.Query == string.Empty ? "?" : "&");
            if (listing.ExclusiveID == null)
            {
                sharethis = sharethis + "property=" + listing.PropertyType + "&mlsnumber=" + listing.MlsNumber;
            }
            else
            {
                sharethis = sharethis + "property=Exclusive&listingid=" + listing.ExclusiveID;
            }
            return sharethis;
        }
    }
}