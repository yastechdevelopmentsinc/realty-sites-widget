﻿var _vdcopts = _vdcopts || {};

_vdcopts.ListingWidget = {
    // check if details page has been loaded
    checkDetails: function() {
        if (typeof _vdcopts.DetailedListing === 'undefined') {

        } else {
            clearInterval(_vdcopts.detailsLoaded);
            _vdcopts.DetailedListing.initialize();
        }
    },
    OpenDetailsWindow: function (jsonpUrl) {
        _vdcopts.jQuery.getJSON(jsonpUrl, function(data) {
            var ele = _vdcopts.jQuery('<div>').attr('id', 'vdc-listing-details')
                .html(data.html);
            ele.lightbox_me({
                centered: true,
                destroyOnClose: true,
                onLoad: function() {
                    _vdcopts.detailsLoaded = setInterval(function() {
                        _vdcopts.ListingWidget.checkDetails();
                    }, 100);
                }
            });
        });
    },
    autoOpenDetailsWindow: function() {
        // auto open details window
        var property = _vdcopts.getParameterByName('property');
        var mlsnumber = _vdcopts.getParameterByName('mlsnumber');
        var listingid = _vdcopts.getParameterByName('listingid');
        if (listingid != undefined && listingid != "") {
            mlsnumber = listingid;
        }
        if (property != undefined && property != "" && mlsnumber != undefined && mlsnumber != "") {
            var url = _vdcopts._defaultHostName + "/widget/detailedlisting?property=" + property + "&mlsnumber=" + mlsnumber + "&vdcsessionid=" + _vdcopts.SessionID + "&callback=?&accountkey=" + _lw._setAccount;
            _vdcopts.ListingWidget.OpenDetailsWindow(url);
        }
    },
    miniSearch: function (status, filter, sort) {
        var jsonpUrl = _vdcopts._defaultHostName + "/widget/SearchListings/?callback=?&accountkey=" + _lw._setAccount + "&SearchCriteria.SessionID=" + _vdcopts.SessionID + "&SearchCriteria." + filter + "=true&SearchCriteria.Sort=" + sort + "&SearchCriteria.Status=" + status;
        _vdcopts.jQuery.getJSON(jsonpUrl, function(data) {
            _vdcopts.jQuery('#vdc-listing-container').html(data.html);
        });
    },
    initBindings: function () {
        _vdcopts.jQuery('#vdc-listing-container .moredetails:not(.bound), .listing-image img:not(.bound)').addClass('bound').on("click", function (event) {
            event.preventDefault();
            var jsonpUrl = _vdcopts._defaultHostName + "/widget/detailedlisting?property=" + _vdcopts.jQuery(this).data('property') + "&mlsnumber=" + _vdcopts.jQuery(this).data('mlsnumber') + "&vdcsessionid=" + _vdcopts.SessionID + "&callback=?&accountkey=" + _lw._setAccount;
            _vdcopts.ListingWidget.OpenDetailsWindow(jsonpUrl);
        });

        _vdcopts.jQuery('#MiniSearch_Status').on('change', function () {
            _vdcopts.ListingWidget.miniSearch(_vdcopts.jQuery('#MiniSearch_Status').val(), _vdcopts.jQuery('#MiniSearch_Filter').val(), _vdcopts.jQuery('#MiniSearch_Sort').val());
        });

        _vdcopts.jQuery('#MiniSearch_Filter').on('change', function () {
            _vdcopts.ListingWidget.miniSearch(_vdcopts.jQuery('#MiniSearch_Status').val(), _vdcopts.jQuery('#MiniSearch_Filter').val(), _vdcopts.jQuery('#MiniSearch_Sort').val());
        });

        _vdcopts.jQuery('#MiniSearch_Sort').on('change', function () {
            _vdcopts.ListingWidget.miniSearch(_vdcopts.jQuery('#MiniSearch_Status').val(), _vdcopts.jQuery('#MiniSearch_Filter').val(), _vdcopts.jQuery('#MiniSearch_Sort').val());
        });

        _vdcopts.jQuery('a[data-url]').on("click", function (event) {
            event.preventDefault();
            var widgetOptions = _vdcopts.getWidgetOptionsByName('_listingwidget');
            if (widgetOptions === null) {
                widgetOptions = _vdcopts.getWidgetOptionsByName('_listingsearch');
            }
            if (widgetOptions === null) {
                widgetOptions = _vdcopts.getWidgetOptionsByName('_customsearchwidget');
            }
            var jsonpUrl = _vdcopts._defaultHostName + "/widget/displaylistings/" + _vdcopts.jQuery(this).data('url') + "?callback=?&accountkey=" + _lw._setAccount + "&sessionid=" + _vdcopts.SessionID;
            _vdcopts.jQuery.getJSON(jsonpUrl, widgetOptions, function (data) {
                _vdcopts.jQuery('#vdc-listing-container').html(data.html);
            });
        });
    }
};

(function () {
    _vdcopts.ListingWidget.autoOpenDetailsWindow();
    _vdcopts.ListingWidget.initBindings();
})();