﻿_vdcopts.jQuery('#vdc-mortgagecalculator-container form').submit(function (event) {
    var jsonpurl = _vdcopts._defaultHostName + "/widget/calculate/?callback=?&accountkey=" + _lw._setAccount;
    _vdcopts.jQuery.getJSON(jsonpurl, _vdcopts.jQuery(this).serializeArray(), function (data) {
        _vdcopts.jQuery('#vdc-mortgagecalculator-container').html(data.html);
    });
    
    event.preventDefault();
    return false;
});

_vdcopts.jQuery('#vdc-mortgagecalculator-container form input:button').on('click', function() {
    _vdcopts.jQuery('#vdc-mortgagecalculator-container form').submit();
})