﻿var _vdcopts = _vdcopts || {};

_vdcopts.FeaturedListings = {
    OpenDetailsWindow: function (jsonpUrl) {
        _vdcopts.jQuery.getJSON(jsonpUrl, function(data) {
            var ele = _vdcopts.jQuery('<div>').attr('id', 'vdc-listing-details')
                .html(data.html);
            ele.lightbox_me({
                centered: true,
                destroyOnClose: true,
                onLoad: function () {
                    _vdcopts.detailsLoaded = setInterval(function () {
                        _vdcopts.FeaturedListings.checkDetails();
                    }, 100);
                }
            });
        });
    },
    
    // check if details page has been loaded
    checkDetails: function () {
        if (typeof _vdcopts.DetailedListing === 'undefined') {

        } else {
            clearInterval(_vdcopts.detailsLoaded);
            _vdcopts.DetailedListing.initialize();
        }
    },
    
    initBindings: function() {
        _vdcopts.jQuery('#vdc-featuredlisting .moredetails:not(.bound), #vdc-featuredlisting .listing-image:not(.bound), #vdc-featuredlistings .moredetails:not(.bound), #vdc-featuredlistings .listing-image:not(.bound)').addClass('bound').on("click", function (event) {
            event.preventDefault();
            var jsonpUrl = _vdcopts._defaultHostName + "/widget/detailedlisting?property=" + _vdcopts.jQuery(this).data('property') + "&mlsnumber= " + _vdcopts.jQuery(this).data('mlsnumber') + "&callback=?&accountkey=" + _lw._setAccount;
            _vdcopts.FeaturedListings.OpenDetailsWindow(jsonpUrl);
        });
    }
};

(function () {
    _vdcopts.FeaturedListings.initBindings();
})();