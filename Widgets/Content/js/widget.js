﻿var _vdcopts = _vdcopts || {};

_vdcopts.getParameterByName = function(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
};

_vdcopts.getWidgetOptionsByName = function (name) {
    for (var item in _lw._widgets) {
        if (_lw._widgets[item].type === name) {
            return _lw._widgets[item].options;
        }
    }
    return null;
};

_vdcopts.controls = [];

(function () {

    /******** Load jQuery if not present *********/
    if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.7.2') {
        var scriptTag = document.createElement('script');
        scriptTag.setAttribute("type", "text/javascript");
        scriptTag.setAttribute("src", "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js");
        if (scriptTag.readyState) {
            scriptTag.onreadystatechange = function () { // For old versions of IE
                if (this.readyState == 'complete' || this.readyState == 'loaded') {
                    scriptLoadHandler();
                }
            };
        } else {
            scriptTag.onload = scriptLoadHandler;
        }
        // Try to find the head, otherwise default to the documentElement
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(scriptTag);
    } else {
        // The jQuery version on the window is the one we want to use
        _vdcopts.jQuery = window.jQuery;
        main();
    }

    /******** Called once jQuery has loaded ******/
    function scriptLoadHandler() {
        // Restore $ and window.jQuery to their previous values and store the
        // new jQuery in our local jQuery variable
        _vdcopts.jQuery = window.jQuery.noConflict(true);
        // Call our main function
        main();
    }

    /******** Our main function ********/
    function main() {
        /******* Load CSS *******/
        var cssLink = _vdcopts.jQuery("<link>", {
            rel: "stylesheet",
            type: "text/css",
            href: _vdcopts._css
        });
        cssLink.appendTo('head');
        /******* Load HTML *******/
        for (var widget in _lw._widgets) {
             switch(_lw._widgets[widget].type) {
                case '_listingwidget':
                    loadListingWidget(_lw._widgets[widget].options);
                    break;            
        
                case '_listingsearch':
                    loadSearchWidget(_lw._widgets[widget].options);
                    break;
        
                case '_listingmapsearch':
                    loadMapSearchWidget(_lw._widgets[widget].options);
                    break;            
        
                case '_featuredlistingwidget':
                    loadFeaturedListingWidget(_lw._widgets[widget].options);
                    break;
                    
                case '_featuredagentwidget':
                    loadFeaturedAgentWidget(_lw._widgets[widget].options);
                    break;
        
                case '_memberswidget':
                    loadMembersWidget(_lw._widgets[widget].options);
                    break;            
        
                case '_contactswidget':
                    loadContactsWidget();
                    break;            
        
                case '_newswidget':
                    loadNewsWidget(_lw._widgets[widget].options);
                    break;            
        
                case '_mortgagecalculator':
                    loadMortgageCalculatorWidget();
                    break;
                    
                case '_latestlistingwidget':
                    loadLatestListingWidget(_lw._widgets[widget].options);
                    break;
                     
                 case '_agentprofilewidget':
                     loadAgentProfileWidget();
                     break;
                 case '_customsearchwidget': //new widget
                     loadCustomSearchWidget(_lw._widgets[widget].options);
                     break;
                 case '_nearmesearch':
                     loadNearMeSearch(_lw._widgets[widget].options);
                     break;
                 case '_inpagelistingdetails':
                     loadListingDetails(_lw._widgets[widget].options);
                     break;
            }   
        }
    }

    function loadListingWidget(options) {
        var agentOptions = { 'agentId': _vdcopts.getParameterByName('agentId') };
        _vdcopts.jQuery.extend(options, agentOptions);
        var jsonpUrl = _vdcopts._listingwidget + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, options, function (data) {
            _vdcopts.jQuery('#vdc-listing-container').html(data.html);
        });
    }

    function loadSearchWidget(options) {
        var opts = options || {};
        opts['sessionid'] = _vdcopts.getParameterByName('vdcsessionid');
        var jsonpUrl = _vdcopts._listingsearch + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, opts, function (data) {
            _vdcopts.jQuery('#vdc-search-container').html(data.html);
        });
    }

    function loadMapSearchWidget(options) {
        var opts = options || {};
        if (!opts.hasOwnProperty('loadsearch') || opts['loadsearch'] == true) {
            if (_vdcopts.jQuery('#vdc-search-container').length > 0) {
                var searchOpts = {};
                if (opts.hasOwnProperty('listings')) {
                    searchOpts['listings'] = opts['listings'];
                }
                if (!opts.hasOwnProperty('searchwidgetid') || opts['searchwidgetid'] == 1) {
                    loadSearchWidget(opts);
                }
                else {
                    loadCustomSearchWidget(opts);
                }
            }
        }
        var mapSearchOpts = {};
        if (opts.hasOwnProperty('latitude')) {
            mapSearchOpts['latitude'] = opts['latitude'];
            mapSearchOpts['longitude'] = opts['longitude'];
        }
        if (_vdcopts.getParameterByName('isswitch') != "") {
            mapSearchOpts['isswitch'] = _vdcopts.getParameterByName('isswitch');
        }
        var jsonpUrl = _vdcopts._listingmapsearch + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, mapSearchOpts, function (data) {
            _vdcopts.jQuery('#vdc-listing-container').html(data.html);
        });
    }

    function loadFeaturedListingWidget(options) {
        var jsonpUrl = _vdcopts._featuredlistingwidget + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, options, function (data) {
            if (options.isjson == true) {
                _vdcopts.jQuery('#vdc-featuredlisting-container-json').val(data.html);
            }
            else {
                _vdcopts.jQuery('#vdc-featuredlisting-container').html(data.html);
            }
        });
    }
    
    function loadFeaturedAgentWidget(options) {
        var jsonpUrl = _vdcopts._featuredagentwidget + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, options, function (data) {
            _vdcopts.jQuery('#vdc-featuredagent-container').html(data.html);
        });
    }

    function loadMembersWidget(options) {
        var jsonpUrl = _vdcopts._memberswidget + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, options, function (data) {
            _vdcopts.jQuery('#vdc-members-container').html(data.html);
        });
    }    
    
    function loadContactsWidget() {
        var jsonpUrl = _vdcopts._contactswidget + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, function (data) {
            _vdcopts.jQuery('#vdc-contacts-container').html(data.html);
        });
    }

    function loadNewsWidget(options) {
        var jsonpUrl = _vdcopts._newswidget + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, options, function (data) {
            _vdcopts.jQuery('#vdc-news-container').html(data.html);
        });
    }
    
    function loadMortgageCalculatorWidget() {
        var jsonpUrl = _vdcopts._mortgagecalculator + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, function (data) {
            _vdcopts.jQuery('#vdc-mortgagecalculator-container').html(data.html);
        });
    }
    
    function loadLatestListingWidget(options) {
        var jsonpUrl = _vdcopts._latestlistingwidget + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, options, function (data) {
            _vdcopts.jQuery('#vdc-latestlisting-container').html(data.html);
        });
    }
    
    function loadAgentProfileWidget() {
        var options = {'agentId': _vdcopts.getParameterByName('agentId')};
        var jsonpUrl = _vdcopts._agentprofilewidget + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, options, function (data) {
            _vdcopts.jQuery('#vdc-agentprofile-container').html(data.html);
        });
    }

    function loadCustomSearchWidget(options) {
        var opts = options || {};
        opts['sessionid'] = _vdcopts.getParameterByName('vdcsessionid');
        if (_vdcopts.getParameterByName('isswitch') != "") {
            opts['isswitch'] = _vdcopts.getParameterByName('isswitch');
        }
        var controlid = '#vdc-search-container'
        if (opts['controlid'] != undefined && opts['controlid'].length > 0)
        {
            controlid = '#'+ opts['controlid'];
        }
        var jsonpUrl = _vdcopts._customsearchwidget + _lw._setAccount;
        _vdcopts.controls.push(controlid);
        _vdcopts.jQuery.getJSON(jsonpUrl, opts, function (data) {
            _vdcopts.jQuery(controlid).html(data.html);
        });
    }

    function loadNearMeSearch(options) {
       
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showNearMe, showNearError);
        } else {
            showNearMe(null);
        }
    }

    function showNearError(error) {
        showNearMe(null);
    }
    function showNearMe(position) {
        var islocated = true;
        var latitude = null;
        var longitude = null;
        if (position != null) {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
        }
        else
        {
            islocated = false;
        }
        var mapSearchOpts = {};
        mapSearchOpts['latitude'] = latitude;
        mapSearchOpts['longitude'] = longitude;
        mapSearchOpts['islocated'] = islocated;
        var jsonpUrl = _vdcopts._nearmesearch + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, mapSearchOpts, function (data) {
            _vdcopts.jQuery('#vdc-listing-container').html(data.html);
        });
    }

    function loadListingDetails(options)
    {
        options['vdcsessionid'] = _vdcopts.getParameterByName('vdcsessionid');
        var jsonpUrl = _vdcopts._inpagelistingdetails + _lw._setAccount;
        _vdcopts.jQuery.getJSON(jsonpUrl, options, function (data) {
            _vdcopts.jQuery('#vdc-listing-details').html(data.html);
        });
    }
})();