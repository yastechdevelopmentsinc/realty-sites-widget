﻿var VDC = (VDC || {});

VDC.Exclusives = function(options) {
    var self = this;
    var map;
    var markers = [];
    
    self.init = function () {
        var defaults = {
            loadSubAreaUrl: null,
            latitude: null,
            longitude: null,intlatitude: null,intlongitude: null
        };

        if (typeof options === 'object') {
            self.options = $.extend(defaults, options);
        } else {
            throw 'The options argument is mandatory.';
        }

        self.initBindings();
        self.loadSubArea($('#Exclusive_AreaCode').val());
        self.initMaps();
    };

    self.initBindings = function() {
        $('#Exclusive_AreaCode').change(function (e) {
            self.loadSubArea($(this).val());
        });
        $('#Exclusive_DistrictCode').change(function (e) {
            self.loadOtherArea($(this).val());
        });
        $('#map-address').on('click', function(e) {
            e.preventDefault();
            var address = $('#Exclusive_Address').val();
            var city = $('#Exclusive_City').val();
            self.geoCodeAddress(address + ', ' + city);
        });
    };

    self.loadSubArea = function (areaCode) {
        if (areaCode.length > 0) {
            var url = self.options.loadSubAreaUrl;
            $.getJSON(url, { areacode: areaCode }, function (data) {
                $('#Exclusive_DistrictCode').empty();
                $.each(data, function (index, value) {
                    var option = new Option(value.AreaName, value.AreaID);
                    $('#Exclusive_DistrictCode').append(option);
                });
            }).complete(function () {
                $('#Exclusive_DistrictCode').val($('#TempDistrictCode').val());
                if (areaCode == '-1') {
                    $('#otherarea').css('display', 'block');
                    $('#othersubarea').css('display', 'block');
                }
                else {
                    $('#otherarea').css('display', 'none');
                    if ($('#Exclusive_DistrictCode').val() != '-1') {
                        $('#othersubarea').css('display', 'none');
                    }
                    else {
                        $('#othersubarea').css('display', 'block');
                    }
                }
            });
        }
        else {
            $('#otherarea').css('display', 'none');
            $('#othersubarea').css('display', 'none');
        }
    };

    self.loadOtherArea = function (areaCode) {
        if (areaCode.length > 0) {
            if (areaCode == '-1') {
                $('#othersubarea').css('display', 'block');
            }
            else {
                $('#othersubarea').css('display', 'none');
            }
        }
        else {
            $('#othersubarea').css('display', 'none');
        }
    };

    self.initMaps = function() {
        var myOptions = {
            zoom: 2,
            center: new google.maps.LatLng(self.options.intlatitude, self.options.intlongitude),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map-search"), myOptions);

        if (!self.options.latitude || !self.options.longitude) {
            self.placeMarker(self.options.intlatitude, self.options.intlongitude);
        } else {
            self.placeMarker(self.options.latitude, self.options.longitude);
        }
    };

    self.updateFormFields = function(latitude, longitude) {
        $('#Exclusive_Latitude').val(latitude);
        $('#Exclusive_Longitude').val(longitude);
    };

    self.placeMarker = function (latitude, longitude) {
        self.clearMarkers();

        var location = new google.maps.LatLng(latitude, longitude);
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            draggable: true
        });
        markers.push(marker);

        map.setCenter(location);
        if (latitude != 0 && longitude != 0) {
            map.setZoom(10);
        }

        google.maps.event.addListener(marker, 'drag', function (e) {
            var position = marker.getPosition();
            self.updateFormFields(position.lat(), position.lng());
        });
    };

    self.clearMarkers = function() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
        $('.success').remove();
    };
    
    self.geoCodeAddress = function (address) {
        var geoCoder = new google.maps.Geocoder();
        geoCoder.geocode({ 'address': address }, function (results, status) {
            if (status === 'OK') {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                self.clearMarkers();
                self.placeMarker(latitude, longitude);
                self.updateFormFields(latitude, longitude);
               
                $('#lb_map_address_error').html("");
            }
            else {
                $('#lb_map_address_error').html("We couldn't map this address.");
            }
        });
    };

    
    self.init();
};