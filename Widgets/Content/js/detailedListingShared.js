﻿// load mortgage calculator
_vdcopts.jQuery('#vdc-calculator-link a').on('click', function (e) {
    e.preventDefault();
    // remove calculator if already previously added to dom
    _vdcopts.jQuery('.vdc-calculator-popup').remove();
     
    var jsonpUrl = _vdcopts._mortgagecalculator + _lw._setAccount + "&principal=" + _vdcopts.principal;
    _vdcopts.jQuery.getJSON(jsonpUrl, function(data) {

        // create calculator container
        var calculatorContainer = _vdcopts.jQuery('<div>').attr('id', 'vdc-mortgagecalculator-container')
            .html(data.html);

        // create popup container
        var ele = _vdcopts.jQuery('<div>').attr('id', 'popup')
            .addClass('vdc-calculator-popup')
            .html(calculatorContainer);

        // add close link to popup
        var closelink = _vdcopts.jQuery('<a>')
            .attr('href', '#')
            .addClass('close')
            .html('close');

        ele.prepend(closelink);
        ele.lightbox_me({
            destroyOnClose: true,
            zIndex: 10000,
            onLoad: function() {
                _vdcopts.jQuery('#vdc-mortgagecalculator-container form input:button').click();
            }
        });
    });
});

// show link to listing pop up
_vdcopts.jQuery('#vdc-listing-link #vdc-link-open').on('click', function (e) {
    e.preventDefault();
    _vdcopts.jQuery('#vdc-link').show();
    _vdcopts.jQuery('#vdc-link input').select();
});

// close link to listing pop up
_vdcopts.jQuery('#vdc-listing-link #vdc-link-close').on('click', function (e) {
    e.preventDefault();

        _vdcopts.jQuery('#vdc-link').hide();
});

_vdcopts.jQuery('#vdc-closepopup').on('click', function (e) {
    if (document.URL.indexOf("listingid") != -1) {
        window.location = "https://"+ window.location.hostname + window.location.pathname;
    }
});


// print listing
_vdcopts.jQuery('#vdc-listing-print a').on('click', function (e) {
    e.preventDefault();
    _vdcopts.jQuery("#vdc-listing-details").jqprint({printContainer: false});
});

// load email form
_vdcopts.jQuery('.vdc-listing-agent-email a, #vdc-listing-brokerage-email a, #vdc-simplelisting-agent-email a').on('click', function (e) {
    e.preventDefault();
    _vdcopts.EmailListing = false;
    var jsonpUrl = _vdcopts._emailForm + _vdcopts.jQuery(this).data('agentid') + "?callback=?&propertyType=" + _vdcopts.jQuery(this).data('property') + "&listingid=" + _vdcopts.jQuery(this).data('listingid') + "&officeid=" + _vdcopts.jQuery(this).data('officeid') + "&accountkey=" + _lw._setAccount;
    _vdcopts.jQuery.getJSON(jsonpUrl, function (data) {
        var ele = _vdcopts.jQuery('<div>').attr('id', 'popup')
            .addClass('vdc-popup')
            .html(data.html);
        ele.lightbox_me({
            destroyOnClose: true,
        });
    });
});

// load email listing form
_vdcopts.jQuery('#vdc-email-listing a').on('click', function (e) {
    e.preventDefault();
    _vdcopts.EmailListing = true;
    var jsonpUrl = _vdcopts._emailListingForm + "?callback=?&propertyType=" + _vdcopts.jQuery(this).data('property') + "&listingId=" + _vdcopts.jQuery(this).data('listingid') + "&accountkey=" + _lw._setAccount;
    _vdcopts.jQuery.getJSON(jsonpUrl, function (data) {
        var ele = _vdcopts.jQuery('<div>').attr('id', 'popup')
            .addClass('vdc-popup')
            .html(data.html);
        ele.lightbox_me({
            destroyOnClose: true,
        });
    });
});