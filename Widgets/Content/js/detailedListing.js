﻿var _vdcopts = _vdcopts || {};

_vdcopts.DetailedListing = {

    // append the google maps script to the dom if the map has not previously been initialized
    loadScripts: function () {
        if (typeof _vdcopts.map === 'undefined') {
            var url = "https://maps.googleapis.com/maps/api/js?key=" + _vdcopts.GoogleApiKey + "&sensor=false&callback=_vdcopts.GoogleMaps.initMaps";
            var mapscript = document.createElement("script");
            mapscript.type = "text/javascript";
            mapscript.src = url;
            document.body.appendChild(mapscript);
        } else {
            if (_vdcopts.GoogleMaps != undefined) {
                _vdcopts.GoogleMaps.initMaps();
            }
        }

        // setup share this scripts
        if (typeof stLight === 'undefined') {
            var stlightscript = document.createElement("script");
            stlightscript.type = "text/javascript";
            stlightscript.src = "http://w.sharethis.com/button/buttons.js";
            (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(stlightscript);
        }
    },

    // wait until scripts have been loaded and then enable 
    initialize: function () {
        _vdcopts.galleriaLoaded = setInterval(function () {
            _vdcopts.DetailedListing.checkGalleria();
        }, 100);

        _vdcopts.stLightLoaded = setInterval(function () {
            _vdcopts.DetailedListing.checkstlight();
        }, 100);
    },

    // check if galleria scripts have been loaded
    checkGalleria: function () {
        if (typeof Galleria === 'undefined') {

        } else {
            clearInterval(_vdcopts.galleriaLoaded);
            this.initGalleria();
        }
    },

    // check if share this scripts have been loaded
    checkstlight: function () {
        if (typeof stLight === 'undefined') {

        } else {
            clearInterval(_vdcopts.stLightLoaded);
            var switchTo5x = false;
            stLight.options({ publisher: "ur-ae4e32fd-1eb6-c01c-b37d-b07d781f6d5c" });
            stButtons.locateElements();
        }
    },

    // init galleria
    initGalleria: function () {
        if (typeof Galleria.theme === 'undefined') {
            var url = _vdcopts._defaultHostName + '/scripts/galleria/themes/classic/galleria.classic.min.js?v1' ;
            Galleria.loadTheme(url);
        }

        Galleria.run('#vdc-listing-gallery-galleria', {
            debug: false,
            showInfo: false,
            responsive: true,
            height: 400,
            dummy: _vdcopts._defaultHostName + '/Content/images/noimageavailable.jpg'
        });

        _vdcopts.jQuery('#vdc-listing-gallery-galleria').show();
        _vdcopts.jQuery("#vdc-listing-details").ready(function () {
            try
            {
                Fix_Header();
            }
            catch(erro){}
        });
    }
};

(function() {
    _vdcopts.DetailedListing.loadScripts();
   
})();