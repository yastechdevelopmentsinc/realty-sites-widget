﻿var _vdcopts = _vdcopts || {};
var inload = false;
_vdcopts.SearchWidget = {
    loadScripts: function () {
        var jqueryui = document.createElement("script");
        jqueryui.type = "text/javascript";
        jqueryui.src = _vdcopts._defaultHostName + "/scripts/jqueryui/jquery-ui.min.js";
        document.getElementsByTagName("head")[0].appendChild(jqueryui);

        var jqueryuicss = document.createElement("link");
        jqueryuicss.type = "text/css";
        jqueryuicss.rel = "stylesheet";
        jqueryuicss.href = _vdcopts._defaultHostName + "/scripts/jqueryui/jquery-ui.css";
        document.getElementsByTagName("head")[0].appendChild(jqueryuicss);
    },
    initBindings: function () {

        _vdcopts.jQuery(function () {
            var jsonpUrl = _vdcopts._defaultHostName + "/widget/Get_AutoComplete_List/?callback=?&accountkey=" + _lw._setAccount;
            _vdcopts.jQuery.getJSON(jsonpUrl, null, function (data) {
                for (var i = 0; i < _vdcopts.controls.length; i++) {
                    _vdcopts.jQuery(_vdcopts.controls[i] + " #SearchCriteria_SearchText").autocomplete({
                        source: data, minLength: 2
                    });
                }
            });
            
        });
      
        for (var i = 0; i < _vdcopts.controls.length; i++) {
            // submit search form
            _vdcopts.jQuery(_vdcopts.controls[i] +' form').submit(function (event) {
                event.preventDefault();
                if (inload) {
                    SubmitSearch(false, this);
                    inload = false
                }
                else {
                    SubmitSearch(true, this);
                }

            });

            // when city changes populate sub areas
            _vdcopts.jQuery(_vdcopts.controls[i] + ' #SearchCriteria_City').on('change', { controlid: _vdcopts.controls[i] }, function (event) {
                Disable_DropDowns(event.data.controlid);
                var boardcode = ""
                var jsonpUrl = _vdcopts._defaultHostName + "/widget/Get_BoardCode/?callback=?&accountkey=" + _lw._setAccount;
                _vdcopts.jQuery.getJSON(jsonpUrl, null, function (data) {
                    boardcode = data;
                });

                var jsonpUrl = _vdcopts._defaultHostName + "/widget/populatedistricts/?callback=?&accountkey=" + _lw._setAccount;
                var listingtype = "all";
                if (_vdcopts.getWidgetOptionsByName('_customsearchwidget') != null) {
                    listingtype = _vdcopts.getWidgetOptionsByName('_customsearchwidget').listings;
                }
                else if (_vdcopts.getWidgetOptionsByName('_listingmapsearch') != null) {
                    listingtype = _vdcopts.getWidgetOptionsByName('_listingmapsearch').listings;
                }
                _vdcopts.jQuery.getJSON(jsonpUrl, { city: _vdcopts.jQuery(event.data.controlid + " #SearchCriteria_City").val(), listings: listingtype }, function (data) {
                    _vdcopts.jQuery(event.data.controlid + " #SearchCriteria_District").empty();
                    var option = new Option("area", "");
                    _vdcopts.jQuery(event.data.controlid + " #SearchCriteria_District").append(option);
                    _vdcopts.jQuery.each(data.districtlist, function (index, optionData) {
                        var areaname;
                        if (optionData.Listings === 0) {
                            if (boardcode.BoardCode != "VIREB") {
                                if (optionData.AreaName.length > 3 && optionData.AreaName[2] == ' ') {

                                    areaname = optionData.AreaName.substring(2)
                                }
                                else {
                                    areaname = optionData.AreaName
                                }
                            }
                            else {
                                areaname = optionData.AreaName;
                            }

                        } else {
                            if (boardcode.BoardCode != "VIREB") {
                                if (optionData.AreaName.length > 3 && optionData.AreaName[2] == ' ') {

                                    areaname = optionData.AreaName.substring(2) + " (" + optionData.Listings + ")"
                                }
                                else {
                                    areaname = optionData.AreaName + " (" + optionData.Listings + ")"
                                }
                            }
                            else {
                                areaname = optionData.AreaName + " (" + optionData.Listings + ")";
                            }
                            //areaname = optionData.AreaName ;

                        }
                        var option = new Option(areaname, optionData.AreaName);
                        _vdcopts.jQuery(event.data.controlid + " #SearchCriteria_District").append(option);
                    });
                    Get_Locations(event.data.controlid);
                    //Enable_DropDowns();
                });
            });

            // when district/area changes populate sub areas
            _vdcopts.jQuery(_vdcopts.controls[i] + ' #SearchCriteria_District').on('change',{controlid:_vdcopts.controls[i]}, function (event) { Get_Locations(event.data.controlid); });
        }

        function SubmitSearch (isclick,form) {
            
            var isswitch = false;
            var switchsessionid = "";
            if (_vdcopts.getParameterByName('isswitch') != "" && isclick == false) {
                isswitch = _vdcopts.getParameterByName('isswitch');
                if (_vdcopts.getParameterByName('vdcsessionid') != "") {
                    switchsessionid = _vdcopts.getParameterByName('vdcsessionid')
                }
            }

            var widgettype = ""
            if (_vdcopts.getWidgetOptionsByName('_listingmapsearch') != null) {
                widgettype = '_listingmapsearch';
            }
            else if (_vdcopts.getWidgetOptionsByName('_customsearchwidget') != null) {
                widgettype = '_customsearchwidget';
            }
            var jsonpUrl = _vdcopts._defaultHostName + "/widget/SearchListings/?callback=?&accountkey=" + _lw._setAccount + "&isswitch=" + isswitch + "&widgettype=" + widgettype + "&switchsessionid=" + switchsessionid;
            _vdcopts.jQuery.getJSON(jsonpUrl, _vdcopts.jQuery(form).serializeArray(), function (data) {
                if (data.action === "RefreshMap" && _vdcopts.MapSearch != undefined) {
                    _vdcopts.MapSearch.refreshMap();
                }
                else if (data.action === "Redirect") {
                    window.location = data.url;
                }
                else {
                    _vdcopts.jQuery('#vdc-listing-container').html(data.html);
                }
            });

            var mlsSearch = _vdcopts.jQuery('#SearchCriteria_MlsNumber');
            if (mlsSearch) {
                mlsSearch.val('');
            }

            return false;
        }

        function Get_Locations(controlid)
        {
            Disable_DropDowns(controlid);
            var jsonpUrl = _vdcopts._defaultHostName + "/widget/populatelocations/?callback=?&accountkey=" + _lw._setAccount;
            var listingtype = "all";
            if (_vdcopts.getWidgetOptionsByName('_customsearchwidget') != null) {
                listingtype = _vdcopts.getWidgetOptionsByName('_customsearchwidget').listings;
            }
            else if (_vdcopts.getWidgetOptionsByName('_listingmapsearch') != null) {
                listingtype = _vdcopts.getWidgetOptionsByName('_listingmapsearch').listings;
            }
            _vdcopts.jQuery.getJSON(jsonpUrl, { city: _vdcopts.jQuery(controlid + " #SearchCriteria_City").val(), district: _vdcopts.jQuery(controlid +" #SearchCriteria_District").val(), listings: listingtype }, function (data) {
                _vdcopts.jQuery(controlid + " #SearchCriteria_AreaName").empty();
                var option = new Option("neighborhood", "");
                _vdcopts.jQuery(controlid +" #SearchCriteria_AreaName").append(option);
                _vdcopts.jQuery.each(data.filteredLocations, function (index, optionData) {
                    var areaname;
                    if (optionData.Listings === 0) {
                        areaname = optionData.AreaName;

                    } else {
                        //areaname = optionData.AreaName ;
                        areaname = optionData.AreaName + " (" + optionData.Listings + ")";
                    }
                    var option = new Option(areaname, optionData.AreaName);
                    _vdcopts.jQuery(controlid + " #SearchCriteria_AreaName").append(option);
                });
                Enable_DropDowns(controlid);
            });
            
        }

        function Disable_DropDowns(controlid)
        {
            _vdcopts.jQuery(controlid+" .loadingimg").addClass("enabled")
            _vdcopts.jQuery(controlid+' #SearchCriteria_City').css("opacity", 0.9);
            _vdcopts.jQuery(controlid+' #SearchCriteria_District').css("opacity", 0.9);
            _vdcopts.jQuery(controlid+' #SearchCriteria_AreaName').css("opacity", 0.9);
            _vdcopts.jQuery(controlid+' #SearchCriteria_City').attr("disabled", "true");
            _vdcopts.jQuery(controlid+' #SearchCriteria_District').attr("disabled", "true");
            _vdcopts.jQuery(controlid+' #SearchCriteria_AreaName').attr("disabled", "true");
            _vdcopts.jQuery(controlid+" .loadingdiv").addClass("disabled")
        }

        function Enable_DropDowns(controlid) {
            _vdcopts.jQuery(controlid+" .loadingimg").removeClass("enabled")
            _vdcopts.jQuery(controlid+' #SearchCriteria_City').css("opacity", 1);
            _vdcopts.jQuery(controlid+' #SearchCriteria_District').css("opacity", 1);
            _vdcopts.jQuery(controlid+' #SearchCriteria_AreaName').css("opacity", 1);
            _vdcopts.jQuery(controlid+' #SearchCriteria_City').removeAttr("disabled");
            _vdcopts.jQuery(controlid+' #SearchCriteria_District').removeAttr("disabled");
            _vdcopts.jQuery(controlid+' #SearchCriteria_AreaName').removeAttr("disabled");
            _vdcopts.jQuery(controlid+" .loadingdiv").removeClass("disabled")
            try
            {
                Fix_Select();
            }
            catch(err){}
        }

        // if open house search setup the datepicker
        if (_vdcopts.jQuery('#SearchCriteria_DateFrom').length > 0) {
            setTimeout(function () {
                _vdcopts.jQuery('#SearchCriteria_DateFrom, #SearchCriteria_DateTo').datepicker();
            }, 500);
        }

        // if the map search is one of the widgets open set the widget type
        for (var item in _lw._widgets) {
            if (_lw._widgets[item].type === '_listingmapsearch' && (!_lw._widgets[item].options.hasOwnProperty('loadsearch') || _lw._widgets[item].options['loadsearch'] == true)) {
                _vdcopts.jQuery('#SearchCriteria_WidgetType').val(_lw._widgets[item].type);
                break;
            }
        }

        for (var i = 0; i < _vdcopts.controls.length; i++) {
            // if widget set to autoload submit the default search form
            if (_vdcopts.jQuery(_vdcopts.controls[i]+' #AutoLoad').val() === "true") {
                inload = true;
                _vdcopts.jQuery(_vdcopts.controls[i]+' input[type=submit]').click();
            }
        }
    }
};

(function () {
    _vdcopts.SearchWidget.loadScripts();
    _vdcopts.SearchWidget.initBindings();
})();