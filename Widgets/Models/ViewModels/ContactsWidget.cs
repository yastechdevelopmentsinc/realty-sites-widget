﻿using System.Collections.Generic;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class ContactsWidget
    {
        public ContactsWidget()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }

        public IEnumerable<ContactsViewModel> Contacts { get; set; }
        public string DefaultHostName { get; private set; }
    }
}