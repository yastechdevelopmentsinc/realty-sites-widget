﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Widgets.Models.ViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [DisplayName("User Name")]
        public string UserName { get; set; }
    }
}