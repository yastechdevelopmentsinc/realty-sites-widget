﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Widgets.Models.ViewModels
{
    public class DisclaimerViewModel
    {
        public int ID { get; set; }

        [Required]
        [DisplayName("Board Code")]
        public string BoardCode { get; set; }

        [DisplayName("MLS Listing")]
        public string MLSListing { get; set; }

        [DisplayName("MLS Listing Details")]
        public string MLSListingDetails { get; set; }

        [DisplayName("Exclusive Listing")]
        public string ExclusiveListing { get; set; }

        [DisplayName("Exclusive Listing Details")]
        public string ExclusiveListingDetails { get; set; }
    }
}