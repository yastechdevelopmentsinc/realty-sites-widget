﻿using System.Collections.Generic;
using Widgets.Domain.Entities;

namespace Widgets.Models.ViewModels
{
    public class ManageOfficesViewModel
    {
        public Account Account { get; set; }
        public IEnumerable<AccountOfficeInfo> Offices { get; set; }
    }
}