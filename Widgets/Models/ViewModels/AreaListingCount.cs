﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Widgets.Models.ViewModels
{
    public class AreaListingCount
    {
        public string AreaName { get; set; }

        public int Listings { get; set; }

        public override bool Equals(object obj)
        {
            AreaListingCount other = obj as AreaListingCount;
            return other != null && AreaName == other.AreaName && Listings == other.Listings;
        }

        public override int GetHashCode()
        {
            return AreaName.GetHashCode();
        } 
    }
}