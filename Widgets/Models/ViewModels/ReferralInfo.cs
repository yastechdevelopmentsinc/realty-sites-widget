using System;
using System.ComponentModel;
using Widgets.Domain.Entities;

namespace Widgets.Models.ViewModels
{
    public class ReferralInfo
    {
        public int Id { get; set; }
        public Agent Agent { get; set; }

        [DisplayName("Position Number")]
        public int? PositionNumber { get; set; }
        public DateTime? LastReferralDate { get; set; }
        public Account Account { get; set; }
        public int LeadCount { get; set; }

        public int DaysSinceLastReferral
        {
            get
            {
                if (LastReferralDate != null) return DateTime.Now.Subtract((DateTime) LastReferralDate).Days;
                return new TimeSpan().Days;
            }
        }
    }
}