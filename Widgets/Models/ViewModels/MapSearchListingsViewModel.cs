﻿using System.Collections.Generic;

namespace Widgets.Models.ViewModels
{
    public class MapSearchListingsViewModel
    {
        public IEnumerable<MapSearchListing> Listings { get; set; }
        public DisclaimerViewModel Disclaimer { get; set; }

        public bool ShowListingDetailsInPage { get; set; }
    }
}