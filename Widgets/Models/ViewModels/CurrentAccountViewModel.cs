﻿using System.ComponentModel;

namespace Widgets.Models.ViewModels
{
    public class CurrentAccountViewModel
    {
        [DisplayName("Account")]
        public string AccountName { get; set; }
        public string AccountType { get; set; }
    }
}