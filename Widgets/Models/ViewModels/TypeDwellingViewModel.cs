﻿using System.ComponentModel;

namespace Widgets.Models.ViewModels
{
    public class TypeDwellingViewModel
    {
        public string Acreage
        {
            get { return TypeDwelling.Acreage; }
        }

        public string Condo
        {
            get { return TypeDwelling.Condo; }
        }

        [DisplayName("Vacant Lot")]
        public string VacantLot
        {
            get { return TypeDwelling.VacantLot; }
        }

        [DisplayName("Cottage / Recreation")]
        public string CottageRecreation
        {
            get { return TypeDwelling.CottageRecreation; }
        }
    }
}