﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using Widgets.Domain.Entities;
using Widgets.Infrastructure;

namespace Widgets.Models.ViewModels
{
    public class AccountOfficeInfo
    {
        public int Id { get; set; }

        [ScriptIgnore]
        public Account Account { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }

        [StringLength(255, ErrorMessage = "*Email can be no longer than 255 characters")]
        [Email(ErrorMessage = "Not a valid email")]
        public string Email { get; set; }

        [DisplayName("Display on IDX Listings")]
        public bool IDXListings { get; set; }
    }
}