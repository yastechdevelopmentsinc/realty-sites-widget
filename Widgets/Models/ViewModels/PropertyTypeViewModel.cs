﻿using System.ComponentModel;

namespace Widgets.Models.ViewModels
{
    public class PropertyTypeViewModel
    {
        public string Residential
        {
            get { return PropertyTypeConstants.Residential; }
        }

        [DisplayName("For Lease")]
        public string ForLease
        {
            get { return PropertyTypeConstants.ForLease; }
        }

        public string Commercial
        {
            get { return PropertyTypeConstants.Commercial; }
        }

        [DisplayName("Farms/Ranch")]
        public string FarmsAndLand
        {
            get { return PropertyTypeConstants.FarmsAndLand; }
        }

        [DisplayName("Multi Family")]
        public string MultiFamily
        {
            get { return PropertyTypeConstants.MultiFamily; }
        }
    }

    public class CommercialPropertyTypeViewModel
    {
        public string Retail
        {
            get { return CommercialPropertyType.Retail; }
        }

        public string Office
        {
            get { return CommercialPropertyType.Office; }
        }

        public string Land
        {
            get { return CommercialPropertyType.Land; }
        }

        public string Industrial
        {
            get { return CommercialPropertyType.Industrial; }
        }
    }
}