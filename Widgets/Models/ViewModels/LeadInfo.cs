using System;

namespace Widgets.Models.ViewModels
{
    public class LeadInfo
    {
        public int LeadId { get; set; }
        public AgentInfo Agent { get; set; }
        public DateTime? Created { get; set; }
        public string Note { get; set; }
    }
}