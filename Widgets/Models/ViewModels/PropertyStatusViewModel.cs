﻿using System.ComponentModel;

namespace Widgets.Models.ViewModels
{
    public class PropertyStatusViewModel
    {
        [DisplayName("Sale/Lease")]
        public string SaleLease
        {
            get { return PropertyStatusConstants.ForSaleOrLease; }
        }

        [DisplayName("Lease")]
        public string ForLease
        {
            get { return PropertyStatusConstants.ForLease; }
        }

        [DisplayName("Sale")]
        public string ForSale
        {
            get { return PropertyStatusConstants.ForSale; }
        }

        [DisplayName("Leased")]
        public string Leased
        {
            get { return PropertyStatusConstants.Leased; }
        }

        [DisplayName("Sold")]
        public string Sold
        {
            get { return PropertyStatusConstants.Sold; }
        }

        [DisplayName("Sold/Leased")]
        public string SoldLeased
        {
            get { return PropertyStatusConstants.SoldOrLeased; }
        } 
  
    }
}