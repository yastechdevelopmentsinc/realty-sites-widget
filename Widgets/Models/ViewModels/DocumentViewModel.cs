﻿namespace Widgets.Models.ViewModels
{
    public class DocumentViewModel
    {
        public DocumentViewModel(int id)
        {
            ExclusiveId = id;
        }

        public int ExclusiveId { get; set; }

    }
}