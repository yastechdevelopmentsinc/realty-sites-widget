using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Widgets.Infrastructure;

namespace Widgets.Models.ViewModels
{
    public class AgentInfo
    {
        public AgentInfo()
        {
            Roles = Enum.GetValues(typeof (UserRole))
                .Cast<UserRole>()
                .Select(type => new SelectListItem
                                    {
                                        Value = type.ToString(),
                                        Text = type.ToString()
                                    })
                .Where(x=>x.Value != UserRole.Super.ToString())
                .ToList();
        }

        public int AgentID { get; set; }

        [Required]
        [DisplayName("Industry Code")]
        public string AgentIndCode { get; set; }

        [Required]
        [DisplayName("User Name")]
        [StringLength(50, ErrorMessage = "Must be between 6 and 50 characters in length", MinimumLength = 6)]
        [NoWhiteSpace(ErrorMessage = "No spaces allowed")]
        public string UserName { get; set; }

        [DisplayName("First Name")]
        [StringLength(50)]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [StringLength(50)]
        public string LastName { get; set; }

        public string Logo { get; set; }

        [StringLength(50)]
        public string Company { get; set; }
        
        [StringLength(14)]
        public string Phone { get; set; }

        [StringLength(14)]
        public string Cell { get; set; }

        [StringLength(14)]
        public string Fax { get; set; }

        [Required]
        [Email(ErrorMessage = "Not a valid email")]
        public string Email { get; set; }

        [Required]
        [DisplayName("Full Name")]
        public string FullName { get; set; }

        [DisplayName("Specialty")]
        public string Specialty { get; set; }
        public string Website { get; set; }

        public string AgentWebsite
        {
             get
             {
                 if (String.IsNullOrEmpty(Website))
                 {
                     return "";
                 }
                 return Website.Contains("http") ? Website : string.Format("{0}://{1}", "http", Website);
             }
        }

        [Required]
        [StringLength(15, ErrorMessage = "Must be between 6 and 15 characters in length", MinimumLength = 6)]
        [NoWhiteSpace(ErrorMessage = "No spaces allowed")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool Active { get; set; }
        public int? UserRank { get; set; }
        public string Notes { get; set; }

        [DisplayName("Title")]
        public string AgentTitle { get; set; }
        
        [DisplayName("On Members Page")]
        public bool RealtorPage { get; set; }

        [DisplayName("On Contacts Page")]
        public bool ContactsPage { get; set; }

        [DisplayName("In Referral System")]
        public bool ReferralSystem { get; set; }

        [DisplayName("Featured Agent")]
        public bool FeaturedAgent { get; set; }

        [DisplayName("Featured Agent Position Number")]
        public int? FeaturedPositionNumber { get; set; }

        [DisplayName("Number Of Days")]
        public int? NubmerOfDays { get; set; }

        public bool InProgress { get; set; }

        public DateTime? StartDate { get; set; }

        public bool? Staff { get; set; }

        public string PhotoMimeType { get; set; }

        [DisplayName("Twitter Link")]
        public string TwitterUrl { get; set; }

        [DisplayName("Facebook Link")]
        public string FacebookUrl { get; set; }

        [DisplayName("LinkedIn Link")]
        public string LinkedInUrl { get; set; }

        [DisplayName("YouTube Link")]
        public string YouTubeUrl { get; set; }

        [DisplayName("Profile")]
        public string AgentProfile { get; set; }

        [ScriptIgnore]
        public AccountViewModel Account { get; set; }

        [ScriptIgnore]
        public ReferralInfo Referral { get; set; }

        public string Role { get; set; }
        [ScriptIgnore]
        public List<SelectListItem> Roles { get; set; }

        [ScriptIgnore]
        public IEnumerable<LeadInfo> Leads { get; set; }

        public AccountOfficeInfo Office { get; set; }

        [DisplayName("Display on IDX Listings")]
        public bool IDXListings { get; set; }

        public string AgentPhoto
        {
            get
            {
                if (PhotoMimeType == null || PhotoMimeType.Length <= 0)
                {
                     return string.Format("{0}/{1}", Properties.Settings.Default.DefaultHostName, "Content/images/agent.png");
                }
                return string.Format("{0}/{1}/{2}", Properties.Settings.Default.DefaultHostName, "Agent/GetImage", AgentID);
            }
        }
    }
}