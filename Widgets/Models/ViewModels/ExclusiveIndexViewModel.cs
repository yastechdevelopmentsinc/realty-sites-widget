﻿using System.Collections.Generic;

namespace Widgets.Models.ViewModels
{
    public class ExclusiveIndexViewModel
    {
        public AccountViewModel Account { get; set; }
        public IEnumerable<ExclusiveListing> Exclusives { get; set; }
    }
}