﻿namespace Widgets.Models.ViewModels
{
    public class ReplacementCoordinates
    {
        public int ReplacementID { get; set; }
        public int PropertyRef { get; set; }
        public double? Latitude { get; set; }
        public double Longitude { get; set; }
        public string BoardCode { get; set; }
    }
}