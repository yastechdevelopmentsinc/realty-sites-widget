﻿using System.ComponentModel.DataAnnotations;

namespace Widgets.Models.ViewModels
{
    public class AccountUrlViewModel
    {
        public int? Id { get; set; }

        [Required]
        public string Url { get; set; }
    }
}