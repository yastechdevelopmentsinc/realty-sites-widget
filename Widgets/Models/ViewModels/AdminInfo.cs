﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Widgets.Infrastructure;

namespace Widgets.Models.ViewModels
{
    public class AdminInfo
    {
        public int AgentID { get; set; }

        [Required]
        [DisplayName("User Name")]
        public string UserName { get; set; }

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [Required]
        [Email(ErrorMessage = "Not a valid email")]
        public string Email { get; set; }

        [Required]
        [DisplayName("Full Name")]
        public string FullName { get; set; }

        [Required]
        public string Password { get; set; }

        [DisplayName("Role")]
        public string Role { get; set; }
    }
}