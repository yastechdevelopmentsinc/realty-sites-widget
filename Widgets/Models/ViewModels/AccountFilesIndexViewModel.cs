﻿using System.Collections.Generic;

namespace Widgets.Models.ViewModels
{
    public class AccountFilesIndexViewModel
    {
        public IEnumerable<AccountFileViewModel> AccountFiles { get; set; }
    }
}