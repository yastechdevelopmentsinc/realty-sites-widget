﻿using System.Collections.Generic;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class BulletinBoardIndexViewModel
    {
        public BulletinBoardIndexViewModel()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }

        public IEnumerable<BulletinBoardViewModel> Bulletins { get; set; }
        public string DefaultHostName { get; set; }
    }
}