﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Widgets.Infrastructure;

namespace Widgets.Models.ViewModels
{
    public class ChangePassword
    {
        [Required]
        [StringLength(15, ErrorMessage = "Must be between 6 and 15 characters in length", MinimumLength = 6)]
        [NoWhiteSpace(ErrorMessage = "No spaces allowed")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DisplayName("Change Password")]
        [Compare("Password")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}