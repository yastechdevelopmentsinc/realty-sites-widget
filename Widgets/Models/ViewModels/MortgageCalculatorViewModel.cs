﻿using System.ComponentModel;

namespace Widgets.Models.ViewModels
{
    public class MortgageCalculatorViewModel
    {
        public MortgageCalculatorViewModel()
        {
            DefaultHostName = Properties.Settings.Default.DefaultHostName;
        }

        public string DefaultHostName { get; set; }

        [DisplayName("Amortization (Years)")]
        public int Amortization { get; set; }

        [DisplayName("Term (Years)")]
        public int Term { get; set; }

        [DisplayName("Yearly Interest Rate (%)")]
        public double InterestRate { get; set; }

        [DisplayName("Principal Amount ($)")]
        public double Principal { get; set; }

        [DisplayName("Downpayment (%)")]
        public double DownPayment { get; set; }

        // results
        public bool ShowResults { get; set; }

        [DisplayName("Downpayment Required")]
        public double DownPaymentRequired { get; set; }

        [DisplayName("Mortgage Principal")]
        public double MortgagePrincipal { get; set; }

        [DisplayName("Monthly Payment")]
        public double MonthlyPayment { get; set; }

        [DisplayName("Still Owing at End of Term")]
        public double OwingAtTerm { get; set; }
    }
}