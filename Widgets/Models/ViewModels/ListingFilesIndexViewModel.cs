﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Widgets.Models.ViewModels
{
    public class ListingFilesIndexViewModel
    {
        public IEnumerable<ListingFilesViewModel> ListingFiles { get; set; }
    }
}