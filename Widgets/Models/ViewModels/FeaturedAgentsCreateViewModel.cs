﻿using System.ComponentModel;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class FeaturedAgentsCreateViewModel
    {
        public FeaturedAgentsCreateViewModel()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }

        [DisplayName("Search")]
        public string Name { get; set; }
        public string DefaultHostName { get; set; }
    }
}