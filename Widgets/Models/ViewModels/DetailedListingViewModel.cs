﻿using System.Collections.Generic;
using System.ComponentModel;
using Widgets.Domain.Entities;

namespace Widgets.Models.ViewModels
{
    public class DetailedListingViewModel
    {
        public DetailedListingViewModel()
        {
            DefaultHostName = Properties.Settings.Default.DefaultHostName;
        }

        public Listing Listing { get; set; }
        public AgentInfo Agent { get; set; }

        public AccountOfficeInfo Office { get; set; }

        [DisplayName("Co-Listing Agent")]
        public AgentInfo CoAgent { get; set; }
        public AgentInfo CoAgent2 { get; set; }
        public AccountViewModel Account { get; set; }
        public string DefaultHostName { get; set; }
        public string ApiKey { get; set; }
        public bool DisplayOpenHouseInfo { get; set; }
        public DisclaimerViewModel Disclaimer { get; set; }
        public bool ShowMortgageCalculator { get; set; }

        public string SessionID { get; set; }

        public List<ListingFiles> ListingFiles { get; set; }
        
        // sharethis jscript sharing
        public string ShareThisUrl { get; set; }
        public string ShareThisTitle
        {
            get {
                return Listing.ExclusiveID == null ? 
                    string.Format("MLS ® {0} | {1} | {2}", Listing.MlsNumber, Listing.Address, Listing.Price) :
                    string.Format("Listing Id {0} | {1} | {2}", Listing.ExclusiveID, Listing.Address, Listing.Price);
            }
        }

    }
}