﻿using System.Collections.Generic;
using System.Web.Mvc;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class ListingSearchViewModel
    {
        public ListingSearchViewModel()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }
        
        public List<SelectListItem> Bathrooms { get; set; }
        public List<SelectListItem> Bedrooms { get; set; }
        public List<SelectListItem> City { get; set; }
        public List<SelectListItem> PriceLow { get; set; }
        public List<SelectListItem> PriceHigh { get; set; }
        public List<SelectListItem> Sort { get; set; }
        public List<SelectListItem> Area { get; set; }
        public List<SelectListItem> District { get; set; }

        public string DefaultHostName { get; set; }
        public bool AutoLoad { get; set; }
        public bool MLSSearch { get; set; }

        public ListingAccessViewModel ListingAccess { get; set; }

        public SearchCriteria SearchCriteria { get; set; }

        public string BoardCode { get; set; }
    }
}