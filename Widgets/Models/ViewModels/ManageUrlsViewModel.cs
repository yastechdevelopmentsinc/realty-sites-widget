﻿using System;
using System.Collections.Generic;

namespace Widgets.Models.ViewModels
{
    public class ManageUrlsViewModel
    {
        public IEnumerable<AccountUrlViewModel> AccountUrls { get; set; }
        public Guid AccountKey { get; set; }
    }
}