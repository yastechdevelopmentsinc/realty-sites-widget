﻿using System;

namespace Widgets.Models.ViewModels
{
    public class AccountFileViewModel
    {
        public Guid Id { get; set; }
        public AccountViewModel Account { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string MimeType { get; set; }
        public DateTime UploadDate { get; set; }
    }
}