﻿using System.Collections.Generic;
using Widgets.Domain.Entities;

namespace Widgets.Models.ViewModels
{
    public class ManageAgentsViewModel
    {
        public Account Account { get; set; }
        public IEnumerable<AgentInfo> Agents { get; set; }
    }
}