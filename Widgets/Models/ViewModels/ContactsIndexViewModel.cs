﻿using System.Collections.Generic;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class ContactsIndexViewModel
    {
        public ContactsIndexViewModel()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }

        public IEnumerable<ContactsViewModel> Contacts { get; set; }
        public string DefaultHostName { get; set; }
    }
}