﻿namespace Widgets.Models.ViewModels
{
    public class AgentProfileViewModel
    {
        public AgentProfileViewModel()
        {
            DefaultHostName = Properties.Settings.Default.DefaultHostName;
        }

        public string DefaultHostName { get; set; }

        public AgentInfo Agent { get; set; }
    }
}