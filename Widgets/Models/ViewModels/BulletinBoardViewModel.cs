﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Widgets.Domain.Entities;

namespace Widgets.Models.ViewModels
{
    public class BulletinBoardViewModel
    {
        public BulletinBoardViewModel()
        {
            StartDate = DateTime.Now.Date;
        }

        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [DisplayName("Date")]
        [Required]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [DisplayName("Public Facing News Feed Item")]
        public bool IsNews { get; set; }

        public Account Account { get; set; }
        public Agent Agent { get; set; }
    }
}