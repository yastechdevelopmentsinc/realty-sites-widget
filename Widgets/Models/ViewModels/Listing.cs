using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Widgets.Domain.Entities;
using Widgets.Infrastructure;

namespace Widgets.Models.ViewModels
{
    public class Listing
    {
        public Listing()
        {
            PhotoCount = 0;
        }

        public string AgentIndCode { get; set; }
        public string CoAgentIndCode { get; set; }
        public string CoAgentIndCode2 { get; set; }
        public string BoardCode { get; set; }

        [DisplayName("MLS�")]
        public int? MlsNumber { get; set; }

        [DisplayName("Exclusive")]
        public int? ExclusiveID { get; set; }

        public int? ListingID
        {
            get { return ExclusiveID ?? MlsNumber; }
        }

        public string ComputedPropertyType
        {
            get
            {
                return ExclusiveID != null ? "Exclusive" : PropertyType;
            }
        }

        [DisplayName("Type")]
        public string PropertyType { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public decimal ListPrice { get; set; }

        [DisplayName("Year Built")]
        public int? YearBuilt { get; set; }

        [DisplayName("Sq.Footage")]
        public string SqFootage { get; set; }

        [DisplayName("Area")]
        public string AreaName { get; set; }
        public string SubAreaName { get; set; }

        [DisplayName("Description")]
        public string InternetComm { get; set; }

        [DisplayName("Listed by")]
        public string BrokerName { get; set; }
        public string AgentBrokerCode { get; set; }

        public int? Bathrooms { get; set; }

        [DisplayName("Bedrooms")]
        public int? NumbBeds { get; set; }

        [DisplayName("Type")]
        public string TypeDwelling { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? DaysMkt { get; set; }
        public DateTime? DateEntered { get; set; }
        public string OwnershipTitle { get; set; }

        [DisplayName("Open House")]
        public IEnumerable<OpenHouseViewModel> OpenHouses { get; set; }
        [DisplayName("Open House")]
        public IEnumerable<VOpenHouse> VOpenHouses { get; set; }
        [DisplayName("Open House")]
        public IEnumerable<COpenHouse> COpenHouses { get; set; }
        public ReplacementCoordinates ReplacementCoordinates { get; set; }

        // Residential/Condo/Acreage
        [DisplayName("Equipment")]
        public string EquipIncl { get; set; }
        public string Exterior { get; set; }
        public string Roof { get; set; }
        public string Basement { get; set; }
        public string Garages { get; set; }
        public string Frontage { get; set; }

        [DisplayName("Depth")]
        public string DepthInformation { get; set; }
        public string Parking { get; set; }
        public string VirtualTour { get; set; }

        // Commercial
        [DisplayName("Name")]
        public string BusName { get; set; }

        [DisplayName("Major Business Type")]
        public string MajorType { get; set; }

        [DisplayName("Office Area")]
        public string OfficeArea { get; set; }

        [DisplayName("Retail Area")]
        public string RetailArea { get; set; }

        // Commercial & Lease
        public int? Taxes { get; set; }

        // Excusive Commercial Listings
        public string Status { get; set; }

        // Farm & Land
        public string Style { get; set; }
        public string Topography { get; set; }

        [DisplayName("Total Area")]
        public string TotalArea { get; set; }

        public string PriceHash { get; set; }
        public bool ShowDecimal { get; set; }
        public bool IsActive { get; set; }

        // Common
        public int PhotoCount { get; set; }

        public string Description
        {
            get
            {
                return string.IsNullOrEmpty(InternetComm) ? string.Empty : HttpUtility.HtmlDecode(InternetComm);
            }
        }

        public string ShortDescription
        {
            get
            {
                return Description.Length > 150 ? Description.Substring(0, 150) : Description;
            }
        }

        public double? GetLatitude
        {
            get { return ReplacementCoordinates != null && (BoardCode == ReplacementCoordinates.BoardCode || ReplacementCoordinates.BoardCode == null) ? ReplacementCoordinates.Latitude : Latitude; }
        }

        public double? GetLongitude
        {
            get { return ReplacementCoordinates != null && (BoardCode == ReplacementCoordinates.BoardCode || ReplacementCoordinates.BoardCode == null) ? ReplacementCoordinates.Longitude : Longitude; }
        }

        public string DefaultPhoto
        {
            get
            {
                var defaultPhoto = ListingPhotos.Photos.FirstOrDefault();
                if (defaultPhoto != null)
                    return defaultPhoto.RelativeFilePath;
                return string.Format("{0}/Content/images/noimageavailable.jpg",
                                     Properties.Settings.Default.DefaultHostName);
            }
        }

        public string Price
        {
            get
            {
                var formattedPrice = string.Format("${0:#,##0}", ListPrice);
                if (ShowDecimal)
                {
                    formattedPrice = string.Format("${0:#,##0.00}", ListPrice);
                }
                if (!string.IsNullOrEmpty(PriceHash))
                {
                    formattedPrice = string.Format("{0}/{1}", formattedPrice, PriceHash);
                }

                return formattedPrice;
            }
        }

        public string PropertyTaxes
        {
            get { return string.Format("{0:c0}", Taxes); }
        }

        public string SquareFootage
        {
            get
            {
                double d;
                if (!string.IsNullOrEmpty(SqFootage) && Double.TryParse(SqFootage, out d))
                {
                    return string.Format("{0:0,0.## sq ft}", Convert.ToDouble(SqFootage));
                }
                return string.Empty;
            }
        }

        public string SubAreaNameCity
        {
            get
            {
                if (!string.IsNullOrEmpty(SubAreaName))
                {
                    var subAreaName = (SubAreaName.Length > 3 && SubAreaName [2]=='-')? SubAreaName.Remove(0, 3) : SubAreaName;
                    if (String.Compare(City, subAreaName, StringComparison.CurrentCultureIgnoreCase) != 0)
                    {
                        return string.Format("{0}, {1}", subAreaName, City);
                    }  
                }
                return City;
            }
        }

        public string FrontageFormatted
        {
            get
            {
                double d;
                if(!string.IsNullOrEmpty(Frontage) && double.TryParse(Frontage, out d))
                {
                    return string.Format("{0:0,0.##}", Convert.ToDouble(Frontage));
                }
                return string.Empty;
            }
        }

        public string RetailAreaFormatted
        {
            get
            {
                return !string.IsNullOrEmpty(RetailArea)
                           ? string.Format("{0:0,0.##}", Convert.ToDouble(RetailArea))
                           : string.Empty;
            }
        }

        public string OfficeAreaFormatted
        {
            get
            {
                return !string.IsNullOrEmpty(OfficeArea)
                           ? string.Format("{0:0,0.##}", Convert.ToDouble(OfficeArea))
                           : string.Empty;
            }
        }

        public string TotalAreaFormatted
        {
            get
            {
                return !string.IsNullOrEmpty(TotalArea) 
                    ? string.Format("{0:0,0.##}", Convert.ToDouble(TotalArea))
                    : string.Empty;
            }
        }

        public ListingPhotos ListingPhotos
        {
            get
            {
                var mediaFolder = ExclusiveID == null ? "RETSPhotos" : "ExclusivePhotos";
                var listingId = ExclusiveID ?? MlsNumber;
                return new ListingPhotos(listingId, mediaFolder,BoardCode);
            }
        }

        public string PostalCode { get; set; }

        [DisplayName("Documents � click the attached document(s) below for full listing details")]
        public ListingDocuments ListingDocuments
        {
            get
            {
                return new ListingDocuments(ExclusiveID);
            }
        }

        public string CondoType { get; set; }

        public MapSearchListing Get_MapObject()
        {
            return new MapSearchListing
            {
                PropertyType = PropertyType,
                Address = Address,
                MlsNumber = MlsNumber,
                ExclusiveID = ExclusiveID,
                ListPrice = ListPrice,
                SubAreaName = SubAreaName,
                TypeDwelling = TypeDwelling,
                BrokerName = BrokerName,
                GetLatitude = GetLatitude,
                GetLongitude = GetLongitude,
                DefaultPhoto = DefaultPhoto
            };
        }
    }
}