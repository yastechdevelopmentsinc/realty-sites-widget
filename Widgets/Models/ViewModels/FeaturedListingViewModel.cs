﻿using System.Collections.Generic;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class FeaturedListingViewModel
    {

        public FeaturedListingViewModel()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }
        public IEnumerable<Listing> Listings { get; set; }
        public Listing Listing { get; set; }
        public string DefaultHostName { get; set; }
    }
}