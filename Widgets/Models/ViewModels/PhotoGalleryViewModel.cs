﻿using System.Collections.Generic;
using System.ComponentModel;
using Widgets.Domain.Entities;

namespace Widgets.Models.ViewModels
{
    public class PhotoGalleryViewModel
    {
        [DisplayName("Photo Gallery")]
        public IEnumerable<BulletinBoardPhoto> Photos { get; set; }
    }
}