﻿using System.Collections.Generic;

namespace Widgets.Models.ViewModels
{
    public class FeaturedListingIndexViewModel
    {
        public IEnumerable<Listing> Listings;
        public bool AutoSelect;
    }
}