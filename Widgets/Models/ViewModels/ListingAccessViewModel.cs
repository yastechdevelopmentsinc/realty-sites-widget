﻿using Widgets.Domain.Entities;

namespace Widgets.Models.ViewModels
{
    public class ListingAccessViewModel
    {
        public int Id { get; set; }
        public ListingDisplayAccess Residential { get; set; }
        public ListingDisplayAccess Commercial { get; set; }
        public ListingDisplayAccess FarmsAndLand { get; set; }
        public ListingDisplayAccess ForLease { get; set; }
        public ListingDisplayAccess MultiFam { get; set; }
    }
}