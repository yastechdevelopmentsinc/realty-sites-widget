﻿using System.Collections.Generic;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class FeaturedAgentViewModel
    {
        public FeaturedAgentViewModel()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }
        public string ProfilePageUrl { get; set; }
        public IEnumerable<AgentInfo> Agents { get; set; }
        public AgentInfo Agent { get; set; }
        public string DefaultHostName { get; set; }

        public string AgentProfileUrl(int agentId)
        {
            return !string.IsNullOrEmpty(ProfilePageUrl) ? string.Format("{0}?agentId={1}", ProfilePageUrl, agentId) : string.Empty;
        }
    }
}