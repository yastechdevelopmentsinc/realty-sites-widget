﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Widgets.Infrastructure;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class Email
    {
        public Email()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }

        public string DefaultHostName { get; set; }

        public int AgentID { get; set; }
        public int OfficeID { get; set; }
        public Guid AccountKey { get; set; }
        public string PropertyType { get; set; }
        public int ListingId { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        [DisplayName("Your First Name")]
        public string FirstName { get; set; }

        [Required]
        [DisplayName("Your Last Name")]
        public string LastName { get; set; }

        [Required]
        [DisplayName("Your Email Address")]
        [Email(ErrorMessage = "Not a valid email")]
        public string EmailAddress { get; set; }
        public string ToAddress { get; set; }

        [Required]
        [DisplayName("Your Phone Number")]
        public string Phone { get; set; }

        [Required]
        public string Comments { get; set; }

        public string Contact { get; set; }
    }
}