﻿using System.ComponentModel;

namespace Widgets.Models.ViewModels
{
    public class ManageSettingsViewModel
    {
        public int Id { get; set; }
        public int Amortization { get; set; }
        public int Term { get; set; }

        [DisplayName("Interest Rate")]
        public double InterestRate { get; set; }
        public double Principal { get; set; }

        [DisplayName("Down Payment")]
        public double DownPayment { get; set; }
    }
}