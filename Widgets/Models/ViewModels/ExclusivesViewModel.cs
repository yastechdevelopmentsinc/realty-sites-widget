using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using Widgets.Domain.Entities;

namespace Widgets.Models.ViewModels
{
    public class ExclusivesViewModel
    {
        public ExclusivesViewModel()
        {
            DefaultHostName = string.Format("{0}", Properties.Settings.Default.DefaultHostName);
            GoogleAPIKey = Properties.Settings.Default.GoogleMapsApiKey;
        }

        public string DefaultHostName { get; set; }
        public string GoogleAPIKey { get; set; }

        [DisplayName("Listing Agent")]
        public IEnumerable<SelectListItem> Agents { get; set; }

        [DisplayName("Co-Listing Agent")]
        public IEnumerable<SelectListItem> CoAgents { get; set; }

        [DisplayName("Co-Listing Agent")]
        public IEnumerable<SelectListItem> CoAgents2 { get; set; }
        public IEnumerable<SelectListItem> Areas { get; set; }
        public IEnumerable<SelectListItem> PropertyTypes { get; set; }
        public IEnumerable<SelectListItem> Styles { get; set; }
        public IEnumerable<SelectListItem> Type { get; set; }

        public ExclusiveListing Exclusive { get; set; }

        public Account Account { get; set; }
    }
}