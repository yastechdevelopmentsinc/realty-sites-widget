using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Widgets.Domain.Entities;
using Widgets.Infrastructure;

namespace Widgets.Models.ViewModels
{
    public class ExclusiveListing
    {
        public ExclusiveListing()
        {
            Status = string.Empty;
        }
        [DisplayName("ID")]
        public int ExclusiveID { get; set; }

        [DisplayName("Property Class")]
        [Required]
        public string PropertyType { get; set; }
        public string Address { get; set; }

        [Required]
        public string AgentIndCode { get; set; }
        public string CoAgentIndCode { get; set; }
        public string CoAgentIndCode2 { get; set; }
        public string AgentBrokerCode { get; set; }
        public string AgentBoardCode { get; set; }
        public string Basement { get; set; }
        public string Bathrooms { get; set; }
        public string City { get; set; }

        [DisplayName("Depth Information")]
        public string DepthInformation { get; set; }

        [DisplayName("Equipment Included")]
        public string EquipIncl { get; set; }
        public string Exterior { get; set; }

        [DisplayName("Description")]
        public string InternetComm { get; set; }
        public string Frontage { get; set; }
        public string Garages { get; set; }
        public string Heating { get; set; }

        [DisplayName("List Price")]
        [Required]
        public decimal? ListPrice { get; set; }
        public int? MlsNumber { get; set; }

        [DisplayName("Bedrooms")]
        public int? NumbBeds { get; set; }

        public int? NumbRooms { get; set; }
        public string Parking { get; set; }

        [DisplayName("Photo Count")]
        public int? PhotoCount { get; set; }
        public string Roof { get; set; }
        public string Slsman1Email { get; set; }
        public string Slsman1Name { get; set; }
        public string Slsman1Phone { get; set; }
        public string Slsman1Webaddr { get; set; }

        [DisplayName("Sq.Footage")]
        public string SqFootage { get; set; }

        public string Status { get; set; }
        public string ForSale { get; set; }
        public string ForLease { get; set; }
        public string Sold { get; set; }
        public string Leased { get; set; }

        [DisplayName("Style")]
        public string Style { get; set; }
        
        [DisplayName("Area")]
        public string AreaName { get; set; }

        [DisplayName("Sub Area")]
        public string SubAreaName { get; set; }

        [DisplayName("Type")]
        public string TypeDwelling { get; set; }

        [DisplayName("Year Built")]
        public int? YearBuilt { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        [DisplayName("Open House Information")]
        public IEnumerable<OpenHouseViewModel> OpenHouses { get; set; }

        public int? DaysMkt { get; set; }
        public DateTime? DateEntered { get; set; }

        [DisplayName("Broker Name")]
        [Required]
        public string BrokerName { get; set; }
        public string DistrictCode { get; set; }

        [Required]
        public int AreaCode { get; set; }
        public Account Account { get; set; }

        public string PriceHash { get; set; }
        [DisplayName("Show .00 on end of price")]
        public bool ShowDecimal { get; set; }

        [DisplayName("Is Active")]
        public bool IsActive { get; set; }

        [DisplayName("Is Rental")]
        public bool IsRental { get; set; }
        
        public string Price
        {
            get
            {
                var formattedPrice = string.Format("${0:#,##0}", ListPrice);
                if (ShowDecimal)
                {
                    formattedPrice = string.Format("${0:#,##0.00}", ListPrice);
                }
                if (!string.IsNullOrEmpty(PriceHash))
                {
                    formattedPrice = string.Format("{0}/{1}", formattedPrice, PriceHash);
                }
                
                return formattedPrice;
            }
        }

        public ListingPhotos ListingPhotos
        {
            get
            {
                return new ListingPhotos(ExclusiveID, "ExclusivePhotos",null);
            }
        }

        [DisplayName("Documents")]
        public ListingDocuments ListingDocuments
        {
            get
{
                return new ListingDocuments(ExclusiveID);
            }
        }

        [DisplayName("Virtual Tour")]
        public string VirtualTour { get; set; }

    }
}