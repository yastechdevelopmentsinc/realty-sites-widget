﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Widgets.Domain.Entities;
using Widgets.Extensions;
using Widgets.Helpers;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class ListingWidgetViewModel
    {
        public ListingWidgetViewModel(Account account)
        {
            DefaultHostName = Settings.Default.DefaultHostName;

            if (account.AccountType == AccountType.Commercial)
            {
                var prop = new CommercialPropertyTypeViewModel();
                var propFilters = prop.GetType().GetProperties(BindingFlags.Public | BindingFlags.Static |
                                                               BindingFlags.Instance)
                                      .ToSelectList(x => (ControllerHelpers.GetPropValue(prop, x.Name).ToString()),
                                                    x => (ControllerHelpers.GetName(x)), "All");

                Filters = propFilters;

                var status = new PropertyStatusViewModel();
                var statusFilters = status.GetType().GetProperties(BindingFlags.Public | BindingFlags.Static |
                                                               BindingFlags.Instance);

                Statuses = statusFilters.ToSelectList(x => (ControllerHelpers.GetPropValue(status, x.Name).ToString()),
                                                    x => (ControllerHelpers.GetName(x)),null);
            }
            else
            {
                // get property types
                var prop = new PropertyTypeViewModel();
                var propFilters = prop.GetType().GetProperties(BindingFlags.Public | BindingFlags.Static |
                                                               BindingFlags.Instance)
                                      .ToSelectList(x => (ControllerHelpers.GetPropValue(prop, x.Name).ToString()),
                                                    x => (ControllerHelpers.GetName(x)), "All");

                // get dwelling types
                var type = new TypeDwellingViewModel();
                var typeFilters = type.GetType().GetProperties(BindingFlags.Public | BindingFlags.Static |
                                                               BindingFlags.Instance)
                                      .ToSelectList(x => (ControllerHelpers.GetPropValue(type, x.Name).ToString()),
                                                    x => (ControllerHelpers.GetName(x)), "");

                // merge the types together
                Filters = propFilters.Concat(typeFilters).ToList();
                Statuses = null;
            }

            // order by
            var sort = new[] { "New Listings", "Address", "Lowest Price", "Highest Price" };
            OrderBy = sort.ToSelectList(x => x.ToString(CultureInfo.InvariantCulture),
                                     x => x.ToString(CultureInfo.InvariantCulture), "");
        }

        public AccountViewModel Account { get; set; }
        public IEnumerable<Listing> Listings { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string DefaultHostName { get; set; }
        public bool DisplayOpenHouseInfo { get; set; }
        public MiniSearch MiniSearch { get; set; }
        public DisclaimerViewModel Disclaimer { get; set; }

        public List<SelectListItem> Filters { get; set; }
        public List<SelectListItem> Statuses { get; set; }
        public List<SelectListItem> OrderBy { get; set; }

        public string SessionID { get; set; }
    }
}