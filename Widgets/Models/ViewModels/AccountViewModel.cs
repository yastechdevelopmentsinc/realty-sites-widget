﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Widgets.Domain.Entities;
using Widgets.Infrastructure;

namespace Widgets.Models.ViewModels
{
    public class AccountViewModel
    {
       public AccountViewModel()
        {
            AccountTypes = Enum.GetValues(typeof (AccountType)).Cast<AccountType>()
                .Select(type => new SelectListItem()
                                    {
                                        Text = type.ToString(),
                                        Value = type.ToString()
                                    }).ToList();
             
        }

        public Guid Id { get; set; }

        [Required]
        [DisplayName("Account Name")]
        public virtual String Name { get; set; }        
        
        [DisplayName("Agent Industry Code")]
        public virtual String AgentCode { get; set; }

        [Required]
        [DisplayName("Broker Industry Code")]
        public virtual String BrokerCode { get; set; }

        [Required]
        [DisplayName("Board Code")]
        public virtual String BoardCode { get; set; }        

        [Required]
        [DisplayName("Account Type")]
        public virtual AccountType? AccountType { get; set; }
       
        public List<SelectListItem> AccountTypes { get; set; }

        public List<SelectListItem> BoardCodes { get; set; }

        public virtual AccountFeaturesViewModel Features { get; set; }

        [StringLength(14)]
        public string Phone { get; set; }

        [StringLength(14)]
        public string Fax { get; set; }

        [Required]
        [Email(ErrorMessage = "Not a valid email")]
        public string Email { get; set; }
        public string Website { get; set; }
        public byte[] Logo { get; set; }
        public string LogoMimeType { get; set; }

        [DisplayName("Map Marker")]
        public byte[] MapMarker { get; set; }
        public string MapMarkerMimeType { get; set; }

        [DisplayName("Map Cluster")]
        public byte[] MapMarkerMore { get; set; }
        public string MapMarkerMoreMimeType { get; set; }
         [Required]
        public double Latitude { get; set; }
         [Required]
        public double Longitude { get; set; }

         [DisplayName("Facebook AppID")]
         public string FacebookAppID { get; set; }

        public string GetLogoPath
        {
            get { return string.Format("{0}/{1}/{2}", Properties.Settings.Default.DefaultHostName, "Account/GetAccountLogo", Id); }
        }
    }
}