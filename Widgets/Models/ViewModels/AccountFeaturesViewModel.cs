﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Widgets.Domain.Entities;

namespace Widgets.Models.ViewModels
{
    public class AccountFeaturesViewModel
    {
        public int Id { get; set; }
        public bool Listing { get; set; }
        public bool Search { get; set; }
        public bool Map { get; set; }

        [DisplayName("Featured Listing")]
        public bool FeaturedListing { get; set; }

        [DisplayName("Featured Agent")]
        public bool FeaturedAgent { get; set; }
        public bool Members { get; set; }
        public bool Contacts { get; set; }
        public bool News { get; set; }

        [DisplayName("Latest Listing")]
        public bool LatestListing { get; set; }        
        
        [DisplayName("Agent Profile")]
        public bool AgentProfile { get; set; }

        [DisplayName("Mortgage Calculator")]
        public bool MortgageCalculator { get; set; }

        [DisplayName("Bulletin Board")]
        public bool BulletinBoard { get; set; }

        [DisplayName("Exclusive Listing")]
        public bool ExclusiveListing { get; set; }

        [DisplayName("Open Houses")]
        public bool OpenHouses { get; set; }

        public bool Referral { get; set; }

        [DisplayName("File Management")]
        public bool FileManagement { get; set; }

        [DisplayName("Link Management")]
        public bool LinkManagement { get; set; }

        [DisplayName("Use Simple Detailed Listing")]
        public bool UseSimpleDetailedListing { get; set; }

        [DisplayName("Number of Listings to Display")]
        public int NumberOfListingsToDisplay { get; set; }

        [Required]
        [DisplayName("Listing Widget Page Size")]
        public int ListingWidgetPageSize { get; set; }

        [DisplayName("Number of Characters in Listing Description")]
        public int NumberOfCharDescription { get; set; }

        [DisplayName("MLS Search")]
        public bool MLSSearch { get; set; }        
        
        [DisplayName("Display Agent Profile Link")]
        public bool AgentProfileLink { get; set; } 
       
        [DisplayName("Display Agent Website Link")]
        public bool AgentWebsiteLink { get; set; }

        [DisplayName("Show Agent Email Address")]
        public virtual bool ShowEmailAddress { get; set; }

        [DisplayName("Automatically Set featured listings")]
        public virtual bool AutoSelectFeaturedListings { get; set; }

        [DisplayName("Sold/Lease Watermark")]
        public bool ShowSoldLeasedWatermark { get; set; }

        [DisplayName("Show Listing Details In a Page")]
        public bool ShowListingDetailsInPage { get; set; }

        [DisplayName("Enable Featured Agents Schedule")]
        public bool EnableFeaturedAgentsSchedule { get; set; }

        [DisplayName("Show IDX Open Houses")]
        public bool IDXOpenHouses { get; set; }

        public virtual Account Account { get; set; }
    }
}