﻿using System.ComponentModel;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class FeaturedListingsCreateViewModel
    {
        public FeaturedListingsCreateViewModel()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }

        [DisplayName("Search")]
        public string Address { get; set; }
        public string DefaultHostName { get; set; }
    }
}