﻿using System.Collections.Generic;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class NewsIndexViewModel
    {
        public NewsIndexViewModel()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }

        public IEnumerable<NewsItemViewModel> Bulletins { get; set; }
        public string DefaultHostName { get; set; }
    }
}