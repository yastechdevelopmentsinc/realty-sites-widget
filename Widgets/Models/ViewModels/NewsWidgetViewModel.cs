﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class NewsWidgetViewModel
    {
        public NewsWidgetViewModel()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }

        public IEnumerable<NewsItemViewModel> News { get; set; }
        public string DefaultHostName { get; set; }

        public int Length { get; set; }
        public string Options { get; set; }

        [DisplayName("Select year to view past articles")]
        public int Archive { get; set; }
        public IEnumerable<SelectListItem> Archives { get; set; }
    }
}