﻿using System.ComponentModel;

namespace Widgets.Models.ViewModels
{
    public class EditCoordinatesViewModel
    {

        public EditCoordinatesViewModel()
        {
            DefaultHostName = Properties.Settings.Default.DefaultHostName;
            GoogleAPIKey = Properties.Settings.Default.GoogleMapsApiKey;
        }

        public string PropertyType { get; set; }
        public int MlsNumber { get; set; }

        [DisplayName("Search")]
        public string Address { get; set; }
        public string City { get; set; }
        public string DefaultHostName { get; set; }
        public string GoogleAPIKey { get; set; }
        public string BoardCode { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }

    }
}