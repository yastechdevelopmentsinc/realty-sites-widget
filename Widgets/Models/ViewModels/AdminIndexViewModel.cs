﻿using System.Collections.Generic;

namespace Widgets.Models.ViewModels
{
    public class AdminIndexViewModel
    {
        public IEnumerable<AdminInfo> Admins { get; set; }
    }
}