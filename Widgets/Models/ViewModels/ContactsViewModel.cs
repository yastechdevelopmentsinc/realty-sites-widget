﻿using Widgets.Domain.Entities;

namespace Widgets.Models.ViewModels
{
    public class ContactsViewModel
    {
        public int Id { get; set; }
        public AgentInfo Agent { get; set; }
        public string AgentTitle { get; set; }
        public int? PositionNumber { get; set; }
        public Account Account { get; set; }
    }
}