﻿using System.Collections.Generic;

namespace Widgets.Models.ViewModels
{
    public class ManageLinksViewModel
    {
        public IEnumerable<AccountLinkViewModel> Links { get; set; } 
    }
}