﻿namespace Widgets.Models.ViewModels
{
    public class CurrentUserViewModel
    {
        public string UserName { get; set; }
    }
}