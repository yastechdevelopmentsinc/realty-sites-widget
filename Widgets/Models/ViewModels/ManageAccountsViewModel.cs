﻿using System.Collections.Generic;
using Widgets.Domain.Entities;

namespace Widgets.Models.ViewModels
{
    public class ManageAccountsViewModel
    {
        public IEnumerable<Account> Accounts { get; set; } 
    }
}