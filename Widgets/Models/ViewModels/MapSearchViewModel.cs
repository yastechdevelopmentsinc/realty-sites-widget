﻿using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class MapSearchViewModel
    {
        public MapSearchViewModel()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }

        public string ApiKey { get; set; }
        public double MapSearchLatitude { get; set; }
        public double MapSearchLongitude { get; set; }
        public string DefaultHostName { get; set; }
        public bool IsLocated { get; set; }
    }
}