using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Widgets.Models.ViewModels
{
    public class OpenHouseViewModel
    {
        public int OpHoID { get; set; }
        public int? AgentMui { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [DisplayName("Date")]
        [DataType(DataType.Date)]
        public DateTime FromDate { get; set; }

        [Required]
        [DisplayName("Time")]
        public int? FromTime { get; set; }
        public DateTime? InputDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public int? ListingMui { get; set; }
        public string ListingType { get; set; }
        public int? MatrixUniqueID { get; set; }
        public int? MlsNumber { get; set; }
        public DateTime? ModificationTimestamp { get; set; }

        [DisplayName("Refreshments")]
        public string OpenHouseRefreshments { get; set; }
        public string OpenHouseType { get; set; }
        public DateTime? ToDateDefunct { get; set; }

        [Required]
        public int? ToTime { get; set; }
        public bool? AdvertOpenHouse { get; set; }

        public IEnumerable<SelectListItem> FromTimes { get; set; }
        public IEnumerable<SelectListItem> ToTimes { get; set; }

        public virtual string StartDate
        {
            get { return String.Format("{0:ddd MMM dd}", FromDate); }
        }

        public virtual string StartTime
        {
            get
            {
                return GetTime(FromTime);
            }
        }

        public virtual string EndTime
        {
            get
            {
                return GetTime(ToTime);
            }
        }

        private string GetTime(int? time)
        {
            try
            {
                var hours = Convert.ToDouble(time.ToString().Substring(0, 2));
                var mins = Convert.ToDouble(time.ToString().Substring(2));
                return String.Format("{0:h:mm tt}", new DateTime().AddHours(hours).AddMinutes(mins));
            }
            catch
            {
                return String.Format("{0:h:mm tt}", new DateTime());
            }
        }
    }
}