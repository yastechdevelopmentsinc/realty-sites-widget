﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Widgets.Infrastructure;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class EmailListing
    {
        public EmailListing()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }

        public string DefaultHostName { get; set; }

        public string PropertyType { get; set; }
        public int ListingId { get; set; }
        public string BoardCode { get; set; }

        [Required]
        [DisplayName("Send To Email Address")]
        [Email(ErrorMessage = "Not a valid email")]
        public string ToEmail { get; set; }
        public string Comments { get; set; }

        [Required]
        [DisplayName("Your Email Address")]
        [Email(ErrorMessage = "Not a valid email")]
        public string FromEmail { get; set; }

        [Required]
        [DisplayName("Your First Name")]
        public string FromFirstName { get; set; }

        [Required]
        [DisplayName("Your Last Name")]
        public string FromLastName { get; set; }
    }
}