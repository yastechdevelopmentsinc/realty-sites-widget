﻿using System.Collections.Generic;
using System.Web.Mvc;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class MembersWidget
    {
        public MembersWidget()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }

        public string ProfilePageUrl { get; set; }
        public IEnumerable<AgentInfo> Agents { get; set; }
        public string DefaultHostName { get; private set; }

        public IEnumerable<SelectListItem> Sort { get; set; }
        public string SortyBy { get; set; }
        public string Search { get; set; }

        public IEnumerable<SelectListItem> Offices { get; set; }
        public string Office { get; set; }

        public string AgentProfileUrl(int agentId)
        {
            return !string.IsNullOrEmpty(ProfilePageUrl) ? string.Format("{0}?agentId={1}", ProfilePageUrl, agentId) : string.Empty;
        }
    }
}