﻿using System.Collections.Generic;

namespace Widgets.Models.ViewModels
{
    public class ManageDisclaimersViewModel
    {
        public IEnumerable<DisclaimerViewModel> Disclaimers { get; set; }
    }
}