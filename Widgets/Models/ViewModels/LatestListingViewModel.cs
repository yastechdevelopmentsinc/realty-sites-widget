﻿using System.Collections.Generic;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class LatestListingViewModel
    {

        public LatestListingViewModel()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }
        public IEnumerable<Listing> Listings { get; set; }
        public string DefaultHostName { get; set; }
    }
}