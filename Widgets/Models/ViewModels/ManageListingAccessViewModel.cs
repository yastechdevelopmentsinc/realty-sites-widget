﻿using System.Collections.Generic;

namespace Widgets.Models.ViewModels
{
    public class ManageListingAccessViewModel
    {
        public List<KeyValuePair<string, string>> PropertyTypes { get; set; }
        public IEnumerable<string> Access { get; set; }
        public ListingAccessViewModel ListingDisplayAccess { get; set; }
    }
}