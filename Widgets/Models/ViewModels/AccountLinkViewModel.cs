﻿using System.ComponentModel.DataAnnotations;
using Widgets.Domain.Entities;
using Widgets.Infrastructure;

namespace Widgets.Models.ViewModels
{
    public class AccountLinkViewModel
    {
        public AccountLinkViewModel()
        {
            if (string.IsNullOrEmpty(Website))
            {
                Website = "http://";
            }
        }

        public int Id { get; set; }
        public Account Account { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [Url(ErrorMessage = "Must be a valid url including http://")]
        public string Website { get; set; }
    }
}