﻿namespace Widgets.Models.ViewModels
{
    public class EmailPassword
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}