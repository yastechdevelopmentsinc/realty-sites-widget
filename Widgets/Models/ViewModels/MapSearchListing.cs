namespace Widgets.Models.ViewModels
{
    public class MapSearchListing
    {
        public string PropertyType { get; set; }
        public string Address { get; set; }
        public int? MlsNumber { get; set; }
        public int? ExclusiveID { get; set; }
        public decimal ListPrice { get; set; }
        public string SubAreaName { get; set; }
        public string TypeDwelling { get; set; }
        public string BrokerName { get; set; }
        public double? GetLatitude { get; set; }
        public double? GetLongitude { get; set; }
        public string DefaultPhoto { get; set; }

        public string Price
        {
            get { return string.Format("{0:c0}", ListPrice); }
        }
    }
}