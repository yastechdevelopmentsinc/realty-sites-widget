﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Widgets.Domain.Entities;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class NewsItemViewModel
    {
        public NewsItemViewModel()
        {
            StartDate = DateTime.Now.Date;
            DefaultHostName = Settings.Default.DefaultHostName;
        }

        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [DisplayName("Date")]
        [Required]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [DisplayName("Public Facing News Feed Item")]
        public bool IsNews { get; set; }

        public Account Account { get; set; }
        public Agent Agent { get; set; }

        public string DefaultHostName { get; set; }

        [DisplayName("Photo Gallery")]
        public IEnumerable<BulletinBoardPhoto> Photos { get; set; }

        public string ShareThisUrl { get; set; }
    }
}