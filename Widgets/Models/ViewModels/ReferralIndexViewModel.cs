﻿using System.Collections.Generic;

namespace Widgets.Models.ViewModels
{
    public class ReferralIndexViewModel
    {
        public IEnumerable<ReferralInfo> Referrals { get; set; }
    }
}