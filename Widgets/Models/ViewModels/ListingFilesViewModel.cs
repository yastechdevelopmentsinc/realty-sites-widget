﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Widgets.Properties;

namespace Widgets.Models.ViewModels
{
    public class ListingFilesViewModel
    {
        public ListingFilesViewModel()
        {
            DefaultHostName = Settings.Default.DefaultHostName;
        }

        [DisplayName("Search")]
        [Required]
        public string Address { get; set; }
        public string DefaultHostName { get; set; }

        public int Id { get; set; }
        public AccountViewModel Account { get; set; }
        [Required]
        public int Mlsnumber { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string MimeType { get; set; }
        public DateTime UploadDate { get; set; }
        [Required]
        public string Name { get; set; }
    }
}