﻿namespace Widgets.Models
{
    public class TypeDwelling
    {
        public const string Acreage = "ACREAGE";
        public const string Condo = "CONDOMINIUM";
        public const string VacantLot = "VACANT";
        public const string CottageRecreation = "COTTAGE";
    }
}