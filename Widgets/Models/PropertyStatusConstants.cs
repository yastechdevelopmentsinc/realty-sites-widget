﻿namespace Widgets.Models
{
    public class PropertyStatusConstants
    {
        public const string ForSale = "For Sale";
        public const string ForLease = "For Lease";
        public const string Sold = "Sold";
        public const string Leased = "Leased";
        public const string ForSaleOrLease = "For Sale or For Lease";
        public const string SoldOrLeased = "Sold or Leased";
    }
}