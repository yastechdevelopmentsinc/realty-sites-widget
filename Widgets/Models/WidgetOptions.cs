﻿namespace Widgets.Models
{
    public class WidgetOptions
    {
        public const string AllListings =  "all";
        public const string BrokerageListing =  "brokerage";
        public const string AgentListings =  "agent";
        public const string OpenHouseListings =  "openhouse";
        public const string VirtualTourListings =  "virtualtour";
        public const string ExclusiveListings =  "exclusive";
        public const string ResidentialListings =  "residential";
        public const string FarmsAndLandListings =  "farmsandland";
        public const string CommercialListings =  "commercial";
        public const string ForLeaseListings =  "forlease";
        public const string MultiFamListings =  "multifam";
        public const string Rentals = "rentals";
    }

    public class NewsOptions
    {
        public const string Headline = "headline";
        public const string All = "all";
        public const string Summary = "summary";
        public const string Archive = "archive";
    }

    public class MembersOptions
    {
        public const string Default = "Default";
        public const string DescFirstName = "DESC_first_name";
        public const string AscFirstName = "ASC_first_name";
        public const string DescLastName = "DESC_last_name";
        public const string AscLastName = "ASC_last_name";
    }
}