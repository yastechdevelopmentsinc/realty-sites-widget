﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Widgets.Models
{
    public class SearchCity : IEquatable<SearchCity>
    {
        public string CityName { get; set; }
        public int Listings { get; set; }

        public bool Equals(SearchCity other)
        {
            if (Object.ReferenceEquals(other, null)) return false;

            if (Object.ReferenceEquals(this, other)) return true;

            return Listings.Equals(other.Listings) && CityName.Equals(other.CityName);
        }

        public override int GetHashCode()
        {

            //Get hash code for the Name field if it is not null.
            int hashCityName = CityName == null ? 0 : CityName.GetHashCode();

            //Get hash code for the Code field.
            int hashListings = Listings.GetHashCode();

            //Calculate the hash code for the product.
            return hashCityName ^ hashListings;
        }
    }

     
}