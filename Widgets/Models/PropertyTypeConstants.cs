﻿namespace Widgets.Models
{
    public class PropertyTypeConstants
    {
        public const string Residential = "Residential";
        public const string Commercial = "Commercial";
        public const string FarmsAndLand = "FarmsAndLand";
        public const string ForLease = "ForLease";
        public const string MultiFamily = "MultiFam";
    }

    public class CommercialPropertyType
    {
        public const string Retail = "Retail";
        public const string Industrial = "Industrial";
        public const string Office = "Office";
        public const string Land = "Land";
    }

    public class ExclusivePropertyType
    {
        public const string Exclusive = "Exclusive";
    }

    public class FarmsAndLandPropertyType
    {
        public const string Cashcrop = "CASHCROP";
        public const string Hobby = "HOBBY";
        public const string Dary = "DARY";
        public const string Beef = "BEEF";
        public const string Game = "GAME";
        public const string Grain = "GRAIN";
        public const string Other = "OTHER";
        public const string Mixed = "Mixed";
        public const string Hay = "Hay";
    }

    public class ResidentialPropertyType
    {
        public const string Condo = "Condo/Strata";
        public const string SingleFamily = "Single Family";
        public const string HalfDuplex = "Half Duplex";
        public const string FullDuplex = "Full Duplex";
        public const string Triplex = "Triplex";
        public const string Fourplex = "Fourplex";
        public const string Mobile = "Manufactured/Mobile";
        public const string Fram = "Farm/Ranch";
        public const string Lot = "Lots/Acreage";

    }
}