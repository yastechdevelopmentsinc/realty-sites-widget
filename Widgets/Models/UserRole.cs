﻿namespace Widgets.Models
{
    public enum UserRole
    {
        Super, Admin, User, News, Listings
    }
}