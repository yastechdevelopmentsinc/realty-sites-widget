﻿using System;
using System.ComponentModel;

namespace Widgets.Models
{
    [Serializable]
    public class MiniSearch
    {
        public bool ShowFilter { get; set; }
        [DisplayName("Property Type")]
        public string Filter { get; set; }
        [DisplayName("Status")]
        public string Status { get; set; }
        public string Sort { get; set; }
    }
}