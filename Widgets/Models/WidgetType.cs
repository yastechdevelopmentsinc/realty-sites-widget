﻿namespace Widgets.Models
{
    public class WidgetType
    {
        public const string ListingWidget = "_listingwidget";
        public const string SearchWidget = "_listingsearch";
        public const string MapSearchWidget = "_listingmapsearch";
        public const string FeaturedListingWidget = "_featuredlistingwidget";
        public const string MembersWidget = "_memberswidget";
        public const string ContactsWidget = "_contactswidget";
        public const string NewsWidget = "_newswidget";
        public const string MortgageCalculatorWidget = "_mortgagecalculator";
        public const string FeaturedAgentWidget = "_featuredagentwidget";
        public const string LatestListingWidget = "_latestlistingwidget";
        public const string AgentProfileWidget = "_agentprofilewidget";

        public const string CustomSearchWidget = "_customsearchwidget"; //new widget
        public const string NearMeWidget = "_nearmesearch";
        public const string InPageListingDetails = "_inpagelistingdetails";
    }
}