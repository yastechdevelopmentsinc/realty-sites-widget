﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Widgets.Domain.Entities;

namespace Widgets.Models
{
    [Serializable]
	public class SearchCriteria
	{
        [DisplayName("MLS®")]
        public int? MlsNumber { get; set; }

        [DisplayName("search by neighborhood, city, zip or address")]
        public string SearchText { get; set; }

        [DisplayName("Price")]
	    public decimal PriceLow { get; set; }
        public decimal PriceHigh { get; set; }

        [DisplayName("Baths")]
	    public int? Bathrooms { get; set; }

        [DisplayName("Beds")]
        public int? Bedrooms { get; set; }
	    public string City { get; set; }

        [DisplayName("Area")]
	    public string AreaName { get; set; }
        public string Sort { get; set; }
        public string Style { get; set; }
        public string Status { get; set; }
        public bool Residential { get; set; }
	    public bool Commercial { get; set; }

        [DisplayName("Area")]
        public string District { get; set; }

        [DisplayName("Farms/Ranch")]
	    public bool FarmsAndLand { get; set; }

        [DisplayName("For Lease")]
	    public bool ForLease { get; set; }
	    public bool Acreage { get; set; }

        [DisplayName("Multi Family")]
        public bool MultiFam { get; set; }

        [DisplayName("Condo")]
	    public bool Condominium { get; set; }

        [DisplayName("Vacant Lot")]
	    public bool Vacant { get; set; }

        [DisplayName("Cottage / Recreation")]
        public bool Cottage { get; set; }

        public string FarmLandType { get; set; }
        
        // Commercial Property Types
        public bool Retail { get; set; }
        public bool Industrial { get; set; }
        public bool Office { get; set; }
        public bool Land { get; set; }

        //Resdential Types
        [DisplayName("Single Family")]
        public bool SingleFamily { get; set; }
        [DisplayName("Half Duplex")]
        public bool HalfDuplex { get; set;}
        [DisplayName("Full Duplex")]
        public bool FullDuplex { get; set; }
        public bool Triplex { get; set;}
        public bool Fourplex { get; set;}
        public bool Mobile { get; set;}

        public int VCommercialType { get; set; }

        public string WidgetType { get; set; }
        public string WidgetOptions { get; set; }
        public string Address { get; set; }
        public string RedirectUrl { get; set; }

        public int PageId { get; set; }

        [DisplayName("From")]
        [DataType(DataType.Date)]
        public DateTime? DateFrom { get; set; }

        [DisplayName("To")]
        [DataType(DataType.Date)]
        public DateTime? DateTo { get; set; }

        public string AgentID { get; set; }

        public string SessionID { get; set; }

        public Account Account { get; set; }

	}
}