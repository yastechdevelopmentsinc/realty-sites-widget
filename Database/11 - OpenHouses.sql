alter table dbo.OpenHouses
drop constraint PK_OpenHouses

ALTER TABLE dbo.OpenHouses ADD CONSTRAINT PK_OpenHouses PRIMARY KEY NONCLUSTERED (OpHoID ASC)

DROP INDEX [IX_OpenHouses_Mls_Number] ON [dbo].[OpenHouses] WITH ( ONLINE = OFF )

-- Create clustered index 
CREATE CLUSTERED INDEX [IX_OpenHouses_Mls_Number] ON [dbo].[OpenHouses]
(
	[mls_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO