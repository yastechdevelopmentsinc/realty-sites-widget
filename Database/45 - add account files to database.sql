alter table dbo.accountfeatures add FileManagement bit not null default 0

-- may not work to create table....see below scripts

USE [RealtyExecutives]
GO

/****** Object:  Table [dbo].[AccountFiles]    Script Date: 06/21/2012 08:01:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AccountFiles](
	[Id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[Account_id] [uniqueidentifier] NOT NULL,
	[FileName] [nvarchar](150) NOT NULL,
	[MimeType] [nvarchar](150) NOT NULL,
	[FileContent] [varbinary](max) FILESTREAM  NULL,
 CONSTRAINT [PK_AccountFiles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] FILESTREAM_ON [RealtyExecutivesFileStreamGroup]
) ON [PRIMARY] FILESTREAM_ON [RealtyExecutivesFileStreamGroup]

GO

SET ANSI_PADDING OFF
GO


-- manually enable filestream object
EXEC sp_configure filestream_access_level, 2
RECONFIGURE

-- enable filestream object
ALTER DATABASE RealtyExecutives
ADD FILEGROUP RealtyExecutivesFileStreamGroup
  CONTAINS FILESTREAM
GO

ALTER DATABASE RealtyExecutives
ADD FILE  
  (NAME = 'RealtyExecutives_Group'
   , FILENAME = 'C:\Databases\RealtyExecutives'
   )
TO FILEGROUP RealtyExecutivesStreamGroup 
GO


-- create account files table
CREATE TABLE [dbo].[AccountFiles](
	[Id] [uniqueidentifier]  NOT NULL,
	[Account_id] [uniqueidentifier] NOT NULL,
	[FileName] [nvarchar](150) NOT NULL,
	[MimeType] [nvarchar](50) NOT NULL
 CONSTRAINT [PK_AccountFiles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))

ALTER TABLE dbo.AccountFiles
  ALTER COLUMN Id ADD ROWGUIDCOL
  
ALTER TABLE dbo.AccountFiles
  ADD FileContent varbinary(max) FILESTREAM NULL
  
