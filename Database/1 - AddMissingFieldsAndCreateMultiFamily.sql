USE [RealtyExecutives]

alter table dbo.residential add Co_Agent_ind_code varchar(255) null
alter table dbo.residential add mort_type varchar(255) null

alter table farmsandland add grain_total varchar(255) null
GO

USE [RealtyExecutives]
GO

/****** Object:  Table [dbo].[MultiFam]    Script Date: 02/07/2012 13:59:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[MultiFam](
	[PropertyType] [varchar](255) NULL,
	[matrix_unique_id] [int] NULL,
	[matrix_modified_dt] [datetime] NULL,
	[listing_date] [datetime] NULL,
	[list_price] [decimal](18, 0) NULL,
	[mls_number] [int] NULL,
	[area_name] [varchar](255) NULL,
	[days_mkt] [int] NULL,
	[major_type] [varchar](255) NULL,
	[status] [varchar](255) NULL,
	[street_dir] [varchar](255) NULL,
	[street_type] [varchar](255) NULL,
	[style] [varchar](255) NULL,
	[sub_area_name] [varchar](255) NULL,
	[total_units] [int] NULL,
	[address] [varchar](255) NULL,
	[broker_name] [varchar](255) NULL,
	[city] [varchar](255) NULL,
	[frontage] [varchar](255) NULL,
	[hi_house_numb] [varchar](50) NULL,
	[lo_house_numb] [varchar](50) NULL,
	[street_name] [varchar](255) NULL,
	[zoning] [varchar](255) NULL,
	[Photo_count] [int] NULL,
	[Photo_count_modified_dt] [datetime] NULL,
	[current_price] [decimal](18, 0) NULL,
	[depth_information] [varchar](255) NULL,
	[settodelete] [bit] NULL,
	[further_info] [varchar](255) NULL,
	[lease_sale] [varchar](255) NULL,
	[levy] [money] NULL,
	[prop_offered] [varchar](255) NULL,
	[roof] [varchar](255) NULL,
	[rural_res] [varchar](255) NULL,
	[tax_year] [int] NULL,
	[taxes] [int] NULL,
	[equip_incl] [varchar](255) NULL,
	[exterior] [varchar](255) NULL,
	[heating] [varchar](255) NULL,
	[outdoor] [varchar](255) NULL,
	[parking] [varchar](255) NULL,
	[broker_phone] [varchar](255) NULL,
	[internet_comm] [varchar](max) NULL,
	[legal_1] [varchar](255) NULL,
	[legal_2] [varchar](255) NULL,
	[owner_name] [varchar](255) NULL,
	[possession] [varchar](255) NULL,
	[postal_code] [varchar](255) NULL,
	[province] [varchar](255) NULL,
	[slsman1_email] [varchar](255) NULL,
	[slsman1_name] [varchar](255) NULL,
	[slsman1_phone] [varchar](255) NULL,
	[slsman1_webaddr] [varchar](255) NULL,
	[slsman2_email] [varchar](255) NULL,
	[slsman2_name] [varchar](255) NULL,
	[slsman2_phone] [varchar](255) NULL,
	[slsman2_webaddr] [varchar](255) NULL,
	[suite_numb] [varchar](255) NULL,
	[year_built] [int] NULL,
	[year_built_code] [varchar](255) NULL,
	[Agent_ind_code] [varchar](255) NULL,
	[Agent_Board_Code] [varchar](255) NULL,
	[Board_Loaded] [bit] NULL,
	[Orig_List_Price] [money] NULL,
	[Prev_List_Price] [money] NULL,
	[Agent_Board_Prov] [varchar](255) NULL,
	[Selling_Agent_Board_Prov] [varchar](255) NULL,
	[Existing_Real_Property_Report] [varchar](max) NULL,
	[List_Price_Change_Date] [varchar](255) NULL,
	[Status_Change_Date] [varchar](255) NULL,
	[Status_Previous] [varchar](50) NULL,
	[Tax_Assessed] [bit] NULL,
	[XHour] [bit] NULL,
	[XHour_Message] [varchar](255) NULL,
	[Price_per_SqFt] [money] NULL,
	[Latitude] [decimal](12, 9) NULL,
	[Longitude] [decimal](12, 9) NULL,
	[SuccessfullyGeocoded] [bit] NULL,
	[Co_Agent_ind_code] [varchar](255) NULL,
	[further_info2] [varchar](255) NULL,
	[parking_yes_no] [varchar](255) NULL,
	[storeys] [varchar](255) NULL,
	[bachelor] [varchar](255) NULL,
	[bedrm_1] [varchar](255) NULL,
	[bedrm_2] [varchar](255) NULL,
	[bedrm_3] [varchar](255) NULL,
	[bedrm_4] [varchar](255) NULL,
	[bedrm_5] [varchar](255) NULL,
	[elevators] [varchar](255) NULL,
	[flooring] [varchar](255) NULL,
	[parking_cov] [varchar](255) NULL,
	[parking_open] [varchar](255) NULL,
	[parking_tot] [varchar](255) NULL,
	[smoke_detector] [varchar](255) NULL,
	[sprinklers] [varchar](255) NULL,
	[amenities] [varchar](255) NULL,
	[site_infl] [varchar](255) NULL,
	[bus_name] [varchar](255) NULL,
	[mort_type] [varchar](255) NULL,
	[System_Sold_Date] [datetime] NULL,
	[construction] [varchar](255) NULL,
	[MultFamID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_MultiFam] PRIMARY KEY CLUSTERED 
(
	[MultFamID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

