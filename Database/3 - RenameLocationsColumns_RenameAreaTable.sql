use [RealtyExecutives]
go

ALTER TABLE dbo.Locations DROP COLUMN [City / City Area]

EXEC sp_RENAME 'Locations.[Area of City / Town]' , 'AreaName', 'COLUMN'
EXEC sp_RENAME 'Locations.[Area Code]' , 'AreaCode', 'COLUMN'
EXEC sp_RENAME 'Locations.[District Code]' , 'DistrictCode', 'COLUMN'
EXEC sp_RENAME '[tblArea]' , 'Area'