USE [RealtyExecutives]
GO

/****** Object:  Table [dbo].[Account]    Script Date: 03/01/2012 12:35:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Account](
	[ID] [uniqueidentifier] NOT NULL,
	[AccountType] [varchar](10) NULL,
	[Name] [varchar](50) NOT NULL,
	[IndustryCode] [varchar](50) NULL,
	[BoardCode] [varchar](50) NULL,
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Account] ADD  DEFAULT ('') FOR [Name]
GO


