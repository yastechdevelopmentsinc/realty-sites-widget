USE [RealtyExecutives]
GO

/****** Object:  Table [dbo].[AccountListingAccess]    Script Date: 03/03/2012 22:04:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AccountListingAccess](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Residential] [varchar](12) NOT NULL,
	[Commercial] [varchar](12) NOT NULL,
	[FarmsAndLand] [varchar](12) NOT NULL,
	[ForLease] [varchar](12) NOT NULL,
	[MultiFam] [varchar](12) NOT NULL,
	[Account_id] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_AccountListingAccess] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


