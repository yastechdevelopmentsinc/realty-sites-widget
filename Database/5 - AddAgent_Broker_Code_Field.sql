alter table dbo.agents add Account_id uniqueidentifier null

alter table dbo.FarmsAndLand add Agent_Broker_Code varchar(255) null 	 
alter table dbo.ForLease add Agent_Broker_Code varchar(255) null	 
alter table dbo.MultiFam add Agent_Broker_Code varchar(255) null	 
alter table dbo.Commercial add Agent_Broker_Code varchar(255) null	 
alter table dbo.Residential add Agent_Broker_Code varchar(255) null	 


ALTER TABLE dbo.FarmsAndLand
ALTER COLUMN gst VARCHAR(60)

ALTER TABLE dbo.ForLease
ALTER COLUMN gst VARCHAR(60)

ALTER TABLE dbo.MultiFam
Add gst VARCHAR(60)

ALTER TABLE dbo.Commercial
ALTER COLUMN gst VARCHAR(60)

ALTER TABLE dbo.Residential
ALTER COLUMN gst VARCHAR(60)