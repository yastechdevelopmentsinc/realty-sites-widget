USE [RealtyExecutives]
GO

/****** Object:  Table [dbo].[AccountUrl]    Script Date: 03/01/2012 11:11:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AccountUrl](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Url] [varchar](250) NOT NULL,
	[Account_id] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_AccountUrl] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Index [IX_AccountUrl_Account_id]    Script Date: 03/01/2012 11:12:07 ******/
CREATE CLUSTERED INDEX [IX_AccountUrl_Account_id] ON [dbo].[AccountUrl] 
(
	[Account_id] asc
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


SET ANSI_PADDING OFF
GO





