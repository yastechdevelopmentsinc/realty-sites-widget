USE [RealtyExecutives]
GO

/****** Object:  Table [dbo].[BulletinBoard]    Script Date: 03/05/2012 16:24:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BulletinBoard](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[Description] [text] NOT NULL,
	[StartDate] [date] NULL,
	[Account_id] [uniqueidentifier] NOT NULL,
	[Agent_id] [int] NOT NULL,
 CONSTRAINT [PK_BulletinBoard] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


