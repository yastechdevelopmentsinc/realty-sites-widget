/****** Object:  Table [dbo].[BulletinBoardPhoto]    Script Date: 04/09/2012 11:06:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BulletinBoardPhoto](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BulletinBoard_id] [int] NOT NULL,
	[PhotoMimeType] [nvarchar](50) NULL,
	[Photo] [varbinary](max) NULL,
	[PhotoCaption] [nvarchar](250) NULL,
 CONSTRAINT [PK_Table] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


