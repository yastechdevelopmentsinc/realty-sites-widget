USE [RealtyExecutives]
GO

/****** Object:  Index [IX_Residential_BrokerCode]    Script Date: 05/14/2012 09:16:47 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Residential]') AND name = N'IX_Residential_BrokerCode')
DROP INDEX [IX_Residential_BrokerCode] ON [dbo].[Residential] WITH ( ONLINE = OFF )
GO

USE [RealtyExecutives]
GO

/****** Object:  Index [IX_Residential_BrokerCode]    Script Date: 05/14/2012 09:16:02 ******/
CREATE NONCLUSTERED INDEX [IX_Residential_BrokerCode] ON [dbo].[Residential] 
(
	[Agent_Broker_Code] ASC,
	[Agent_Board_Prov] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

USE [RealtyExecutives]
GO

USE [RealtyExecutives]
GO

/****** Object:  Index [IX_MultiFam_BoardCode]    Script Date: 05/14/2012 09:18:10 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[MultiFam]') AND name = N'IX_MultiFam_BoardCode')
DROP INDEX [IX_MultiFam_BoardCode] ON [dbo].[MultiFam] WITH ( ONLINE = OFF )
GO

/****** Object:  Index [IX_MultiFam_BoardCode]    Script Date: 05/14/2012 09:17:47 ******/
CREATE NONCLUSTERED INDEX [IX_MultiFam_BoardCode] ON [dbo].[MultiFam] 
(
	[Agent_Board_Code] ASC,
	[Agent_Board_Prov] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


USE [RealtyExecutives]
GO

/****** Object:  Index [IX_ForLease_BoardCode]    Script Date: 05/14/2012 09:18:35 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ForLease]') AND name = N'IX_ForLease_BoardCode')
DROP INDEX [IX_ForLease_BoardCode] ON [dbo].[ForLease] WITH ( ONLINE = OFF )
GO


USE [RealtyExecutives]
GO

/****** Object:  Index [IX_ForLease_BoardCode]    Script Date: 05/14/2012 09:18:29 ******/
CREATE NONCLUSTERED INDEX [IX_ForLease_BoardCode] ON [dbo].[ForLease] 
(
	[Agent_Board_Code] ASC,
	[Agent_Board_Prov] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


USE [RealtyExecutives]
GO

/****** Object:  Index [IX_FarmsAndLand_BoardCode]    Script Date: 05/14/2012 09:19:00 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FarmsAndLand]') AND name = N'IX_FarmsAndLand_BoardCode')
DROP INDEX [IX_FarmsAndLand_BoardCode] ON [dbo].[FarmsAndLand] WITH ( ONLINE = OFF )
GO

USE [RealtyExecutives]
GO

/****** Object:  Index [IX_FarmsAndLand_BoardCode]    Script Date: 05/14/2012 09:18:57 ******/
CREATE NONCLUSTERED INDEX [IX_FarmsAndLand_BoardCode] ON [dbo].[FarmsAndLand] 
(
	[Agent_Board_Code] ASC,
	[Agent_Board_Prov] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

USE [RealtyExecutives]
GO

/****** Object:  Index [IX_Commercial_BoardCode]    Script Date: 05/14/2012 09:19:31 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Commercial]') AND name = N'IX_Commercial_BoardCode')
DROP INDEX [IX_Commercial_BoardCode] ON [dbo].[Commercial] WITH ( ONLINE = OFF )
GO


USE [RealtyExecutives]
GO

/****** Object:  Index [IX_Commercial_BoardCode]    Script Date: 05/14/2012 09:19:28 ******/
CREATE NONCLUSTERED INDEX [IX_Commercial_BoardCode] ON [dbo].[Commercial] 
(
	[Agent_Board_Code] ASC,
	[Agent_Board_Prov] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO





