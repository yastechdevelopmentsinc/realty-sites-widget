USE [RealtyExecutives]
GO

-- Drop Existing Primary Index 
ALTER TABLE dbo.Residential
DROP CONSTRAINT PK_Residential

-- Create the primary key again as a non clustered index
ALTER TABLE dbo.Residential ADD CONSTRAINT PK_Residential PRIMARY KEY NONCLUSTERED (ResiID ASC)

-- Create clustered index 
CREATE CLUSTERED INDEX [IX_Residential_ListPrice_Bathrooms_Bedrooms] ON [dbo].[Residential] 
(
	[list_price] ASC,
	[bathrooms] ASC,
	[numb_beds] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_Residential_AgentCode]
ON [dbo].[Residential] ([Agent_ind_code])
GO
	
CREATE NONCLUSTERED INDEX [IX_Residential_BrokerCode]
ON [dbo].[Residential] ([Agent_Broker_Code])
GO

CREATE NONCLUSTERED INDEX [IX_Residential_BoardCode] 
ON [dbo].[Residential] ([Agent_Board_Code])


-- Drop Existing Primary Index 
ALTER TABLE dbo.MultiFam
DROP CONSTRAINT PK_MultiFam

-- Create the primary key again as a non clustered index
ALTER TABLE dbo.MultiFam ADD CONSTRAINT PK_MultiFam PRIMARY KEY NONCLUSTERED (MultFamID ASC)

-- Create clustered index 
CREATE CLUSTERED INDEX [IX_MultiFam_ListPrice] ON [dbo].[MultiFam] 
(
	[list_price] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_MultiFam_AgentCode]
ON [dbo].[MultiFam] ([Agent_ind_code])
GO
	
CREATE NONCLUSTERED INDEX [IX_MultiFam_BrokerCode]
ON [dbo].[MultiFam] ([Agent_Broker_Code])
GO

CREATE NONCLUSTERED INDEX [IX_MultiFam_BoardCode] 
ON [dbo].[MultiFam] ([Agent_Board_Code])

-- Drop Existing Primary Index 
ALTER TABLE dbo.FarmsAndLand
DROP CONSTRAINT PK_FarmsAndLand

-- Create the primary key again as a non clustered index
ALTER TABLE dbo.FarmsAndLand ADD CONSTRAINT PK_FarmsAndLand PRIMARY KEY NONCLUSTERED (FarmsAndLandID ASC)

-- Create clustered index 
CREATE CLUSTERED INDEX [IX_FarmsAndLand_ListPrice_Bathrooms_Bedrooms] ON [dbo].[FarmsAndLand] 
(
	[list_price] ASC,
	[bathrooms] ASC,
	[numb_beds] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_FarmsAndLand_AgentCode]
ON [dbo].[FarmsAndLand] ([Agent_ind_code])
GO
	
CREATE NONCLUSTERED INDEX [IX_FarmsAndLand_BrokerCode]
ON [dbo].[FarmsAndLand] ([Agent_Broker_Code])
GO

CREATE NONCLUSTERED INDEX [IX_FarmsAndLand_BoardCode] 
ON [dbo].[FarmsAndLand] ([Agent_Board_Code])

-- Drop Existing Primary Index 
ALTER TABLE dbo.ForLease
DROP CONSTRAINT PK_ForLease

-- Create the primary key again as a non clustered index
ALTER TABLE dbo.ForLease ADD CONSTRAINT PK_ForLease PRIMARY KEY NONCLUSTERED (ForLeaseID ASC)

-- Create clustered index 
CREATE CLUSTERED INDEX [IX_ForLease_ListPrice] ON [dbo].[ForLease] 
(
	[list_price] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_ForLease_AgentCode]
ON [dbo].[ForLease] ([Agent_ind_code])
GO
	
CREATE NONCLUSTERED INDEX [IX_ForLease_BrokerCode]
ON [dbo].[ForLease] ([Agent_Broker_Code])
GO

CREATE NONCLUSTERED INDEX [IX_ForLease_BoardCode] 
ON [dbo].[ForLease] ([Agent_Board_Code])

-- Drop Existing Primary Index 
ALTER TABLE dbo.Commercial
DROP CONSTRAINT PK_Commercial

-- Create the primary key again as a non clustered index
ALTER TABLE dbo.Commercial ADD CONSTRAINT PK_Commercial PRIMARY KEY NONCLUSTERED (CommID ASC)

-- Create clustered index 
CREATE CLUSTERED INDEX [IX_Commercial_ListPrice] ON [dbo].[Commercial] 
(
	[list_price] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_Commercial_AgentCode]
ON [dbo].[Commercial] ([Agent_ind_code])
GO
	
CREATE NONCLUSTERED INDEX [IX_Commercial_BrokerCode]
ON [dbo].[Commercial] ([Agent_Broker_Code])
GO

CREATE NONCLUSTERED INDEX [IX_Commercial_BoardCode] 
ON [dbo].[Commercial] ([Agent_Board_Code])




