alter table dbo.exclusives alter column address varchar(255)
alter table dbo.exclusives alter column basement varchar(255)
alter table dbo.exclusives alter column bathrooms int
alter table dbo.exclusives alter column depth_information varchar(255)
alter table dbo.exclusives alter column equip_incl varchar(255)
alter table dbo.exclusives alter column exterior varchar(255)
alter table dbo.exclusives alter column frontage varchar(255)
alter table dbo.exclusives alter column garages varchar(255)
alter table dbo.exclusives alter column heating varchar(255)
alter table dbo.exclusives alter column parking varchar(255)
alter table dbo.exclusives alter column roof varchar(255)
alter table dbo.exclusives alter column slsman1_email varchar(255)
alter table dbo.exclusives alter column slsman1_name varchar(255)
alter table dbo.exclusives alter column slsman1_phone varchar(255)
alter table dbo.exclusives alter column slsman1_webaddr varchar(255)
alter table dbo.exclusives alter column sq_footage varchar(255)
alter table dbo.exclusives alter column style varchar(255)
alter table dbo.exclusives alter column sub_area_name varchar(255)
alter table dbo.exclusives alter column type_dwelling varchar(255)
alter table dbo.exclusives add AreaCode nvarchar(255)
alter table dbo.exclusives add DistrictCode nvarchar(255)