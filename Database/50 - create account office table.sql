USE [RealtyExecutives]
GO

/****** Object:  Table [dbo].[AccountOffice]    Script Date: 01/06/2013 17:17:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AccountOffice](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Account_id] [uniqueidentifier] NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Address] [varchar](250) NOT NULL,
	[City] [varchar](250) NOT NULL,
	[Province] [varchar](250) NOT NULL,
	[PostalCode] [varchar](250) NULL,
	[Phone] [varchar](250) NULL,
	[Fax] [varchar](250) NULL,
	[Website] [varchar](250) NULL,
 CONSTRAINT [PK_AccountOffice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


