USE [RealtyExecutives]
GO

/****** Object:  Table [dbo].[Disclaimer]    Script Date: 05/25/2012 11:42:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Disclaimer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BoardCode] [varchar](255) NULL,
	[MLSListing] [nvarchar](max) NULL,
	[MLSListingDetails] [nvarchar](max) NULL,
	[ExclusiveListing] [nvarchar](max) NULL,
	[ExclusiveListingDetails] [nvarchar](max) NULL,
 CONSTRAINT [PK_Disclaimer] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


