USE [RealtyExecutives]
GO

/****** Object:  View [dbo].[vwListings]    Script Date: 04/04/2012 10:38:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[vwListings]
AS
SELECT     ResiID AS ID, PropertyType, Address, Agent_Board_Code, Agent_Board_Prov, Agent_ind_code, LEFT(area_name, 2) AS area_code, LEFT(sub_area_name, 2) as sub_area_code, sub_area_name, city, list_price, 
                      mls_number, sq_footage, status, type_dwelling, year_built
FROM         dbo.Residential
UNION ALL
SELECT     CommID AS ID, PropertyType, Address, Agent_Board_Code, Agent_Board_Prov, Agent_ind_code, LEFT(area_name, 2) AS area_code, LEFT(sub_area_name, 2) as sub_area_code, sub_area_name, city, list_price, 
                      mls_number, '' AS sq_footage, status, '' AS type_dwelling, year_built
FROM         dbo.Commercial
UNION ALL
SELECT     FarmsAndLandID AS ID, PropertyType, Address, Agent_Board_Code, Agent_Board_Prov, Agent_ind_code, LEFT(area_name, 2) AS area_code, LEFT(sub_area_name, 2) as sub_area_code, sub_area_name, city, 
                      list_price, mls_number, sq_footage, status, '' AS type_dwelling, year_built
FROM         dbo.FarmsAndLand
UNION ALL
SELECT     ForLeaseID AS ID, PropertyType, Address, Agent_Board_Code, Agent_Board_Prov, Agent_ind_code, LEFT(area_name, 2) AS area_code, LEFT(sub_area_name, 2) as sub_area_code, sub_area_name, city, 
                      list_price, mls_number, '' AS sq_footage, status, '' AS type_dwelling, '' AS year_built
FROM         dbo.ForLease
UNION ALL
SELECT     MultFamID AS ID, PropertyType, Address, Agent_Board_Code, Agent_Board_Prov, Agent_ind_code, LEFT(area_name, 2) AS area_code, LEFT(sub_area_name, 2) as sub_area_code, sub_area_name, city, list_price, 
                      mls_number, '' as sq_footage, status, '' as type_dwelling, year_built
FROM         dbo.MultiFam



GO


