USE [RealtyExecutives]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AccountFeatures](
	Id int identity(1,1) ,
	Listing bit ,
	Search bit ,
	Map bit ,
	FeaturedListing bit ,
	Account_id uniqueidentifier
 CONSTRAINT [PK_AccountFeatures] PRIMARY KEY NONCLUSTERED 
	(
		[Id] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

CREATE CLUSTERED INDEX IX_AccountFeatures_Account_id ON dbo.AccountFeatures (
	Account_id asc
)
Go