alter table dbo.agents add Role Varchar(10) default ('') not null

update dbo.Agents set Role = 'User'
update dbo.Agents set Role = 'Super' Where IsAdmin = 1

alter table dbo.agents drop column IsAdmin