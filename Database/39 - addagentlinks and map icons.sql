alter table dbo.agents add LinkedInUrl varchar(255) null
alter table dbo.agents add YouTubeUrl varchar(255) null

alter table dbo.account add MapMarker varbinary(max) null
alter table dbo.account add MapMarkerMimeType nvarchar(50) null
alter table dbo.account add MapMarkerMore varbinary(max) null 
alter table dbo.account add MapMarkerMoreMimeType nvarchar(50) null