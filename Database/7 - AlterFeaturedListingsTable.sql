USE [RealtyExecutives]

alter table dbo.FeaturedListing add Account_id uniqueidentifier
alter table dbo.FeaturedListing drop column SiteName

-- Drop Existing Primary Index 
ALTER TABLE dbo.FeaturedListing
DROP CONSTRAINT PK_FeaturedListing

-- drop old column
ALTER TABLE dbo.FeaturedListing
DROP COLUMN FeaturedListingID

-- recreate as identity column
Alter table dbo.featuredlisting add Id int Identity(1,1)

-- Create the primary key again as a non clustered index
ALTER TABLE dbo.FeaturedListing ADD CONSTRAINT PK_FeaturedListing PRIMARY KEY NONCLUSTERED (Id ASC)

-- create clustered index
CREATE CLUSTERED INDEX [IX_FeaturedListing_MlsNumber]
ON [dbo].[FeaturedListing] (
	MlsNumber ASC,
	Account_id ASC
)
GO