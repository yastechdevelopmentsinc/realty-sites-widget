USE [RealtyExecutives]
GO

alter table dbo.accountfeatures add FeaturedAgent bit not null default ('0')
GO


CREATE TABLE [dbo].[FeaturedAgent](
	[Agent_id] [int] NULL,
	[Account_id] [uniqueidentifier] NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_FeaturedAgent] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

