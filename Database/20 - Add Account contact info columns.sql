USE [RealtyExecutives]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[Account] ADD [Phone] varchar(14) NULL;
ALTER TABLE [dbo].[Account] ADD [Fax] varchar(14) NULL;
ALTER TABLE [dbo].[Account] ADD [Email] varchar(100) NULL;
ALTER TABLE [dbo].[Account] ADD [Website] varchar(255) NULL;
ALTER TABLE [dbo].[Account] ADD [Logo] varbinary(max) NULL;
ALTER TABLE [dbo].[Account] ADD [LogoMimeType] nvarchar(50) NULL;
GO


