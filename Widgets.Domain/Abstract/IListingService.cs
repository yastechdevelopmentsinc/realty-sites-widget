﻿using System.Linq;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Abstract
{
    public interface IListingService
    {
        IQueryable<Residential> GetAllResidential();
        IQueryable<ForLease> GetAllForLease();
        IQueryable<Commercial> GetAllCommercial();
        IQueryable<FarmsAndLand> GetAllFarmsAndLand();
        IQueryable<MultiFam> GetAllMultiFamily();
        IQueryable<Exclusive> GetAllExclusives(Account account);

        Residential GetResidentialListing(int mlsnumber);
        ForLease GetForLeaseListing(int mlsnumber);
        Commercial GetCommercialListing(int mlsnumber);
        FarmsAndLand GetFarmsAndLandListing(int mlsnumber);
        MultiFam GetMultiFamilyListing(int mlsnumber);
        Exclusive GetExclusiveListing(int exclusiveid);

        void Save(Residential property);
        void Save(ForLease property);
        void Save(Commercial property);
        void Save(FarmsAndLand property);
        void Save(MultiFam property);
        void Save(Exclusive property);

        IQueryable<VResidential> GetAllVResidential();
        IQueryable<VCommercial> GetAllVCommercial();

        VResidential GetVResidentialListing(int mlsnumber);
        VCommercial GetVCommercialListing(int mlsnumber);

        void Save(VResidential property);
        void Save(VCommercial property);

        IQueryable<CListing> GetAllCListings();

        IQueryable<CBoardID> GetAllCBoardID();

        CListing GetCListings(int mlsnumber);

        void Save(CListing property);

    }
}
