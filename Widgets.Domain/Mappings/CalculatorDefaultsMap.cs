﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class CalculatorDefaultsMap : ClassMap<CalculatorDefaults>
    {
        public CalculatorDefaultsMap()
        {
            Cache.NonStrictReadWrite();
            Table("CalculatorDefaults");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Amortization).Column("Amortization");
            Map(x => x.Term).Column("Term");
            Map(x => x.InterestRate).Column("InterestRate");
            Map(x => x.Principal).Column("Principal");
            Map(x => x.DownPayment).Column("DownPayment");
        }
    }
}
