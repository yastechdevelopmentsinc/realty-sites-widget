﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class BulletinBoardPhotoMap : ClassMap<BulletinBoardPhoto>
    {
        public BulletinBoardPhotoMap()
        {
            Cache.NonStrictReadWrite();
            Table("BulletinBoardPhoto");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.PhotoMimeType).Column("PhotoMimeType").Length(50);
            Map(x => x.Photo).Column("Photo").Length(2147483647);
            Map(x => x.PhotoCaption).Column("PhotoCaption").Length(250);
            Map(x => x.PhotoIndex).Column("PhotoIndex");
            References(x => x.BulletinBoard, "BulletinBoard_id");
        }
    }
}
