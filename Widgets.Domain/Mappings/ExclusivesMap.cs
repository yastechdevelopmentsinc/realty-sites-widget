﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class ExclusivesMap : ClassMap<Exclusive>
    {
        public ExclusivesMap()
        {
            Cache.NonStrictReadWrite();
            Table("Exclusives");
            LazyLoad();
            Id(x => x.ExclusiveID).GeneratedBy.Identity().Column("ExclusiveID");
            Map(x => x.PropertyType).Column("PropertyType").Length(255);
            Map(x => x.Address).Column("address").Length(255);
            Map(x => x.AgentIndCode).Column("Agent_ind_code").CustomType("AnsiString").Length(255);
            Map(x => x.CoAgentIndCode).Column("Co_Agent_ind_code").Length(255);
            Map(x => x.CoAgentIndCode2).Column("Co_Agent_ind_code2").Length(255);
            Map(x => x.AgentBoardCode).Column("Agent_Board_Code").CustomType("AnsiString").Length(255);
            Map(x => x.Basement).Column("basement").Length(255);
            Map(x => x.Bathrooms).Column("bathrooms").Length(255);
            Map(x => x.City).Column("city").Length(255);
            Map(x => x.DepthInformation).Column("depth_information").Length(255);
            Map(x => x.EquipIncl).Column("equip_incl").Length(255);
            Map(x => x.Exterior).Column("exterior").Length(255);
            Map(x => x.InternetComm).Column("internet_comm");
            Map(x => x.Frontage).Column("frontage").Length(255);
            Map(x => x.Garages).Column("garages").Length(255);
            Map(x => x.Heating).Column("heating").Length(255);
            Map(x => x.ListPrice).Column("list_price");
            Map(x => x.MlsNumber).Column("mls_number");
            Map(x => x.NumbBeds).Column("numb_beds");
            Map(x => x.NumbRooms).Column("numb_rooms");
            Map(x => x.Parking).Column("parking").Length(255);
            Map(x => x.PhotoCount).Column("Photo_Count");
            Map(x => x.Roof).Column("roof").Length(255);
            Map(x => x.Slsman1Email).Column("slsman1_email").Length(255);
            Map(x => x.Slsman1Name).Column("slsman1_name").Length(255);
            Map(x => x.Slsman1Phone).Column("slsman1_phone").Length(255);
            Map(x => x.Slsman1Webaddr).Column("slsman1_webaddr").Length(255);
            Map(x => x.SqFootage).Column("sq_footage").Length(255);
            Map(x => x.Style).Column("style").Length(255);
            Map(x => x.Status).Column("PropertyStatus").Length(255);
            Map(x => x.SubAreaName).Column("sub_area_name").CustomType("AnsiString").Length(255).Length(255);
            Map(x => x.TypeDwelling).Column("type_dwelling").Length(255);
            Map(x => x.YearBuilt).Column("year_built");
            Map(x => x.Latitude).Column("Latitude").CustomSqlType("decimal(12,9)");
            Map(x => x.Longitude).Column("Longitude").CustomSqlType("decimal(12,9)");
            Map(x => x.DaysMkt).Column("days_mkt");
            Map(x => x.AreaName).Column("area_name").Length(255);
            Map(x => x.VirtualTour).Column("virtual_tour").Length(255);
            Map(x => x.DateEntered).Column("dateEntered");
            Map(x => x.BrokerName).Column("broker_name").Length(255);
            Map(x => x.AgentBrokerCode).Column("Agent_Broker_Code").CustomType("AnsiString").Length(255);
            Map(x => x.AreaCode).Column("AreaCode");
            Map(x => x.DistrictCode).Column("DistrictCode");
            Map(x => x.PriceHash).Column("price_hash");
            Map(x => x.ShowDecimal).Column("showdecimal");
            Map(x => x.IsActive).Column("isactive");
            Map(x => x.IsRental).Column("isrental");
            Map(x => x.PostalCode).Column("PostalCode");
            References(x => x.Account, "Account_id");

            HasMany(x => x.OpenHouses)
                .AsBag()
                .ForeignKeyConstraintName("none")
                .KeyColumn("mls_number");

            HasMany(x => x.VOpenHouses)
                .AsBag()
                .ForeignKeyConstraintName("none")
                .KeyColumn("Listing_Id");
        }
    }
}
