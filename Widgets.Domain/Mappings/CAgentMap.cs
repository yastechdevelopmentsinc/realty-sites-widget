﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class CAgentMap : ClassMap<CAgent>
    {
        public CAgentMap()
        {
            Cache.NonStrictReadWrite();
            Table("Calgary_Agent");
            LazyLoad();
            Id(x => x.ID).Column("ID");

            Map(x => x.AgentIndCode).Column("ID");
            Map(x => x.AdministrationInformation).Column("AdministrationInformation");
            Map(x => x.AgentStatus).Column("AgentStatus");
            Map(x => x.AgentType).Column("AgentType");
            Map(x => x.Board).Column("Board");
            Map(x => x.Cell).Column("CellPhone");
            Map(x => x.CREAID).Column("CREAID");
            Map(x => x.Phone).Column("DirectWorkPhone");
            Map(x => x.Email).Column("Email");
            Map(x => x.FaxPhone).Column("FaxPhone");
            Map(x => x.FirstName).Column("FirstName");
            Map(x => x.FullName).Column("FullName");
            Map(x => x.GenerationalName).Column("GenerationalName");
            Map(x => x.HomePhone).Column("HomePhone");
            Map(x => x.IDXOptInYN).Column("IDXOptInYN");
            Map(x => x.IsDeleted).Column("IsDeleted");
            Map(x => x.LastName).Column("LastName");
            Map(x => x.LicenseNumber).Column("LicenseNumber");
            Map(x => x.MailAddress).Column("MailAddress");
            Map(x => x.MailCareOf).Column("MailCareOf");
            Map(x => x.MailCity).Column("MailCity");
            Map(x => x.MailPostalCode).Column("MailPostalCode");
            Map(x => x.MailPostalCodePlus4).Column("MailPostalCodePlus4");
            Map(x => x.MailStateOrProvince).Column("MailStateOrProvince");
            Map(x => x.MatrixModifiedDT).Column("MatrixModifiedDT");
            Map(x => x.MatrixTesting).Column("MatrixTesting");
            Map(x => x.MatrixUserType).Column("MatrixUserType");
            Map(x => x.MembershipDate).Column("MembershipDate");
            Map(x => x.MiddleName).Column("MiddleName");
            Map(x => x.MLS).Column("MLS");
            Map(x => x.MLSID).Column("MLSID");
            Map(x => x.AgentTitle).Column("MUC");
            Map(x => x.Office_MUI).Column("Office_MUI");
            Map(x => x.OfficeMLSID).Column("OfficeMLSID");
            Map(x => x.OtherPhone).Column("OtherPhone");
            Map(x => x.PhotoCount).Column("PhotoCount");
            Map(x => x.PhotoModificationTimestamp).Column("PhotoModificationTimestamp");
            Map(x => x.ProviderKey).Column("ProviderKey");
            Map(x => x.ProviderModificationTimestamp).Column("ProviderModificationTimestamp");
            Map(x => x.RETSOfficeVisibility).Column("RETSOfficeVisibility");
            Map(x => x.RUC).Column("RUC");
            Map(x => x.StreetAddress).Column("StreetAddress");
            Map(x => x.StreetCity).Column("StreetCity");
            Map(x => x.StreetPostalCode).Column("StreetPostalCode");
            Map(x => x.StreetPostalCodePlus4).Column("StreetPostalCodePlus4");
            Map(x => x.StreetStateOrProvince).Column("StreetStateOrProvince");
            Map(x => x.TerminationDate).Column("TerminationDate");
            Map(x => x.Fax).Column("UserFax");
            Map(x => x.FacebookUrl).Column("WebFacebook");
            Map(x => x.LinkedInUrl).Column("WebLinkedIn");
            Map(x => x.Website).Column("WebPageAddress");
            Map(x => x.TwitterUrl).Column("WebTwitter");
            

            References(x => x.AgentOffice, "Office_MUI").NotFound.Ignore();
        }

    }
}
