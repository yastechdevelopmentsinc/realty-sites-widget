﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class ListingFilesMap : ClassMap<ListingFiles>
    {
        public ListingFilesMap()
        {
            Cache.NonStrictReadWrite();
            Table("ListingFiles");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Mlsnumber).Column("MLSNumber");
            Map(x => x.FileName).Column("FileName").Not.Nullable().Length(150);
            Map(x => x.FilePath).Column("FilePath").Not.Nullable();
            Map(x => x.MimeType).Column("MimeType").Nullable().Length(150);
            Map(x => x.UploadDate).Column("UploadDate").Not.Nullable();
            Map(x => x.Name).Column("Name").Not.Nullable().Length(150);

            References(x => x.Account).Column("Account_id").Fetch.Join();
        }
    }
}
