﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class BulletinBoardMap : ClassMap<BulletinBoard>
    {
        public BulletinBoardMap()
        {
            Cache.NonStrictReadWrite();
            LazyLoad();
            Table("BulletinBoard");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Title).Column("Title").Not.Nullable().Length(255);
            Map(x => x.Description).Column("Description").Not.Nullable();
            Map(x => x.StartDate).Column(("StartDate"));
            Map(x => x.IsNews).Column(("IsNews")).Not.Nullable();
            References(x => x.Account, "Account_id");
            References(x => x.Agent, "Agent_id")
				.ForeignKey("none");
            HasMany(x => x.Photos)
                .KeyColumn("BulletinBoard_id")
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .AsBag();
        }
    }
}
