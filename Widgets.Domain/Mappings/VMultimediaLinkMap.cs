﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class VMultimediaLinkMap : ClassMap<VMultimediaLink>
    {
        public VMultimediaLinkMap()
        {
            Cache.NonStrictReadWrite();
            Table("VIREB_VMultimediaLinks");
            LazyLoad();
            Id(x => x.ID).Column("ID");
            Map(x => x.Link_Url).Column("Link_Url");
            Map(x => x.Modification_Date).Column("Modification_Date");
            References(x => x.RListing, "Listing_Id");
            References(x => x.CListing, "Listing_Id");
            References(x => x.Link_Type, "Link_Type");
        }
    }
}
