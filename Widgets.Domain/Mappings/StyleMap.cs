﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class StyleMap : ClassMap<Style>
    {
        public StyleMap()
        {
            Cache.NonStrictReadWrite();
            Table("Styles");
            LazyLoad();
            Id(x => x.StyleID).GeneratedBy.Identity().Column("StyleID");
            Map(x => x.StyleName).Column("Style").Not.Nullable().Length(50);
        }
    }
}
