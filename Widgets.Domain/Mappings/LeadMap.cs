﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class LeadMap : ClassMap<Lead>
    {
        public LeadMap()
        {
            Cache.NonStrictReadWrite();
            Table("Leads");
            LazyLoad();
            Id(x => x.LeadId).GeneratedBy.Identity().Column("Lead_Id");
            Map(x => x.Created).Column("Created");
            Map(x => x.Note).Column("Note");

            References(x => x.Agent, "Agent_Id")
                .ForeignKey("none");
        }
    }
}
