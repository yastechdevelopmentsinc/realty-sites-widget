﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class AgentSpecialtyMap: ClassMap<AgentSpecialty>
    {
        public AgentSpecialtyMap()
        {
            Cache.NonStrictReadWrite();
            Table("AgentSpecialty");
            LazyLoad();
            Id(x => x.AgentSpecialtyID).GeneratedBy.Identity().Column("agentspecialtyid");
            Map(x => x.Specialty).Column("specialty").Not.Nullable().Length(50);
        }
    }
}
