﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class FeaturedAgentMap : ClassMap<FeaturedAgent>
    {
        public FeaturedAgentMap()
        {
            Cache.NonStrictReadWrite();
            Table("FeaturedAgent");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            References(x => x.Agent).Column("Agent_id").NotFound.Ignore()
                .Fetch.Join();
            References(x => x.Account).Column("Account_id")
                .Fetch.Join();
        }
    }
}
