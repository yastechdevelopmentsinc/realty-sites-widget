﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class vwListingsMap : ClassMap<vwListings>
    {
        public vwListingsMap()
        {
            Id(x => x.Id).GeneratedBy.Assigned();
            Table("vwListings");
            LazyLoad();
            ReadOnly();
            Map(x => x.PropertyType).Column("PropertyType");
            Map(x => x.Address).Column("Address");
            Map(x => x.AgentBoardCode).Column("Agent_Board_Code");
            Map(x => x.AgentBoardProv).Column("Agent_Board_Prov");
            Map(x => x.AgentIndCode).Column("Agent_ind_code");
            Map(x => x.AreaCode).Column("area_code");
            Map(x => x.SubAreaCode).Column("sub_area_code");
            Map(x => x.SubAreaName).Column("sub_area_name");
            Map(x => x.City).Column("city");
            Map(x => x.ListPrice).Column("list_price");
            Map(x => x.MlsNumber).Column("mls_number");
            Map(x => x.SqFootage).Column("sq_footage");
            Map(x => x.Status).Column("status");
            Map(x => x.TypeDwelling).Column("type_dwelling");
            Map(x => x.YearBuilt).Column("year_built");
        }
    }
}
