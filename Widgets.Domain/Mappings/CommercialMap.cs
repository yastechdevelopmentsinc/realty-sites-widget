using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class CommercialMap : ClassMap<Commercial>
    {
        public CommercialMap()
        {
            Cache.NonStrictReadWrite();
            Table("Commercial");
            LazyLoad();
            Id(x => x.CommID).GeneratedBy.Identity().Column("CommID");
            Map(x => x.PropertyType).Column("PropertyType").Length(255);
            Map(x => x.Address).Column("address").Length(255);
            Map(x => x.AgentBoardCode).Column("Agent_Board_Code").CustomType("AnsiString").Length(255);
            Map(x => x.AgentBoardProv).Column("Agent_Board_Prov").Length(255);
            Map(x => x.AgentIndCode).Column("Agent_ind_code").CustomType("AnsiString").Length(255);
            Map(x => x.Amenities).Column("amenities").Length(255);
            Map(x => x.Amps).Column("amps").Length(255);
            Map(x => x.AreaLease).Column("area_lease").Length(255);
            Map(x => x.AreaName).Column("area_name").Length(255);
            Map(x => x.BrokerName).Column("broker_name").Length(255);
            Map(x => x.BrokerPhone).Column("broker_phone").Length(255);
            Map(x => x.BusName).Column("bus_name").Length(255);
            Map(x => x.CeilCentre).Column("ceil_centre").Length(255);
            Map(x => x.CeilEaves).Column("ceil_eaves").Length(255);
            Map(x => x.City).Column("city").Length(255);
            Map(x => x.Construction).Column("construction").Length(255);
            Map(x => x.DaysMkt).Column("days_mkt");
            Map(x => x.Elevators).Column("elevators").Length(255);
            Map(x => x.EquipIncl).Column("equip_incl").Length(255);
            Map(x => x.GradeDoor).Column("grade_door").Length(250);
            Map(x => x.Heating).Column("heating").Length(255);
            Map(x => x.HiHouseNumb).Column("hi_house_numb").Length(255);
            Map(x => x.InternetComm).Column("internet_comm");
            Map(x => x.LandSize).Column("land_size");
            Map(x => x.LandSizeTotal).Column("land_size_total");
            Map(x => x.Legal1).Column("legal_1").Length(255);
            Map(x => x.Legal2).Column("legal_2").Length(255);
            Map(x => x.ListPrice).Column("list_price");
            Map(x => x.MajorType).Column("major_type").Length(255);
            Map(x => x.MatrixModifiedDt).Column("Matrix_Modified_DT").Length(255);
            Map(x => x.MatrixUniqueID).Column("matrix_unique_id");
            Map(x => x.MlsNumber).Column("mls_number");
            Map(x => x.OfficeArea).Column("office_area").Length(255);
            Map(x => x.ParkingYesNo).Column("parking_yes_no").Length(255);
            Map(x => x.PhotoCount).Column("Photo_Count");
            Map(x => x.PhotoCountModifiedDt).Column("Photo_Count_Modified_DT").Length(255);
            Map(x => x.PostalCode).Column("postal_code").Length(255);
            Map(x => x.Power).Column("power").Length(255);
            Map(x => x.RailDoor).Column("rail_door").Length(255);
            Map(x => x.RetailArea).Column("retail_area").Length(255);
            //Map(x => x.Slsman1Email).Column("slsman1_email").Length(255);
            //Map(x => x.Slsman1Name).Column("slsman1_name").Length(255);
            //Map(x => x.Slsman1Phone).Column("slsman1_phone").Length(255);
            //Map(x => x.Slsman1Webaddr).Column("slsman1_webaddr").Length(255);
            //Map(x => x.Slsman2Email).Column("slsman2_email").Length(255);
            //Map(x => x.Slsman2Name).Column("slsman2_name").Length(255);
            //Map(x => x.Slsman2Phone).Column("slsman2_phone").Length(255);
            Map(x => x.Slsman2Webaddr).Column("slsman2_webaddr").Length(255);
            Map(x => x.StreetDir).Column("street_dir").Length(255);
            Map(x => x.StreetName).Column("street_name").Length(255);
            Map(x => x.StreetType).Column("street_type").Length(255);
            Map(x => x.SubAreaName).Column("sub_area_name").Length(255);
            Map(x => x.Taxes).Column("taxes");
            Map(x => x.TotBldgArea).Column("tot_bldg_area");
            Map(x => x.Volts).Column("volts");
            Map(x => x.WarehouseArea).Column("warehouse_area").Length(255);
            Map(x => x.YearBuilt).Column("year_built");
            Map(x => x.Latitude).Column("Latitude").CustomSqlType("decimal(12,9)");
            Map(x => x.Longitude).Column("Longitude").CustomSqlType("decimal(12,9)");
            //Map(x => x.SuccessfullyGeocoded).Column("SuccessfullyGeocoded");
            //Map(x => x.SetToDelete).Column("SetToDelete");
            Map(x => x.VirtualTour).Column("virtual_tour").Length(255);
            Map(x => x.DateEntered).Column("dateEntered").Length(255);
            Map(x => x.AgentBrokerCode).Column("Agent_Broker_Code").CustomType("AnsiString").Length(255);

            HasOne(x => x.ReplacementCoordinates)
                .PropertyRef(x => x.PropertyRef);

            HasMany(x => x.OpenHouses)
                .AsBag()
                .KeyColumn("mls_number")
                .PropertyRef("MlsNumber");

            HasMany(x => x.ListingFiles)
              .AsBag()
              .KeyColumn("MlsNumber")
              .PropertyRef("MlsNumber");
        }
    }
}