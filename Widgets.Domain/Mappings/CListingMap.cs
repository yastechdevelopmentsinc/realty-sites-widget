﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class CListingMap : ClassMap<CListing>
    {
        public CListingMap()
        {
            Cache.NonStrictReadWrite();
            Table("Calgary_Listings");
            LazyLoad();
            Id(x => x.ID).Column("ID");

            Map(x => x.AddlRooms).Column("AddlRooms");
            Map(x => x.Address).Column("AddressDisplay");
            Map(x => x.AddressDisplayYN).Column("AddressDisplayYN");
            Map(x => x.Basement).Column("Basement");
            Map(x => x.BasementDevelopment).Column("BasementDevelopment");
            Map(x => x.Bathrooms).Column("BathsTotal");
            Map(x => x.NumbBeds).Column("BedsTotal");
            Map(x => x.BoardCode).Column("BoardID");
            Map(x => x.City).Column("City");
            Map(x => x.CoAgentIndCode).Column("CoListAgentMLSID");
            Map(x => x.CoListAgentMLSID).Column("CoListAgentMLSID");
            Map(x => x.CoListOffice_MUI).Column("CoListOffice_MUI");
            Map(x => x.CoListOfficeMLSID).Column("CoListOfficeMLSID");
            Map(x => x.SubAreaName).Column("Community");
            Map(x => x.CondoFee).Column("CondoFee");
            Map(x => x.CondoFeeIncl).Column("CondoFeeIncl");
            Map(x => x.CondoName).Column("CondoName");
            Map(x => x.CondoNameNew).Column("CondoNameNew");
            Map(x => x.CondoType).Column("CondoType");
            Map(x => x.Exterior).Column("Exterior");
            Map(x => x.Fireplace).Column("Fireplace");
            Map(x => x.Flooring).Column("Flooring");
            Map(x => x.Foundation).Column("Foundation");
            Map(x => x.FPFuel).Column("FPFuel");
            Map(x => x.FrontageMetres).Column("FrontageMetres");
            Map(x => x.FrontExposure).Column("FrontExposure");
            Map(x => x.Garages).Column("GarageYN");
            Map(x => x.HeatingFuel).Column("HeatingFuel");
            Map(x => x.HeatingType).Column("HeatingType");
            Map(x => x.LastChangeTimestamp).Column("LastChangeTimestamp");
            Map(x => x.AgentIndCode).Column("ListAgentMLSID");
            Map(x => x.ListAgentDirectWorkPhone).Column("ListAgentDirectWorkPhone");
            Map(x => x.ListAgentEmail).Column("ListAgentEmail");
            Map(x => x.ListAgentFullName).Column("ListAgentFullName");
            Map(x => x.ListAgentMLSID).Column("ListAgentMLSID");
            Map(x => x.ListingFirm1Email).Column("ListingFirm1Email");
            Map(x => x.ListingFirm1Website).Column("ListingFirm1Website");
            Map(x => x.ListingFirm2Email).Column("ListingFirm2Email");
            Map(x => x.ListingFirm2Website).Column("ListingFirm2Website");
            Map(x => x.ListingRealtor1Website).Column("ListingRealtor1Website");
            Map(x => x.ListingRealtor2Website).Column("ListingRealtor2Website");
            Map(x => x.ListingRltr1MemberCode).Column("ListingRltr1MemberCode");
            Map(x => x.ListOffice_MUI).Column("ListOffice_MUI");
            Map(x => x.ListOfficeMLSID).Column("ListOfficeMLSID");
            Map(x => x.ListOfficeName).Column("ListOfficeName");
            Map(x => x.ListOfficePhone).Column("ListOfficePhone");
            Map(x => x.ListPrice).Column("ListPrice");
            Map(x => x.LotShape).Column("LotShape");
            Map(x => x.LotSqMetres).Column("LotSqMetres");
            Map(x => x.MatrixModifiedDT).Column("MatrixModifiedDT");
            Map(x => x.MlsNumber).Column("MLSNumber");
            Map(x => x.Parking).Column("Parking");
            Map(x => x.PhotoCount).Column("PhotoCount");
            Map(x => x.PhotoModificationTimestamp).Column("PhotoModificationTimestamp");
            Map(x => x.PostalCode).Column("PostalCode");
            Map(x => x.PropertySubType).Column("PropertySubType");
            Map(x => x.PropertyType).Column("PropertyType");
            Map(x => x.InternetComm).Column("PublicRemarks");
            Map(x => x.AreaName).Column("Region");
            Map(x => x.RoofType).Column("RoofType");
            Map(x => x.Section).Column("Section");
            Map(x => x.SiteInfluences).Column("SiteInfluences");
            Map(x => x.StateOrProvince).Column("StateOrProvince");
            Map(x => x.StreetDirSuffix).Column("StreetDirSuffix");
            Map(x => x.StreetName).Column("StreetName");
            Map(x => x.StreetNumber).Column("StreetNumber");
            Map(x => x.StreetSuffix).Column("StreetSuffix");
            Map(x => x.Style).Column("Style");
            Map(x => x.TotalAcreage).Column("TotalAcreage");
            Map(x => x.TotalFlrAreaSF).Column("TotalFlrAreaSF");
            Map(x => x.TotalParking).Column("TotalParking");
            Map(x => x.TotalArea).Column("TotFlrAreaAGMetres");
            Map(x => x.UnitNumber).Column("UnitNumber");
            Map(x => x.VirtualTour).Column("URLVirtualTour");
            Map(x => x.WestMeridian).Column("WestMeridian");
            Map(x => x.YearBuilt).Column("YrBuilt");
            Map(x => x.Zone).Column("Zone");
            Map(x => x.Latitude).Column("Latitude");
            Map(x => x.Longitude).Column("Longitude");
            Map(x => x.SqFootage).Column("SqFtTotal");

            References(x => x.Listing_Agent, "ListAgent_MUI").NotFound.Ignore();
            References(x => x.CoListing_Agent, "CoListAgent_MUI").NotFound.Ignore();
            References(x => x.Listing_Office, "ListOffice_MUI").NotFound.Ignore();
            References(x => x.CoListing_Office, "CoListOffice_MUI").NotFound.Ignore();

            HasOne(x => x.ReplacementCoordinates)
               .PropertyRef(x => x.PropertyRef);

            HasMany(x => x.COpenHouses).KeyColumn("Listing_MUI");

            HasMany(x => x.ListingFiles)
              .AsBag()
              .KeyColumn("Listing_MUI")
              .PropertyRef("MlsNumber");
        }
    }
}
