﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class AccountOfficeMap : ClassMap<AccountOffice>
    {
        public AccountOfficeMap()
        {
            Cache.NonStrictReadWrite();
            Table("AccountOffice");
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Name).Column("Name").Not.Nullable().Length(250);
            Map(x => x.Address).Column("Address").Not.Nullable().Length(250);
            Map(x => x.City).Column("City").Not.Nullable().Length(250);
            Map(x => x.Province).Column("Province").Not.Nullable().Length(250);
            Map(x => x.PostalCode).Column("PostalCode").Length(250);
            Map(x => x.Phone).Column("Phone").Length(250);
            Map(x => x.Fax).Column("Fax").Length(250);
            Map(x => x.Website).Column("Website").Length(250);
            Map(x => x.Email).Column("Email").Length(250);
            Map(x => x.IDXListings).Column("IDXListings");

            References(x => x.Account, "Account_id");
        }
    }
}
