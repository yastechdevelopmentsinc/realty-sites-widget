﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class VDistrictMap : ClassMap<VDistrict>
    {
        public VDistrictMap()
        {
            Cache.NonStrictReadWrite();
            Table("VIREB_District");
            LazyLoad();
            Id(x => x.AreaID).Column("ID");
            Map(x => x.AreaDescription).Column("Name");
            Map(x => x.LastChangedDate).Column("LastChangedDate");
        }
    }
}
