﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class ReferralMap : ClassMap<Referral>
    {
        public ReferralMap()
        {
            Cache.NonStrictReadWrite();
            Table("Referral");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.PositionNumber).Column("PositionNumber");
            Map(x => x.LastReferralDate).Column("LastReferralDate");
            Map(x => x.LeadCount).Formula("(select count(*) from leads l where l.Agent_id = Agent_ID)");
            References(x => x.Agent).Column("Agent_ID")
				.ForeignKey("none");
            References(x => x.Account).Column("Account_id");
        }
    }
}
