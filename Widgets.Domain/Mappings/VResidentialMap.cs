﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class VResidentialMap : ClassMap<VResidential>
    {
        public VResidentialMap()
        {
            Cache.NonStrictReadWrite();
            Table("VIREB_VResidentials");
            LazyLoad();
            Id(x => x.ID).Column("ID");
            Map(x => x.Bathrooms).Column("Bathrooms");
            Map(x => x.NumbBeds).Column("Bedrooms");
            Map(x => x.City).Column("City");
            Map(x => x.TempCity).Column("City");
            Map(x => x.Crea_Display_Address).Column("Crea_Display_Address");
            Map(x => x.Crea_Display_Listing).Column("Crea_Display_Listing");
            Map(x => x.Fireplace_Types).Column("Fireplace_Types");
            Map(x => x.Fireplaces).Column("Fireplaces");
            Map(x => x.Fuel).Column("Fuel");
            Map(x => x.Heating).Column("Heating");
            Map(x => x.Interior_Features).Column("Interior_Features");
            Map(x => x.InternetComm).Column("Internet_Remarks");
            Map(x => x.TempInternetComm).Column("Internet_Remarks");
            Map(x => x.Latitude).Column("Latitude");
            Map(x => x.Longitude).Column("Longitude");
            Map(x => x.PhotoCount).Column("Listing_Photo_Count");
            Map(x => x.Lot_Depth).Column("Lot_Depth");
            Map(x => x.Lot_Features).Column("Lot_Features");
            Map(x => x.Lot_Sqft).Column("Lot_Sqft");
            Map(x => x.Lot_Width).Column("Lot_Width");
            Map(x => x.Modification_Date).Column("Modification_Date");
            Map(x => x.Parking_Types).Column("Parking_Types");
            Map(x => x.Photo_Modification_Date).Column("Photo_Modification_Date");
            Map(x => x.PostalCode).Column("Postal_Code");
            Map(x => x.ListPrice).Column("Price_Current");
            Map(x => x.Remarks).Column("Remarks");
            Map(x => x.SqFootage).Column("Sqft_Total");
            Map(x => x.Street_Name).Column("Street_Name");
            Map(x => x.Street_Number).Column("Street_Number");
            Map(x => x.Styles).Column("Style");
            Map(x => x.Title).Column("Title");
            Map(x => x.Unit_Number).Column("Unit_Number");
            Map(x => x.YearBuilt).Column("Year_Built");
            Map(x => x.MlsNumber).Column("ID");
            Map(x => x.AgentIndCode).Column("Listing_Agent_Id");
            Map(x => x.CoAgentIndCode).Column("Colisting_Agent_Id");
            Map(x => x.CoAgentIndCode2).Column("Third_Listing_Agent_Id");
            Map(x => x.AgentBrokerCode).Column("Listing_Office_Id");

            References(x => x.Colisting_Agent, "Colisting_Agent_Id");
            References(x => x.Colisting_Office, "Colisting_Office_Id");
            References(x => x.Cooling, "Cooling");
            References(x => x.Listing_Agent, "Listing_Agent_Id");
            References(x => x.Listing_Office, "Listing_Office_Id");
            References(x => x.Property_Type, "Property_Type");
            References(x => x.Third_Listing_Agent, "Third_Listing_Agent_Id");
            References(x => x.Third_Listing_Office, "Third_Listing_Office_Id");
            References(x => x.Map_Area, "Map_Area").NotFound.Ignore();

            HasOne(x => x.ReplacementCoordinates)
                .PropertyRef(x => x.PropertyRef);

            HasMany(x => x.VOpenHouses)
               .PropertyRef("MlsNumber")
               .KeyColumn("Listing_Id");

            HasMany(x => x.ListingFiles)
              .AsBag()
              .KeyColumn("MlsNumber")
              .PropertyRef("MlsNumber");
        }
    }
}
