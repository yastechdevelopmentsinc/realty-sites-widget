﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class AccountUrlMap : ClassMap<AccountUrl>
    {
        public AccountUrlMap()
        {
            Cache.NonStrictReadWrite();
            Table("AccountUrl");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Url).Column("Url").Not.Nullable().Length(250);
            References(x => x.Account, "Account_id");
        }
    }
}
