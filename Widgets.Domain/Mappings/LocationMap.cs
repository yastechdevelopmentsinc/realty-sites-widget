﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class LocationMap : ClassMap<Location>
    {
        public LocationMap()
        {
            Cache.NonStrictReadWrite();
            Table("Locations");
            LazyLoad();
            Id(x => x.AreaID).GeneratedBy.Identity().Column("AreaID");
            Map(x => x.CityName).Column("CityName").Length(255);
            Map(x => x.AreaName).Column("AreaName").Length(255);
            Map(x => x.AreaCode).Column("AreaCode");
            Map(x => x.DistrictCode).Column("DistrictCode").Length(255);
            Map(x => x.SubAreaName).Column("sub_area_name").Length(255);
            Map(x => x.Latitude).Column("Latitude").CustomSqlType("decimal(12,9)");
            Map(x => x.Longitude).Column("Longitude").CustomSqlType("decimal(12,9)");
            Map(x => x.Active).Column("Active");
            Map(x => x.BoardCode).Column("BoardCode").Nullable().Length(50);
            Map(x => x.Listings).Formula(
                @"(SELECT COUNT(*) from vwListings vw 
            WHERE vw.sub_area_name = sub_area_name
            and vw.city = CityName and vw.Agent_Board_Code = BoardCode )");
        }
    }
}
