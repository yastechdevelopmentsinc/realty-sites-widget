﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class VZoningJurisdictionMap: ClassMap<VZoningJurisdiction>
    {
        public VZoningJurisdictionMap()
        {
            Cache.NonStrictReadWrite();
            Table("VIREB_ZoningJurisdiction");
            LazyLoad();
            Id(x => x.ID).Column("ID");
            Map(x => x.Name).Column("Name");
            Map(x => x.LastChangedDate).Column("LastChangedDate");
        }
    }
}
