﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class VIndustrialTypeMap: ClassMap<VIndustrialType>
    {
        public VIndustrialTypeMap()
        {
            Cache.NonStrictReadWrite();
            Table("VIREB_IndustrialType");
            LazyLoad();
            Id(x => x.ID).Column("ID");
            Map(x => x.Name).Column("Name");
            Map(x => x.LastChangedDate).Column("LastChangedDate");
        }
    }
}
