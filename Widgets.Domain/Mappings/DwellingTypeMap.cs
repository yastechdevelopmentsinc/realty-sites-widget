﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class DwellingTypeMap : ClassMap<DwellingType>
    {

        public DwellingTypeMap()
        {
            Cache.NonStrictReadWrite();
            Table("DwellingTypes");
            LazyLoad();
            Id(x => x.TypeID).GeneratedBy.Identity().Column("TypeID");
            Map(x => x.Type).Column("Type").Not.Nullable().Length(50);
        }
    }
}
