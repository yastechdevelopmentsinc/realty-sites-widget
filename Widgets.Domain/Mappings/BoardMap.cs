﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class BoardMap : ClassMap<Board>
    {
        public BoardMap()
        {
            Cache.NonStrictReadWrite();
            Table("Boards");
            LazyLoad();
            Id(x => x.BoardID).GeneratedBy.Identity().Column("boardid");
            Map(x => x.BoardCode).Column("boardcode").Not.Nullable().Length(50);

            HasMany(x => x.Areas)
                .PropertyRef("BoardCode")
                .KeyColumn("boardcode")
                .Inverse()
                .Cascade.None();
        }
    }
}
