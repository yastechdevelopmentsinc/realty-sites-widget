﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class ReplacementCoordMap : ClassMap<ReplacementCoord>
    {
        public ReplacementCoordMap()
        {
            Cache.NonStrictReadWrite();
            Table("ReplacementCoords");
            LazyLoad();
            Id(x => x.ReplacementID).GeneratedBy.Identity().Column("ReplacementID");
            Map(x => x.PropertyRef).Column("PropertyRef").Not.Nullable();
            Map(x => x.Latitude).Column("Latitude").CustomSqlType("decimal(12,9)");
            Map(x => x.Longitude).Column("Longitude").CustomSqlType("decimal(12,9)");
            Map(x => x.BoardCode).Column("BoardCode").CustomType("AnsiString").Nullable().Length(50);
        }
    }
}
