﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class COpenHouseMap : ClassMap<COpenHouse>
    {
        public COpenHouseMap()
        {
            Cache.NonStrictReadWrite();
            Table("Calgary_OpenHouses");
            LazyLoad();
            Id(x => x.ID).Column("ID");

            Map(x => x.ActiveYN).Column("ActiveYN");
            Map(x => x.Description).Column("Description");
            Map(x => x.EndTime).Column("EndTime");
            Map(x => x.EntryOrder).Column("EntryOrder");
            Map(x => x.IsDeleted).Column("IsDeleted");
            Map(x => x.Latitude).Column("Latitude");
            Map(x => x.Listing_MUI).Column("Listing_MUI");
            Map(x => x.Longitude).Column("Longitude");
            Map(x => x.MatrixModifiedDT).Column("MatrixModifiedDT");
            Map(x => x.MLS).Column("MLS");
            Map(x => x.OpenHouseDate).Column("OpenHouseDate");
            Map(x => x.OpenHouseType).Column("OpenHouseType");
            Map(x => x.ParentProviderKey).Column("ParentProviderKey");
            Map(x => x.ProviderKey).Column("ProviderKey");
            Map(x => x.Refreshments).Column("Refreshments");
            Map(x => x.StartTime).Column("StartTime");
        }
    }
}
