﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class AccountFeaturesMap : ClassMap<AccountFeatures>
    {
        public AccountFeaturesMap()
        {
            Cache.NonStrictReadWrite();
            Table("AccountFeatures");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Listing).Column("Listing");
            Map(x => x.Search).Column("Search");
            Map(x => x.Map).Column("Map");
            Map(x => x.FeaturedListing).Column("FeaturedListing");
            Map(x => x.FeaturedAgent).Column("FeaturedAgent");
            Map(x => x.LatestListing).Column("LatestListing");
            Map(x => x.AgentProfile).Column("AgentProfile");
            Map(x => x.Members).Column("Members");
            Map(x => x.Contacts).Column("Contacts");
            Map(x => x.News).Column("News");
            Map(x => x.MortgageCalculator).Column("MortgageCalculator");
            Map(x => x.BulletinBoard).Column("BulletinBoard");
            Map(x => x.ExclusiveListing).Column("ExclusiveListing");
            Map(x => x.OpenHouses).Column("OpenHouses");
            Map(x => x.Referral).Column("Referral");
            Map(x => x.FileManagement).Column("FileManagement");
            Map(x => x.LinkManagement).Column("LinkManagement");
            Map(x => x.UseSimpleDetailedListing).Column("UseSimpleDetailedListing");
            Map(x => x.NumberOfListingsToDisplay).Column("NumberOfListingsToDisplay");
            Map(x => x.ListingWidgetPageSize).Column("ListingWidgetPageSize");
            Map(x => x.MLSSearch).Column("MLSSearch");
            Map(x => x.AgentProfileLink).Column("AgentProfileLink");
            Map(x => x.AgentWebsiteLink).Column("AgentWebsiteLink");
            Map(x => x.ShowEmailAddress).Column("ShowEmailAddress");
            Map(x => x.AutoSelectFeaturedListings).Column("AutoSelectFeaturedListings");
            Map(x => x.NumberOfCharDescription).Column("NumberOfCharDescription");
            Map(x => x.ShowSoldLeasedWatermark).Column("ShowSoldLeasedWatermark");
            Map(x => x.ShowListingDetailsInPage).Column("ShowListingDetailsInPage");
            Map(x => x.EnableFeaturedAgentsSchedule).Column("EnableFeaturedAgentsSchedule");
            Map(x => x.IDXOpenHouses).Column("IDXOpenHouses");
            References(x => x.Account, "Account_id").Fetch.Join();
        }
    }
}
