﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class AccountListingAccessMap : ClassMap<AccountListingAccess>
    {
        public AccountListingAccessMap()
        {
            Cache.NonStrictReadWrite();
            Table("AccountListingAccess");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Residential).Column("Residential").Not.Nullable().Length(12);
            Map(x => x.Commercial).Column("Commercial").Not.Nullable().Length(12);
            Map(x => x.FarmsAndLand).Column("FarmsAndLand").Not.Nullable().Length(12);
            Map(x => x.ForLease).Column("ForLease").Not.Nullable().Length(12);
            Map(x => x.MultiFam).Column("MultiFam").Not.Nullable().Length(12);
            References(x => x.Account, "Account_id");
        }
    }
}
