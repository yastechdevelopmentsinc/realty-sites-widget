﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class VGeocodingResultMap: ClassMap<VGeocodingResult>
    {
        public VGeocodingResultMap()
        {
            Cache.NonStrictReadWrite();
            Table("VIREB_GeocodingResult");
            LazyLoad();
            Id(x => x.ID).Column("ID");
            Map(x => x.Name).Column("Name");
            Map(x => x.LastChangedDate).Column("LastChangedDate");
        }
    }
}
