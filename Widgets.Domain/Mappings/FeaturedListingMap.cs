﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class FeaturedListingMap : ClassMap<FeaturedListing>
    {
        public FeaturedListingMap()
        {
            Cache.NonStrictReadWrite();
			Table("FeaturedListing");
			LazyLoad();
			Id(x => x.Id).GeneratedBy.Identity().Column("Id");
			Map(x => x.Mlsnumber).Column("MLSNumber");
			References(x => x.Account).Column("Account_id").Fetch.Join();
        }
    }
}
