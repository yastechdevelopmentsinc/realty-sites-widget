﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class VMapAreaMap : ClassMap<VMapArea>
    {
        public VMapAreaMap()
        {
            Cache.NonStrictReadWrite();
            Table("VIREB_MapArea");
            LazyLoad();
            Id(x => x.AreaID).Column("ID");
            Map(x => x.AreaName).Column("Name");
            Map(x => x.LastChangedDate).Column("LastChangedDate");
        }
    }
}
