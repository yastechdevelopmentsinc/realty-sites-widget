﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NHibernate.Mapping.ByCode;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class VAgentMap : ClassMap<VAgent>
    {
        public VAgentMap()
        {
            Cache.NonStrictReadWrite();
            Table("VIREB_Agent");
            LazyLoad();
            Id(x => x.AgentID).Column("ID");
            Map(x => x.Phone).Column("Contact_Phone");
            Map(x => x.Email).Column("Email");
            Map(x => x.FirstName).Column("First_Name");
            Map(x => x.LastName).Column("Last_Name");
            Map(x => x.Modification_Date).Column("Modification_Date");
            Map(x => x.Website).Column("Url");
            References(x => x.AgentOffice, "Office_Id");
            References(x => x.Accreditation, "Accreditation").NotFound.Ignore();
            References(x => x.Classification, "Classification");
            
        }
    }
}
