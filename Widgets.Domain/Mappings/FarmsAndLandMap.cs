using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class FarmsAndLandMap : ClassMap<FarmsAndLand>
    {
        public FarmsAndLandMap()
        {
            Cache.NonStrictReadWrite();
            Table("FarmsAndLand");
            LazyLoad();
            Id(x => x.FarmsAndLandID).GeneratedBy.Identity().Column("FarmsAndLandID");
            Map(x => x.PropertyType).Column("PropertyType").Length(255);
            Map(x => x.Address).Column("address").Length(255);
            Map(x => x.AgentBoardCode).Column("Agent_Board_Code").CustomType("AnsiString").Length(255);
            Map(x => x.AgentBoardProv).Column("Agent_Board_Prov").Length(255);
            Map(x => x.AgentIndCode).Column("Agent_ind_code").CustomType("AnsiString").Length(255);
            Map(x => x.AreaName).Column("area_name").Length(255);
            Map(x => x.Basement).Column("basement").Length(255);
            Map(x => x.BasementDevel).Column("basement_devel").Length(255);
            Map(x => x.Bathrooms).Column("bathrooms");
            Map(x => x.BrokerName).Column("broker_name").Length(255);
            Map(x => x.Bush).Column("bush").Length(255);
            Map(x => x.BrokerPhone).Column("broker_phone").Length(255);
            Map(x => x.City).Column("city").Length(255);
            Map(x => x.DaysMkt).Column("days_mkt");
            Map(x => x.DistElev).Column("dist_elev").Length(255);
            Map(x => x.DistEsch).Column("dist_esch").Length(255);
            Map(x => x.DistHsch).Column("dist_hsch").Length(255);
            Map(x => x.DistTown).Column("dist_town").Length(255);
            Map(x => x.DrinkingWater).Column("drinking_water").Length(255);
            Map(x => x.Exterior).Column("exterior").Length(255);
            Map(x => x.Fences).Column("fences").Length(255);
            Map(x => x.Garages).Column("garages").Length(255);
            Map(x => x.Heating).Column("heating").Length(255);
            Map(x => x.HiHouseNumb).Column("hi_house_numb").Length(255);
            Map(x => x.InternetComm).Column("internet_comm");
            Map(x => x.Legal1).Column("legal_1").Length(255);
            Map(x => x.Legal2).Column("legal_2").Length(255);
            Map(x => x.ListPrice).Column("list_price");
            Map(x => x.MainResLevelsAbove).Column("main_res_levels_above");
            Map(x => x.MatrixModifiedDt).Column("Matrix_Modified_DT").Length(255);
            Map(x => x.MatrixUniqueID).Column("matrix_unique_id");
            Map(x => x.MlsNumber).Column("mls_number");
            Map(x => x.NumbBeds).Column("numb_beds");
            Map(x => x.NumbRooms).Column("numb_rooms");
            Map(x => x.Latitude).Column("Latitude").CustomSqlType("decimal(12,9)");
            Map(x => x.Longitude).Column("Longitude").CustomSqlType("decimal(12,9)");
            //Map(x => x.SuccessfullyGeocoded).Column("SuccessfullyGeocoded");
            Map(x => x.PhotoCount).Column("Photo_Count").Length(255);
            Map(x => x.PhotoCountModifiedDt).Column("Photo_Count_Modified_DT").Length(255);
            Map(x => x.PostalCode).Column("postal_code").Length(255);
            Map(x => x.Power).Column("power").Length(255);
            Map(x => x.PropOffered).Column("prop_offered").Length(255);
            Map(x => x.PropaneTank).Column("propane_tank").Length(255);
            Map(x => x.Roof).Column("roof").Length(255);
            Map(x => x.SchoolBus).Column("school_bus").Length(255);
            Map(x => x.SewerDisp).Column("sewer_disp").Length(255);
            Map(x => x.Sloughs).Column("sloughs").Length(255);
            //Map(x => x.Slsman1Email).Column("slsman1_email").Length(255);
            //Map(x => x.Slsman1Name).Column("slsman1_name").Length(255);
            //Map(x => x.Slsman1Phone).Column("slsman1_phone").Length(255);
            //Map(x => x.Slsman1Webaddr).Column("slsman1_webaddr").Length(255);
            //Map(x => x.Slsman2Email).Column("slsman2_email").Length(255);
            //Map(x => x.Slsman2Name).Column("slsman2_name").Length(255);
            //Map(x => x.Slsman2Phone).Column("slsman2_phone").Length(255);
            //Map(x => x.Slsman2Webaddr).Column("slsman2_webaddr").Length(255);
            Map(x => x.SqFootage).Column("sq_footage").Length(255);
            Map(x => x.StreetDir).Column("street_dir").Length(255);
            Map(x => x.StreetName).Column("street_name").Length(255);
            Map(x => x.StreetType).Column("street_type").Length(255);
            Map(x => x.Style).Column("style").Length(255);
            Map(x => x.SubAreaName).Column("sub_area_name").Length(255);
            Map(x => x.Taxes).Column("taxes");
            Map(x => x.Topography).Column("topography").Length(255);
            Map(x => x.TotalArea).Column("total_area").Length(255);
            Map(x => x.WaterHeater).Column("water_heater").Length(255);
            Map(x => x.WaterPurifier).Column("water_purifier").Length(255);
            Map(x => x.WaterSoftener).Column("water_softener").Length(255);
            Map(x => x.WaterSupp).Column("water_supp").Length(255);
            Map(x => x.YardLight).Column("yard_light").Length(255);
            Map(x => x.YearBuilt).Column("year_built");
            //Map(x => x.SetToDelete).Column("SetToDelete");
            Map(x => x.VirtualTour).Column("virtual_tour").Length(255);
            Map(x => x.DateEntered).Column("dateEntered").Length(255);
            Map(x => x.EquipIncl).Column("Equip_incl").Length(255);
            Map(x => x.MajorType).Column("major_type").Length(255);
            Map(x => x.AgentBrokerCode).Column("Agent_Broker_Code").CustomType("AnsiString").Length(255);

            HasOne(x => x.ReplacementCoordinates)
                .PropertyRef(x => x.PropertyRef);

            HasMany(x => x.OpenHouses)
                .AsBag()
                .KeyColumn("mls_number")
                .PropertyRef("MlsNumber");

            HasMany(x => x.ListingFiles)
               .AsBag()
               .KeyColumn("mls_number")
               .PropertyRef("MlsNumber");
        }
    }
}
