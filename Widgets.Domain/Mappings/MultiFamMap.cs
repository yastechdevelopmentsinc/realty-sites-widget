﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class MultiFamMap : ClassMap<MultiFam>
    {
        public MultiFamMap()
        {
            Cache.NonStrictReadWrite();
            Table("MultiFam");
            LazyLoad();
            Id(x => x.MultFamID).GeneratedBy.Identity().Column("MultFamID");
            Map(x => x.PropertyType).Column("PropertyType").Length(255);
            Map(x => x.MatrixUniqueID).Column("matrix_unique_id");
            Map(x => x.MatrixModifiedDt).Column("matrix_modified_dt");
            Map(x => x.ListingDate).Column("listing_date");
            Map(x => x.ListPrice).Column("list_price");
            Map(x => x.MlsNumber).Column("mls_number");
            Map(x => x.AreaName).Column("area_name").Length(255);
            Map(x => x.DaysMkt).Column("days_mkt");
            Map(x => x.MajorType).Column("major_type").Length(255);
            Map(x => x.StreetDir).Column("street_dir").Length(255);
            Map(x => x.StreetType).Column("street_type").Length(255);
            Map(x => x.Style).Column("style").Length(255);
            Map(x => x.SubAreaName).Column("sub_area_name").Length(255);
            Map(x => x.TotalUnits).Column("total_units");
            Map(x => x.Address).Column("address").Length(255);
            Map(x => x.BrokerName).Column("broker_name").Length(255);
            Map(x => x.City).Column("city").Length(255);
            Map(x => x.Frontage).Column("frontage").Length(255);
            Map(x => x.HiHouseNumb).Column("hi_house_numb").Length(50);
            Map(x => x.StreetName).Column("street_name").Length(255);
            Map(x => x.PhotoCount).Column("Photo_count");
            Map(x => x.PhotoCountModifiedDt).Column("Photo_count_modified_dt");
            Map(x => x.CurrentPrice).Column("current_price");
            Map(x => x.DepthInformation).Column("depth_information").Length(255);
            //Map(x => x.Settodelete).Column("settodelete");
            Map(x => x.Roof).Column("roof").Length(255);
            Map(x => x.Taxes).Column("taxes");
            Map(x => x.EquipIncl).Column("equip_incl").Length(255);
            Map(x => x.Exterior).Column("exterior").Length(255);
            Map(x => x.Heating).Column("heating").Length(255);
            Map(x => x.Outdoor).Column("outdoor").Length(255);
            Map(x => x.BrokerPhone).Column("broker_phone").Length(255);
            Map(x => x.InternetComm).Column("internet_comm");
            Map(x => x.Legal1).Column("legal_1").Length(255);
            Map(x => x.Legal2).Column("legal_2").Length(255);
            Map(x => x.PostalCode).Column("postal_code").Length(255);
            //Map(x => x.Slsman1Email).Column("slsman1_email").Length(255);
            //Map(x => x.Slsman1Name).Column("slsman1_name").Length(255);
            //Map(x => x.Slsman1Phone).Column("slsman1_phone").Length(255);
            //Map(x => x.Slsman1Webaddr).Column("slsman1_webaddr").Length(255);
            //Map(x => x.Slsman2Email).Column("slsman2_email").Length(255);
            //Map(x => x.Slsman2Name).Column("slsman2_name").Length(255);
            //Map(x => x.Slsman2Phone).Column("slsman2_phone").Length(255);
            //Map(x => x.Slsman2Webaddr).Column("slsman2_webaddr").Length(255);
            Map(x => x.YearBuilt).Column("year_built");
            Map(x => x.AgentIndCode).Column("Agent_ind_code").CustomType("AnsiString").Length(255);
            Map(x => x.AgentBoardCode).Column("Agent_Board_Code").CustomType("AnsiString").Length(255);
            Map(x => x.AgentBoardProv).Column("Agent_Board_Prov").Length(255);
            Map(x => x.ListPriceChangeDate).Column("List_Price_Change_Date").Length(255);
            Map(x => x.Latitude).Column("Latitude").CustomSqlType("decimal(12,9)");
            Map(x => x.Longitude).Column("Longitude").CustomSqlType("decimal(12,9)");
            //Map(x => x.SuccessfullyGeocoded).Column("SuccessfullyGeocoded");
            Map(x => x.CoAgentIndCode).Column("Co_Agent_ind_code").Length(255);
            Map(x => x.ParkingYesNo).Column("parking_yes_no").Length(255);
            Map(x => x.Storeys).Column("storeys").Length(255);
            Map(x => x.Bachelor).Column("bachelor").Length(255);
            //Map(x => x.Bedrm1).Column("bedrm_1").Length(255);
            //Map(x => x.Bedrm2).Column("bedrm_2").Length(255);
            //Map(x => x.Bedrm3).Column("bedrm_3").Length(255);
            //Map(x => x.Bedrm4).Column("bedrm_4").Length(255);
            //Map(x => x.Bedrm5).Column("bedrm_5").Length(255);
            Map(x => x.Elevators).Column("elevators").Length(255);
            Map(x => x.Flooring).Column("flooring").Length(255);
            Map(x => x.Amenities).Column("amenities").Length(255);
            Map(x => x.Construction).Column("construction").Length(255);
            Map(x => x.AgentBrokerCode).Column("Agent_Broker_Code").CustomType("AnsiString").Length(255);
            Map(x => x.VirtualTour).Column("virtual_tour").Length(255);

            HasOne(x => x.ReplacementCoordinates)
                .PropertyRef(x => x.PropertyRef);

            HasMany(x => x.OpenHouses)
                .KeyColumn("mls_number")
                .PropertyRef("MlsNumber");

            HasMany(x => x.ListingFiles)
              .AsBag()
              .KeyColumn("MlsNumber")
              .PropertyRef("MlsNumber");
        }
    }
}
