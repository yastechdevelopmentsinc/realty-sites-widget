﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class DisclaimerMap : ClassMap<Disclaimer>
    {
        public DisclaimerMap()
        {
            Cache.NonStrictReadWrite();
            Table("Disclaimer");
            LazyLoad();
            Id(x => x.ID).GeneratedBy.Identity();
            Map(x => x.BoardCode).Column("BoardCode").Length(255);
            Map(x => x.MLSListing).Column("MLSListing").Length(255);
            Map(x => x.MLSListingDetails).Column("MLSListingDetails").Length(255);
            Map(x => x.ExclusiveListing).Column("ExclusiveListing").Length(255);
            Map(x => x.ExclusiveListingDetails).Column("ExclusiveListingDetails").Length(255);
        }
    }
}
