﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class AgentMap : ClassMap<Agent>
    {
        public AgentMap()
        {
            Cache.NonStrictReadWrite();
            Table("Agents");
            LazyLoad();
            Id(x => x.AgentID).GeneratedBy.Identity().Column("Agent_id");
            Map(x => x.AgentIndCode).Column("Agent_ind_code").Length(50);
            Map(x => x.UserName).Column("UserName").Length(50);
            Map(x => x.FirstName).Column("First_Name").Length(50);
            Map(x => x.LastName).Column("Last_Name").Length(50);
            Map(x => x.Logo).Column("Logo").Length(50);
            Map(x => x.Company).Column("Company").Length(50);
            Map(x => x.Phone).Column("Phone").Length(14);
            Map(x => x.Cell).Column("Cell").Length(14);
            Map(x => x.Fax).Column("Fax").Length(14);
            Map(x => x.Email).Column("Email").Not.Nullable().Length(100);
            Map(x => x.FullName).Column("Full_Name").Not.Nullable().Length(100);
            Map(x => x.Specialty).Column("Specialty").Length(50);
            Map(x => x.Website).Column("Website").Length(255);
            Map(x => x.Password).Column("Password").Not.Nullable();
            Map(x => x.Active).Column("Active").Not.Nullable();
            Map(x => x.UserRank).Column("UserRank");
            Map(x => x.Notes).Column("Notes").Length(255);
            Map(x => x.AgentTitle).Column("AgentTitle").Length(100);
            Map(x => x.RealtorPage).Column("Realtor_page");
            Map(x => x.Staff).Column("Staff");
            Map(x => x.Photo).Column("Photo").Length(2147483647);
            Map(x => x.PhotoMimeType).Column("PhotoMimeType");
            Map(x => x.ContactsPage).Formula("Case when exists(SELECT c.Agent_ID from Contacts c where c.Agent_ID = Agent_ID) then 1 else 0 end");
            Map(x => x.ReferralSystem).Formula("Case when exists(SELECT r.Agent_ID from Referral r where r.Agent_ID = Agent_ID) then 1 else 0 end");
            Map(x => x.FeaturedAgent).Formula("Case when exists(SELECT f.Agent_id from FeaturedAgent f where f.Agent_id = Agent_ID) then 1 else 0 end");
            Map(x => x.TwitterUrl).Column("TwitterUrl").Length(250);
            Map(x => x.FacebookUrl).Column("FacebookUrl").Length(250);
            Map(x => x.LinkedInUrl).Column("LinkedInUrl").Length(250);
            Map(x => x.YouTubeUrl).Column("YouTubeUrl").Length(250);
            Map(x => x.Role).Column("Role").Length(10).Not.Nullable();
            Map(x => x.AgentProfile).Column("AgentProfile").Length(4001);
            Map(x => x.IDXListings).Column("IDXListings");
            Map(x => x.FeaturedPositionNumber).Column("FeaturedPositionNumber");
            Map(x => x.NubmerOfDays).Column("NubmerOfDays");
            Map(x => x.InProgress).Column("InProgress");
            Map(x => x.StartDate).Column("StartDate").Nullable();

            References(s => s.Account, "Account_id").Nullable()
                .Fetch.Join();
            References(s => s.Office, "AccountOffice_id").Nullable()
                .Fetch.Join();

            HasOne(x => x.Contact)
                .PropertyRef(x => x.Agent)
                .Not.LazyLoad()
                .Fetch.Join()
                .Cascade.Delete();

            HasOne(s => s.Referral)
                .PropertyRef(x => x.Agent)
                .Not.LazyLoad()
                .Fetch.Join()
                .Cascade.Delete();

            HasMany(s => s.Leads)
                .Inverse()
                .Fetch.Join()
                .Cascade.Delete();
        }
    }
}
