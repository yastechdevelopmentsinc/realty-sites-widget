﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class AccountMap: ClassMap<Account>
    {
        public AccountMap()
        {
            Cache.NonStrictReadWrite();
            Table("Account");
            Id(x => x.Id).GeneratedBy.GuidComb().Column("ID");
            Map(x => x.Name).Column("Name").Not.Nullable().Length(50);
            Map(x => x.AgentCode).Column("AgentCode").CustomType("AnsiString").Nullable().Length(50);
            Map(x => x.BrokerCode).Column("BrokerCode").CustomType("AnsiString").Not.Nullable();
            Map(x => x.BoardCode).Column("BoardCode").CustomType("AnsiString").Not.Nullable().Length(50);
            Map(x => x.AccountType).Column("AccountType").Not.Nullable().Length(10);
            Map(x => x.Phone).Column("Phone").Length(14);
            Map(x => x.Fax).Column("Fax").Length(14);
            Map(x => x.Email).Column("Email").Not.Nullable().Length(100);
            Map(x => x.Website).Column("Website").Length(255);
            Map(x => x.Logo).Column("Logo").Length(2147483647);
            Map(x => x.LogoMimeType).Column("LogoMimeType");            
            Map(x => x.MapMarker).Column("MapMarker").Length(2147483647);
            Map(x => x.MapMarkerMimeType).Column("MapMarkerMimeType");            
            Map(x => x.MapMarkerMore).Column("MapMarkerMore").Length(2147483647);
            Map(x => x.MapMarkerMoreMimeType).Column("MapMarkerMoreMimeType");
            Map(x => x.Latitude).Column("Latitude").CustomSqlType("decimal(12,9)");
            Map(x => x.Longitude).Column("Longitude").CustomSqlType("decimal(12,9)");
            Map(x => x.FacebookAppID).Column("FacebookAppID").Length(50);
            HasOne(x => x.Features)
                .PropertyRef(d => d.Account)
                .Fetch.Join();
            HasOne(x => x.Access)
                .PropertyRef(d => d.Account)
                .Fetch.Join();

        }
    }
}
