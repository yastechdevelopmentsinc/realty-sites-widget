﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class CCommunityMap : ClassMap<CCommunity>
    {
        public CCommunityMap()
        {
            Cache.NonStrictReadWrite();
            Table("Calgary_Community");
            LazyLoad();
            Id(x => x.ID).Column("ID");
            Map(x => x.AreaName).Column("Name");
            Map(x => x.Value).Column("VALUE");
            Map(x => x.LastChangedDate).Column("LastChangedDate");
        }
    }
}
