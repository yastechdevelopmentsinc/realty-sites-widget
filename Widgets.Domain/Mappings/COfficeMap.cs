﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class COfficeMap :ClassMap<COffice>
    {
        public COfficeMap()
        {
            Cache.NonStrictReadWrite();
            Table("Calgary_Office");
            LazyLoad();
            Id(x => x.ID).Column("ID");
            Map(x => x.AdditionalInformation).Column("AdditionalInformation");
            Map(x => x.CREAID).Column("CREAID");
            Map(x => x.Email).Column("Email");
            Map(x => x.Fax).Column("FaxPhone");
            Map(x => x.FranchiseID).Column("FranchiseID");
            Map(x => x.HeadOffice_MUI).Column("HeadOffice_MUI");
            Map(x => x.HeadOfficeMLSID).Column("HeadOfficeMLSID");
            Map(x => x.IDXOptInYN).Column("IDXOptInYN");
            Map(x => x.IsDeleted).Column("IsDeleted");
            Map(x => x.MailAddress).Column("MailAddress");
            Map(x => x.MailCareOf).Column("MailCareOf");
            Map(x => x.MailCity).Column("MailCity");
            Map(x => x.MailPostalCode).Column("MailPostalCode");
            Map(x => x.MailPostalCodePlus4).Column("MailPostalCodePlus4");
            Map(x => x.MailStateOrProvince).Column("MailStateOrProvince");
            Map(x => x.MatrixTesting).Column("MatrixTesting");
            Map(x => x.MembershipDate).Column("MembershipDate");
            Map(x => x.MLS).Column("MLS");
            Map(x => x.MLSID).Column("MLSID");
            Map(x => x.OfficeContact_MUI).Column("OfficeContact_MUI");
            Map(x => x.OfficeContactMLSID).Column("OfficeContactMLSID");
            Map(x => x.OfficeLongName).Column("OfficeLongName");
            Map(x => x.Name).Column("OfficeName");
            Map(x => x.OfficeStatus).Column("OfficeStatus");
            Map(x => x.OfficeType).Column("OfficeType");
            Map(x => x.Phone).Column("Phone");
            Map(x => x.PhotoCount).Column("PhotoCount");
            Map(x => x.PhotoModificationTimestamp).Column("PhotoModificationTimestamp");
            Map(x => x.ProviderKey).Column("ProviderKey");
            Map(x => x.ProviderModificationTimestamp).Column("ProviderModificationTimestamp");
            Map(x => x.StreetAddress).Column("StreetAddress");
            Map(x => x.City).Column("StreetCity");
            Map(x => x.PostalCode).Column("StreetPostalCode");
            Map(x => x.StreetPostalCodePlus4).Column("StreetPostalCodePlus4");
            Map(x => x.Province).Column("StreetStateOrProvince");
            Map(x => x.TerminationDate).Column("TerminationDate");
            Map(x => x.WebFacebook).Column("WebFacebook");
            Map(x => x.WebLinkedIn).Column("WebLinkedIn");
            Map(x => x.Website).Column("WebPageAddress");
            Map(x => x.WebTwitter).Column("WebTwitter");

            References(x => x.HeadOffice, "HeadOffice_MUI").NotFound.Ignore();
        }
    }
}
