﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class ContactMap : ClassMap<Contact>
    {
        public ContactMap()
        {
            Cache.NonStrictReadWrite();
            Table("Contacts");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.AgentTitle).Column("AgentTitle").Length(50);
            Map(x => x.PositionNumber).Column("PositionNumber");
            References(x => x.Account, "Account_id");
            References(x => x.Agent, "Agent_ID")
                .ForeignKey("none");
        }
    }
}
