﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class CRegionMap : ClassMap<CRegion>
    {
        public CRegionMap()
        {
            Cache.NonStrictReadWrite();
            Table("Calgary_Region");
            LazyLoad();
            Id(x => x.ID).Column("ID");
            Map(x => x.AreaDescription).Column("Name");
            Map(x => x.Value).Column("VALUE");
            Map(x => x.LastChangedDate).Column("LastChangedDate");
        }
    }
}
