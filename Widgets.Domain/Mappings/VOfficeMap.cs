﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class VOfficeMap: ClassMap<VOffice>
    {
        public VOfficeMap()
        {
            Cache.NonStrictReadWrite();
            Table("VIREB_Office");
            LazyLoad();
            Id(x => x.ID).Column("ID");
            Map(x => x.Name).Column("Name");
            Map(x => x.Address_Line_1).Column("Address_Line_1");
            Map(x => x.Address_Line_2).Column("Address_Line_2");
            Map(x => x.Email).Column("Email");
            Map(x => x.Fax).Column("Fax");
            Map(x => x.Phone).Column("Phone");
            Map(x => x.PostalCode).Column("Postal_Code");
            Map(x => x.Website).Column("Url");
            
        }
    }
}
