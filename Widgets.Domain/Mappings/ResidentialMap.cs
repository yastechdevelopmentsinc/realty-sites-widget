﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class ResidentialMap : ClassMap<Residential>
    {
        public ResidentialMap()
        {
            Cache.NonStrictReadWrite();
            Table("Residential");
            LazyLoad();
            Id(x => x.ResiID).GeneratedBy.Identity().Column("ResiID");
            Map(x => x.PropertyType).Column("PropertyType").Length(255);
            Map(x => x.Address).Column("address").Length(255);
            Map(x => x.AgentBoardCode).Column("Agent_Board_Code").CustomType("AnsiString").Length(255);
            Map(x => x.AgentBoardProv).Column("Agent_Board_Prov").Length(255);
            Map(x => x.AgentIndCode).Column("Agent_ind_code").CustomType("AnsiString").Length(255);
            Map(x => x.AreaName).Column("area_name").Length(255);
            Map(x => x.Basement).Column("basement").Length(255);
            //Map(x => x.BasementDevel).Column("basement_devel").Length(255);
            Map(x => x.Bathrooms).Column("bathrooms");
            Map(x => x.BrokerName).Column("broker_name").Length(255);
            Map(x => x.BrokerPhone).Column("broker_phone").Length(255);
            Map(x => x.City).Column("city").Length(255);
            //Map(x => x.CondoFees).Column("condo_fees");
            Map(x => x.CondoName).Column("condo_name").Length(255);
            Map(x => x.DaysMkt).Column("days_mkt");
            Map(x => x.DepthInformation).Column("depth_information").Length(255);
            Map(x => x.EquipIncl).Column("equip_incl").Length(255);
            Map(x => x.Exterior).Column("exterior").Length(255);
            Map(x => x.Features).Column("features").Length(255);
            //Map(x => x.FireplNo).Column("firepl_no");
            //Map(x => x.FireplType).Column("firepl_type").Length(255);
            //Map(x => x.FireplType2).Column("firepl_type2").Length(255);
            Map(x => x.Frontage).Column("frontage").Length(255);
            Map(x => x.Garages).Column("garages").Length(255);
            Map(x => x.Heating).Column("heating").Length(255);
            //Map(x => x.HiHouseNumb).Column("hi_house_numb").Length(255);
            Map(x => x.InternetComm).Column("internet_comm");;
            //Map(x => x.Legal1).Column("legal_1").Length(255);
            //Map(x => x.Legal2).Column("legal_2").Length(255);
            Map(x => x.ListPrice).Column("list_price");
            //Map(x => x.MainResLevelsAbove).Column("main_res_levels_above");
            //Map(x => x.MatrixModifiedDt).Column("Matrix_Modified_DT");
            //Map(x => x.MatrixUniqueID).Column("matrix_unique_id");
            Map(x => x.MlsNumber).Column("mls_number");
            Map(x => x.NumbBeds).Column("numb_beds");
            Map(x => x.NumbRooms).Column("numb_rooms");
            Map(x => x.OpenHouseCount).Column("OpenHouseCount");
            Map(x => x.Outdoor).Column("outdoor").Length(255);
            Map(x => x.Parking).Column("parking").Length(255);
            //Map(x => x.Pets).Column("pets").Length(255);
            Map(x => x.PhotoCount).Column("Photo_Count");
            Map(x => x.PhotoCountModifiedDt).Column("Photo_Count_Modified_DT");
            Map(x => x.PostalCode).Column("postal_code").Length(255);
            Map(x => x.Roof).Column("roof").Length(255);
            //Map(x => x.Room1RoomCode).Column("room_1_room_code").Length(255);
            //Map(x => x.Room1RoomDim1).Column("room_1_room_dim1").Length(255);
            //Map(x => x.Room1RoomDim2).Column("room_1_room_dim2").Length(255);
            //Map(x => x.Room1RoomFloor).Column("room_1_room_floor").Length(255);
            //Map(x => x.Room1RoomLevel).Column("room_1_room_level").Length(255);
            //Map(x => x.Room10RoomCode).Column("room_10_room_code").Length(255);
            //Map(x => x.Room10RoomDim1).Column("room_10_room_dim1").Length(255);
            //Map(x => x.Room10RoomDim2).Column("room_10_room_dim2").Length(255);
            //Map(x => x.Room10RoomFloor).Column("room_10_room_floor").Length(255);
            //Map(x => x.Room10RoomLevel).Column("room_10_room_level").Length(255);
            //Map(x => x.Room11RoomCode).Column("room_11_room_code").Length(255);
            //Map(x => x.Room11RoomDim1).Column("room_11_room_dim1").Length(255);
            //Map(x => x.Room11RoomDim2).Column("room_11_room_dim2").Length(255);
            //Map(x => x.Room11RoomFloor).Column("room_11_room_floor").Length(255);
            //Map(x => x.Room11RoomLevel).Column("room_11_room_level").Length(255);
            //Map(x => x.Room12RoomCode).Column("room_12_room_code").Length(255);
            //Map(x => x.Room12RoomDim1).Column("room_12_room_dim1").Length(255);
            //Map(x => x.Room12RoomDim2).Column("room_12_room_dim2").Length(255);
            //Map(x => x.Room12RoomFloor).Column("room_12_room_floor").Length(255);
            //Map(x => x.Room12RoomLevel).Column("room_12_room_level").Length(255);
            //Map(x => x.Room13RoomCode).Column("room_13_room_code").Length(255);
            //Map(x => x.Room13RoomDim1).Column("room_13_room_dim1").Length(255);
            //Map(x => x.Room13RoomDim2).Column("room_13_room_dim2").Length(255);
            //Map(x => x.Room13RoomFloor).Column("room_13_room_floor").Length(255);
            //Map(x => x.Room13RoomLevel).Column("room_13_room_level").Length(255);
            //Map(x => x.Room14RoomCode).Column("room_14_room_code").Length(255);
            //Map(x => x.Room14RoomDim1).Column("room_14_room_dim1").Length(255);
            //Map(x => x.Room14RoomDim2).Column("room_14_room_dim2").Length(255);
            //Map(x => x.Room14RoomFloor).Column("room_14_room_floor").Length(255);
            //Map(x => x.Room14RoomLevel).Column("room_14_room_level").Length(255);
            //Map(x => x.Room15RoomCode).Column("room_15_room_code").Length(255);
            //Map(x => x.Room15RoomDim1).Column("room_15_room_dim1").Length(255);
            //Map(x => x.Room15RoomDim2).Column("room_15_room_dim2").Length(255);
            //Map(x => x.Room15RoomFloor).Column("room_15_room_floor").Length(255);
            //Map(x => x.Room15RoomLevel).Column("room_15_room_level").Length(255);
            //Map(x => x.Room16RoomCode).Column("room_16_room_code").Length(255);
            //Map(x => x.Room16RoomDim1).Column("room_16_room_dim1").Length(255);
            //Map(x => x.Room16RoomDim2).Column("room_16_room_dim2").Length(255);
            //Map(x => x.Room16RoomFloor).Column("room_16_room_floor").Length(255);
            //Map(x => x.Room16RoomLevel).Column("room_16_room_level").Length(255);
            //Map(x => x.Room17RoomCode).Column("room_17_room_code").Length(255);
            //Map(x => x.Room17RoomDim1).Column("room_17_room_dim1").Length(255);
            //Map(x => x.Room17RoomDim2).Column("room_17_room_dim2").Length(255);
            //Map(x => x.Room17RoomFloor).Column("room_17_room_floor").Length(255);
            //Map(x => x.Room17RoomLevel).Column("room_17_room_level").Length(255);
            //Map(x => x.Room18RoomCode).Column("room_18_room_code").Length(255);
            //Map(x => x.Room18RoomDim1).Column("room_18_room_dim1").Length(255);
            //Map(x => x.Room18RoomDim2).Column("room_18_room_dim2").Length(255);
            //Map(x => x.Room18RoomFloor).Column("room_18_room_floor").Length(255);
            //Map(x => x.Room18RoomLevel).Column("room_18_room_level").Length(255);
            //Map(x => x.Room19RoomCode).Column("room_19_room_code").Length(255);
            //Map(x => x.Room19RoomDim1).Column("room_19_room_dim1").Length(255);
            //Map(x => x.Room19RoomDim2).Column("room_19_room_dim2").Length(255);
            //Map(x => x.Room19RoomFloor).Column("room_19_room_floor").Length(255);
            //Map(x => x.Room19RoomLevel).Column("room_19_room_level").Length(255);
            //Map(x => x.Room2RoomCode).Column("room_2_room_code").Length(255);
            //Map(x => x.Room2RoomDim1).Column("room_2_room_dim1").Length(255);
            //Map(x => x.Room2RoomDim2).Column("room_2_room_dim2").Length(255);
            //Map(x => x.Room2RoomFloor).Column("room_2_room_floor").Length(255);
            //Map(x => x.Room2RoomLevel).Column("room_2_room_level").Length(255);
            //Map(x => x.Room20RoomCode).Column("room_20_room_code").Length(255);
            //Map(x => x.Room20RoomDim1).Column("room_20_room_dim1").Length(255);
            //Map(x => x.Room20RoomDim2).Column("room_20_room_dim2").Length(255);
            //Map(x => x.Room20RoomFloor).Column("room_20_room_floor").Length(255);
            //Map(x => x.Room20RoomLevel).Column("room_20_room_level").Length(255);
            //Map(x => x.Room3RoomCode).Column("room_3_room_code").Length(255);
            //Map(x => x.Room3RoomDim1).Column("room_3_room_dim1").Length(255);
            //Map(x => x.Room3RoomDim2).Column("room_3_room_dim2").Length(255);
            //Map(x => x.Room3RoomFloor).Column("room_3_room_floor").Length(255);
            //Map(x => x.Room3RoomLevel).Column("room_3_room_level").Length(255);
            //Map(x => x.Room4RoomCode).Column("room_4_room_code").Length(255);
            //Map(x => x.Room4RoomDim1).Column("room_4_room_dim1").Length(255);
            //Map(x => x.Room4RoomDim2).Column("room_4_room_dim2").Length(255);
            //Map(x => x.Room4RoomFloor).Column("room_4_room_floor").Length(255);
            //Map(x => x.Room4RoomLevel).Column("room_4_room_level").Length(255);
            //Map(x => x.Room5RoomCode).Column("room_5_room_code").Length(255);
            //Map(x => x.Room5RoomDim1).Column("room_5_room_dim1").Length(255);
            //Map(x => x.Room5RoomDim2).Column("room_5_room_dim2").Length(255);
            //Map(x => x.Room5RoomFloor).Column("room_5_room_floor").Length(255);
            //Map(x => x.Room5RoomLevel).Column("room_5_room_level").Length(255);
            //Map(x => x.Room6RoomCode).Column("room_6_room_code").Length(255);
            //Map(x => x.Room6RoomDim1).Column("room_6_room_dim1").Length(255);
            //Map(x => x.Room6RoomDim2).Column("room_6_room_dim2").Length(255);
            //Map(x => x.Room6RoomFloor).Column("room_6_room_floor").Length(255);
            //Map(x => x.Room6RoomLevel).Column("room_6_room_level").Length(255);
            //Map(x => x.Room7RoomCode).Column("room_7_room_code").Length(255);
            //Map(x => x.Room7RoomDim1).Column("room_7_room_dim1").Length(255);
            //Map(x => x.Room7RoomDim2).Column("room_7_room_dim2").Length(255);
            //Map(x => x.Room7RoomFloor).Column("room_7_room_floor").Length(255);
            //Map(x => x.Room7RoomLevel).Column("room_7_room_level").Length(255);
            //Map(x => x.Room8RoomCode).Column("room_8_room_code").Length(255);
            //Map(x => x.Room8RoomDim1).Column("room_8_room_dim1").Length(255);
            //Map(x => x.Room8RoomDim2).Column("room_8_room_dim2").Length(255);
            //Map(x => x.Room8RoomFloor).Column("room_8_room_floor").Length(255);
            //Map(x => x.Room8RoomLevel).Column("room_8_room_level").Length(255);
            //Map(x => x.Room9RoomCode).Column("room_9_room_code").Length(255);
            //Map(x => x.Room9RoomDim1).Column("room_9_room_dim1").Length(255);
            //Map(x => x.Room9RoomDim2).Column("room_9_room_dim2").Length(255);
            //Map(x => x.Room9RoomFloor).Column("room_9_room_floor").Length(255);
            //Map(x => x.Room9RoomLevel).Column("room_9_room_level").Length(255);
            Map(x => x.SiteLot).Column("site_lot").Length(255);
            //Map(x => x.Slsman1Email).Column("slsman1_email").Length(255);
            //Map(x => x.Slsman1Name).Column("slsman1_name").Length(255);
            //Map(x => x.Slsman1Phone).Column("slsman1_phone").Length(255);
            //Map(x => x.Slsman1Webaddr).Column("slsman1_webaddr").Length(255);
            //Map(x => x.Slsman2Email).Column("slsman2_email").Length(255);
            //Map(x => x.Slsman2Name).Column("slsman2_name").Length(255);
            //Map(x => x.Slsman2Phone).Column("slsman2_phone").Length(255);
            //Map(x => x.Slsman2Webaddr).Column("slsman2_webaddr").Length(255);
            Map(x => x.SqFootage).Column("sq_footage").Length(255);
            //Map(x => x.StreetDir).Column("street_dir").Length(255);
            //Map(x => x.StreetName).Column("street_name").Length(255);
            //Map(x => x.StreetType).Column("street_type").Length(255);
            Map(x => x.Style).Column("style").Length(255);
            Map(x => x.SubAreaName).Column("sub_area_name").Length(255);
            Map(x => x.SuiteNumb).Column("suite_numb").Length(255);
            Map(x => x.Taxes).Column("taxes");
            Map(x => x.TypeDwelling).Column("type_dwelling").Length(255);
            Map(x => x.OwnershipTitle).Column("ownership_title").Length(255);
            //Map(x => x.WaterHeatType).Column("water_heat_type").Length(255);
            //Map(x => x.WaterHeater).Column("water_heater").Length(255);
            //Map(x => x.WaterSoftener).Column("water_softener").Length(255);
            Map(x => x.YearBuilt).Column("year_built");
            Map(x => x.Latitude).Column("Latitude").CustomSqlType("decimal(12,9)");
            Map(x => x.Longitude).Column("Longitude").CustomSqlType("decimal(12,9)");
            //Map(x => x.SuccessfullyGeocoded).Column("SuccessfullyGeocoded");
            //Map(x => x.SetToDelete).Column("SetToDelete");
            Map(x => x.VirtualTour).Column("virtual_tour").Length(255);
            Map(x => x.DateEntered).Column("dateEntered");
            Map(x => x.AgentBrokerCode).Column("Agent_Broker_Code").CustomType("AnsiString").Length(255);
            Map(x => x.CoAgentIndCode).Column("Co_Agent_ind_code").Length(255);

            HasOne(x => x.ReplacementCoordinates)
                .PropertyRef(x => x.PropertyRef);

            HasMany(x => x.OpenHouses)
                .PropertyRef("MlsNumber")
                .KeyColumn("mls_number");

            HasMany(x => x.ListingFiles)
              .AsBag()
              .KeyColumn("MlsNumber")
              .PropertyRef("MlsNumber");

        }
    }
}
