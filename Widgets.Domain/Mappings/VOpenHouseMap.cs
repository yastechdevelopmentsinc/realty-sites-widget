﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    class VOpenHouseMap : ClassMap<VOpenHouse>
    {
        public VOpenHouseMap()
        {
            Cache.NonStrictReadWrite();
            Table("VIREB_VOpenHouses");
            LazyLoad();
            Id(x => x.ID).Column("ID");
            Map(x => x.Date_Created).Column("Date_Created");
            Map(x => x.Date_Last_Modified).Column("Date_Last_Modified");
            Map(x => x.Endtime).Column("Endtime");
            Map(x => x.Hosted).Column("Hosted");
            Map(x => x.Notes).Column("Notes");
            Map(x => x.StartTime).Column("StartTime");
            Map(x => x.Listing_Id).Column("Listing_Id");
            References(x => x.Agent, "Agent_Id");
            References(x => x.Status, "Status").NotFound.Ignore();
        }
    }
}
