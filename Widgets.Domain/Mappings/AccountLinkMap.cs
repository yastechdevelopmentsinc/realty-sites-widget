﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class AccountLinkMap : ClassMap<AccountLink>
    {
        public AccountLinkMap()
        {
            Cache.NonStrictReadWrite();
            Table("AccountLink");
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Name).Column("Name").Not.Nullable().Length(250);
            Map(x => x.Website).Column("Website").Length(250);

            References(x => x.Account, "Account_id");
        }
    }
}
