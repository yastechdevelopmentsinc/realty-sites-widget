﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class AreaMap : ClassMap<Area>
    {
        public AreaMap()
        {
            Cache.NonStrictReadWrite();
            Table("Area");
            LazyLoad();
            Id(x => x.AreaID).GeneratedBy.Identity().Column("AreaID");
            Map(x => x.AreaCode).Column("AreaCode").Nullable().Length(2);
            Map(x => x.AreaDescription).Column("AreaDescription").Not.Nullable().Length(50);
            Map(x => x.BoardCode).Column("BoardCode").Nullable().Length(50);
            References(s => s.Account, "Account_id").Nullable()
               .Fetch.Join();
            HasMany(x => x.Locations)
                .PropertyRef("AreaCode")
                .KeyColumn("AreaID")
                .Inverse()
                .Cascade.None();

        }
    }
}
