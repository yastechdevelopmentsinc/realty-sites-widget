﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class AccountFileMap : ClassMap<AccountFile>
    {
        public AccountFileMap()
        {
            Table("AccountFiles");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.GuidComb().Column("Id");
            Map(x => x.FileName).Column("FileName").Not.Nullable().Length(150);
            Map(x => x.FilePath).Column("FilePath").Not.Nullable();
            Map(x => x.MimeType).Column("MimeType").Not.Nullable().Length(150);
            Map(x => x.UploadDate).Column("UploadDate").Nullable();

            References(x => x.Account, "Account_id");
        }
    }
}
