﻿using FluentNHibernate.Mapping;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Mappings
{
    public class OpenHouseMap : ClassMap<OpenHouse>
    {
        public OpenHouseMap()
        {
            Cache.NonStrictReadWrite();
            Table("OpenHouses");
            LazyLoad();
            Id(x => x.OpHoID).GeneratedBy.Identity().Column("OpHoID");
            Map(x => x.AgentMui).Column("Agent_MUI");
            Map(x => x.Description).Column("Description");
            Map(x => x.FromDate).Column("FromDate");
            Map(x => x.FromTime).Column("FromTime");
            Map(x => x.InputDate).Column("InputDate");
            Map(x => x.IsActive).Column("IS_ACTIVE");
            Map(x => x.IsDeleted).Column("IsDeleted");
            Map(x => x.ListingMui).Column("Listing_MUI");
            Map(x => x.ListingType).Column("listing_type").Length(100);
            Map(x => x.MatrixUniqueID).Column("matrix_unique_id");
            Map(x => x.ModificationTimestamp).Column("ModificationTimestamp");
            Map(x => x.OpenHouseRefreshments).Column("Open_House_Refreshments").Length(100);
            Map(x => x.OpenHouseType).Column("Open_House_Type").Length(100);
            Map(x => x.ToDateDefunct).Column("ToDate_defunct");
            Map(x => x.ToTime).Column("ToTime");
            Map(x => x.AdvertOpenHouse).Column("AdvertOpenHouse");
            Map(x => x.MlsNumber).Column("mls_number");
        }
    }
}
