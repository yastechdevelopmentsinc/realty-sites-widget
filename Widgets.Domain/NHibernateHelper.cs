﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Caches.SysCache;
using Widgets.Domain.Entities;

namespace Widgets.Domain
{
    public class NHibernateHelper
    {
        private static ISessionFactory _sessionFactory;

        public static ISessionFactory SessionFactory
        {
            get
            {
                if (_sessionFactory == null)
                    InitializeSessionFactory();

                return _sessionFactory;
            }
        }

        private static void InitializeSessionFactory()
        {
            _sessionFactory = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2008
                              .ConnectionString(
                                  x => x.FromConnectionStringWithKey("Widgets.Properties.Settings.Connection")))
                .Cache(c => c.ProviderClass<SysCacheProvider>()
                                         .UseQueryCache())
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Residential>())
                .BuildSessionFactory();
        }

        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }
    }
}
