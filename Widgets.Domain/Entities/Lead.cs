﻿using System;

namespace Widgets.Domain.Entities
{
    public class Lead
    {
        public virtual int LeadId { get; set; }
        public virtual Agent Agent { get; set; }
        public virtual DateTime? Created { get; set; }
        public virtual string Note { get; set; }
    }

}
