﻿using System.Collections.Generic;

namespace Widgets.Domain.Entities
{
    public class BaseEntity
    {
        public virtual decimal? ListPrice { get; set; }
        public virtual int? Bathrooms { get; set; }
        public virtual int? NumbBeds { get; set; }
        public virtual string City { get; set; }
        public virtual string AreaName { get; set; }
        public virtual string SubAreaName { get; set; }
        public virtual string Status { get; set; }
        public virtual string AgentIndCode { get; set; }
        public virtual string AgentBrokerCode { get; set; }
        public virtual string AgentBoardCode { get; set; }
        public virtual string AgentBoardProv { get; set; }
        public virtual string CoAgentIndCode { get; set; }
        public virtual string CoAgentIndCode2 { get; set; }
        public virtual string Address { get; set; }
        public virtual int? DaysMkt { get; set; }
        public virtual int? MlsNumber { get; set; }
        public virtual string VirtualTour { get; set; }
        public virtual ReplacementCoord ReplacementCoordinates { get; set; }
        public virtual IEnumerable<OpenHouse> OpenHouses { get; set; }
        public virtual IEnumerable<ListingFiles> ListingFiles { get; set; }
        public virtual string PriceHash { get; set; }
        public virtual bool ShowDecimal {get; set;}
        public virtual bool IsActive { get; set; }
        public virtual bool IsRental { get; set; }
        public virtual string PostalCode { get; set; }
        //vancouver
        public virtual VMapArea Map_Area { get; set; }
        public virtual string Street_Name { get; set; }
        public virtual int? Street_Number { get; set; }
        public virtual IEnumerable<VOpenHouse> VOpenHouses { get; set; }

        //Calgary
        public virtual IEnumerable<COpenHouse> COpenHouses { get; set; }
    }
}
