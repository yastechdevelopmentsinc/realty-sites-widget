﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    public class ListingFiles
    {
        public virtual int Id { get; set; }
        public virtual Account Account { get; set; }
        public virtual int Mlsnumber { get; set; }
        public virtual string FileName { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string MimeType { get; set; }
        public virtual DateTime UploadDate { get; set; }
        public virtual string Name { get; set; }
    }
}
