﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;

namespace Widgets.Domain.Entities
{
    public class Agent
    {
        public virtual int AgentID { get; set; }
        public virtual string AgentIndCode { get; set; }
        public virtual string UserName { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Logo { get; set; }
        public virtual string Company { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Cell { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Email { get; set; }
        public virtual string FullName { get; set; }
        public virtual string Specialty { get; set; }
        public virtual string Website { get; set; }
        public virtual string Password { get; set; }
        public virtual bool Active { get; set; }
        public virtual int? UserRank { get; set; }
        public virtual string Notes { get; set; }
        public virtual string AgentTitle { get; set; }
        public virtual bool? RealtorPage { get; set; }
        public virtual bool? Staff { get; set; }
        public virtual byte[] Photo { get; set; }
        public virtual string PhotoMimeType { get; set; }
        public virtual bool ContactsPage { get; set; }
        public virtual bool ReferralSystem { get; set; }
        public virtual bool FeaturedAgent { get; set; }
        public virtual string TwitterUrl { get; set; }
        public virtual string FacebookUrl { get; set; }
        public virtual string LinkedInUrl { get; set; }
        public virtual string YouTubeUrl { get; set; }
        public virtual string Role { get; set; }
        public virtual string AgentProfile { get; set; }
        public virtual bool IDXListings { get; set; }
        public virtual int? FeaturedPositionNumber { get; set; }
        public virtual int? NubmerOfDays { get; set; }
        public virtual bool InProgress { get; set; }
        public virtual DateTime? StartDate { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual Account Account { get; set; }
        public virtual Referral Referral { get; set; }
        public virtual IEnumerable<Lead> Leads { get; set; }
        public virtual AccountOffice Office { get; set; }

        public static Agent GetByID(ISession session, int id)
        {
            return session.Get<Agent>(id);
        }

        public static IEnumerable<Agent> GetByAccount(ISession session, Account account)
        {
            return session.Query<Agent>().Where(x => x.Account == account);
        }
    }
}