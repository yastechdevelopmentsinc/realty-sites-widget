﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Caches.SysCache;
using NHibernate.Linq;
using NHibernate.Mapping;
using NHibernate.Transform;

namespace Widgets.Domain.Entities
{
    public class VResidential : BaseEntity
    {
        public virtual int ID { get; set; }
        public virtual VAgent Colisting_Agent { get; set; }
        public virtual VOffice Colisting_Office { get; set; }
        public virtual VCooling Cooling { get; set; }
        public virtual string Crea_Display_Address { get; set; }
        public virtual string Crea_Display_Listing { get; set; }
        public virtual string Fireplace_Types { get; set; }
        public virtual string Fireplaces { get; set; }
        public virtual string Fuel { get; set; }
        public virtual string Heating { get; set; }
        public virtual string Interior_Features { get; set; }
        public virtual string TempInternetComm { get; set; }
        public virtual string InternetComm
        {
            get
            {
                if (string.IsNullOrEmpty(TempInternetComm))
                {
                    return Remarks;
                }
                else
                {
                    return TempInternetComm;
                }

            }
            set { }
        }
        public virtual double? Latitude { get; set; }
        public virtual double? Longitude { get; set; }
        public virtual VAgent Listing_Agent { get; set; }
        public virtual VOffice Listing_Office { get; set; }
        public virtual int PhotoCount { get; set; }
        public virtual double? Lot_Depth { get; set; }
        public virtual string Lot_Features { get; set; }
        public virtual double? Lot_Sqft { get; set; }
        public virtual double? Lot_Width { get; set; }
        public virtual DateTime Modification_Date { get; set; }
        public virtual string Parking_Types { get; set; }
        public virtual DateTime Photo_Modification_Date { get; set; }
        public virtual VPropertyType Property_Type { get; set; }
        public virtual string Remarks { get; set; }
        public virtual string SqFootage { get; set; }
        public virtual string Styles { get; set; }
        public virtual VAgent Third_Listing_Agent { get; set; }
        public virtual VOffice Third_Listing_Office { get; set; }
        public virtual string Title { get; set; }
        public virtual int? Unit_Number { get; set; }
        public virtual int? YearBuilt { get; set; }
        public virtual string TempCity { get; set; }

        public virtual string Style
        {
            get
            {
                /*string tempstylestring = "";
                List<string> tempstyles = Styles.Trim().Split(new string[] {","},StringSplitOptions.RemoveEmptyEntries).ToList();
                for (int i = 0; i < tempstyles.Count; i++)
                {
                    VStyle currentstyle = _session.Query<VStyle>().SingleOrDefault(x => x.ID == Convert.ToInt32(tempstyles[i]));
                    tempstylestring += currentstyle.Name;
                    tempstylestring += " ";
                }
                tempstylestring = tempstylestring.Trim();
                return tempstylestring;*/
                return null;
            }
        }

        public virtual string Parking
        {
            get
            {
                /*string tempparkingstring = "";
                List<string> tempparkings = Parking_Types.Trim().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                for (int i = 0; i < tempparkings.Count; i++)
                {
                    VParkingTypes currentparkingtype = _session.Query<VParkingTypes>().SingleOrDefault(x => x.ID == Convert.ToInt32(tempparkings[i]));
                    tempparkingstring += currentparkingtype.Name;
                    tempparkingstring += " ";
                }
                tempparkingstring = tempparkingstring.Trim();
                return tempparkingstring;*/
                return null;
            }
        }

        public virtual string PropertyType
        {
            get
            {
                if (Property_Type != null)
                {
                    return Property_Type.Name;
                }
                else
                {
                    return null;
                }
            }
        }

        public override string AreaName
        {
            get
            {
                if (Map_Area != null)
                {
                    string name = Map_Area.AreaName.Substring(2);
                    if (!Map_Area.AreaName.StartsWith("Z"))
                    {
                        int indexofzonepart = name.IndexOf('(');
                        name = name.Remove(indexofzonepart);
                    }
                    return (char.ToUpper(name[0]) + name.Substring(1)).Trim();
                }
                else
                {
                    return null;
                }
            }
        }

        public override string City
        {
            get
            {
                if (TempCity != null)
                {
                    string[] citysplit = TempCity.Split(' ');
                    string city = "";
                    for (int i = 0; i < citysplit.Length; i++)
                    {
                        city += char.ToUpper(citysplit[i][0]) + citysplit[i].Substring(1).ToLower() + " ";
                    }
                    city.Trim();
                    return city;
                }
                return TempCity;
            }
        }

        public override string SubAreaName
        {
            get
            {
                if (Map_Area != null)
                {
                    string name = Map_Area.AreaName.Substring(2);
                    if (!Map_Area.AreaName.StartsWith("Z"))
                    {
                        int indexofzonepart = name.IndexOf('(');
                        name = name.Remove(indexofzonepart);
                    }
                    return (char.ToUpper(name[0]) + name.Substring(1)).Trim();
                }
                else
                {
                    return null;
                }
            }
        }

        public override string Address
        {
            get
            {
                if (Street_Name != null)
                {
                    Street_Name = Street_Name.ToLower();
                    List<string> splitaddress = Street_Name.Split(' ').ToList();
                    Street_Name = "";
                    for (int i = 0; i < splitaddress.Count; i++)
                    {
                        if (splitaddress[i].Length > 0)
                        {
                            Street_Name += (" " + char.ToUpper(splitaddress[i][0]) + splitaddress[i].Substring(1));
                        }
                    }
                    //Street_Name = char.ToUpper(Street_Name[0]) + Street_Name.Substring(1);
                }
                string unitnumber = Unit_Number != null && Unit_Number > 0 ? Unit_Number.Value.ToString() : null;
                string tempaddress = string.IsNullOrEmpty(unitnumber) ? Street_Number + " " + Street_Name : "#" + unitnumber + "-" + Street_Number + " " + Street_Name;
                return tempaddress.Trim();
            }
        }

        public virtual string BoardCode
        {
            get
            {
                return "VIREB";
            }
        }
        public virtual string Class
        {
            get
            {
                return "Residential";
            }
        }

        public virtual string BrokerName
        {
            get
            {
                if (Listing_Office != null)
                {
                    try
                    {
                        return Listing_Office.Name;
                    }
                    catch
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

    }
}
