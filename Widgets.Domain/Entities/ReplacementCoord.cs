﻿namespace Widgets.Domain.Entities
{
    public class ReplacementCoord
    {
        public virtual int ReplacementID { get; set; }
        public virtual int PropertyRef { get; set; }
        public virtual double? Latitude { get; set; }
        public virtual double Longitude { get; set; }
        public virtual string BoardCode { get; set; }
    }
}
