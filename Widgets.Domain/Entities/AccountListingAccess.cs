﻿using System;

namespace Widgets.Domain.Entities
{
    [Serializable]
    public class AccountListingAccess
    {
        public virtual int Id { get; set; }
        public virtual Account Account { get; set; }
        public virtual ListingDisplayAccess Residential { get; set; }
        public virtual ListingDisplayAccess Commercial { get; set; }
        public virtual ListingDisplayAccess FarmsAndLand { get; set; }
        public virtual ListingDisplayAccess ForLease { get; set; }
        public virtual ListingDisplayAccess MultiFam { get; set; }
    }
}
