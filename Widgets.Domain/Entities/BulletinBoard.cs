﻿using System;
using System.Collections.Generic;

namespace Widgets.Domain.Entities
{
    public class BulletinBoard
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual Account Account { get; set; }
        public virtual Agent Agent { get; set; }
        public virtual bool IsNews { get; set; }

        public virtual IEnumerable<BulletinBoardPhoto> Photos { get; set; }
    }

}
