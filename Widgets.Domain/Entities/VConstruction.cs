﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    public class VConstruction
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual DateTime LastChangedDate { get; set; }
    }
}
