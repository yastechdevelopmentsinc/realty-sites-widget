﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    public class Board
    {
        public virtual int BoardID{ get; set; }
        public virtual string BoardCode { get; set; }
        public virtual IEnumerable<Area> Areas { get; set; }
    }
}
