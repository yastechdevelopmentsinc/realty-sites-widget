﻿namespace Widgets.Domain.Entities
{
    public class Disclaimer
    {
        public virtual int ID { get; set; }
        public virtual string BoardCode { get; set; }
        public virtual string MLSListing { get; set; }
        public virtual string MLSListingDetails { get; set; }
        public virtual string ExclusiveListing { get; set; }
        public virtual string ExclusiveListingDetails { get; set; }
    }
}
