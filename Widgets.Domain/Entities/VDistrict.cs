﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    public class VDistrict
    {
        public virtual int AreaID { get; set; }
        public virtual string AreaDescription { get; set; }
        public virtual DateTime LastChangedDate { get; set; }
    }
}
