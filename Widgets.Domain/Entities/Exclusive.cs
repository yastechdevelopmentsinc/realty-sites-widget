﻿using System;

namespace Widgets.Domain.Entities
{
    public class Exclusive : BaseEntity
    {
        public virtual int ExclusiveID { get; set; }
        public virtual string PropertyType { get; set; }
        public virtual string Basement { get; set; }
        public virtual string DepthInformation { get; set; }
        public virtual string EquipIncl { get; set; }
        public virtual string Exterior { get; set; }
        public virtual string InternetComm { get; set; }
        public virtual string Frontage { get; set; }
        public virtual string Garages { get; set; }
        public virtual string Heating { get; set; }
        public virtual int? NumbRooms { get; set; }
        public virtual string Parking { get; set; }
        public virtual int? PhotoCount { get; set; }
        public virtual string Roof { get; set; }
        public virtual string Slsman1Email { get; set; }
        public virtual string Slsman1Name { get; set; }
        public virtual string Slsman1Phone { get; set; }
        public virtual string Slsman1Webaddr { get; set; }
        public virtual string SqFootage { get; set; }
        public virtual string Style { get; set; }
        public virtual string TypeDwelling { get; set; }
        public virtual int? YearBuilt { get; set; }
        public virtual double? Latitude { get; set; }
        public virtual double? Longitude { get; set; }
        public virtual DateTime? DateEntered { get; set; }
        public virtual string BrokerName { get; set; }
        public virtual int DistrictCode { get; set; }
        public virtual int AreaCode { get; set; }
        
        public virtual Account Account { get; set; }
    }
}