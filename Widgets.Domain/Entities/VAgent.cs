﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    public class VAgent 
    {
        public virtual int AgentID { get; set; }
        public virtual VAccreditation Accreditation { get; set; }
        public virtual VClassification Classification { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Email { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual DateTime? Modification_Date { get; set; }
        public virtual VOffice AgentOffice { get; set; }
        public virtual string Website { get; set; }

        public virtual string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        public virtual string AgentTitle
        {
            get
            {
                if (Classification != null)
                {
                    return Classification.Name;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
