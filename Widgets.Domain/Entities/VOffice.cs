﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    public class VOffice
    {
        public virtual int ID { get; set; }
        public virtual string Address_Line_1 { get; set; }
        public virtual string Address_Line_2 { get; set; }
        public virtual string Email { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Name { get; set; }
        public virtual string Phone { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string Website { get; set; }

        public virtual string Address
        {
            get
            {
                return Address_Line_1 + " " + Address_Line_2;
            }
        }
    }
}
