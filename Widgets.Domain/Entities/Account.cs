﻿using System;
using System.Web.Script.Serialization;
using NHibernate;

namespace Widgets.Domain.Entities
{
    [Serializable]
    public class Account
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string AgentCode { get; set; }
        public virtual string BrokerCode { get; set; }
        public virtual string BoardCode { get; set; }
        public virtual AccountType AccountType { get; set; }
        public virtual AccountFeatures Features { get; set; }

        [ScriptIgnore]
        public virtual AccountListingAccess Access { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Email { get; set; }
        public virtual string Website { get; set; }
        public virtual byte[] Logo { get; set; }
        public virtual string LogoMimeType { get; set; }
        public virtual byte[] MapMarker { get; set; }
        public virtual string MapMarkerMimeType { get; set; }
        public virtual byte[] MapMarkerMore { get; set; }
        public virtual string MapMarkerMoreMimeType { get; set; }
        public virtual double Latitude { get; set; }
        public virtual double Longitude { get; set; }
        public virtual string FacebookAppID { get; set; }

        public override string ToString()
        {
            return Id.ToString();
        }

        public static Account GetById(ISession session, Guid id)
        {
            return session.Get<Account>(id);
        }
    }
}