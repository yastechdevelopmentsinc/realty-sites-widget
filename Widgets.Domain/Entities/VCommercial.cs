﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    public class VCommercial:BaseEntity
    {
        public virtual int ID { get; set; }
        public virtual string Business_Subtype { get; set; }
        public virtual string Business_Type { get; set; }
        public virtual VAgent Colisting_Agent { get; set; }
        public virtual VOffice Colisting_Office { get; set; }
        public virtual VCommercialType Commercial_Type { get; set; }
        public virtual string Crea_Display_Address { get; set; }
        public virtual string Crea_Display_Listing { get; set; }
        public virtual VIcxRealEstateType Icx_Real_Estate { get; set; }
        public virtual string TempInternetComm { get; set; }
        public virtual string InternetComm
        {
            get
            {
                if (string.IsNullOrEmpty(TempInternetComm))
                {
                    return Remarks;
                }
                else
                {
                    return TempInternetComm;
                }
            }
            set { }
        }
        public virtual double? Latitude { get; set; }
        public virtual double? Longitude { get; set; }
        public virtual VAgent Listing_Agent { get; set; }
        public virtual VOffice Listing_Office { get; set; }
        public virtual int PhotoCount { get; set; }
        public virtual double? Lot_Depth_Side_1 { get; set; }
        public virtual double? Lot_Width_Front { get; set; }
        public virtual DateTime Modification_Date { get; set; }
        public virtual DateTime Photo_Modification_Date { get; set; }
        public virtual VPriceType Price_Type { get; set; }
        public virtual string Remarks { get; set; }
        public virtual string SqFootage { get; set; }

        public virtual VAgent Third_Listing_Agent { get; set; }
        public virtual VOffice Third_Listing_Office { get; set; }
        public virtual string Title { get; set; }
        public virtual int? Unit_Number { get; set; }
        public virtual int? YearBuilt { get; set; }
        public virtual string TempCity { get; set; }
       
        public virtual string PropertyType
        {
            get
            {
                if (Commercial_Type != null)
                {
                    return Commercial_Type.Name;
                }
                else
                {
                    return null;
                }
            }
        }

        public override string AreaName
        {
            get
            {
                if (Map_Area != null)
                {
                    string name = Map_Area.AreaName.Substring(2);
                    return (char.ToUpper(name[0]) + name.Substring(1)).Trim();
                }
                else
                {
                    return null;
                }
            }
        }

        public override string City
        {
            get
            {
                if (TempCity != null)
                {
                    string[] citysplit = TempCity.Split(' ');
                    string city = "";
                    for (int i = 0; i < citysplit.Length; i++)
                    {
                        city += char.ToUpper(citysplit[i][0]) + citysplit[i].Substring(1).ToLower() + " ";
                    }
                    city = city.Trim();
                    return city;
                }
                return TempCity;
            }
        }

        public override string SubAreaName
        {
            get
            {
                if (Map_Area != null)
                {
                    string name = Map_Area.AreaName.Substring(2);
                    return (char.ToUpper(name[0]) + name.Substring(1)).Trim();
                }
                else
                {
                    return null;
                }
            }
        }

        public override string Address
        {
            get
            {
                if (Street_Name != null)
                {
                    Street_Name = Street_Name.ToLower();
                    List<string> splitaddress = Street_Name.Split(' ').ToList();
                    Street_Name = "";
                    for (int i = 0; i < splitaddress.Count; i++)
                    {
                        if (splitaddress[i].Length > 0)
                        {
                            Street_Name += (" " + char.ToUpper(splitaddress[i][0]) + splitaddress[i].Substring(1));
                        }
                    }
                    //Street_Name = char.ToUpper(Street_Name[0]) + Street_Name.Substring(1);
                }
                string unitnumber = Unit_Number != null && Unit_Number > 0 ? Unit_Number.Value.ToString() : null;
                string tempaddress = string.IsNullOrEmpty(unitnumber) ? Street_Number + " " + Street_Name : "#"+unitnumber + "-" + Street_Number + " " + Street_Name;
                return tempaddress.Trim();
            }
        }

        public virtual string BoardCode
        {
            get
            {
                return "VIREB";
            }
        }

        public virtual string Class
        {
            get
            {
                return "Commercial";
            }
        }

        public virtual string BrokerName
        {
            get
            {
                if (Listing_Office != null)
                {
                    try
                    {
                        return Listing_Office.Name;
                    }
                    catch
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }
      
    }
}
