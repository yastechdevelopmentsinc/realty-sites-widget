﻿namespace Widgets.Domain.Entities
{
    public class Location {
        public virtual int AreaID { get; set; }
        public virtual string CityName { get; set; }
        public virtual string AreaName { get; set; }
        public virtual int AreaCode { get; set; }
        public virtual string DistrictCode { get; set; }
        public virtual string SubAreaName { get; set; }
        public virtual double? Latitude { get; set; }
        public virtual double? Longitude { get; set; }
        public virtual bool? Active { get; set; }
        public virtual int Listings { get; set; }
        public virtual string BoardCode { get; set; }
    }

    public class LocationData
    {
        public LocationData(string cityName, int listings)
        {
            Listings = listings;
            CityName = cityName;
        }

        public int Listings { get; private set; }
        public string CityName { get; private set; }
    }

}
