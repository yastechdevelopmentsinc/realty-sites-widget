﻿namespace Widgets.Domain.Entities
{
    public class vwListings
    {
        public virtual int Id { get; set; }
        public virtual string PropertyType { get; set; }
        public virtual string Address { get; set; }
        public virtual string AgentBoardCode { get; set; }
        public virtual string AgentBoardProv { get; set; }
        public virtual string AgentIndCode { get; set; }
        public virtual string AreaCode { get; set; }
        public virtual string SubAreaCode { get; set; }
        public virtual string SubAreaName { get; set; }
        public virtual string City { get; set; }
        public virtual decimal ListPrice { get; set; }
        public virtual int MlsNumber { get; set; }
        public virtual string SqFootage { get; set; }
        public virtual string Status { get; set; }
        public virtual string TypeDwelling { get; set; }
        public virtual int YearBuilt { get; set; }

    }
}
