﻿namespace Widgets.Domain.Entities
{
    public class Contact
    {
        public virtual int Id { get; set; }
        public virtual Agent Agent { get; set; }
        public virtual string AgentTitle { get; set; }
        public virtual int? PositionNumber { get; set; }
        public virtual Account Account { get; set; }
    }
}
