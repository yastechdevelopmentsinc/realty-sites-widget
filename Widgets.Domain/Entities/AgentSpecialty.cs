﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    public class AgentSpecialty
    {
        public virtual int AgentSpecialtyID { get; set; }
        public virtual string Specialty { get; set; }
    }
}
