﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    public class VMapArea
    {
        public virtual int AreaID { get; set; }
        public virtual string AreaName { get; set; }
        public virtual DateTime LastChangedDate { get; set; }
    }
}
