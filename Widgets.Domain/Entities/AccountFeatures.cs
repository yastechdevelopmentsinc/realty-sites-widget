﻿using System;

namespace Widgets.Domain.Entities
{
    [Serializable]
    public class AccountFeatures
    {
        public virtual int Id { get; set; }
        public virtual bool Listing { get; set; }
        public virtual bool Search { get; set; }
        public virtual bool Map { get; set; }
        public virtual bool FeaturedListing { get; set; }
        public virtual bool FeaturedAgent { get; set; }
        public virtual bool LatestListing { get; set; }
        public virtual bool AgentProfile { get; set; }
        public virtual bool Members { get; set; }
        public virtual bool Contacts { get; set; }
        public virtual bool News { get; set; }
        public virtual bool MortgageCalculator { get; set; }
        public virtual bool BulletinBoard { get; set; }
        public virtual bool ExclusiveListing { get; set; }
        public virtual bool OpenHouses { get; set; }
        public virtual bool Referral { get; set; }
        public virtual bool UseSimpleDetailedListing { get; set; }
        public virtual bool FileManagement { get; set; }
        public virtual bool LinkManagement { get; set; }
        public virtual int NumberOfListingsToDisplay { get; set; }
        public virtual int NumberOfCharDescription { get; set; }
        public virtual int ListingWidgetPageSize { get; set; }
        public virtual bool MLSSearch { get; set; }
        public virtual bool AgentProfileLink { get; set; }
        public virtual bool AgentWebsiteLink { get; set; }
        public virtual bool ShowEmailAddress { get; set; }
        public virtual bool AutoSelectFeaturedListings { get; set; }
        public virtual bool ShowSoldLeasedWatermark { get; set; }
        public virtual bool ShowListingDetailsInPage { get; set; }
        public virtual bool EnableFeaturedAgentsSchedule { get; set; }
        public virtual bool IDXOpenHouses { get; set; }
        public virtual Account Account { get; set; }
    }
}
