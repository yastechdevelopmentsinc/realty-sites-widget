﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    //area or district
    public class CRegion
    {
        public virtual int ID { get; set; }
        public virtual string AreaDescription { get; set; }
        public virtual string Value { get; set; }
        public virtual DateTime LastChangedDate { get; set; }
    }
}
