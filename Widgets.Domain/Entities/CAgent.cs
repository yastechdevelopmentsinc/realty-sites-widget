﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    public class CAgent
    {
        public virtual string AdministrationInformation { get; set; }
        public virtual string AgentStatus { get; set; }
        public virtual string AgentType { get; set; }
        public virtual string Board { get; set; }
        public virtual string Cell { get; set; }
        public virtual int? CREAID { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Email { get; set; }
        public virtual string FaxPhone { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string FullName { get; set; }
        public virtual string GenerationalName { get; set; }
        public virtual string HomePhone { get; set; }
        public virtual string IDXOptInYN { get; set; }
        public virtual bool? IsDeleted { get; set; }
        public virtual string LastName { get; set; }
        public virtual string LicenseNumber { get; set; }
        public virtual string MailAddress { get; set; }
        public virtual string MailCareOf { get; set; }
        public virtual string MailCity { get; set; }
        public virtual string MailPostalCode { get; set; }
        public virtual string MailPostalCodePlus4 { get; set; }
        public virtual string MailStateOrProvince { get; set; }
        public virtual int ID { get; set; }
        public virtual int AgentIndCode { get; set; }
        public virtual string MatrixModifiedDT { get; set; }
        public virtual string MatrixTesting { get; set; }
        public virtual string MatrixUserType { get; set; }
        public virtual DateTime? MembershipDate { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string MLS { get; set; }
        public virtual string MLSID { get; set; }
        public virtual string AgentTitle { get; set; }
        public virtual int? Office_MUI { get; set; }
        public virtual string OfficeMLSID { get; set; }
        public virtual string OtherPhone { get; set; }
        public virtual int? PhotoCount { get; set; }
        public virtual DateTime? PhotoModificationTimestamp { get; set; }
        public virtual int? ProviderKey { get; set; }
        public virtual DateTime? ProviderModificationTimestamp { get; set; }
        public virtual string RETSOfficeVisibility { get; set; }
        public virtual string RUC { get; set; }
        public virtual string StreetAddress { get; set; }
        public virtual string StreetCity { get; set; }
        public virtual string StreetPostalCode { get; set; }
        public virtual string StreetPostalCodePlus4 { get; set; }
        public virtual string StreetStateOrProvince { get; set; }
        public virtual DateTime? TerminationDate { get; set; }
        public virtual string Fax { get; set; }
        public virtual string FacebookUrl { get; set; }
        public virtual string LinkedInUrl { get; set; }
        public virtual string Website { get; set; }
        public virtual string TwitterUrl { get; set; }

        public virtual COffice AgentOffice { set; get; }

    }
}
