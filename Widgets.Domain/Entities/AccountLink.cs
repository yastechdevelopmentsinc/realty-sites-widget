﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;

namespace Widgets.Domain.Entities
{
    public class AccountLink
    {
        public virtual int Id { get; set; }
        public virtual Account Account { get; set; }
        public virtual string Name { get; set; }
        public virtual string Website { get; set; }

        public static AccountLink GetById(ISession session, int id)
        {
            return session.Get<AccountLink>(id);
        }

        public static IEnumerable<AccountLink> GetByAccount(ISession session, Account account)
        {
            return session.Query<AccountLink>().Where(x => x.Account == account);
        }
    }

}
