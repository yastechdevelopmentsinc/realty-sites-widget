﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;

namespace Widgets.Domain.Entities
{
    public class AccountOffice
    {
        public virtual int Id { get; set; }
        public virtual Account Account { get; set; }
        public virtual string Name { get; set; }
        public virtual string Address { get; set; }
        public virtual string City { get; set; }
        public virtual string Province { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Website { get; set; }
        public virtual string Email { get; set; }
        public virtual bool IDXListings { get; set; }

        public static AccountOffice GetById(ISession session, int id)
        {
            return session.Get<AccountOffice>(id);
        }

        public static IEnumerable<AccountOffice> GetByAccount(ISession session, Account account)
        {
            return session.Query<AccountOffice>().Where(x => x.Account == account);
        }
    }

}
