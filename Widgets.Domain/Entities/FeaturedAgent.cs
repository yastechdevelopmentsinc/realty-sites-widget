﻿namespace Widgets.Domain.Entities
{
    public class FeaturedAgent
    {
        public virtual int Id { get; set; }
        public virtual Agent Agent { get; set; }
        public virtual Account Account { get; set; }
    }
}
