﻿namespace Widgets.Domain.Entities
{
    public enum AccountType
    {
        Agent,
        Broker,
        Team,
        Commercial
    }
}
