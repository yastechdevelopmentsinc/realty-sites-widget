﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    public class CListing : BaseEntity
    {
        public virtual string AddlRooms { get; set; }
        public virtual string AddressDisplayYN { get; set; }
        public virtual string Basement { get; set; }
        public virtual string BasementDevelopment { get; set; }
        public virtual int? BathsFull { get; set; }
        public virtual int? BathsHalf { get; set; }
        public virtual int? BedrmsAboveGrade { get; set; }
        public virtual string BoardCode { get; set; }
        public virtual string CoListAgentDirectWorkPhone { get; set; }
        public virtual string CoListAgentEmail { get; set; }
        public virtual string CoListAgentFullName { get; set; }
        public virtual string CoListAgentMLSID { get; set; }
        public virtual int? CoListOffice_MUI { get; set; }
        public virtual string CoListOfficeMLSID { get; set; }
        public virtual string CoListOfficeName { get; set; }
        public virtual string CoListOfficePhone { get; set; }
        public virtual double? CondoFee { get; set; }
        public virtual string CondoFeeIncl { get; set; }
        public virtual string CondoFeePaySched { get; set; }
        public virtual string CondoName { get; set; }
        public virtual string CondoNameNew { get; set; }
        public virtual string CondoType { get; set; }
        public virtual string ConstructionType { get; set; }
        public virtual string EfficiencyRating { get; set; }
        public virtual string EnsuiteYN { get; set; }
        public virtual string Exterior { get; set; }
        public virtual string Fireplace { get; set; }
        public virtual string Flooring { get; set; }
        public virtual string Foundation { get; set; }
        public virtual string FPFuel { get; set; }
        public virtual double? FrontageMetres { get; set; }
        public virtual string FrontExposure { get; set; }
        public virtual string Garages { get; set; }
        public virtual string HeatingFuel { get; set; }
        public virtual string HeatingType { get; set; }
        public virtual string HighRangeAlpha { get; set; }
        public virtual string HOAFeeYN { get; set; }
        public virtual string InternetDisplayYN { get; set; }
        public virtual string InternetRemarks { get; set; }
        public virtual DateTime? LastChangeTimestamp { get; set; }
        public virtual string ListAgentDirectWorkPhone { get; set; }
        public virtual string ListAgentEmail { get; set; }
        public virtual string ListAgentFullName { get; set; }
        public virtual string ListAgentMLSID { get; set; }
        public virtual string ListingFirm1Email { get; set; }
        public virtual string ListingFirm1Website { get; set; }
        public virtual string ListingFirm2Email { get; set; }
        public virtual string ListingFirm2Website { get; set; }
        public virtual string ListingRealtor1Website { get; set; }
        public virtual string ListingRealtor2Website { get; set; }
        public virtual string ListingRealtor3Email { get; set; }
        public virtual int? ListingRealtor3ID { get; set; }
        public virtual string ListingRealtor3Name { get; set; }
        public virtual string ListingRealtor3Ph { get; set; }
        public virtual string ListingRealtor3Website { get; set; }
        public virtual string ListingRltr1MemberCode { get; set; }
        public virtual int? ListOffice_MUI { get; set; }
        public virtual string ListOfficeMLSID { get; set; }
        public virtual string ListOfficeName { get; set; }
        public virtual string ListOfficePhone { get; set; }
        public virtual string LotDimInfo { get; set; }
        public virtual string LotShape { get; set; }
        public virtual double? LotSqMetres { get; set; }
        public virtual double? SqFootage { get; set;}
        public virtual string LowRangeAlpha { get; set; }
        public virtual string LowRangeNum { get; set; }
        public virtual int ID { get; set; }
        public virtual string MatrixModifiedDT { get; set; }
        public virtual int? MlsNumber { get; set; }
        public virtual string Parking { get; set; }
        public virtual int? PhotoCount { get; set; }
        public virtual DateTime? PhotoModificationTimestamp { get; set; }
        public virtual string PropertySubType { get; set; }
        public virtual string PropertyType { get; set; }
        public virtual string InternetComm { get; set; }
        public virtual string Quarter { get; set; }
        public virtual string Range { get; set; }
        public virtual string RegisteredSizeMeters { get; set; }
        public virtual string RoofType { get; set; }
        public virtual string SchoolBusServiceYN { get; set; }
        public virtual string SchoolDistrict { get; set; }
        public virtual string Section { get; set; }
        public virtual string SewerSeptic { get; set; }
        public virtual string SiteInfluences { get; set; }
        public virtual string StateOrProvince { get; set; }
        public virtual string StreetDirSuffix { get; set; }
        public virtual string StreetName { get; set; }
        public virtual string StreetNumber { get; set; }
        public virtual string StreetSuffix { get; set; }
        public virtual string Style { get; set; }
        public virtual double? TotalAcreage { get; set; }
        public virtual double? TotalFlrAreaSF { get; set; }
        public virtual double? TotalParking { get; set; }
        public virtual double? TotalArea { get; set; }
        public virtual string Township { get; set; }
        public virtual string UnitNumber { get; set; }
        public virtual string UnitNumber2 { get; set; }
        public virtual string WestMeridian { get; set; }
        public virtual string YearBuilt { get; set; }
        public virtual string Zone { get; set; }
        public virtual double Latitude { get; set; }
        public virtual double Longitude { get; set; }

        public virtual CAgent Listing_Agent { get; set; }
        public virtual CAgent CoListing_Agent { get; set; }

        public virtual COffice Listing_Office { get; set; }
        public virtual COffice CoListing_Office { get; set; }

        public virtual string BrokerName
        {
            get
            {
                if (Listing_Office != null)
                {
                    try
                    {
                        return Listing_Office.Name;
                    }
                    catch
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
