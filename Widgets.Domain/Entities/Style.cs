﻿namespace Widgets.Domain.Entities
{
    public class Style
    {
        public virtual int StyleID { get; set; }
        public virtual string StyleName { get; set; }
    }
}
