﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    public class VMultimediaLink
    {
        public virtual int ID { get; set; }
        public virtual VLinkType Link_Type { get; set; }
        public virtual string Link_Url { get; set; }
        public virtual DateTime? Modification_Date { get; set; }
        public virtual VResidential RListing { get; set; }
        public virtual VCommercial CListing { get; set; }
    }
}
