﻿namespace Widgets.Domain.Entities
{
    public class CalculatorDefaults
    {
        public virtual int Id { get; set; }
        public virtual int Amortization { get; set; }
        public virtual int Term { get; set; }
        public virtual double InterestRate { get; set; }
        public virtual double Principal { get; set; }
        public virtual double DownPayment { get; set; }
    }
}
