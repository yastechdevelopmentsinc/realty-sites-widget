﻿using System;

namespace Widgets.Domain.Entities
{
    public class OpenHouse
    {
        public virtual int OpHoID { get; set; }
        public virtual int? AgentMui { get; set; }
        public virtual string Description { get; set; }
        public virtual DateTime? FromDate { get; set; }
        public virtual int? FromTime { get; set; }
        public virtual DateTime? InputDate { get; set; }
        public virtual bool? IsActive { get; set; }
        public virtual bool? IsDeleted { get; set; }
        public virtual int? ListingMui { get; set; }
        public virtual string ListingType { get; set; }
        public virtual int? MatrixUniqueID { get; set; }
        public virtual int? MlsNumber { get; set; }
        public virtual DateTime? ModificationTimestamp { get; set; }
        public virtual string OpenHouseRefreshments { get; set; }
        public virtual string OpenHouseType { get; set; }
        public virtual DateTime? ToDateDefunct { get; set; }
        public virtual int? ToTime { get; set; }
        public virtual bool? AdvertOpenHouse { get; set; }

        
    }
}