﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    public class VOpenHouse
    {
        public virtual int ID { get; set; }
        public virtual VAgent Agent { get; set; }
        public virtual DateTime? Date_Created { get; set; }
        public virtual DateTime? Date_Last_Modified { get; set; }
        public virtual DateTime? Endtime { get; set; }
        public virtual string Hosted { get; set; }
        public virtual int? Listing_Id { get; set; }
        public virtual VStatus Status { get; set; }
        public virtual string Notes { get; set; }
        public virtual DateTime? StartTime { get; set; }
    }
}
