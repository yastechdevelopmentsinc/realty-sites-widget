﻿namespace Widgets.Domain.Entities
{
    public class FeaturedListing
    {
        public virtual int Id { get; set; }
        public virtual int Mlsnumber { get; set; }
        public virtual Account Account { get; set; }
    }
}
