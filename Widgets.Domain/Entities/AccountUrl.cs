﻿namespace Widgets.Domain.Entities
{
    public class AccountUrl
    {
        public virtual int Id { get; set; }
        public virtual Account Account { get; set; }
        public virtual string Url { get; set; }
    }
}
