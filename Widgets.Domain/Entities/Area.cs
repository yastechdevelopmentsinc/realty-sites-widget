﻿using System.Collections.Generic;

namespace Widgets.Domain.Entities
{
    public class Area
    {
        public virtual int AreaID { get; set; }
        public virtual string AreaCode { get; set; }
        public virtual string AreaDescription { get; set; }
        public virtual string BoardCode { get; set; }
        public virtual IEnumerable<Location> Locations { get; set; }
        public virtual Account Account { get; set; }
    }
}
