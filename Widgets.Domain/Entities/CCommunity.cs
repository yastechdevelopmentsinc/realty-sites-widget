﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    //subarea or naibourhood
    public class CCommunity
    {
        public virtual int ID { get; set; }
        public virtual string AreaName { get; set; }
        public virtual string Value { get; set; }
        public virtual DateTime LastChangedDate { get; set; }
    }
}
