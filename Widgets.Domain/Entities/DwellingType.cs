﻿namespace Widgets.Domain.Entities
{
    public class DwellingType
    {
        public virtual int TypeID { get; set; }
        public virtual string Type { get; set; }
    }
}
