﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
    public class COffice
    {
        public virtual string AdditionalInformation { get; set; }
        public virtual int? CREAID { get; set; }
        public virtual string Email { get; set; }
        public virtual string Fax { get; set; }
        public virtual int? FranchiseID { get; set; }
        public virtual int? HeadOffice_MUI { get; set; }
        public virtual string HeadOfficeMLSID { get; set; }
        public virtual string IDXOptInYN { get; set; }
        public virtual bool? IsDeleted { get; set; }
        public virtual string MailAddress { get; set; }
        public virtual string MailCareOf { get; set; }
        public virtual string MailCity { get; set; }
        public virtual string MailPostalCode { get; set; }
        public virtual string MailPostalCodePlus4 { get; set; }
        public virtual string MailStateOrProvince { get; set; }
        public virtual int ID { get; set; }
        public virtual string MatrixTesting { get; set; }
        public virtual DateTime? MembershipDate { get; set; }
        public virtual string MLS { get; set; }
        public virtual string MLSID { get; set; }
        public virtual int? OfficeContact_MUI { get; set; }
        public virtual string OfficeContactMLSID { get; set; }
        public virtual string OfficeLongName { get; set; }
        public virtual string Name { get; set; }
        public virtual string OfficeStatus { get; set; }
        public virtual string OfficeType { get; set; }
        public virtual string Phone { get; set; }
        public virtual int? PhotoCount { get; set; }
        public virtual DateTime? PhotoModificationTimestamp { get; set; }
        public virtual int? ProviderKey { get; set; }
        public virtual DateTime? ProviderModificationTimestamp { get; set; }
        public virtual string StreetAddress { get; set; }
        public virtual string City { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string StreetPostalCodePlus4 { get; set; }
        public virtual string Province { get; set; }
        public virtual DateTime? TerminationDate { get; set; }
        public virtual string WebFacebook { get; set; }
        public virtual string WebLinkedIn { get; set; }
        public virtual string Website { get; set; }
        public virtual string WebTwitter { get; set; }

        public virtual COffice HeadOffice { set; get; }

        public virtual string Address {
            get
            {
                return StreetAddress + ", " + City + ", " + Province + " " + PostalCode;
            }
        }

    }
}
