﻿using System;

namespace Widgets.Domain.Entities
{
    public class AccountFile
    {
        public virtual Guid Id { get; set; }
        public virtual Account Account { get; set; }
        public virtual string FileName { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string MimeType { get; set; }
        public virtual DateTime UploadDate { get; set; }
    }
}
