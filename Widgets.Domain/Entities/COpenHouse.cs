﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Widgets.Domain.Entities
{
   public class COpenHouse
    {
        public virtual string ActiveYN { get; set; }
        public virtual string Description { get; set; }
        public virtual string EndTime { get; set; }
        public virtual string EntryOrder { get; set; }
        public virtual bool? IsDeleted { get; set; }
        public virtual double? Latitude { get; set; }
        public virtual int Listing_MUI { get; set; }
        public virtual double? Longitude { get; set; }
        public virtual int ID { get; set; }
        public virtual string MatrixModifiedDT { get; set; }
        public virtual string MLS { get; set; }
        public virtual DateTime? OpenHouseDate { get; set; }
        public virtual string OpenHouseType { get; set; }
        public virtual int? ParentProviderKey { get; set; }
        public virtual int? ProviderKey { get; set; }
        public virtual string Refreshments { get; set; }
        public virtual string StartTime { get; set; }

    }
}
