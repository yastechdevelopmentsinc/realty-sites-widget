namespace Widgets.Domain.Entities
{
    public class ForLease : BaseEntity
    {
        public virtual int ForLeaseID { get; set; }
        public virtual string PropertyType { get; set; }
        public virtual string Amenities { get; set; }
        public virtual string Amps { get; set; }
        public virtual string AreaLease { get; set; }
        public virtual string BrokerName { get; set; }
        public virtual string BrokerPhone { get; set; }
        public virtual string BusName { get; set; }
        public virtual string CeilCentre { get; set; }
        public virtual string CeilEaves { get; set; }
        public virtual string Construction { get; set; }
        public virtual string Elevators { get; set; }
        public virtual string EquipIncl { get; set; }
        public virtual string GradeDoor { get; set; }
        public virtual string Heating { get; set; }
        public virtual string HiHouseNumb { get; set; }
        public virtual string InternetComm { get; set; }
        public virtual int? LandSize { get; set; }
        public virtual int? LandSizeTotal { get; set; }
        public virtual string Legal1 { get; set; }
        public virtual string Legal2 { get; set; }
        public virtual string MajorType { get; set; }
        public virtual string MatrixModifiedDt { get; set; }
        public virtual int? MatrixUniqueID { get; set; }
        public virtual string OfficeArea { get; set; }
        public virtual string ParkingYesNo { get; set; }
        public virtual int? PhotoCount { get; set; }
        public virtual string PhotoCountModifiedDt { get; set; }
        public virtual string Power { get; set; }
        public virtual string RailDoor { get; set; }
        public virtual string RetailArea { get; set; }
        //public virtual string Slsman1Email { get; set; }
        //public virtual string Slsman1Name { get; set; }
        //public virtual string Slsman1Phone { get; set; }
        //public virtual string Slsman1Webaddr { get; set; }
        //public virtual string Slsman2Email { get; set; }
        //public virtual string Slsman2Name { get; set; }
        //public virtual string Slsman2Phone { get; set; }
        //public virtual string Slsman2Webaddr { get; set; }
        public virtual string StreetDir { get; set; }
        public virtual string StreetName { get; set; }
        public virtual string StreetType { get; set; }
        public virtual int? Taxes { get; set; }
        public virtual int? TotBldgArea { get; set; }
        public virtual int? Volts { get; set; }
        public virtual string WarehouseArea { get; set; }
        public virtual int? YearBuilt { get; set; }
        public virtual double? Latitude { get; set; }
        public virtual double? Longitude { get; set; }
        //public virtual bool? SuccessfullyGeocoded { get; set; }
        //public virtual bool? SetToDelete { get; set; }
        public virtual string DateEntered { get; set; }
    }
}