﻿using System;

namespace Widgets.Domain.Entities
{
    public class Referral
    {
        public virtual int Id { get; set; }
        public virtual Agent Agent { get; set; }
        public virtual int? PositionNumber { get; set; }
        public virtual DateTime? LastReferralDate { get; set; }
        public virtual Account Account { get; set; }
        public virtual int LeadCount { get; set; }
    }
}
