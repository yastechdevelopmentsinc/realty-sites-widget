﻿namespace Widgets.Domain.Entities
{
    public class BulletinBoardPhoto
    {
        public virtual int Id { get; set; }
        public virtual BulletinBoard BulletinBoard { get; set; }
        public virtual string PhotoMimeType { get; set; }
        public virtual byte[] Photo { get; set; }
        public virtual string PhotoCaption { get; set; }
        public virtual int PhotoIndex { get; set; }

        public override bool Equals(object obj)
        {
            var toCompare = obj as BulletinBoardPhoto;
            if(toCompare == null)
            {
                return false;
            }
            if(this.Id != toCompare.Id)
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;
            hashCode = hashCode ^ Id.GetHashCode();
            return hashCode;
        }
    }
 }
