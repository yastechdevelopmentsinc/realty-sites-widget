﻿using System;

namespace Widgets.Domain.Entities
{
    public class MultiFam : BaseEntity
    {
        public virtual int MultFamID { get; set; }
        public virtual string PropertyType { get; set; }
        public virtual int? MatrixUniqueID { get; set; }
        public virtual DateTime? MatrixModifiedDt { get; set; }
        public virtual DateTime? ListingDate { get; set; }
        public virtual string MajorType { get; set; }
        public virtual string StreetDir { get; set; }
        public virtual string StreetType { get; set; }
        public virtual string Style { get; set; }
        public virtual int? TotalUnits { get; set; }
        public virtual string BrokerName { get; set; }
        public virtual string Frontage { get; set; }
        public virtual string HiHouseNumb { get; set; }
        public virtual string StreetName { get; set; }
        public virtual int? PhotoCount { get; set; }
        public virtual DateTime? PhotoCountModifiedDt { get; set; }
        public virtual decimal? CurrentPrice { get; set; }
        public virtual string DepthInformation { get; set; }
        //public virtual bool? Settodelete { get; set; }
        public virtual string Roof { get; set; }
        public virtual int? Taxes { get; set; }
        public virtual string EquipIncl { get; set; }
        public virtual string Exterior { get; set; }
        public virtual string Heating { get; set; }
        public virtual string Outdoor { get; set; }
        public virtual string BrokerPhone { get; set; }
        public virtual string InternetComm { get; set; }
        public virtual string Legal1 { get; set; }
        public virtual string Legal2 { get; set; }
        //public virtual string Slsman1Email { get; set; }
        //public virtual string Slsman1Name { get; set; }
        //public virtual string Slsman1Phone { get; set; }
        //public virtual string Slsman1Webaddr { get; set; }
        //public virtual string Slsman2Email { get; set; }
        //public virtual string Slsman2Name { get; set; }
        //public virtual string Slsman2Phone { get; set; }
        //public virtual string Slsman2Webaddr { get; set; }
        public virtual int? YearBuilt { get; set; }
        public virtual string ListPriceChangeDate { get; set; }
        public virtual double? Latitude { get; set; }
        public virtual double? Longitude { get; set; }
        //public virtual bool? SuccessfullyGeocoded { get; set; }
        public virtual string ParkingYesNo { get; set; }
        public virtual string Storeys { get; set; }
        public virtual string Bachelor { get; set; }
        //public virtual string Bedrm1 { get; set; }
        //public virtual string Bedrm2 { get; set; }
        //public virtual string Bedrm3 { get; set; }
        //public virtual string Bedrm4 { get; set; }
        //public virtual string Bedrm5 { get; set; }
        public virtual string Elevators { get; set; }
        public virtual string Flooring { get; set; }
        public virtual string Amenities { get; set; }
        public virtual string Construction { get; set; }
    }
}