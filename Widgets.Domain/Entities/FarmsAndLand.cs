namespace Widgets.Domain.Entities
{
    public class FarmsAndLand : BaseEntity
    {
        public virtual int FarmsAndLandID { get; set; }
        public virtual string PropertyType { get; set; }
        public virtual string Basement { get; set; }
        public virtual string BasementDevel { get; set; }
        public virtual string BrokerName { get; set; }
        public virtual string Bush { get; set; }
        public virtual string BrokerPhone { get; set; }
        public virtual string DistElev { get; set; }
        public virtual string DistEsch { get; set; }
        public virtual string DistHsch { get; set; }
        public virtual string DistTown { get; set; }
        public virtual string DrinkingWater { get; set; }
        public virtual string Exterior { get; set; }
        public virtual string Fences { get; set; }
        public virtual string Garages { get; set; }
        public virtual string Heating { get; set; }
        public virtual string HiHouseNumb { get; set; }
        public virtual string InternetComm { get; set; }
        public virtual string Legal1 { get; set; }
        public virtual string Legal2 { get; set; }
        public virtual decimal? MainResLevelsAbove { get; set; }
        public virtual string MatrixModifiedDt { get; set; }
        public virtual int? MatrixUniqueID { get; set; }
        public virtual int? NumbRooms { get; set; }
        public virtual double? Latitude { get; set; }
        public virtual double? Longitude { get; set; }
        //public virtual bool? SuccessfullyGeocoded { get; set; }
        public virtual string PhotoCount { get; set; }
        public virtual string PhotoCountModifiedDt { get; set; }
        public virtual string Power { get; set; }
        public virtual string PropOffered { get; set; }
        public virtual string PropaneTank { get; set; }
        public virtual string Roof { get; set; }
        public virtual string SchoolBus { get; set; }
        public virtual string SewerDisp { get; set; }
        public virtual string Sloughs { get; set; }
        //public virtual string Slsman1Email { get; set; }
        //public virtual string Slsman1Name { get; set; }
        //public virtual string Slsman1Phone { get; set; }
        //public virtual string Slsman1Webaddr { get; set; }
        //public virtual string Slsman2Email { get; set; }
        //public virtual string Slsman2Name { get; set; }
        //public virtual string Slsman2Phone { get; set; }
        //public virtual string Slsman2Webaddr { get; set; }
        public virtual string SqFootage { get; set; }
        public virtual string StreetDir { get; set; }
        public virtual string StreetName { get; set; }
        public virtual string StreetType { get; set; }
        public virtual string Style { get; set; }
        public virtual int? Taxes { get; set; }
        public virtual string Topography { get; set; }
        public virtual string TotalArea { get; set; }
        public virtual string WaterHeater { get; set; }
        public virtual string WaterPurifier { get; set; }
        public virtual string WaterSoftener { get; set; }
        public virtual string WaterSupp { get; set; }
        public virtual string YardLight { get; set; }
        public virtual int? YearBuilt { get; set; }
        //public virtual bool? SetToDelete { get; set; }
        public virtual string DateEntered { get; set; }
        public virtual string EquipIncl { get; set; }

        public virtual string MajorType { get; set; }
    }
}