﻿namespace Widgets.Domain.Entities
{
    public enum ListingDisplayAccess
    {
        IDX,
        Brokerage,
        Agent,
        None
    }
}
