﻿using System.Linq;
using NHibernate;
using NHibernate.Linq;
using Widgets.Domain.Abstract;
using Widgets.Domain.Entities;

namespace Widgets.Domain.Concrete
{
    public class ListingService : IListingService
    {
        private readonly ISession _session;

        public ListingService(ISession session)
        {
            _session = session;
        }

        public IQueryable<Residential> GetAllResidential()
        {
            return _session.Query<Residential>()
                .Fetch(x => x.OpenHouses)
                .Fetch(x => x.ReplacementCoordinates);
        }

        public IQueryable<ForLease> GetAllForLease()
        {
            return _session.Query<ForLease>()
                .Fetch(x => x.OpenHouses)
                .Fetch(x => x.ReplacementCoordinates);
        }

        public IQueryable<Commercial> GetAllCommercial()
        {
            return _session.Query<Commercial>()
                .Fetch(x => x.OpenHouses)
                .Fetch(x => x.ReplacementCoordinates);
        }

        public IQueryable<FarmsAndLand> GetAllFarmsAndLand()
        {
            return _session.Query<FarmsAndLand>()
                .Fetch(x => x.OpenHouses)
                .Fetch(x => x.ReplacementCoordinates);
        }

        public IQueryable<MultiFam> GetAllMultiFamily()
        {
            return _session.Query<MultiFam>()
                .Fetch(x => x.OpenHouses)
                .Fetch(x => x.ReplacementCoordinates);
        }

        public IQueryable<Exclusive> GetAllExclusives(Account account)
        {
            return _session.Query<Exclusive>()
                .Fetch(x => x.OpenHouses)
                .Where(x => x.Account == account);
        }

        public Residential GetResidentialListing(int mlsnumber)
        {
            return _session.Query<Residential>()
                .SingleOrDefault(x => x.MlsNumber == mlsnumber);
        }

        public ForLease GetForLeaseListing(int mlsnumber)
        {
            return _session.Query<ForLease>()
                .SingleOrDefault(x => x.MlsNumber == mlsnumber);
        }

        public Commercial GetCommercialListing(int mlsnumber)
        {
            return _session.Query<Commercial>()
                .SingleOrDefault(x => x.MlsNumber == mlsnumber);
        }

        public FarmsAndLand GetFarmsAndLandListing(int mlsnumber)
        {
            return _session.Query<FarmsAndLand>()
                .SingleOrDefault(x => x.MlsNumber == mlsnumber);
        }        
        
        public MultiFam GetMultiFamilyListing(int mlsnumber)
        {
            return _session.Query<MultiFam>()
                .SingleOrDefault(x => x.MlsNumber == mlsnumber);
        }

        public Exclusive GetExclusiveListing(int exclusiveid)
        {
            return _session.Query<Exclusive>()
                .SingleOrDefault(x => x.ExclusiveID == exclusiveid);
        }

        public void Save(Residential property)
        {
            SaveEntity(property);
        }

        public void Save(ForLease property)
        {
            SaveEntity(property);
        }

        public void Save(Commercial property)
        {
            SaveEntity(property);
        }

        public void Save(FarmsAndLand property)
        {
            SaveEntity(property);
        }

        public void Save(MultiFam property)
        {
            SaveEntity(property);
        }

        public void Save(Exclusive property)
        {
            SaveEntity(property);
        }

        private void SaveEntity(object property)
        {
            _session.Save(property);
            _session.Flush();
        }

        //Vancouver 

        public IQueryable<VResidential> GetAllVResidential()
        {
            return _session.Query<VResidential>()
                .Fetch(x => x.ReplacementCoordinates);
        }

        public IQueryable<VCommercial> GetAllVCommercial()
        {
            return _session.Query<VCommercial>()
                .Fetch(x => x.ReplacementCoordinates);
        }

        public VResidential GetVResidentialListing(int mlsnumber)
        {
            return _session.Query<VResidential>()
                .SingleOrDefault(x => x.ID == mlsnumber);
        }

        public VCommercial GetVCommercialListing(int mlsnumber)
        {
            return _session.Query<VCommercial>()
                .SingleOrDefault(x => x.ID == mlsnumber);
        }

        public void Save(VResidential property)
        {
            SaveEntity(property);
        }

        public void Save(VCommercial property)
        {
            SaveEntity(property);
        }

        //Calgary

        public IQueryable<CListing> GetAllCListings()
        {
            return _session.Query<CListing>()
                .Fetch(x => x.ReplacementCoordinates);
        }

        public CListing GetCListings(int mlsnumber)
        {
            return _session.Query<CListing>()
                .SingleOrDefault(x => x.MlsNumber == mlsnumber);
        }

        public void Save(CListing property)
        {
            SaveEntity(property);
        }

        public IQueryable<CBoardID> GetAllCBoardID()
        {
            return _session.Query<CBoardID>();
        }

    }
}
